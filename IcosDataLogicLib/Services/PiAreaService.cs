﻿using IcosCoreExt.Models.PIAreaModels;
using IcosDataLib.Data;
using IcosModelLib.DTO;
using IdentitySample.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services
{
    public class PiAreaService : IPiAreaService
    {
        private readonly IcosDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public PiAreaService(IcosDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        public async Task<SitesToUser> GetSitesToUserListAsync(int? userId)
        {
            IEnumerable<Site> _tList = await GetSitesListAsync(userId);
            SitesToUser sitesToUser = new SitesToUser();
            sitesToUser.UserId = (int)userId;
            sitesToUser.SitesPiList = _tList;
            return sitesToUser;
        }

        public async Task<IEnumerable<Site>> GetSitesListAsync(int? userId)
        {
            /*
             * DIEGO
             * needs to make some cleanup between Site, ICOS_site and Station classes...
             * */
            Site[] fakeSites = new Site[]
            {
                new Site{ Id=70, SiteClass=1, SiteCode="FA-Lso", SiteName="Falso" },
                new Site{ Id=78, SiteClass=1, SiteCode="EU-Tst", SiteName="Test Site" }
            };
            
            //string[] fakeSites = {"EU-Tst", "FA-Lso" };
            return fakeSites.ToList();
        }

        public async Task<bool> SaveSubmittedFileAsync(SubmittedFile subFile)
        {
            try
            {
                await _context.SubmittedFiles.AddAsync(subFile);
                int res = await _context.SaveChangesAsync();
                return res > 0;
            }
            catch(Exception e)
            {
                string hhh = e.ToString();
                return false;
            }
            //return true;
        }

        public async Task<bool> SaveFieldBookFileAsync(Fieldbook fieldbook)
        {
            fieldbook.ImportDate = DateTime.Now;
            try
            {
                await _context.Fieldbooks.AddAsync(fieldbook);
                int res = await _context.SaveChangesAsync();
                return res > 0;
            }
            catch (Exception e)
            {
                string hhh = e.ToString();
                return false;
            }
        }

        public async Task<bool> DeleteFieldBookFileAsync(int id, int siteId)
        {
            var fieldbook = await _context.Fieldbooks.Where(fb => fb.Id == id && fb.SiteId == siteId).FirstOrDefaultAsync();
            if (fieldbook == null)
            {
                return false;
            }
            fieldbook.DeletedDate= DateTime.Now;
            fieldbook.Status = 1;
            int num = _context.SaveChanges();
            return num > 0;
        }

        public async Task<IEnumerable<SubmittedFile>> GetSubmittedFilesAsync(int siteid)
        {
            var subFilesList = await _context.SubmittedFiles.Where(file => file.SiteId == siteid)
                                                            .Include(ft=>ft.FileType)
                                                            .Include(st=>st.Site)
                                                            .OrderBy(dt => dt.FileTypeId)
                                                            .OrderBy(nn => nn.OriginalName)
                                                            .OrderBy(dt => dt.Date).ToListAsync();
            return subFilesList;
        }

        public async Task<IEnumerable<Fieldbook>> GetFieldbooksAsync(int siteid)
        {
            var fieldbooks = await _context.Fieldbooks.Where(file => file.SiteId == siteid && file.Status == 0)
                                                            //.Include(st => st.Site)
                                                            .OrderBy(dt => dt.DataTypeId)
                                                            .OrderBy(nn => nn.OriginalName)
                                                            .OrderBy(dt => dt.Date).ToListAsync();
            return fieldbooks;
        }

        public async Task<string> GetSubmitterCredentialsAsync(int siteId, int userId)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(uu => uu.LegacyID == userId);
            if (user != null)
            {
                return "<br /><br />Submitter credentials: "+user.Email;
            }
            return string.Empty;
           // throw new NotImplementedException();
        }

        public async Task<bool> CPRecordsExistAsync(int siteID)
        {
            int cp = await _context.GRP_PLOT.Where(ds => ds.SiteId == siteID && ds.PLOT_ID.IndexOf("CP") >= 0 && ds.DataStatus == 0).CountAsync();
            return cp > 0;
        }

    }
}
