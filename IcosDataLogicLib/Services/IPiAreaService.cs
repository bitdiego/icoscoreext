﻿using IcosCoreExt.Models.PIAreaModels;
using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services
{
    public interface IPiAreaService
    {
        Task<SitesToUser> GetSitesToUserListAsync(int? userId);
        Task<IEnumerable<Site>> GetSitesListAsync(int? userId);
        Task<bool> SaveSubmittedFileAsync(SubmittedFile subFile);
        Task<bool> SaveFieldBookFileAsync(Fieldbook fieldbook);
        Task<bool> DeleteFieldBookFileAsync(int id, int siteId);
        Task<IEnumerable<SubmittedFile>> GetSubmittedFilesAsync(int siteid);
        Task<IEnumerable<Fieldbook>> GetFieldbooksAsync(int siteid);
        Task<string> GetSubmitterCredentialsAsync(int siteId, int userId);
        Task<bool> CPRecordsExistAsync(int siteID);
    }
}
