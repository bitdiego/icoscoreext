﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using IcosDataLib.Data;
using IcosModelLib.Utils;


namespace IcosDataLogicLib.Services
{
    public class GenericService<T> : IGenericService<T>
        where T : BaseClass, new()
    {
        private readonly IcosDbContext _context;
//        private readonly IEmailSender _iemailSender;

        //, IEmailSender iemailSender
        public GenericService(IcosDbContext ctx)
        {
            _context = ctx;
            //_iemailSender = iemailSender;
        }

        public async Task<T> GetItemAsync()
        {
            var item = await _context.Set<T>().FirstOrDefaultAsync();
            return item;
        }

        public async Task<T> GetItemValueByIdAsync(int siteId, int? id)
        {
            var item = await _context.Set<T>().FirstOrDefaultAsync(_item => _item.Id == id && _item.SiteId == siteId && _item.DataStatus == 0);
            if (item != null) return item;
            else return null;
        }
        public async Task<T> GetInstMainValuesByRecordIdAsync(int t, int siteId, int? id)
        {
            dynamic item = null;
            dynamic xitem = null;
            switch (t)
            {
                case (int)Globals.Groups.GRP_INST:
                    xitem = await _context.GRP_INST.Where(_item => _item.Id == id && _item.SiteId == siteId && _item.DataStatus == 0)
                                                .Select(ii => new GRP_INST
                                                {
                                                    INST_MODEL = ii.INST_MODEL,
                                                    INST_SN = ii.INST_SN
                                                }).FirstOrDefaultAsync();
                    break;
                case (int)Globals.Groups.GRP_EC:
                    xitem = await _context.GRP_EC.Where(_item => _item.Id == id && _item.SiteId == siteId && _item.DataStatus == 0)
                                                .Select(ec => new GRP_EC
                                                {
                                                    EC_MODEL = ec.EC_MODEL,
                                                    EC_SN = ec.EC_SN
                                                }).FirstOrDefaultAsync();
                    break;
                case (int)Globals.Groups.GRP_BM:
                    xitem = await _context.GRP_BM.Where(_item => _item.Id == id && _item.SiteId == siteId && _item.DataStatus == 0)
                                                .Select(bm => new GRP_BM
                                                {
                                                    BM_MODEL = bm.BM_MODEL,
                                                    BM_SN = bm.BM_SN
                                                }).FirstOrDefaultAsync();
                    break;
            }
            
            if (xitem != null)
            {
                item = xitem;
            }

            return xitem;
        }
        

        public async Task<T> GetItemValueAsync(int siteId)
        {
            //throw new NotImplementedException();
            var item = await _context.Set<T>().Where(x => x.SiteId == siteId && x.DataStatus == 0).FirstOrDefaultAsync();
            return item;
            //  return _context.Set<T>().Find(id);
        }
        public async Task<IEnumerable<T>> GetItemValuesAsync(int siteId)
        {
            //DIEGO:: maybe better not to load and show invalid data
            //bandwidth save and they look awful on the web UI
            var items = await _context.Set<T>()
                                        .Where(x => x.SiteId == siteId && x.DataStatus == 0) //<= 2
                                        .OrderBy(x => x.DataStatus).ToListAsync();
            return items;
        }
        public async Task<bool> SaveItemAsync(T t, int insertUserId, int siteId)
        {
            /*var item = await GetItemValueAsync(siteId);
            if (item != null)
            {
                bool b = await SetItemInvalidAsync(siteId, insertUserId, item);
                if (!b) return false;
            }*/
            if (siteId < 1) return false;
            t.DataStatus = 0;
            t.InsertUserId = insertUserId;
            t.SiteId = siteId;
            t.InsertDate = DateTime.Now;
            t.DataOrigin = 1;
            var item = await ItemInDbAsync(t, siteId);
            if(item != null)
            {
                bool b = await SetItemInvalidAsync(siteId, insertUserId, item);
                if (!b) return false;
            }
            
            _context.Set<T>().Add(t);
            int res = await _context.SaveChangesAsync();
            /*
            string subject = "BADM Web UI for site " + siteId.ToString()+"|"+t.GroupId;
            string body = @"Inserted new item: VAR_TYPE for site CC-Xxx<br />";
            body += "Item added by " + insertUserId.ToString();
            await _iemailSender.SendEmailAsync("info@icos-etc.eu",subject, body);

            DIEGO:: maybe a better solution can be to store a message in a proper table (say, WebUiQueueMessages)
            with subject, body, to, cc, etc already set. Then scan the table every X minutes to send the message

            */
            return res > 0;
        }
        public async Task<bool> DeleteItemAsync(int? id, int siteId, int userId)
        {
            int res = 0;
            if (id == null || siteId < 1) return false;
            var item = await _context.Set<T>().FirstOrDefaultAsync(x => x.Id == id && x.SiteId == siteId);
            if (item != null)
            {
                item.DataStatus = 3;
                item.DeletedDate = DateTime.Now;
                item.DeleteUserId = userId;
                res = await _context.SaveChangesAsync();
            }
            return res > 0;
        }
        public async Task<bool> UpdateItemAsync(int? id, int siteId, int userId, T t)
        {
            bool b = false;
            if (id == null || siteId<1) return false;
            var item = await _context.Set<T>().FirstOrDefaultAsync(x => x.Id == id && x.SiteId == siteId);
            if (item != null)
            {
                b = await SetItemInvalidAsync(siteId, userId, item);
            }
            if (b)
            {
                t.Id = 0;
                t.DataStatus = 0;
                t.InsertUserId = userId;
                t.SiteId = siteId;
                t.InsertDate = DateTime.Now;
                _context.Set<T>().Add(t);
                int res = await _context.SaveChangesAsync();
            }
            return b;
        }
        public async Task<bool> SetItemInvalidAsync(int siteId, int userId, T t)
        {
            int res = 0;
            t.DataStatus = 2;
            t.DeletedDate = DateTime.Now;
            t.DeleteUserId = userId;
            res = await _context.SaveChangesAsync();
            return res > 0;
        }

        public async Task<IQueryable<string>> GetBadmListDataAsync(int dataId)
        {
            var iList = await _context.BADMList.Where(bList => bList.cv_index == dataId).ToListAsync();
            return (IQueryable<string>)iList;
        }

        public  IQueryable<string> GetBadmListData(int dataId)
        {
            IQueryable<string> iList = _context.BADMList.Where(bList => bList.cv_index == dataId).OrderBy(x=>x.shortname).Select(rec => rec.shortname);
            return iList;
        }

        public async Task<T> ItemInDbAsync(T t, int siteId)
        {
            int res = 0;
            dynamic xitem=null;
            switch (t.GroupId)
            {
                case (int)Globals.Groups.GRP_DM:
                    GRP_DM dm = t as GRP_DM;
                   /* var _dm = await _context.GRP_DM.FirstOrDefaultAsync(item => item == dm);
                    if (_dm != null)
                    {
                        xitem = _dm;
                    }*/
                    break;
                case (int)Globals.Groups.GRP_PLOT:
                    GRP_PLOT plot = t as GRP_PLOT;
                    var _plot = await _context.GRP_PLOT.FirstOrDefaultAsync(plt => plt.PLOT_ID == plot.PLOT_ID && plt.SiteId == plot.SiteId && plt.DataStatus == 0);
                    if (_plot != null)
                    {
                        xitem = _plot;
                    }
                    break;
                case (int)Globals.Groups.GRP_INST:
                    GRP_INST inst = t as GRP_INST;
                    var item = await _context.GRP_INST.FirstOrDefaultAsync(md => md.INST_MODEL == inst.INST_MODEL && md.INST_SN == inst.INST_SN
                                                                && md.INST_FACTORY == inst.INST_FACTORY && md.SiteId == inst.SiteId && md.DataStatus==0);
                    bool isEqual = item == inst;
                    if(isEqual)
                    {
                        res = item.Id;
                        xitem = item;
                    }
                    break;
                case (int)Globals.Groups.GRP_LOCATION:
                    GRP_LOCATION loc = t as GRP_LOCATION;
                    var iteml = await _context.GRP_LOCATION.FirstOrDefaultAsync(l => l.SiteId == loc.SiteId && l.DataStatus == 0);

                    if (iteml != null)
                    {
                        bool qq = iteml == loc;
                        xitem = iteml;
                    }
                    break;
                case (int)Globals.Groups.GRP_TOWER:
                    GRP_TOWER tower = t as GRP_TOWER;
                    var itemt = await _context.GRP_TOWER.FirstOrDefaultAsync(t => t.SiteId == tower.SiteId && t.DataStatus == 0);
                    if (itemt != null)
                        xitem = itemt;
                    break;
                case (int)Globals.Groups.GRP_CLIM_AVG:
                    GRP_CLIM_AVG climavg = t as GRP_CLIM_AVG;
                    var itemc = await _context.GRP_CLIM_AVG.FirstOrDefaultAsync(c => c.SiteId == climavg.SiteId && c.DataStatus == 0);
                    if (itemc != null)
                        xitem = itemc;
                    break;
                case (int)Globals.Groups.GRP_DHP:
                    GRP_DHP dhp = t as GRP_DHP;
                    var itemd = await _context.GRP_DHP.FirstOrDefaultAsync(d => d.SiteId == dhp.SiteId && d.DHP_ID == dhp.DHP_ID && d.DataStatus == 0);
                    if (itemd != null)
                        xitem = itemd;
                    break;
                case (int)Globals.Groups.GRP_BULKH:
                    GRP_BULKH bulkh = t as GRP_BULKH;
                    var itemb = await _context.GRP_BULKH.FirstOrDefaultAsync(b => b.SiteId == bulkh.SiteId && b.BULKH_PLOT == bulkh.BULKH_PLOT && bulkh.BULKH_DATE == b.BULKH_DATE && b.DataStatus == 0);
                    if (itemb != null)
                        xitem = itemb;
                    break;
            }
            return xitem;
        }

    }
}
