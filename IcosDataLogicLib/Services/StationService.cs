﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services
{
    public class StationService : IStationService
    {
        private readonly IcosDbContext _context;
        private readonly IDistributedCache _distributedCache;

        public StationService(IcosDbContext ctx, IDistributedCache distributedCache)
        {
            _context = ctx;
            _distributedCache = distributedCache;
        }
        public async Task<List<Station>> GetIcosSitesAsync()
        {
            var cacheKey = "STATION_LIST";
            List<Station> stations = new List<Station>();

            var cachedData = await _distributedCache.GetAsync(cacheKey);
            if (cachedData != null)
            {
                var cachedDataString = Encoding.UTF8.GetString(cachedData);
                stations = JsonConvert.DeserializeObject<List<Station>>(cachedDataString);
            }
            else
            {
                try
                {
                    stations = await _context.Stations.ToListAsync();
                    var cachedDataString = JsonConvert.SerializeObject(stations);
                    var newDataToCache = Encoding.UTF8.GetBytes(cachedDataString);

                    // set cache options 
                    var options = new DistributedCacheEntryOptions()
                        .SetAbsoluteExpiration(DateTime.Now.AddMinutes(1))
                        .SetSlidingExpiration(TimeSpan.FromMinutes(1));

                    // Add data in cache
                    await _distributedCache.SetAsync(cacheKey, newDataToCache, options);

                }
                catch (Exception e)
                {
                    //return null;
                    string xxx = e.ToString();
                }
            }
            return stations;
        }

        public async Task<IEnumerable<WGUsers>> GetWorkingGroupUsers(int WGId)
        {
            var data = await _context.WGUsers
               .Join(
                   _context.UsersToWG,
                   user => user.Id,
                   utw => utw.idUser,
                   (user, utw) => new { U = user, W = utw })
               .Where(w => w.W.idWG == WGId && w.U.auto)
               .Select(x => x.U).OrderBy(x => x.surname).ToListAsync();
            
            return data;
        }

        public async Task<List<WorkGroups>> GetWorkGroupsList()
        {
            var data = await _context.WorkGroups.Select(w => w).OrderBy(x => x.id).ToListAsync();
            //List<SelectListItem> list = new List<SelectListItem>();
            //foreach(WorkGroups w in data)
            //{
            //    list.Add(new SelectListItem
            //    {
            //        Text = w.description.ToString(),
            //        Value = w.id.ToString()
            //    });
            //}

            return data;
        }

        public async Task<WGUsers> SaveWGUser(WGUsers WGuser, List<int> wgs)
        {
            int res;
            int userID;

            WGuser.name = dangerRemove(WGuser.name);
            WGuser.surname = dangerRemove(WGuser.surname);
            WGuser.institution = dangerRemove(WGuser.institution);
            WGuser.mail = dangerRemove(WGuser.mail);
            WGuser.auto = true;

            var user = await _context.WGUsers.FirstOrDefaultAsync(u => u.mail.ToLower() == WGuser.mail.ToLower());
            if(user != null)
            {
                userID = user.Id;
            }
            else
            {
                _context.Set<WGUsers>().Add(WGuser);
                await _context.SaveChangesAsync();
                userID = WGuser.Id;
            }
            foreach(int id in wgs)
            {
                UsersToWG uw = new UsersToWG();
                uw.idUser = userID;
                uw.idWG = id;
                uw.date = DateTime.Today;
                _context.Set<UsersToWG>().Add(uw);
            }
            res = await _context.SaveChangesAsync();

            if (res > 0)
            {
                return WGuser;
            }
            return null;
        }

        private string dangerRemove(string input)
        {
            string[] dangers = { "insert", "delete", "update", "truncate", "drop", ";", "exec", "--", "xp_", "/*", "*/", "cast", "varchar" };
            string temp = input.ToLower();
            for (int i = 0; i < dangers.Length; i++)
            {
                int index = temp.IndexOf(dangers[i]);
                if (index >= 0)
                {
                    input = input.Remove(index, dangers[i].Length);
                    temp = input.ToLower();
                }
            }
            return input;
        }
    }
}
