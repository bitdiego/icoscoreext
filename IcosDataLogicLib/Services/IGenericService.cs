﻿using IcosModelLib.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services
{
    public interface IGenericService<T>
    {
        Task<T> GetItemAsync();
        Task<T> GetItemValueAsync(int siteId);
        Task<T> ItemInDbAsync(T t, int siteId);
        Task<T> GetItemValueByIdAsync(int siteId, int? id);
        Task<T> GetInstMainValuesByRecordIdAsync(int t, int siteId, int? id);
        Task<IEnumerable<T>> GetItemValuesAsync(int siteId);
        Task<bool> SaveItemAsync(T t, int insertUserId, int siteId);
        Task<bool> DeleteItemAsync(int? id, int siteId, int userId);
        Task<bool> UpdateItemAsync(int? id, int siteId, int userId, T t);
        Task<bool> SetItemInvalidAsync(int siteId, int userId, T t);
        Task<IQueryable<string>> GetBadmListDataAsync(int dataId);
        IQueryable<string> GetBadmListData(int dataId);
       
    }
}
