﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services
{
    public interface IBasicInstrumentValidator<T>
    {
        Task<int> InstrumentInGrpInst(T model, int siteId);
        Task<int> LastExpectedOpByDateAsync(T model, int siteId);
       // Task<int> LastExpectedOpByDateAsync(string instModel, string instSn, string instFactory, string instDate, int siteId);
        int LoggerFileCheck(T model, int siteId);
        int LoggerFileSamplingIntCheck(T model, int siteId);
    }
}
