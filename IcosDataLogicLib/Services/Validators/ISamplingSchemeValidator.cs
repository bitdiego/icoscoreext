﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface ISamplingSchemeValidator
    {
        int SamplingSchemeValidate(GRP_PLOT model);

    }
}
