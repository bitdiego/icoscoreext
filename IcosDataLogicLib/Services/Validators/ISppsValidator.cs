﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface ISppsValidator
    {
        Task<int> SppsPlotExistsAsync(string plot, int siteId);
        int ValidateSpps(GRP_SPPS model, int siteId);
    }
}
