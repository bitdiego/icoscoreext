﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public class CeptValidator : ICeptValidator
    {
        private readonly IcosDbContext _context;

        public CeptValidator(IcosDbContext ctx)
        {
            _context = ctx;
        }
        public int ValidateCeptModel(GRP_CEPT model, string ecosystem)
        {
            if(String.Compare(ecosystem, "grassland", true) == 0)
            {
                if( (model.CEPT_FIRST==null && model.CEPT_LAST!=null)||
                    (model.CEPT_FIRST != null && model.CEPT_LAST == null))
                {
                    return (int)Globals.ErrorValidationCodes.CEPT_FIRST_LAST;
                }
            }
            return 0;
        }
    }
}
