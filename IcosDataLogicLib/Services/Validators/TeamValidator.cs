﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
//using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IcosModelLib.Utils;

namespace IcosDataLogicLib.Services.Validators
{
    public class TeamValidator : ITeamValidator
    {
        private readonly IcosDbContext _context;
        public TeamValidator(IcosDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// If a TEAM_MEMBER_WORKEND is passed: check if the related TEAM_MEMBER exist
        /// If yes, compare _DATE with W.E.DATE and raise error if WE_DATE <= _DATE
        /// If user has an account for that site, delete his/her account
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<int> WorkEndValidatorAsync(GRP_TEAM model, int siteId)
        {
            GRP_TEAM teamMember = null;
            if (!String.IsNullOrEmpty(model.TEAM_MEMBER_WORKEND))
            {
                teamMember = await _context.GRP_TEAM.Where(tm => String.Compare(tm.TEAM_MEMBER_EMAIL.ToLower(), model.TEAM_MEMBER_EMAIL.ToLower()) == 0
                                                              && tm.DataStatus == 0 && tm.SiteId == siteId 
                                                              && String.Compare(tm.TEAM_MEMBER_DATE, model.TEAM_MEMBER_WORKEND) < 0).
                                                              OrderByDescending(tm => tm.TEAM_MEMBER_DATE).FirstOrDefaultAsync();
                if(teamMember == null)
                {
                    return (int)Globals.ErrorValidationCodes.TEAM_MEMBER_WE_NOT_FOUND;
                }
                else
                {
                    if (String.IsNullOrEmpty(model.TEAM_MEMBER_DATE))
                    {
                        model.TEAM_MEMBER_DATE = teamMember.TEAM_MEMBER_DATE;
                    }
                   
                }
            }
            return 0;
        }

        /// <summary>
        ///1. every Team member must have ONLY ONE role
        ///2. There must be ONLY ONE PI and ONLY ONE MANAGER
        ///3. TEAM_MEMBER_WORKEND: can be used to communicate the end of a role; from the day after, that role becomes available (if MANAGER)
        ///4. PI cannot be removed / changed directly but only after a communication to ICOS ETC
        ///5. A Login account must be automatically created for: SCI, DATA and MANAGER roles. For other roles, a formal request must be done to ICOS ETC
        ///6. For each new user created, whichever role, an item must be added to the ICOS_Uid table
        /// </summary>
        /// <param name="model"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public async Task<int> ManageTeamMemberAsync(GRP_TEAM model, int siteId)
        {
            string tmRole = model.TEAM_MEMBER_ROLE;
            string tmWorkEnd = model.TEAM_MEMBER_WORKEND;
            if (!String.IsNullOrEmpty(tmWorkEnd))
            {
                return 7777;
            }
            if (!String.IsNullOrEmpty(tmRole))
            {
                switch (tmRole.ToLower())
                {
                    case "pi":
                    case "manager":
                        //check if exists a different member having pi or manager role
                        //if yes, compare WE_DATE with model passed date
                        var PiManagerList = await _context.GRP_TEAM.Where(tm => String.Compare(tm.TEAM_MEMBER_EMAIL, model.TEAM_MEMBER_EMAIL, true) != 0
                                                                           && (String.Compare(tm.TEAM_MEMBER_ROLE, "PI") == 0 || String.Compare(tm.TEAM_MEMBER_ROLE, "MANAGER") == 0)
                                                                           && tm.DataStatus==0 && tm.SiteId==siteId)
                                                                  .ToListAsync();
                        foreach (var tm in PiManagerList)
                        {
                            if (String.IsNullOrEmpty(tm.TEAM_MEMBER_WORKEND))
                            {
                                //there is already a pi/manager registered with no workend: error
                                return (int)Globals.ErrorValidationCodes.PI_MANAGER_ALREADY_REGISTERED;
                            }
                            else
                            {
                                //there is already a pi/manager registered with workend: trying to add a new Manager
                                //compare the dates
                                if (String.Compare(tm.TEAM_MEMBER_WORKEND, model.TEAM_MEMBER_DATE) >= 0)
                                {
                                    //WE date is >= submitted date: error
                                    return (int)Globals.ErrorValidationCodes.MANAGER_WORKEND_DATE_ERROR;
                                }
                                else
                                {
                                    //WE date is < submitted date: but _DATE MUST BE WE_DATE + 1 day
                                    DateTime teamMemberDate = ValidationUtils.isoDate2DT(model.TEAM_MEMBER_DATE);
                                    DateTime teamMemberWE = ValidationUtils.isoDate2DT(tm.TEAM_MEMBER_DATE);
                                    DateTime _added = teamMemberWE.AddDays(1);
                                    if(_added.Year!=teamMemberDate.Year || _added.Month!=teamMemberDate.Month || _added.Day != teamMemberDate.Day)
                                    {
                                        return (int)Globals.ErrorValidationCodes.MANAGER_WORKEND_DATE_ERROR_PLUS_1;
                                    }
                                }
                            }
                        }
                        break;
                    default:
                        var OtherRoleList = await _context.GRP_TEAM.Where(tm => String.Compare(tm.TEAM_MEMBER_EMAIL, model.TEAM_MEMBER_EMAIL, true) == 0
                                                                           && (String.Compare(tm.TEAM_MEMBER_ROLE, "PI") != 0 && String.Compare(tm.TEAM_MEMBER_ROLE, "MANAGER") != 0)
                                                                           && tm.DataStatus == 0 && tm.SiteId == siteId)
                                                                  .ToListAsync();
                        foreach (var tm in OtherRoleList)
                        {
                            if (String.IsNullOrEmpty(tm.TEAM_MEMBER_WORKEND))
                            {
                                //team member has already a role registered with no workend: error
                                return (int)Globals.ErrorValidationCodes.TEAM_MEMBER_ALREADY_REGISTERED;
                            }
                            else
                            {
                                //there is already a role registered with workend
                                //compare the dates
                                if (String.Compare(tm.TEAM_MEMBER_WORKEND, model.TEAM_MEMBER_DATE) >= 0)
                                {
                                    //WE date is >= submitted date: error
                                    return (int)Globals.ErrorValidationCodes.MANAGER_WORKEND_DATE_ERROR;
                                }
                               /* else
                                {
                                    //WE date is < submitted date: but _DATE MUST BE WE_DATE + 1 day
                                    DateTime teamMemberDate = ValidationUtils.isoDate2DT(model.TEAM_MEMBER_DATE);
                                    DateTime teamMemberWE = ValidationUtils.isoDate2DT(tm.TEAM_MEMBER_DATE);
                                    DateTime _added = teamMemberWE.AddDays(1);
                                    if (_added.Year != teamMemberDate.Year || _added.Month != teamMemberDate.Month || _added.Day != teamMemberDate.Day)
                                    {
                                        return 6787;
                                    }
                                }*/
                            }
                        }
                        break;
                }
            }
            ////All was ok: the passed model is going to be saved
            ///If model role is a scientist or a Data or Manager, create an account for this site
            ///Create also an entry in ICOSUid_Test table
            return 0;
        }
    }
}
