﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public class FlsmValidator : IFlsmValidator
    {
        private readonly IcosDbContext _context;

        public FlsmValidator(IcosDbContext context)
        {
            _context = context;
        }

        public async Task<int> ItemInSamplingPointGroupAsync(GRP_FLSM model, int siteId)
        {
            var item = await _context.GRP_PLOT.Where(plot => plot.SiteId == siteId && plot.DataStatus == 0 &&
                                                String.Compare(plot.PLOT_ID, model.FLSM_PLOT_ID) == 0 &&
                                                String.Compare(plot.PLOT_DATE, model.FLSM_DATE) <= 0).FirstOrDefaultAsync();
            if (item == null)
            {
                return (int)Globals.ErrorValidationCodes.FLSM_PLOT_ID_NOT_FOUND;
            }
            return 0;
        }

        public int ValidateFlsm(GRP_FLSM model, int siteId)
        {
            if (String.IsNullOrEmpty(model.FLSM_SAMPLE_TYPE))
            {
                return (int)Globals.ErrorValidationCodes.FLSM_SAMPLE_TYPE_REQUIRED;
            }
            if (model.FLSM_SAMPLE_ID == null || String.IsNullOrEmpty(model.FLSM_SAMPLE_ID.ToString()))
            {
                return (int)Globals.ErrorValidationCodes.FLSM_SAMPLE_ID_REQUIRED;
            }
            if(!ValidationUtils.XORNull<string>(model.FLSM_SPP, model.FLSM_PTYPE))
            {
                return (int)Globals.ErrorValidationCodes.FLSM_SPP_PTYPE_XOR;
            }
            return 0;
        }
    }
}
