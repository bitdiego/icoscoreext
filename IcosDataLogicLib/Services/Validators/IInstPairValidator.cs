﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface IInstPairValidator
    {
        Task<int> InstrumentInGrpInst(GRP_INSTPAIR model, int siteId);
        //Task<int> LastExpectedOpByDateAsync(GRP_INSTPAIR model, int siteId);
        Task<int> ValidateInstPairAsync(GRP_INSTPAIR model, int siteId);
    }
}
