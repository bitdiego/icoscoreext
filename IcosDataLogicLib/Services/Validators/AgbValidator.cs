﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public class AgbValidator : IAgbValidator
    {
        private readonly IcosDbContext _context;
        private readonly string Ecosystem = "Forest"; //DIEGO:: to retrieve!!! from validation utils?

        public AgbValidator(IcosDbContext context)
        {
            _context = context;
        }

        public async Task<int> ItemInSamplingPointGroupAsync(GRP_AGB model, int siteId)
        {
            var item = await _context.GRP_PLOT.Where(plot => plot.SiteId == siteId && plot.DataStatus == 0 &&
                                                String.Compare(plot.PLOT_ID, model.AGB_PLOT) == 0 &&
                                                String.Compare(plot.PLOT_DATE, model.AGB_DATE) <= 0).FirstOrDefaultAsync();
            if (item == null)
            {
                return (int)Globals.ErrorValidationCodes.AGB_PLOT_NOT_FOUND;
            }
            return 0;
        }
        public int ValidateAgb(GRP_AGB model, int siteId)
        {
            switch (Ecosystem.ToLower())
            {
                case "cropland":
                    if(ValidationUtils.FindMandatoryNull<GRP_AGB>(model, "AGB", "AGB_LOCATION", "AGB_AREA", "AGB_PHEN", "AGB_PTYPE"))
                    {
                        return (int)Globals.ErrorValidationCodes.AGB_CROPLAND_MANDATORY;
                    }
                    break;
                case "grassland":
                    if (ValidationUtils.FindMandatoryNull<GRP_AGB>(model, "AGB", "AGB_AREA", "AGB_PHEN", "AGB_PLOT_TYPE"))
                    {
                        return (int)Globals.ErrorValidationCodes.AGB_GRASSLAND_MANDATORY;
                    }
                    break;
                case "forest":
                    if (String.IsNullOrEmpty(model.AGB_PTYPE))
                    {
                        return (int)Globals.ErrorValidationCodes.AGB_FOREST_AGB_PTYPE_MANDATORY;
                    }
                    else
                    {
                        switch (model.AGB_PTYPE.ToLower())
                        {
                            case "moss":
                                if (model.AGB_LOCATION == null)
                                {
                                    return (int)Globals.ErrorValidationCodes.AGB_FOREST_AGB_LOCATION_MANDATORY;
                                }
                                if((model.AGB_NPP_MOSS !=null && model.AGB != null) ||
                                    (model.AGB_NPP_MOSS == null && model.AGB == null))
                                {
                                    return (int)Globals.ErrorValidationCodes.AGB_FOREST_AGB_XOR_AGB_NPP_MOSS;
                                }
                                if(model.AGB != null)
                                {
                                    if (String.IsNullOrEmpty(model.AGB_PHEN))
                                    {
                                        return (int)Globals.ErrorValidationCodes.AGB_FOREST_AGB_PHEN_MANDATORY;
                                    }
                                }
                                if (model.AGB_NPP_MOSS != null)
                                {
                                    if (!String.IsNullOrEmpty(model.AGB_PHEN))
                                    {
                                        return (int)Globals.ErrorValidationCodes.AGB_FOREST_AGB_PHEN_MANDATORY;
                                    }
                                }
                                break;
                            case "sapling":
                                if (ValidationUtils.FindMandatoryNull<GRP_AGB>(model, "AGB", "AGB_AREA", "AGB_LOCATION", "AGB_SPP", "AGB_PTYPE"))
                                {
                                    return (int)Globals.ErrorValidationCodes.AGB_FOREST_SAPLING_MANDATORY;
                                }
                                break;
                            case "ferns":
                            case "herb":
                            case "shrub":
                                if (ValidationUtils.FindMandatoryNull<GRP_AGB>(model, "AGB", "AGB_AREA", "AGB_LOCATION", "AGB_PHEN", "AGB_PTYPE"))
                                {
                                    return (int)Globals.ErrorValidationCodes.AGB_FOREST_FHS_MANDATORY;
                                }
                                break;
                        }
                    }
                    break;
                default:
                    if (String.IsNullOrEmpty(model.AGB_SPP))
                    {
                        return (int)Globals.ErrorValidationCodes.AGB_MIRES_SPP;
                    }
                    if ((model.AGB_NPP_MOSS != null && model.AGB != null) ||
                                    (model.AGB_NPP_MOSS == null && model.AGB == null))
                    {
                        return (int)Globals.ErrorValidationCodes.AGB_MIRES_AGB_XOR_AGB_NPP_MOSS;
                    }
                    if (!String.IsNullOrEmpty(model.AGB_ORGAN))
                    {
                        if(model.AGB_PTYPE.ToLower()!="shrub" && model.AGB_PTYPE.ToLower() != "sapling")
                        {
                            return (int)Globals.ErrorValidationCodes.AGB_MIRES_AGB_ORGAN;
                        }
                    }
                    break;
            }
            return 0;
        }
    }
}
