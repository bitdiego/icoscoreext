﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public class InstPairValidator : IInstPairValidator
    {
        private readonly IcosDbContext _context;

        public InstPairValidator(IcosDbContext context)
        {
            _context = context;
        }

        public async Task<int> InstrumentInGrpInst(GRP_INSTPAIR model, int siteId)
        {
            int resp = await CheckGrpInstAsync(model.INSTPAIR_MODEL_1, model.INSTPAIR_SN_1, model.INSTPAIR_DATE, siteId);
            if (resp > 0) return resp;
            resp = await CheckGrpInstAsync(model.INSTPAIR_MODEL_2, model.INSTPAIR_SN_2, model.INSTPAIR_DATE, siteId);
            return resp;
        }

        private async Task<int> CheckGrpInstAsync(string model, string sn, string date, int siteId)
        {
            int resp = 0;

            var inst = await _context.GRP_INST.Where(md => md.INST_MODEL == model && md.INST_SN == sn && md.SiteId == siteId && md.INST_FACTORY.ToLower() == "purchase")
                                              .OrderBy(md => md.INST_DATE).FirstOrDefaultAsync();
            if (inst == null)
            {
                resp = (int)Globals.ErrorValidationCodes.NOT_IN_GRP_INST;
            }
            else
            {
                string _iDate = inst.INST_DATE;

                if (String.Compare(_iDate, date) > 0)
                {
                    resp = (int)Globals.ErrorValidationCodes.INST_PURCHASE_DATE_GREATER_THAN_INST_OP_DATE;
                }
            }
            return resp;
        }

        public async Task<int> ValidateInstPairAsync(GRP_INSTPAIR model, int siteId)
        {
            if(String.Compare(model.INSTPAIR_MODEL_1, model.INSTPAIR_MODEL_2, true) == 0)
            {
                return (int)Globals.ErrorValidationCodes.INSTPAIR_SAME_SENSORS;
            }

            if(String.Compare(model.INSTPAIR_MODEL_1.Substring(0,3), model.INSTPAIR_MODEL_2.Substring(0, 3), true) == 0)
            {
                return (int)Globals.ErrorValidationCodes.INSTPAIR_SAME_SENSORS;
            }

            int resp = await CheckInstmanGrpAsync(model.INSTPAIR_MODEL_1, model.INSTPAIR_SN_1, model.INSTPAIR_DATE, siteId);
            if (resp > 0) return resp;
            resp = await CheckInstmanGrpAsync(model.INSTPAIR_MODEL_2, model.INSTPAIR_SN_2, model.INSTPAIR_DATE, siteId);
            if (resp > 0) return resp;


            decimal? tempEast = (decimal ?)model.INSTPAIR_EASTWARD_SEP;
            decimal? tempNorth = (decimal?)model.INSTPAIR_NORTHWARD_SEP;
            decimal? tempV = (decimal?)model.INSTPAIR_HEIGHT_SEP;
            if (tempEast == null || tempNorth == null || tempV == null)
            {
                return (int)Globals.ErrorValidationCodes.INSTPAIR_SEP_MANDATORY;
            }

            return 0;
        }

        private async Task<int> CheckInstmanGrpAsync(string model, string sn, string date, int siteId)
        {
            var instman = await _context.GRP_INSTMAN.Where(inst => inst.INSTMAN_MODEL == model && inst.INSTMAN_SN == sn &&
                                                      String.Compare(inst.INSTMAN_DATE, date) < 0 && inst.INSTMAN_TYPE == "Installation"
                                                      && inst.SiteId == siteId && inst.DataStatus == 0).OrderBy(md => md.INSTMAN_DATE).FirstOrDefaultAsync();
            if (instman == null)
            {
                return (int)Globals.ErrorValidationCodes.INSTPAIR_NOT_IN_GRP_INSTMAN;
            }
            return 0;
        }
    }
}
