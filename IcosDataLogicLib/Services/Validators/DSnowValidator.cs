﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public class DSnowValidator : IDSnowValidator
    {
        private readonly IcosDbContext _context;

        public DSnowValidator(IcosDbContext context)
        {
            _context = context;
        }

        public async Task<int> ItemInSamplingPointGroupAsync(GRP_D_SNOW model, int siteId)
        {
            var item = await _context.GRP_PLOT.Where(plot => plot.SiteId == siteId && plot.DataStatus == 0 &&
                                                String.Compare(plot.PLOT_ID, model.D_SNOW_PLOT) == 0 &&
                                                String.Compare(plot.PLOT_DATE, model.D_SNOW_DATE) <= 0).FirstOrDefaultAsync();
            if (item == null)
            {
                return (int)Globals.ErrorValidationCodes.D_SNOW_PLOT_NOT_FOUND;
            }
            return 0;
        }
        public int ValidateDSnow(GRP_D_SNOW model, int siteId)
        {
            if (Globals.IsValidCoordinateSystem(model.D_SNOW_EASTWARD_DIST, model.D_SNOW_NORTHWARD_DIST,
                                                model.D_SNOW_DISTANCE_POLAR, model.D_SNOW_ANGLE_POLAR) > 0)
            {
                return (int)Globals.ErrorValidationCodes.INVALID_COORDINATE_SYSTEM;
            }
            if (!ValidationUtils.XORNull<string>(model.D_SNOW_PLOT, model.D_SNOW_VARMAP))
            {
                return (int)Globals.ErrorValidationCodes.D_SNOW_PLOT_VARMAP;
            }

            return 0;
        }
    }
}
