﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface ITreeValidator
    {
        Task<int> ItemInSamplingPointGroupAsync(GRP_TREE model, int siteId);
        int ValidateTree(GRP_TREE model, int siteId);
    }
}
