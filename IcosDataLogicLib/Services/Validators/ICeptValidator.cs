﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface ICeptValidator
    {
        int ValidateCeptModel(GRP_CEPT model, string ecosystem);
    }
}
