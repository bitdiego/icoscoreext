﻿using IcosDataLib.Data;
using IcosModelLib.Utils;
using System;
using System.Collections.Generic;
//using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public class LoggerValidator : ILoggerValidator
    {
        private readonly IcosDbContext _context;
        public LoggerValidator(IcosDbContext context)
        {
            _context = context;
        }
        public async Task<int> IsLoggerIdAsync(int loggerId, int siteId)
        {
            //maybe check also date??? the logger installation date must be <= than file date???
            //var x = await _context.GRP_LOGGER.Where(logger => logger.LOGGER_ID == loggerId && logger.SiteId == siteId).ToListAsync();
            var log = await _context.GRP_LOGGER.AnyAsync(logger => logger.LOGGER_ID == loggerId && logger.SiteId == siteId);
            if (!log)
            {
                return (int)Globals.ErrorValidationCodes.LOGGER_ID_NOT_REGISTERED;
            }
            return 0;
        }
        public int CheckFileFormat(string fileFormat, string fileExt, int siteId)
        {
            int res = 0;
            if(fileFormat == null && fileExt == null)
            {
                return 0;
            }
            if ( (fileFormat != null && fileExt == null) || (fileFormat == null && fileExt != null))
            {
                return (int)Globals.ErrorValidationCodes.FILE_FORMAT_FILEXT;
            }
            switch (fileFormat.ToLower())
            {
                case "binary":
                    if(String.Compare(fileExt.ToLower(), ".csv", true)==0)
                    {
                        res = (int)Globals.ErrorValidationCodes.CSV_NOT_ALLOWED_FOR_BINARY;
                    }
                    break;
                case "ascii":
                    if (String.Compare(fileExt.ToLower(), ".bin", true) == 0)
                    {
                        res = (int)Globals.ErrorValidationCodes.BIN_NOT_ALLOWED_FOR_ASCII;
                    }
                    break;
            }
            return res;
        }
        public int CheckHeadConstraints(string fileFormat, int? fileHeadNum, int? fileHeadVars, int? fileHeadType)
        {
            int res = 0;
            if (fileFormat == null)
            {
                if (fileHeadNum == null && fileHeadVars == null && fileHeadType == null)
                {
                    return 0;
                }
                else
                {
                    return 555;
                }
            }
            if (String.Compare(fileFormat.ToLower(), "binary", true) == 0)
            {
                /*
                 * 
                 * string head_type = ((TextBox)item.FindControl("_qual6")).Text.Trim();
                        if (!String.IsNullOrEmpty(head_num) && (String.IsNullOrEmpty(head_vars) || String.IsNullOrEmpty(head_type)))
                        {
                            int i_head_num = int.Parse(head_num);
                            if (i_head_num != 0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Error! You have to enter FILE_HEAD_VARS and FILE_HEAD_TYPE for binary files if FILE_HEAD_NUM is entered')", true);
                                e.Canceled = true;
                                return;
                            }
                        }
                        else if (!String.IsNullOrEmpty(head_vars) && (String.IsNullOrEmpty(head_num) || String.IsNullOrEmpty(head_type)))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Error! You have to enter FILE_HEAD_NUM and FILE_HEAD_TYPE for binary files if FILE_HEAD_VARS is entered')", true);
                            e.Canceled = true;
                            return;
                        }
                        else if (!String.IsNullOrEmpty(head_type) && (String.IsNullOrEmpty(head_num) || String.IsNullOrEmpty(head_vars)))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Error! You have to enter FILE_HEAD_NUM and FILE_HEAD_VARS for binary files if FILE_HEAD_TYPE is entered')", true);
                            e.Canceled = true;
                            return;
                        }
                        
                        else
                        {
                            int h_num = int.Parse(head_num);
                            int h_vars = 0;
                            int h_type = 0;
                            if (!String.IsNullOrEmpty(head_vars))
                            {
                                h_vars = int.Parse(head_vars);
                            }
                            if (!String.IsNullOrEmpty(head_type))
                            {
                                h_type = int.Parse(head_type);
                            }
                            if (h_vars > h_num)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Error! FILE_HEAD_VARS cannot be greater than FILE_HEAD_NUM')", true);
                                e.Canceled = true;
                                return;
                            }
                            if (h_type > h_num)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Error! FILE_HEAD_TYPE cannot be greater than FILE_HEAD_NUM')", true);
                                e.Canceled = true;
                                return;
                            }
                            if (h_vars == h_type && h_num!=0)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('Error! FILE_HEAD_TYPE must be different from FILE_HEAD_VARS')", true);
                                e.Canceled = true;
                                return;
                            }
                        }
                 * */


                if (fileHeadNum==null && fileHeadVars == null && fileHeadType == null)
                {
                    return 0;
                }
                if (fileHeadType==null && fileHeadNum!=null && fileHeadVars != null)
                {
                    if (fileHeadNum != 0)
                    {
                        res = (int)Globals.ErrorValidationCodes.FILE_HEAD_VARS_TYPE_MISSING_BINARY;
                    }
                    else if (fileHeadType==null && fileHeadNum==null && fileHeadVars!=null)
                    {
                        res = (int)Globals.ErrorValidationCodes.FILE_HEAD_NUM_TYPE_MISSING_BINARY;
                    }
                    else if (fileHeadType!=null && fileHeadNum==null && fileHeadVars==null)
                    {
                        res = (int)Globals.ErrorValidationCodes.FILE_HEAD_NUM_VARS_MISSING_BINARY;
                    }
                    else
                    {
                        
                        if (fileHeadVars > fileHeadNum)
                        {
                            res = (int)Globals.ErrorValidationCodes.FILE_HEAD_VARS_GT_FILE_HEAD_NUM;
                        }
                        if (fileHeadType > fileHeadNum)
                        {
                            res = (int)Globals.ErrorValidationCodes.FILE_HEAD_TYPE_GT_FILE_HEAD_NUM;
                        }
                        if (fileHeadVars == fileHeadType && fileHeadNum != 0)
                        {
                            res = (int)Globals.ErrorValidationCodes.FILE_HEAD_TYPE_GT_FILE_HEAD_VARS;
                        }
                    }
                }
            }
            else //ASCII
            {
                if (fileHeadNum!=null && fileHeadVars==null)
                {
                    if (fileHeadNum != 0)
                    {
                        res = (int)Globals.ErrorValidationCodes.FILE_HEAD_VARS_MISSING;
                    }

                }
                else if (fileHeadNum!=null && fileHeadVars==null)
                {
                    //OK....
                }
                else if (fileHeadNum==null && fileHeadVars!=null)
                {
                    res = (int)Globals.ErrorValidationCodes.FILE_HEAD_NUM_MISSING;
                }
                else
                {
                   

                    if (fileHeadVars != null && fileHeadVars > fileHeadNum)
                    {
                        res = (int)Globals.ErrorValidationCodes.FILE_HEAD_VARS_GT_FILE_HEAD_NUM;
                    }
                    if (fileHeadVars == fileHeadType && fileHeadNum != 0)
                    {
                        res = (int)Globals.ErrorValidationCodes.FILE_HEAD_TYPE_EQ_FILE_HEAD_VARS;
                    }
                }
            }
            return res;
        }
    }
}
