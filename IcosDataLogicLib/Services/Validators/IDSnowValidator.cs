﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface IDSnowValidator
    {
        Task<int> ItemInSamplingPointGroupAsync(GRP_D_SNOW model, int siteId);
        int ValidateDSnow(GRP_D_SNOW model, int siteId);
    }
}
