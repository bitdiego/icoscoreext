﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public class TreeValidator : ITreeValidator
    {
        private readonly IcosDbContext _context;
        public TreeValidator(IcosDbContext context)
        {
            _context = context;
        }

        public async Task<int> ItemInSamplingPointGroupAsync(GRP_TREE model, int siteId)
        {
            var item = await _context.GRP_PLOT.Where(plot=>plot.SiteId==siteId && plot.DataStatus==0 &&
                                                String.Compare(plot.PLOT_ID, model.TREE_PLOT)==0 &&
                                                String.Compare(plot.PLOT_DATE, model.TREE_DATE) <= 0).FirstOrDefaultAsync();
            if (item == null)
            {
                return (int)Globals.ErrorValidationCodes.TREE_PLOT_NOT_FOUND;
            }
            return 0;
        }
        public int ValidateTree(GRP_TREE model, int siteId)
        {
            if(model.TREE_PLOT.ToLower().StartsWith("cp") || model.TREE_PLOT.ToLower().EndsWith("cp"))
            {
                if (model.TREE_ID == null)
                {
                    return (int)Globals.ErrorValidationCodes.TREE_ID_MANDATORY_CP;
                }
            }

            if(model.TREE_DBH==null || String.IsNullOrEmpty(model.TREE_SPP) || String.IsNullOrEmpty(model.TREE_STATUS))
            {
                return (int)Globals.ErrorValidationCodes.TREE_MANDATORY_VARIABLES;
            }
            if(Globals.IsValidCoordinateSystem<decimal?>(model.TREE_EASTWARD_DIST, model.TREE_NORTHWARD_DIST, model.TREE_DISTANCE_POLAR, model.TREE_ANGLE_POLAR) > 0)
            {
                return (int)Globals.ErrorValidationCodes.INVALID_COORDINATE_SYSTEM;
            }
            return 0;
        }
    }
}
