﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface ILitterPntValidator
    {
        Task<int> ItemInSamplingPointGroupAsync(GRP_LITTERPNT model, int siteId);
        int ValidateLitterPnt(GRP_LITTERPNT model, int siteId);
    }
}
