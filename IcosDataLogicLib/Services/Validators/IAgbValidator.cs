﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface IAgbValidator
    {
        Task<int> ItemInSamplingPointGroupAsync(GRP_AGB model, int siteId);
        int ValidateAgb(GRP_AGB model, int siteId);
    }
}
