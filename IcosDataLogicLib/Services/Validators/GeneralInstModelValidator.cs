﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services
{
    public class GeneralInstModelValidator : IGeneralInstModelValidator
    {
        private readonly IcosDbContext _context;

        public GeneralInstModelValidator(IcosDbContext context)
        {
            _context = context;
        }

        public async Task<int> InstrumentInGrpInst(string instModel, string instSn, string date, int siteId)
        {
            int resp = 0;
            var inst = await _context.GRP_INST.Where(model => model.INST_MODEL == instModel && model.INST_SN == instSn && model.SiteId == siteId && model.INST_FACTORY == "Purchase")
                                              .OrderBy(model => model.INST_DATE).FirstOrDefaultAsync();
            if (inst == null)
            {
                resp = (int)Globals.ErrorValidationCodes.NOT_IN_GRP_INST;
            }
            else
            {
                string _iDate = inst.INST_DATE;
                if (String.Compare(_iDate, date) > 0)
                {
                    resp = (int)Globals.ErrorValidationCodes.INST_PURCHASE_DATE_GREATER_THAN_INST_OP_DATE;
                }
            }
            return resp;
        }

        public async Task<int> InstrumentInGrpEcAsync(string instModel, string instSn, string date, int siteId)
        {
            var inst = await _context.GRP_EC.Where(model => model.EC_MODEL == instModel && model.EC_SN == instSn 
                                                    && model.SiteId == siteId && model.EC_TYPE == "Installation"
                                                    && String.Compare(model.EC_DATE, date)<0)
                                             .OrderBy(model => model.EC_DATE).FirstOrDefaultAsync();
            if (inst == null)
            {
                inst = await _context.GRP_EC.Where(model => model.EC_MODEL == instModel && model.EC_SN == instSn
                                                    && model.SiteId == siteId && model.EC_TYPE == "Installation"
                                                    && String.Compare(model.EC_DATE, date) > 0)
                                             .OrderBy(model => model.EC_DATE).FirstOrDefaultAsync();
                if (inst == null)
                {
                    return (int)Globals.ErrorValidationCodes.SA_GA_MISSING_IN_EC_GROUP;
                }
                else
                {
                    return (int)Globals.ErrorValidationCodes.ECSYS_DATE_WRONG;
                }
                
            }
            return 0;
        }

        public int SerialNumberCheck(string instModel, string instSn)
        {
            if (!IGeneralInstModelValidator.regulars.ContainsKey(instModel.ToLower())) return 0;
            string instReg = IGeneralInstModelValidator.regulars[instModel.ToLower()];
            Match instMatch = Regex.Match(instSn, instReg, RegexOptions.IgnoreCase);
            if (!instMatch.Success)
            {
                return (int)Globals.ErrorValidationCodes.WRONG_SERIALNUMBER_FORMAT;
            }
            return 0;
        }

        public int SerialNumberCheck(GRP_INST inst)
        {
            if (!IGeneralInstModelValidator.regulars.ContainsKey(inst.INST_MODEL)) return 0;
            string instReg = IGeneralInstModelValidator.regulars[inst.INST_MODEL.ToLower()];
            Match instMatch = Regex.Match(inst.INST_SN, instReg, RegexOptions.IgnoreCase);
            if (!instMatch.Success)
            {
                return (int)Globals.ErrorValidationCodes.WRONG_SERIALNUMBER_FORMAT;
            }
            return 0;
        }

        public async Task<int> UniqueLoggerIdAsync(string loggerModel, string loggerSn, int loggerId, string loggerDate, int siteId)
        {
            var loggers = await _context.GRP_LOGGER.Where(model => /*model.LOGGER_MODEL == loggerModel && model.LOGGER_SN == loggerSn &&*/
                                                        model.LOGGER_ID==loggerId && model.SiteId == siteId && model.DataStatus==0)
                                                        .OrderByDescending(model => model.LOGGER_DATE).ToListAsync();
            if (loggers.Count == 0)
            {
                //no other logger mapped on passed loggerId
                return 0;
            }
            if(loggers[0].LOGGER_MODEL!=loggerModel)
            {
                //same loggerId, different logger (by model): error
                return 555;              
            }
            else
            {
                //same loggerId, same logger (by model): error
                if (String.Compare(loggerDate, loggers[0].LOGGER_DATE) < 0)
                {
                    //if submitted loggerDate < previous logger date, OK
                    return 557;
                }
                else
                {
                    //if submitted loggerDate>=previous logger date, OK
                    return 0;
                }
            }
        }
    }
}
