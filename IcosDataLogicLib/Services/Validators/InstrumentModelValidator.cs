﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services
{
    public class InstrumentModelValidator : IBasicInstrumentValidator<GRP_INST>
    {
        private readonly IcosDbContext _context;

        public InstrumentModelValidator(IcosDbContext context)
        {
            _context = context;
        }

        /*
         * First, a Purchase operation must be performed
         * If passed model factory == Purchase, this is possible only if entity table is empty;
         * If model factory != Purchase, then check if a Purchase item is in table
         *      a) if item in table, compare dates and raise error if model date < item purchase date
         *      b) no purchased item, raise error
         *
         * */
        public async Task<int> InstrumentInGrpInst(GRP_INST model, int siteId)
        {
            int resp = 0;
            if(String.Compare(model.INST_FACTORY.ToLower(), "purchase") == 0)
            {
                return 0;
            }
            var inst = await _context.GRP_INST.Where(md => md.INST_MODEL == model.INST_MODEL && md.INST_SN == model.INST_SN && md.SiteId == siteId && md.INST_FACTORY.ToLower() == "purchase")
                                              .OrderBy(md => md.INST_DATE).FirstOrDefaultAsync();
            if (inst == null)
            {
                resp = (int)Globals.ErrorValidationCodes.NOT_IN_GRP_INST;
            }
            else
            {
                string _iDate = inst.INST_DATE;
                if(String.Compare(_iDate, model.INST_DATE) > 0)
                {
                    resp = (int)Globals.ErrorValidationCodes.INST_PURCHASE_DATE_GREATER_THAN_INST_OP_DATE;
                }
            }
            return resp;
        }
        public async Task<int> LastExpectedOpByDateAsync(GRP_INST model, int siteId)
        {
            string modelFactory = model.INST_FACTORY;
            var _instPurchased = await _context.GRP_INST.Where(md => md.INST_FACTORY.ToLower() == "purchase"
                                                    && md.INST_MODEL.ToLower() == model.INST_MODEL.ToLower()
                                                    && md.INST_SN.ToLower() == model.INST_SN.ToLower() && model.SiteId == siteId).FirstOrDefaultAsync();

            if (String.Compare(modelFactory, "purchase", true) == 0)
            {
                if (_instPurchased!=null)
                {
                    //WARNING: check also if entered same infos
                    return (int)Globals.ErrorValidationCodes.GRP_INST_ALREADY_PURCHASED;
                }
            }
            else
            {
                if (_instPurchased == null)
                {
                    return (int)Globals.ErrorValidationCodes.GRP_INST_NOT_PURCHASE;
                }
                else
                {
                    if(String.Compare(model.INST_DATE, _instPurchased.INST_DATE) < 0)
                    {
                        return (int)Globals.ErrorValidationCodes.GRP_INST_NOT_PURCHASE;
                    }
                }
            }
            
            return 0;
        }

        public async Task<int> LastExpectedOpByDateAsync(string instModel, string instSn, string instFactory, string instDate, int siteId)
        {
            
            if (String.Compare(instFactory, "purchase", true) == 0)
            {
                if (await _context.GRP_INST.AnyAsync(inst => inst.INST_FACTORY.ToLower() == "purchase" && inst.INST_MODEL.ToLower() == instModel.ToLower() 
                                                    && inst.INST_SN.ToLower() == instSn.ToLower() && inst.SiteId == siteId))
                {
                    return (int)Globals.ErrorValidationCodes.GRP_INST_ALREADY_PURCHASED;
                }
            }
            else
            {
                var inst = await _context.GRP_INST.FirstOrDefaultAsync(inst => (string.Compare(inst.INST_FACTORY.ToLower(), "purchase", true) == 0)
                                                                        && (string.Compare(inst.INST_MODEL.ToLower(), instModel, true) == 0) 
                                                                        && (string.Compare(inst.INST_SN.ToLower(), instSn, true) == 0) && inst.SiteId == siteId);
                if (instModel == null)
                {
                    return (int)Globals.ErrorValidationCodes.GRP_INST_NOT_PURCHASE;
                }
                else
                {
                    if (String.Compare(instDate, inst.INST_DATE) < 0)
                    {
                        return (int)Globals.ErrorValidationCodes.GRP_INST_NOT_PURCHASE;
                    }
                }
            }

            return 0;
        }

        public int LoggerFileCheck(GRP_INST model, int siteId)
        {
            throw new NotImplementedException();
        }
        public int LoggerFileSamplingIntCheck(GRP_INST model, int siteId)
        {
            throw new NotImplementedException();
        }

        /*public int SerialNumberCheck(GRP_INST model)
        {
            if (!IBasicInstrumentValidator<GRP_INST>.regulars.ContainsKey(model.INST_MODEL.ToLower())) return 0;
            string instReg = IBasicInstrumentValidator<GRP_INST>.regulars[model.INST_MODEL.ToLower()];
            Match instMatch = Regex.Match(model.INST_SN, instReg, RegexOptions.IgnoreCase);
            if (!instMatch.Success)
            {
                return (int)Globals.ErrorValidationCodes.WRONG_SERIALNUMBER_FORMAT;
            }
            return 0;
        }*/

        /* public int SerialNumberCheck(string instModel, string instSn)
         {
             if (!IBasicInstrumentValidator<GRP_INST>.regulars.ContainsKey(instModel)) return 0;
             string instReg = IBasicInstrumentValidator<GRP_INST>.regulars[instModel.ToLower()];
             Match instMatch = Regex.Match(instSn, instReg, RegexOptions.IgnoreCase);
             if (!instMatch.Success)
             {
                 return (int)Globals.ErrorValidationCodes.WRONG_SERIALNUMBER_FORMAT;
             }
             return 0;
         }*/
    }
}
