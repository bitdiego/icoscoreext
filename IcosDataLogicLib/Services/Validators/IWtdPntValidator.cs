﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface IWtdPntValidator
    {
        Task<int> ItemInSamplingPointGroupAsync(GRP_WTDPNT model, int siteId);
        int ValidateWtdPnt(GRP_WTDPNT model, int siteId);
    }
}
