﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services
{
    public interface IGeneralInstModelValidator
    {
        Task<int> InstrumentInGrpInst(string instModel, string instSn, string date, int siteId);
        
        //for ecsys only: perhaps to move in a dedicated interface???
        Task<int> InstrumentInGrpEcAsync(string instModel, string instSn, string date, int siteId);
        int SerialNumberCheck(string instModel, string instSn);
        int SerialNumberCheck(GRP_INST inst);
        /************************************/
        //For Logger Checks: maybe to create a separate service?
        Task<int> UniqueLoggerIdAsync(string loggerModel, string loggerSn, int loggerId, string loggerDate, int siteId);

        /// <summary>
        /// May be create a table in db for this???
        /// </summary>
        static Dictionary<string, string> regulars = new Dictionary<string, string>
        {
            { "ga_aiu-li-cor li-7550", @"^(AIU)-\d{4}$" },  ///^(AIU)-\d{4}$/
            { "ga_cp-li-cor li-7200", @"^(72H)-\d{4}$" },   ////^(72H)-\d{4}$/;
            { "ga_cp-li-cor li-7200rs", @"^(72H)-\d{4}$" },
            {"sa-gill hs-100",@"^H\d{6}$"},                ////^H\d{6}$/;
            {"sa-gill hs-50",@"^H\d{6}$"},
            {"rad_4c-k&z cnr1",@"^\d{6}$"},               ////RAD_4C-K&Z CNR4 RAD_4C-K&Z CNR1  rad_4c-k&z cnr1
            {"rad_4c-k&z cnr4",@"^\d{6}$"},
            {"ga_cp-li-cor li-7000",  @"^(IRG4)-\d{4}$"},
            {"dl-li-cor sflux2", @"^(SMART2)\-[0-9]{5}$"}
        };

    }
}
