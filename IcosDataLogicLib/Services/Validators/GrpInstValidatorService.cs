﻿using IcosDataLib.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services
{
    public class GrpInstValidatorService : IGrpInstValidatorService
    {
        private readonly IcosDbContext _context;

        public GrpInstValidatorService(IcosDbContext ctx)
        {
            _context = ctx;
        }
        public async Task<int> InstrumentPurchasedAsync(string model, string serial, int siteId, string date, bool isPurchase)
        {
            if (isPurchase)
            {
                var item = await _context.GRP_INST.FirstOrDefaultAsync(inst => inst.INST_MODEL == model && inst.INST_SN == serial && inst.SiteId == siteId);
                if (item != null) return 100;
            }
            else
            {
                var item = await _context.GRP_INST.FirstOrDefaultAsync(inst => inst.INST_MODEL == model && inst.INST_SN == serial 
                                                                        && inst.SiteId == siteId 
                                                                        && inst.INST_FACTORY.ToLower() =="purchase");
                if (item == null)
                {
                    return 101;
                }
                else
                {

                }
            }
            //var x = _context.GRP_INST.FindAsync(inst => inst.INST_MODEL == )
            return 0;
        }


    }
}
