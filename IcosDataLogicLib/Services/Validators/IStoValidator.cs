﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface IStoValidator<T> : IBasicInstrumentValidator<T>
    {
        Task<int> ValidateStoByOpAsync(GRP_STO model, int siteId);
    }
}
