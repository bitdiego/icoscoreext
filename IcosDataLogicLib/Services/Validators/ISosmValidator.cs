﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface ISosmValidator
    {
        Task<int> ItemInSamplingPointGroupAsync(GRP_SOSM model, int siteId);
        int ValidateSosm(GRP_SOSM model, int siteId);
    }
}
