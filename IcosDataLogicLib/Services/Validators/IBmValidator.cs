﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface IBmValidator<T> : IBasicInstrumentValidator<T>
    {
        int VarHVRCorrect(string varHVR);

        int ValidateByBmType(GRP_BM model, int siteId);

        int VarMapped(GRP_BM model, int siteId);
    }
}


