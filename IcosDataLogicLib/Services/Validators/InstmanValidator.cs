﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public class InstmanValidator : IInstmanValidator
    {
        private readonly IcosDbContext _context;

        public InstmanValidator(IcosDbContext context)
        {
            _context = context;
        }

        public async Task<int> InstrumentInGrpInst(GRP_INSTMAN model, int siteId)
        {
            int resp = 0;

            var inst = await _context.GRP_INST.Where(md => md.INST_MODEL == model.INSTMAN_MODEL && md.INST_SN == model.INSTMAN_SN && md.SiteId == siteId && md.INST_FACTORY.ToLower() == "purchase")
                                              .OrderBy(md => md.INST_DATE).FirstOrDefaultAsync();
            if (inst == null)
            {
                resp = (int)Globals.ErrorValidationCodes.NOT_IN_GRP_INST;
            }
            else
            {
                string _iDate = inst.INST_DATE;
                string instmanDate = String.IsNullOrEmpty(model.INSTMAN_DATE) ? model.INSTMAN_DATE_START : model.INSTMAN_DATE;
                if (String.Compare(_iDate, instmanDate) > 0)
                {
                    resp = (int)Globals.ErrorValidationCodes.INST_PURCHASE_DATE_GREATER_THAN_INST_OP_DATE;
                }
            }
            return resp;
        }

        public async Task<int> LastExpectedOpByDateAsync(GRP_INSTMAN model, int siteId)
        {
            int result = 0;
            string bmType = model.INSTMAN_TYPE.ToLower();
            string bmDate = (String.IsNullOrEmpty(model.INSTMAN_DATE)) ? model.INSTMAN_DATE_START : model.INSTMAN_DATE;
            string lastOpToSearch = (bmType == "installation") ? "removal" : "installation";

            /////step 0
            ///check if passed op date is prior to purchase inst date
            var beforePurchase = await _context.GRP_INST.Where(inst => inst.INST_DATE.CompareTo(bmDate) < 0).FirstOrDefaultAsync();
            if (beforePurchase == null)
            {
                return (int)Globals.ErrorValidationCodes.INST_PURCHASE_DATE_GREATER_THAN_INST_OP_DATE;
            }
            ////First check if sensor has been first installed (for any op != installation)
            ///or removed or empty (if op==installation)

            var foundBm = await _context.GRP_INSTMAN.Where(bm => ((bm.INSTMAN_TYPE.ToLower() == "installation") || (bm.INSTMAN_TYPE.ToLower() == "removal"))
                                                                   && bm.DataStatus == 0 && bm.SiteId == siteId
                                                                   && model.INSTMAN_MODEL == bm.INSTMAN_MODEL && model.INSTMAN_SN == bm.INSTMAN_SN)
                                               .OrderByDescending(bm => bm.INSTMAN_DATE)
                                               .ToListAsync();
            GRP_INSTMAN _bmItem = null;
            if (foundBm != null)
            {
                _bmItem = foundBm.FirstOrDefault(item => String.Compare(item.INSTMAN_DATE, bmDate) < 0);
            }
            //                                   

            if (_bmItem == null)
            {
                if (bmType == "installation")
                {
                    result = 0;
                }
                else
                {
                    result = (int)Globals.ErrorValidationCodes.INSTMAN_SENSOR_NOT_INSTALLED;
                }
            }
            else
            {
                if (bmType == "installation")
                {
                    if (_bmItem.INSTMAN_TYPE.ToLower().CompareTo("removal") != 0)
                    {
                        result = (int)Globals.ErrorValidationCodes.INSTMAN_SENSOR_NOT_REMOVED;
                    }
                }
                else
                {
                    if (_bmItem.INSTMAN_TYPE.ToLower().CompareTo("installation") != 0)
                    {
                        result = (int)Globals.ErrorValidationCodes.INSTMAN_SENSOR_NOT_INSTALLED;
                    }
                }
            }
            ///add a check also for the future
            ///for example, if an installation or a removal is sent,
            ///check if there are ops for dates >= passed date
            ///raise error for this
            /////DIEGO::: TO TEST!!!!
            if (new string[] { "installation", "removal" }.Contains(bmType.ToLower()))
            {
                var noFuture = await _context.GRP_INSTMAN.Where(bm => bm.DataStatus == 0 && bm.SiteId == siteId
                                                && model.INSTMAN_MODEL == bm.INSTMAN_MODEL && model.INSTMAN_SN == bm.INSTMAN_SN &&
                                                (String.Compare(bm.INSTMAN_DATE, bmDate) > 0 || String.Compare(bm.INSTMAN_DATE_START, bmDate) > 0))
                                               .OrderBy(bm => bm.INSTMAN_DATE)
                                               .FirstOrDefaultAsync();
                if (noFuture != null)
                {
                    result = (int)Globals.ErrorValidationCodes.INSTMAN_SENSOR_FOLLOWING_OPERATION;
                }
            }
            return result;
        }
        public int VarHVRCorrect(string varHVR)
        {
            return 0;
        }

        public int ValidateByInstManType(GRP_INSTMAN model, int siteId)
        {
            int result = 0;
            string bmType = model.INSTMAN_TYPE.ToLower();
            string bmVhr = model.INSTMAN_VARIABLE_H_V_R;
            if (!String.IsNullOrEmpty(bmVhr))
            {
                result = VarHVRCorrect(bmVhr);
                if (result > 0) return result;
            }

            switch (bmType)
            {
                case "installation":

                    if (model.INSTMAN_HEIGHT == null || model.INSTMAN_EASTWARD_DIST == null || model.INSTMAN_NORTHWARD_DIST == null)
                    {
                        return (int)Globals.ErrorValidationCodes.INSTMAN_INSTALLATION_HEIGHT_EAST_NORTH_MANDATORY;
                    }
                    if (!ValidationUtils.CountBoundedProps<GRP_INSTMAN>(model, 2, "INSTMAN_VARIABLE_H_V_R", "INSTMAN_SAMPLING_INT"))
                    {
                        return (int)Globals.ErrorValidationCodes.INSTMAN_INSTALLATION_VAR_SAMPLING;
                    }
                    break;
                case "maintenance":
                    break;
                case "variable map":
                    
                    if (String.IsNullOrEmpty(bmVhr) || model.INSTMAN_SAMPLING_INT == null)
                    {
                        return (int)Globals.ErrorValidationCodes.INSTMAN_VARMAP_SAMPLING;
                    }
                    break;
                case "disturbance":
                case "field calibration":
                case "field calibration check":
                case "field cleaning":
                case "firmware update":
                case "parts substitution":
                case "filter change":
                case "tube change":
                    //check INSTMAN_VARIABLE_H_V_R validity; performed elsewhere
                    break;
                case "general comment":
                    break;
            }

            return result;
        }

        public int VarMapped(GRP_INSTMAN model, int siteId)
        {

            string bmDateToCompare = (String.IsNullOrEmpty(model.INSTMAN_DATE)) ? model.INSTMAN_DATE_START : model.INSTMAN_DATE;
            var _mappedInst = _context.GRP_INSTMAN.Where(bm => bm.SiteId == siteId && bm.DataStatus == 0 &&
                                                        bm.INSTMAN_VARIABLE_H_V_R.ToLower() == model.INSTMAN_VARIABLE_H_V_R.ToLower() &&
                                                        (bm.INSTMAN_MODEL.ToLower().CompareTo(model.INSTMAN_MODEL.ToLower()) != 0 ||
                                                        bm.INSTMAN_SN.ToLower().CompareTo(model.INSTMAN_SN.ToLower()) != 0)).ToList();
            if (_mappedInst == null || _mappedInst.Count == 0)
            {
                return 0;
            }
            foreach (var record in _mappedInst)
            {
                var lastOp = _context.GRP_INSTMAN.Where(bm => bm.SiteId == siteId && bm.DataStatus == 0 &&
                                                        bm.INSTMAN_MODEL == record.INSTMAN_MODEL && bm.INSTMAN_SN == record.INSTMAN_SN &&
                                                        bm.INSTMAN_DATE.CompareTo(bmDateToCompare) <= 0 &&
                                                        (new string[] { "installation", "removal", "variable map" }).Contains(bm.INSTMAN_TYPE.ToLower())                                                        )
                                            .OrderByDescending(bm => bm.INSTMAN_DATE).FirstOrDefault();
                if (lastOp != null)
                {
                    if (lastOp.INSTMAN_TYPE.ToLower() != "removal")
                    {
                        if (!String.IsNullOrEmpty(lastOp.INSTMAN_VARIABLE_H_V_R))
                        {
                            if (lastOp.INSTMAN_VARIABLE_H_V_R.CompareTo(model.INSTMAN_VARIABLE_H_V_R) == 0)
                            {
                                return (int)Globals.ErrorValidationCodes.INSTMAN_VAR_MAPPED_DIFFERENT_INST;
                            }
                        }
                    }
                }
            }
            return 0;
        }
     }
}
