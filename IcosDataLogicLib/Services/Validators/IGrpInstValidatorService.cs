﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services
{
    public interface IGrpInstValidatorService
    {
        Task<int> InstrumentPurchasedAsync(string model, string serial, int siteId, string date, bool isPurchase);
    }
}
