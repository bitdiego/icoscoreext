﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface IInstmanValidator
    {
        Task<int> InstrumentInGrpInst(GRP_INSTMAN model, int siteId);

        Task<int> LastExpectedOpByDateAsync(GRP_INSTMAN model, int siteId);
        int VarHVRCorrect(string varHVR);

        int ValidateByInstManType(GRP_INSTMAN model, int siteId);

        int VarMapped(GRP_INSTMAN model, int siteId);
    }
}
