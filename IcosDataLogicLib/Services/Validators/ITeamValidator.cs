﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface ITeamValidator
    {
        Task<int> WorkEndValidatorAsync(GRP_TEAM model, int siteId);

        /// <summary>
        ///1. every Team member must have ONLY ONE role
        ///2. There must be ONLY ONE PI and ONLY ONE MANAGER
        ///3. TEAM_MEMBER_WORKEND: can be used to communicate the end of a role; from the day after, that role becomes available (if MANAGER)
        ///4. PI cannot be removed / changed directly but only after a communication to ICOS ETC
        ///5. A Login account must be automatically created for: SCI, DATA and MANAGER roles. For other roles, a formal request must be done to ICOS ETC
        ///6. For each new user created, whichever role, an item must be added to the ICOS_Uid table
        /// </summary>
        /// <param name="model"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        Task<int> ManageTeamMemberAsync(GRP_TEAM model, int siteId);
    }
}
