﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public class SosmValidator : ISosmValidator
    {
        private readonly IcosDbContext _context;

        public SosmValidator(IcosDbContext context)
        {
            _context = context;
        }
        public async Task<int> ItemInSamplingPointGroupAsync(GRP_SOSM model, int siteId)
        {
            var item = await _context.GRP_PLOT.Where(plot => plot.SiteId == siteId && plot.DataStatus == 0 &&
                                                String.Compare(plot.PLOT_ID, model.SOSM_PLOT_ID) == 0 &&
                                                String.Compare(plot.PLOT_DATE, model.SOSM_DATE) <= 0).FirstOrDefaultAsync();
            if (item == null)
            {
                return (int)Globals.ErrorValidationCodes.SOSM_PLOT_ID_NOT_FOUND;
            }
            return 0;
        }
        public int ValidateSosm(GRP_SOSM model, int siteId)
        {
            string sosmPlotid = model.SOSM_PLOT_ID.ToLower();
            string sosmSampleMat = model.SOSM_SAMPLE_MAT.ToLower();


            if (sosmPlotid.StartsWith("sp-i") && !sosmPlotid.StartsWith("sp-ii"))
            {
                if (sosmSampleMat.StartsWith("o"))
                {
                    return (int)Globals.ErrorValidationCodes.SOSM_SAMPLE_MAT_O_NOT_ALLOWED;
                }
                if(model.SOSM_UD==null || model.SOSM_LD == null)
                {
                    return (int)Globals.ErrorValidationCodes.SOSM_UD_LD;
                }
                if (!ValidationUtils.IsValidPattern(model.SOSM_SAMPLE_ID, Globals.spiSosmM))
                {
                    return (int)Globals.ErrorValidationCodes.SOSM_INVALID_SPI_M;
                }
            }
            else if(sosmPlotid.StartsWith("sp-ii"))
            {
                if (sosmSampleMat=="m")
                {
                    if (model.SOSM_UD == null || model.SOSM_LD == null)
                    {
                        return (int)Globals.ErrorValidationCodes.SOSM_UD_LD;
                    }
                    if(!ValidationUtils.CountBoundedProps<GRP_SOSM>(model, 2, "SOSM_AREA", "SOSM_VOLUME"))
                    {
                        return (int)Globals.ErrorValidationCodes.SOSM_AREA_VOLUME;
                    }
                    if (!ValidationUtils.IsValidPattern(model.SOSM_SAMPLE_ID, Globals.spiiSosmM))
                    {
                        return (int)Globals.ErrorValidationCodes.SOSM_INVALID_SPII_M;
                    }
                }
                else
                {
                    //O, Oi, Oa, Oe
                    if (!ValidationUtils.CountBoundedProps<GRP_SOSM>(model, 3, "SOSM_THICKNESS", "SOSM_AREA", "SOSM_VOLUME"))
                    {
                        return (int)Globals.ErrorValidationCodes.SOSM_THICKNESS_AREA_VOLUME;
                    }
                    if (!ValidationUtils.IsValidPattern(model.SOSM_SAMPLE_ID, Globals.spiiSosmOrganic))
                    {
                        return (int)Globals.ErrorValidationCodes.SOSM_INVALID_SPII_O;
                    }
                }

            }
            else
            {
                return (int)Globals.ErrorValidationCodes.SOSM_CP_NOT_ALLOWED;
            }
            return 0;
        }
    }
}
