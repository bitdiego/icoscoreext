﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using System;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace IcosDataLogicLib.Services.Validators
{
    public class BmValidator : IBmValidator<GRP_BM>
    {
        private readonly IcosDbContext _context;
        public BmValidator(IcosDbContext ctx)
        {
            _context = ctx;
        }

        public async Task<int> InstrumentInGrpInst(GRP_BM model, int siteId)
        {
            int resp = 0;
            
            var inst = await _context.GRP_INST.Where(md => md.INST_MODEL == model.BM_MODEL && md.INST_SN == model.BM_SN && md.SiteId == siteId && md.INST_FACTORY.ToLower() == "purchase")
                                              .OrderBy(md => md.INST_DATE).FirstOrDefaultAsync();
            if (inst == null)
            {
                resp = (int)Globals.ErrorValidationCodes.NOT_IN_GRP_INST;
            }
            else
            {
                string _iDate = inst.INST_DATE;
                string bmDate = String.IsNullOrEmpty(model.BM_DATE) ? model.BM_DATE_START : model.BM_DATE;
                if (String.Compare(_iDate, bmDate) > 0)
                {
                    resp = (int)Globals.ErrorValidationCodes.INST_PURCHASE_DATE_GREATER_THAN_INST_OP_DATE;
                }
            }
            return resp;
        }

        public int VarHVRCorrect(string varHVR)
        {
            if (varHVR.Where(ch => ch == '_').Count() < 3)
            {
                return (int)Globals.ErrorValidationCodes.BM_VARIABLE_H_V_R_WRONG_FORMAT;
            }
            int count=0;
            string temp = "";
            int i = varHVR.Count() - 1;
            for(; i>0; i--)
            //while (count <= 2)
            {
                if (varHVR[i] == '_')
                {
                    if(++count>=3) break;
                }
            }
            temp = varHVR.Substring(0, i);
            var inVarList = _context.BADMList.Any(item => item.cv_index == 73 && item.shortname.ToLower() == temp.ToLower());
            if (!inVarList)
            {
                return (int)Globals.ErrorValidationCodes.BM_VARIABLE_H_V_R_NOT_FOUND;
            }
            return 0;
        }

        public async Task<int> LastExpectedOpByDateAsync(GRP_BM model, int siteId)
        {
            int result = 0;
            string bmType = model.BM_TYPE.ToLower();
            string bmDate = (String.IsNullOrEmpty(model.BM_DATE)) ? model.BM_DATE_START : model.BM_DATE;
            string lastOpToSearch = (bmType == "installation") ? "removal" : "installation";

            /////step 0
            ///check if passed op date is prior to purchase inst date
            var beforePurchase = await _context.GRP_INST.Where(inst => inst.INST_DATE.CompareTo(bmDate) < 0).FirstOrDefaultAsync();
            if (beforePurchase == null)
            {
                return (int)Globals.ErrorValidationCodes.INST_PURCHASE_DATE_GREATER_THAN_INST_OP_DATE;
            }
            ////First check if sensor has been first installed (for any op != installation)
            ///or removed or empty (if op==installation)

            var foundBm = await _context.GRP_BM.Where(bm => ((bm.BM_TYPE.ToLower() == "installation") || (bm.BM_TYPE.ToLower() == "removal"))
                                                                   && bm.DataStatus == 0 && bm.SiteId == siteId
                                                                   && model.BM_MODEL == bm.BM_MODEL && model.BM_SN == bm.BM_SN)
                                               .OrderByDescending(bm => bm.BM_DATE)
                                               .ToListAsync();
            GRP_BM _bmItem = null;
            if (foundBm != null)
            {
                _bmItem = foundBm.FirstOrDefault(item => String.Compare(item.BM_DATE, bmDate) < 0);
            }
             //                                   

            if (_bmItem == null)
            {
                if (bmType == "installation")
                {
                    result = 0;
                }
                else
                {
                    result = (int)Globals.ErrorValidationCodes.BM_SENSOR_NOT_INSTALLED;
                }
            }
            else
            {
                if (bmType == "installation")
                {
                    if (_bmItem.BM_TYPE.ToLower().CompareTo("removal") != 0)
                    {
                        result = (int)Globals.ErrorValidationCodes.BM_SENSOR_NOT_REMOVED;
                    }
                }
                else
                {
                    if (_bmItem.BM_TYPE.ToLower().CompareTo("installation") != 0)
                    {
                        result = (int)Globals.ErrorValidationCodes.BM_SENSOR_NOT_INSTALLED;
                    }
                }
            }
            ///add a check also for the future
            ///for example, if an installation or a removal is sent,
            ///check if there are ops for dates >= passed date
            ///raise error for this
            /////DIEGO::: TO TEST!!!!
            if( new string[] { "installation", "removal" }.Contains(bmType.ToLower()))
            {
                var noFuture = await _context.GRP_BM.Where(bm =>  bm.DataStatus == 0 && bm.SiteId == siteId
                                                && model.BM_MODEL == bm.BM_MODEL && model.BM_SN == bm.BM_SN &&
                                                ( String.Compare(bm.BM_DATE, bmDate)>0 || String.Compare(bm.BM_DATE_START, bmDate) > 0))
                                               .OrderBy(bm => bm.BM_DATE)
                                               .FirstOrDefaultAsync();
                if (noFuture != null)
                {
                    result = (int)Globals.ErrorValidationCodes.BM_SENSOR_FOLLOWING_OPERATION;
                }
            }
            return result;
        }

        public int ValidateByBmType(GRP_BM model, int siteId)
        {
            int result = 0;
            string bmType = model.BM_TYPE.ToLower();
            string bmVhr = model.BM_VARIABLE_H_V_R;
            if (!String.IsNullOrEmpty(bmVhr))
            {
                result = VarHVRCorrect(bmVhr);
                if (result > 0) return result;
            }
            if (model.BM_LOGGER != null && model.BM_FILE != null && model.BM_SAMPLING_INT != null)
            {
                result = LoggerFileCheck(model, siteId);
            }

            switch (bmType)
            {
                case "installation":
                    /*
                     * mandatory:
                     * BM_HEIGHT
                        BM_EASTWARD_DIST
                        BM_NORTHWARD_DIST
                    non mandatory but bound together:
                        BM_VARIABLE_H_V_R
                        BM_SAMPLING_INT
                        BM_LOGGER
                        BM_FILE
                     * */
                    if (model.BM_HEIGHT == null || model.BM_EASTWARD_DIST == null|| model.BM_NORTHWARD_DIST == null)
                    {
                        return (int)Globals.ErrorValidationCodes.BM_INSTALLATION_HEIGHT_EAST_NORTH_MANDATORY;
                    }
                    if (!ValidationUtils.CountBoundedProps<GRP_BM>(model, 4, "BM_VARIABLE_H_V_R", "BM_SAMPLING_INT", "BM_LOGGER", "BM_FILE"))
                    {
                        return (int)Globals.ErrorValidationCodes.BM_INSTALLATION_VAR_SAMPLING_LOGGER_FILE;
                    }
                    break;
                case "maintenance":
                    break;
                case "variable map":
                    /*
                     * BM_VARIABLE_H_V_R
                        BM_SAMPLING_INT
                        BM_LOGGER
                        BM_FILE
                    */
                    if (String.IsNullOrEmpty(bmVhr)||model.BM_SAMPLING_INT==null ||model.BM_FILE==null||model.BM_LOGGER==null)
                    {
                        return (int)Globals.ErrorValidationCodes.BM_VARMAP_SAMPLING_FILE_LOGGER_MANDATORY;
                    }
                    break;
                case "disturbance":
                case "field calibration":
                case "field calibration check":
                case "field cleaning":
                case "firmware update":
                case "parts substitution":
                    //check BM_VARIABLE_H_V_R validity; performed elsewhere
                    break;
                case "connection failure":
                case "general comment":
                    break;
            }
            
            return result;
        }

        //check if the passed BM_FILE and BM_LOGGER exist in the GRP_FILE table with BM FILE_TYPE
        public int LoggerFileCheck(GRP_BM model, int siteId)
        {
            string bmDateToCompare = String.IsNullOrEmpty(model.BM_DATE) ? model.BM_DATE_START : model.BM_DATE;
            var exist = _context.GRP_FILE.Where(file => file.FILE_ID == model.BM_FILE &&
                                                      file.FILE_LOGGER_ID == model.BM_LOGGER &&
                                                      file.SiteId == siteId && file.DataStatus == 0 &&
                                                      file.FILE_DATE.CompareTo(bmDateToCompare) <=0 &&
                                                      file.FILE_TYPE == "BM").Any();
            if (!exist)
            {
                return (int)Globals.ErrorValidationCodes.BM_LOGGER_FILE_MISSING_IN_GRP_FILE;
            }
            return 0;
        }

        //check if the passed BM_SAMPLING_INT is consistent with BM_SAMPLING_INT, BM_FILE and BM_LOGGER existing in the GRP_BM table for a given site
        //it is not possible to have two different sampling int mapped on a same file_id!!!
        public int LoggerFileSamplingIntCheck(GRP_BM model, int siteId)
        {
            string bmDateToCompare = String.IsNullOrEmpty(model.BM_DATE) ? model.BM_DATE_START : model.BM_DATE;
            var lastRecord = _context.GRP_BM.Where(bm => bm.BM_FILE == model.BM_FILE &&
                                                             //DIEGO:: FILE_ID must be different for different sampling int, but LOGGER_ID??? bm.BM_LOGGER == model.BM_LOGGER &&
                                                             bm.BM_DATE.CompareTo(bmDateToCompare) <= 0 &&
                                                             (bm.BM_TYPE.ToLower()=="installation" || bm.BM_TYPE.ToLower() == "variable map") &&
                                                             bm.SiteId == siteId && bm.DataStatus == 0).
                                                             OrderByDescending(bm=>bm.BM_DATE).
                                                             FirstOrDefault();
            if (lastRecord != null) 
            { 
                if(lastRecord.BM_SAMPLING_INT != model.BM_SAMPLING_INT)
                {
                    return (int)Globals.ErrorValidationCodes.BM_DIFFERENT_SAMPLING_INT;
                }
            }
            return 0;
        }

        //check if passed var_H_V_R is already mapped on other sensors
        //only if previous sensors are removed the situation is ok
        public  int VarMapped(GRP_BM model, int siteId)
        {
            //DIEGO:: expression to remember...
            var bb= (new string[] { "a", "b", "c" }).Contains(model.BM_VARIABLE_H_V_R);
            string bmDateToCompare = (String.IsNullOrEmpty(model.BM_DATE)) ? model.BM_DATE_START : model.BM_DATE;
            var _mappedInst = _context.GRP_BM.Where(bm => bm.SiteId == siteId && bm.DataStatus == 0 &&
                                                        bm.BM_VARIABLE_H_V_R.ToLower() == model.BM_VARIABLE_H_V_R.ToLower() &&
                                                        (bm.BM_MODEL.ToLower().CompareTo(model.BM_MODEL.ToLower()) != 0 ||
                                                        bm.BM_SN.ToLower().CompareTo(model.BM_SN.ToLower()) != 0)).ToList();
            if(_mappedInst==null || _mappedInst.Count == 0)
            {
                return 0;
            }
            foreach(var record in _mappedInst)
            {
                var lastOp = _context.GRP_BM.Where(bm => bm.SiteId == siteId && bm.DataStatus == 0 &&
                                                        bm.BM_MODEL == record.BM_MODEL && bm.BM_SN == record.BM_SN &&
                                                        bm.BM_DATE.CompareTo(bmDateToCompare) <= 0 &&
                                                        (new string[] { "installation", "removal", "variable map" }).Contains(bm.BM_TYPE.ToLower())
                                                        //(bm.BM_TYPE.ToLower()=="installation" || bm.BM_TYPE.ToLower() == "removal" || bm.BM_TYPE.ToLower() == "variable map")
                                                        )
                                            .OrderByDescending(bm => bm.BM_DATE).FirstOrDefault();
                if (lastOp != null)
                {
                    if (lastOp.BM_TYPE.ToLower() != "removal")
                    {
                        if (!String.IsNullOrEmpty(lastOp.BM_VARIABLE_H_V_R))
                        {
                            if (lastOp.BM_VARIABLE_H_V_R.CompareTo(model.BM_VARIABLE_H_V_R) == 0)
                            {
                                return (int)Globals.ErrorValidationCodes.BM_VAR_MAPPED_DIFFERENT_INST;
                            }
                        }
                    }
                }
            }
            return 0;
        }
    }
}
