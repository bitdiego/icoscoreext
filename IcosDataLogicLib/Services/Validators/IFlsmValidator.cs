﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface IFlsmValidator
    {
        Task<int> ItemInSamplingPointGroupAsync(GRP_FLSM model, int siteId);
        int ValidateFlsm(GRP_FLSM model, int siteId);
    }
}
