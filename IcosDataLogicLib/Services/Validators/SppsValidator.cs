﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public class SppsValidator : ISppsValidator
    {
        private readonly IcosDbContext _context;

        public SppsValidator(IcosDbContext context)
        {
            _context = context;
        }
        public async Task<int> SppsPlotExistsAsync(string plotSpp, int siteId)
        {
            var item = await _context.GRP_PLOT.Where(plot => plot.SiteId == siteId && plot.DataStatus == 0 &&
                                                String.Compare(plot.PLOT_ID, plotSpp) == 0).FirstOrDefaultAsync();
            if (item == null)
            {
                return (int)Globals.ErrorValidationCodes.SPPS_PLOT_NOT_FOUND;
            }
            return 0;
        }
        public int ValidateSpps(GRP_SPPS model, int siteId)
        {
            bool isBound = false;
            if (Globals.IsValidCoordinateSystem<decimal?>(model.SPPS_LOCATION_LAT, model.SPPS_LOCATION_LON, model.SPPS_LOCATION_DIST, model.SPPS_LOCATION_ANG) > 0)
            {
                return (int)Globals.ErrorValidationCodes.INVALID_COORDINATE_SYSTEM;
            }
            if(model.SPPS_LOCATION_LAT!=null && model.SPPS_LOCATION_LON != null)
            {
                isBound = ValidationUtils.CountBoundedProps<GRP_SPPS>(model, 3, "SPPS_LOCATION_LAT", "SPPS_LOCATION_LON", "SPPS_LOCATION");
            }
            else
            {
                isBound = ValidationUtils.CountBoundedProps<GRP_SPPS>(model, 3, "SPPS_LOCATION_DIST", "SPPS_LOCATION_ANG", "SPPS_LOCATION");
            }
            if (!isBound)
            {
                return (int)Globals.ErrorValidationCodes.SPPS_LOCATION_BOUND;
            }
            if (!String.IsNullOrEmpty(model.SPPS_PTYPE))
            {
                if (!model.SPPS_PLOT.ToLower().StartsWith("cp"))
                {
                    return (int)Globals.ErrorValidationCodes.SPPS_PTYPE_CP_ALLOWED;
                }
            }


            //Only for mires!!! wtf are they??????????
            if (model.SPPS_PERC_COVER != null)
            {

            }

            if (!String.IsNullOrEmpty(model.SPPS_TWSP_PCT))
            {

            }

            return 0;
        }
    }
}
