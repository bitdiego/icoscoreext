﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface ILoggerValidator
    {
        Task<int> IsLoggerIdAsync(int loggerId, int siteId);
        int CheckFileFormat(string fileFormat, string fileExt, int siteId);
        int CheckHeadConstraints(string fileFormat, int? fileHeadNum, int? fileHeadVars, int? fileHeadType);

    }
}
