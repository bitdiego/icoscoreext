﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public interface IGaiValidator
    {
        int GaiPlotInDb(string gaiPlot, int siteId);
        int NumericFieldValidator(GRP_GAI model, int siteId);
        int PlotStringValidation(string gaiplot);
        int ValidateGaiByMethod(GRP_GAI model, int siteId, string ecosystem);
    }
}
