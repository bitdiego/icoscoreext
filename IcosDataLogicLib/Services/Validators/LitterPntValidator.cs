﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public class LitterPntValidator : ILitterPntValidator
    {
        private readonly IcosDbContext _context;
        private readonly string Ecosystem = "forest";
        public LitterPntValidator(IcosDbContext context)
        {
            _context = context;
        }

        public async Task<int> ItemInSamplingPointGroupAsync(GRP_LITTERPNT model, int siteId)
        {
            var item = await _context.GRP_PLOT.Where(plot => plot.SiteId == siteId && plot.DataStatus == 0 &&
                                                String.Compare(plot.PLOT_ID, model.LITTERPNT_PLOT) == 0 &&
                                                String.Compare(plot.PLOT_DATE, model.LITTERPNT_DATE) <= 0).FirstOrDefaultAsync();
            if (item == null)
            {
                return (int)Globals.ErrorValidationCodes.LITTERPNT_PLOT_NOT_FOUND;
            }
            return 0;
        }
        public int ValidateLitterPnt(GRP_LITTERPNT model, int siteId)
        {
            string litterType = model.LITTERPNT_TYPE.ToLower();
            switch (Ecosystem.ToLower())
            {
                case "forest":
                    if (String.Compare(litterType, "coarse") == 0)
                    {
                        if (!ValidationUtils.FindMandatoryNull<GRP_LITTERPNT>(model, "LITTERPNT_COARSE_DIAM", "LITTERPNT_ID", "LITTERPNT_COARSE_LENGTH", "LITTERPNT_COARSE_ANGLE", "LITTERPNT_COARSE_DECAY"))
                        {
                            return (int)Globals.ErrorValidationCodes.LITTERPNT_FOREST_COARSE_MANDATORY;
                        }
                    }
                    else if (String.Compare(litterType, "fine-woody") == 0)
                    {
                        if (!ValidationUtils.FindMandatoryNull<GRP_LITTERPNT>(model, "LITTERPNT", "LITTERPNT_AREA"))
                        {
                            return (int)Globals.ErrorValidationCodes.LITTERPNT_FOREST_FINE_WOODY_MANDATORY;
                        }
                    }
                    else if (String.Compare(litterType, "non-woody") == 0)
                    {
                        if (!ValidationUtils.FindMandatoryNull<GRP_LITTERPNT>(model, "LITTERPNT", "LITTERPNT_AREA", "LITTERPNT_ID", "LITTERPNT_FRACTION"))
                        {
                            return (int)Globals.ErrorValidationCodes.LITTERPNT_FOREST_NON_WOODY_MANDATORY;
                        }
                        if (model.LITTERPNT_FRACTION.ToLower() == "leaves")
                        {
                            if (String.IsNullOrEmpty(model.LITTERPNT_SPP))
                            {
                                return (int)Globals.ErrorValidationCodes.LITTERPNT_FOREST_SPP_MANDATORY;
                            }
                        }
                        if(ValidationUtils.IsAnyPropNotNull<GRP_LITTERPNT>(model, "LITTERPNT_EASTWARD_DIST", "LITTERPNT_NORTHWARD_DIST", "LITTERPNT_DISTANCE_POLAR", "LITTERPNT_ANGLE_POLAR"))
                        {
                            int coo = Globals.IsValidCoordinateSystem(model.LITTERPNT_EASTWARD_DIST, model.LITTERPNT_NORTHWARD_DIST,
                                                                model.LITTERPNT_DISTANCE_POLAR, model.LITTERPNT_ANGLE_POLAR);
                            if (coo!=0)
                            {
                                return coo;
                            }
                        }
                    }
                    break;
                case "cropland":
                    if(String.Compare(litterType, "natural") == 0)
                    {
                        if(!ValidationUtils.FindMandatoryNull<GRP_LITTERPNT>(model, "LITTERPNT", "LITTERPNT_ID", "LITTERPNT_AREA", "LITTERPNT_FRACTION"))
                        {
                            return (int)Globals.ErrorValidationCodes.LITTERPNT_CROP_NATURAL_MANDATORY;
                        } 
                    }
                    else if (String.Compare(litterType, "residual") == 0)
                    {
                        if (!ValidationUtils.FindMandatoryNull<GRP_LITTERPNT>(model, "LITTERPNT", "LITTERPNT_AREA", "LITTERPNT_FRACTION"))
                        {
                            return (int)Globals.ErrorValidationCodes.LITTERPNT_CROP_RESIDUAL_MANDATORY;
                        }
                    }
                    else
                    {
                        return (int)Globals.ErrorValidationCodes.LITTERPNT_LITTERTYPE_NOT_ALLOWED;
                    }
                    break;
                case "grassland":
                    if(litterType!="residual" && litterType != "natural")
                    {
                        return (int)Globals.ErrorValidationCodes.LITTERPNT_LITTERTYPE_NOT_ALLOWED;
                    }
                    if(model.LITTERPNT == null || model.LITTERPNT_AREA == null)
                    {
                        return (int)Globals.ErrorValidationCodes.LITTERPNT_AREA_GRASSLAND_MANDATORY;
                    }
                    
                    break;
            }
            return 0;
        }
    }
}
