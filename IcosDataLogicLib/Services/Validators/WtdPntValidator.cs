﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public class WtdPntValidator : IWtdPntValidator
    {
        private readonly IcosDbContext _context;

        public WtdPntValidator(IcosDbContext context)
        {
            _context = context;
        }

        public async Task<int> ItemInSamplingPointGroupAsync(GRP_WTDPNT model, int siteId)
        {
            var item = await _context.GRP_PLOT.Where(plot => plot.SiteId == siteId && plot.DataStatus == 0 &&
                                                String.Compare(plot.PLOT_ID, model.WTDPNT_PLOT) == 0 &&
                                                String.Compare(plot.PLOT_DATE, model.WTDPNT_DATE) <= 0).FirstOrDefaultAsync();
            if (item == null)
            {
                return (int)Globals.ErrorValidationCodes.WTDPNT_PLOT_NOT_FOUND;
            }
            return 0;
        }
        public int ValidateWtdPnt(GRP_WTDPNT model, int siteId)
        {
            if( Globals.IsValidCoordinateSystem(model.WTDPNT_EASTWARD_DIST, model.WTDPNT_NORTHWARD_DIST,
                                                model.WTDPNT_DISTANCE_POLAR, model.WTDPNT_ANGLE_POLAR) > 0)
            {
                return (int)Globals.ErrorValidationCodes.INVALID_COORDINATE_SYSTEM;
            }
            if(!ValidationUtils.XORNull<string>(model.WTDPNT_PLOT, model.WTDPNT_VARMAP))
            {
                return (int)Globals.ErrorValidationCodes.WTDPNT_PLOT_VARMAP;
            }
            
            return 0;
        }
    }
}
