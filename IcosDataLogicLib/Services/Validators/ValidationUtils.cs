﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using IcosModelLib.Utils;
using Microsoft.EntityFrameworkCore;

namespace IcosDataLogicLib.Services.Validators
{
    public class ValidationUtils
    {
        /// <summary>
        /// Given a model in input, it returns if there are some properties not null
        /// in addiction to the only one mandatory. This is for DM group, when there are
        /// no mandatory variables except DM_DATE
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool IsAnyPropNotNull<T>(T model)
        {
            Type myType = model.GetType();
            IList<PropertyInfo> props = new List<System.Reflection.PropertyInfo>(myType.GetProperties());

            var subList = props.Where(item => !item.Name.Contains("_DATE") &&
                                               !item.Name.Contains("COMMENT") &&
                                               item.Name != "Id" &&
                                               item.Name != "DataStatus" &&
                                               !item.Name.Contains("UserId") &&
                                               !item.Name.Contains("Date") &&
                                               !item.Name.Contains("SiteId") &&
                                               !item.Name.Contains("GroupId") &&
                                               !item.Name.Contains("DataOrigin")).ToList();

            var isAnyVAlue = subList.Any(item => item.GetValue(model, null) != null);

            return isAnyVAlue;
        }

        public static bool IsAnyPropNotNull<T>(T model, params string[] vars)
        {
            Type myType = model.GetType();
            IList<PropertyInfo> props = new List<System.Reflection.PropertyInfo>(myType.GetProperties());
            var subList = props.Where(item => vars.Contains(item.Name)).ToList();
            var isAnyVAlue = subList.Any(item => item.GetValue(model, null) != null);

            return isAnyVAlue;
        }

        public static bool FindMandatoryNull<T>(T model, params string[] vars)
        {
            Type myType = model.GetType();
            IList<PropertyInfo> props = new List<System.Reflection.PropertyInfo>(myType.GetProperties());

            var subList = props.Where(item => vars.Contains(item.Name)).ToList();

            var isAnyVAlue = subList.Any(item => item.GetValue(model, null) == null);

            return isAnyVAlue;
        }

        /// <summary>
        /// Given an input model, counts the number of bounded properties
        /// The result must be 0 (all null) or bound
        /// This is for ex when installing a BM sensor:
        /// BM_VARIABLE_H_V_R,BM_SAMPLING_INT,BM_LOGGER,BM_FILE are to submitted alltogether or none of them
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <param name="bound"></param>
        /// <param name="vars"></param>
        /// <returns></returns>
        public static bool CountBoundedProps<T>(T model, int bound, params string[] vars)
        {
            Type myType = model.GetType();
            IList<PropertyInfo> props = new List<System.Reflection.PropertyInfo>(myType.GetProperties());

            var subList = props.Where(item => vars.Contains(item.Name)).ToList();

            var countValue = subList.Count(item => item.GetValue(model, null) != null);

            return (countValue == 0) || (countValue == bound);
        }

        public static bool XOR<T>(T obja, T objb) where T : IComparable<T>
        {
            return (obja.CompareTo(objb) != 0);
        }

        public static bool XORNull<T>(string obja, string objb) where T : IComparable<T>
        {
            if(String.IsNullOrEmpty(obja) && !String.IsNullOrEmpty(objb))
            {
                return true;
            }
            if (!String.IsNullOrEmpty(obja) && String.IsNullOrEmpty(objb))
            {
                return true;
            }
            //if ((obja == null && objb != null) || (obja != null && objb == null)) return true;
            return false;
        }
        public static DateTime isoDate2DT(string isoDate)
        {
            int year = int.Parse(isoDate.Substring(0, 4));
            int month = 0;
            int day = 0;
            int hh = 0;
            int mm = 0;
            int sec = 0;
            if (isoDate.Length == 4)
            {
                return (new DateTime(year));
            }
            if (isoDate.Length > 4)
            {
                month = int.Parse(isoDate.Substring(4, 2));
            }
            if (isoDate.Length > 6)
            {
                day = int.Parse(isoDate.Substring(6, 2));
            }
            else
            {
                day = 1;
            }
            if (isoDate.Length > 8)
            {
                hh = int.Parse(isoDate.Substring(8, 2));
            }
            if (isoDate.Length > 10)
            {
                mm = int.Parse(isoDate.Substring(10, 2));
            }
            if (isoDate.Length > 12)
            {
                sec = int.Parse(isoDate.Substring(12, 2));
            }
            return (new DateTime(year, month, day, hh, mm, sec));
        }

        public static string DateTimeToIsoDate(DateTime dt)
        {
           
            string isodate = dt.ToString("yyyyMMddHHmmss");
            int index = isodate.LastIndexOf("00");
            int start = "yyyyMMdd".Length - 1;
            while (index > start)
            {
                isodate = isodate.TrimEnd(new char[] { '0', '0' });
                index = isodate.LastIndexOf("00");
            }

            return isodate;
        }

        public static int CheckDates(string date, string dateStart, string dateEnd)
        {
            if (String.IsNullOrEmpty(date) && String.IsNullOrEmpty(dateStart) && String.IsNullOrEmpty(dateEnd)) return (int)Globals.ErrorValidationCodes.DATES_CONSTRAINTS_ERROR;
            if (String.IsNullOrEmpty(date) && (String.IsNullOrEmpty(dateStart) || String.IsNullOrEmpty(dateEnd))) return (int)Globals.ErrorValidationCodes.DATES_CONSTRAINTS_ERROR;
            if (!String.IsNullOrEmpty(date) && !String.IsNullOrEmpty(dateStart) && !String.IsNullOrEmpty(dateEnd)) return (int)Globals.ErrorValidationCodes.DATES_CONSTRAINTS_ERROR;
            if (!String.IsNullOrEmpty(date) && (!String.IsNullOrEmpty(dateStart) || !String.IsNullOrEmpty(dateEnd))) return (int)Globals.ErrorValidationCodes.DATES_CONSTRAINTS_ERROR;
            if(String.Compare(dateStart, dateEnd) > 0)
            {
                return (int)Globals.ErrorValidationCodes.DATE_START_DATE_END_ERROR;
            }
            return 0;
        }

        public static bool IsValidPattern(string pattern, string regex)
        {
            Match match = Regex.Match(pattern, regex);
            return match.Success;
        }
    }
}
