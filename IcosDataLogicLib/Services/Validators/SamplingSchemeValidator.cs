﻿using IcosModelLib.DTO;
using IcosModelLib.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services.Validators
{
    public class SamplingSchemeValidator : ISamplingSchemeValidator
    {
        public int SamplingSchemeValidate(GRP_PLOT model)
        {
            if (String.IsNullOrEmpty(model.PLOT_ID))
            {
                //error
                return (int)Globals.ErrorValidationCodes.MISSING_REQUIRED_FIELD;
            }
            else
            {
                //verify PLOT_ID correct format
                if (!Globals.IsValidPlotId(model.PLOT_ID, model.GroupId))
                {
                    return (int)Globals.ErrorValidationCodes.PLOT_ID_INVALID_FORMAT;
                }
            }
            //PLOT_ID must match with plotType value: PLOT_TYPE is mandatory
            if (String.IsNullOrEmpty(model.PLOT_TYPE))
            {
                //error
                return (int)Globals.ErrorValidationCodes.MISSING_REQUIRED_FIELD;
            }
            else
            {
                string _subPlot = model.PLOT_ID.Substring(0, model.PLOT_ID.IndexOf('_')); //plot.Value.Substring(0, plotType.Value.Length);
                if (String.Compare(_subPlot, model.PLOT_TYPE, true) != 0)
                {
                    return (int)Globals.ErrorValidationCodes.PLOT_ID_PLOT_TYPE_MISMATCH;
                }
            }
            //validate coordinate system:  PLOT_EASTWARD_DIST/PLOT_NORTHWARD_DIST or PLOT_ANGLE_POLAR/PLOT_DISTANCE_POLAR or PLOT_LOCATION_LAT/PLOT_LOCATION_LONG
            //must be mutually exclusive
            int xc = Globals.IsValidCoordinateSystem<decimal?>(model.PLOT_EASTWARD_DIST, model.PLOT_NORTHWARD_DIST,
                                                               model.PLOT_DISTANCE_POLAR, model.PLOT_ANGLE_POLAR,
                                                               model.PLOT_LOCATION_LAT, model.PLOT_LOCATION_LONG);
            if (xc > 0)
            {
                return (int)Globals.ErrorValidationCodes.INVALID_COORDINATE_SYSTEM;
            }
            if (!String.IsNullOrEmpty(model.PLOT_REFERENCE_POINT))
            {
                if (!model.PLOT_ID.ToLower().StartsWith("sp-ii"))
                {
                    return (int)Globals.ErrorValidationCodes.PLOT_REF_SP_II_ALLOWED;
                }
                if(model.PLOT_LOCATION_LAT!=null && model.PLOT_LOCATION_LONG != null)
                {
                    return (int)Globals.ErrorValidationCodes.PLOT_REF_POINT_ERROR; 
                }
            }
            else
            {
                if (model.PLOT_ID.ToLower().StartsWith("sp-ii"))
                {
                    if ((model.PLOT_ANGLE_POLAR != null && model.PLOT_DISTANCE_POLAR != null) ||
                    (model.PLOT_EASTWARD_DIST != null && model.PLOT_NORTHWARD_DIST != null))
                    {
                        return (int)Globals.ErrorValidationCodes.PLOT_REF_SP_II_MISSING;
                    }
                }   
            }
            return 0;
        }
    }
}
