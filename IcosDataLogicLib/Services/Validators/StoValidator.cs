﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using IcosModelLib.Utils;

namespace IcosDataLogicLib.Services.Validators
{
    public class StoValidator : IStoValidator<GRP_STO>
    {
        private readonly IcosDbContext _context;
        private List<StoOps> UiOperations;
        public StoValidator(IcosDbContext ctx)
        {
            _context = ctx;
            UiOperations = new List<StoOps>();
        }
        /// <summary>
        /// Returns the available operations filtered
        /// by sto type selection, if they are mandatory
        /// and their constraints, if any
        /// </summary>
        /// <param name="stoType"></param>
        

        public async Task<int> ValidateStoByOpAsync(GRP_STO model, int siteId)
        {
            string stoType = (!String.IsNullOrEmpty( model.STO_TYPE))? model.STO_TYPE.ToLower():null;
            string stoConfig = (!String.IsNullOrEmpty(model.STO_CONFIG)) ? model.STO_CONFIG.ToLower() : null;
            string gaModel = (!String.IsNullOrEmpty(model.STO_GA_MODEL)) ? model.STO_GA_MODEL.ToLower() : null; 
            await GetAvailableOpsByTypeAsync(model.STO_TYPE);

            if (!(new string[] { "ga installation", "ga removal", "level installation", "level removal", "variable map" }).Contains(stoType))
            {
                int xc = ValidationUtils.CheckDates(model.STO_DATE, model.STO_DATE_START, model.STO_DATE_END);
                if (xc > 0)
                {
                    return xc;
                }
            }

            switch (stoType)
            {
                case "ga installation":
                    if (model.STO_GA_SAMPLING_INT == null)
                    {
                        model.STO_GA_SAMPLING_INT = 1;
                    }
                    var subUiOps = UiOperations.Where(ui => !ui.Variable.Contains("TUBE") && !ui.Variable.Contains("MOUNT") && !ui.Variable.Contains("FLOW_RATE"));
                    var subTube = UiOperations.Where(ui => ui.Variable.Contains("TUBE"));
                    var subMount = UiOperations.Where(ui => ui.Variable.Contains("MOUNT"));
                    if (ValidationUtils.FindMandatoryNull<GRP_STO>(model, subUiOps.Select(x => x.Variable).ToArray()))
                    {
                        return (int)Globals.ErrorValidationCodes.STO_GA_INSTALLATION_MANDATORY;
                    }
                    //STO_GA_TUBE* mandatory for closed path sensors
                    if (gaModel.StartsWith("ga_cp")|| gaModel.StartsWith("ga_sr") || gaModel.StartsWith("ga-ot"))
                    {
                        if (ValidationUtils.FindMandatoryNull<GRP_STO>(model, subTube.Select(x => x.Variable).ToArray()))
                        {
                            return (int)Globals.ErrorValidationCodes.STO_GA_INSTALLATION_CP_TUBE_MANDATORY;
                        }
                        if(model.STO_GA_VERT_MOUNT!=null || model.STO_GA_AZIM_MOUNT != null)
                        {
                            return (int)Globals.ErrorValidationCodes.STO_GA_MOUNT_CP_NOT_ALLOWED;
                        }
                    }
                    else if (gaModel.StartsWith("ga_op"))
                    {
                        if(ValidationUtils.IsAnyPropNotNull<GRP_STO>(model, subTube.Select(x => x.Variable).ToArray()))
                        {
                            return (int)Globals.ErrorValidationCodes.STO_GA_INSTALLATION_OP_TUBE_NOT_ALLOWED;
                        }
                        if (model.STO_GA_FLOW_RATE != null)
                        {
                            return (int)Globals.ErrorValidationCodes.STO_GA_FLOW_RATE_OP_NOT_ALLOWED;
                        }
                        if (stoConfig.CompareTo("separate") == 0)
                        {
                            if (model.STO_GA_VERT_MOUNT == null || model.STO_GA_AZIM_MOUNT == null)
                            {
                                return (int)Globals.ErrorValidationCodes.STO_GA_MOUNT_OP_ERROR;
                            }
                        }
                        else
                        {
                            if (model.STO_GA_VERT_MOUNT != null || model.STO_GA_AZIM_MOUNT != null)
                            {
                                return (int)Globals.ErrorValidationCodes.STO_GA_MOUNT_OP_NOT_SEPARATE_ERROR;
                            }
                        }
                        
                    }
                    ////
                    ///
                    /*
                     * The same STO_GA_PROF_ID can be used for different GA in case of STO_CONFIG == 'sequential', 
                     * but it must be unique in case of STO_CONFIG == 'separate'
                     * 
                     */
                    var stoca = _context.GRP_STO.Where(sto => sto.DataStatus == 0 && sto.SiteId == siteId
                                                              && String.Compare(sto.STO_DATE, model.STO_DATE) < 0 &&
                                                              sto.STO_PROF_ID == model.STO_GA_PROF_ID &&
                                                              (stoType == "level installation" || stoType == "level removal" || stoType == "ga installation"))
                                                        .OrderByDescending(sto => sto.STO_DATE);
                    GRP_STO lastGaProfId = await stoca.FirstOrDefaultAsync();


                    if (lastGaProfId == null)
                    {
                        return (int)Globals.ErrorValidationCodes.STO_GA_PROF_ID_NOT_FOUND_OR_REMOVED;
                    }
                    if(string.Compare(lastGaProfId.STO_TYPE.ToLower(), "level removal") == 0)
                    {
                        return (int)Globals.ErrorValidationCodes.STO_GA_PROF_ID_NOT_FOUND_OR_REMOVED;
                    }
                    
                    foreach (var sto in stoca.ToList())
                    {
                        if((sto.STO_GA_MODEL != model.STO_GA_MODEL || sto.STO_GA_SN != model.STO_GA_SN) && 
                            (sto.STO_CONFIG.ToLower() != "sequential" || model.STO_CONFIG.ToLower() != "separate"))
                        {
                            return (int)Globals.ErrorValidationCodes.STO_GA_PROF_ID_NOT_SEQUENTIAL_ERROR;
                        }
                    }

                    break;
                case "ga removal":
                    //first check if ga has been installed before...
                    if (String.IsNullOrEmpty(model.STO_DATE) || String.IsNullOrEmpty(model.STO_GA_MODEL)|| String.IsNullOrEmpty(model.STO_GA_SN))
                    {
                        return (int)Globals.ErrorValidationCodes.STO_GA_REMOVAL_MANDATORY_ERROR;
                    }

                    if (! await IsGaInstalledAsync(model.STO_GA_MODEL, model.STO_GA_SN, model.STO_DATE, siteId))
                    {
                        return (int)Globals.ErrorValidationCodes.STO_GA_REMOVAL_GA_NOT_FOUND;
                    }
                    break;
                case "level installation":
                    if(ValidationUtils.FindMandatoryNull<GRP_STO>(model, UiOperations.Select(x => x.Variable).ToArray()))
                    {
                        return (int)Globals.ErrorValidationCodes.STO_LEVEL_INSTALLATION_MANDATORY;
                    }
                    if (model.STO_PROF_HORIZ_S_POINTS == null)
                    {
                        model.STO_PROF_HORIZ_S_POINTS = 1;
                    }
                    if (model.STO_PROF_SAMPLING_TIME != null)
                    {
                        if(String.Compare(model.STO_CONFIG.ToLower(), "sequential") != 0)
                        {
                            return (int)Globals.ErrorValidationCodes.STO_SEQ_SAMPLING_INT;
                        }
                    }
                    //check if, for a given prof id, the same prof level id has been submitted
                    var item = await _context.GRP_STO.Where(sto => sto.DataStatus == 0 && sto.SiteId == siteId
                                                              && String.Compare(sto.STO_DATE, model.STO_DATE) < 0 &&
                                                              sto.STO_PROF_ID == model.STO_PROF_ID &&
                                                              (stoType == "level installation" || stoType == "level removal"))
                                                        .OrderByDescending(sto => sto.STO_DATE).FirstOrDefaultAsync();
                    if (item != null)
                    {
                        if (item.STO_TYPE == "level removal")
                        {

                        }
                        else
                        {
                            if (item.STO_PROF_LEVEL == model.STO_PROF_LEVEL)
                            {
                                return (int)Globals.ErrorValidationCodes.STO_PROF_LEVEL_ALREADY_SUBMITTED;
                            }
                        }
                    }
                    break;
                case "level removal":
                    //first check if level has been installed before...
                    if(String.IsNullOrEmpty(model.STO_DATE) || model.STO_PROF_ID == null)
                    {
                        return (int)Globals.ErrorValidationCodes.STO_LEVEL_REMOVAL_MANDATORY_ERROR;
                    }
                    var isProfId = await _context.GRP_STO.Where(sto => sto.DataStatus == 0 && sto.SiteId == siteId
                                                              && String.Compare(sto.STO_DATE, model.STO_DATE) < 0 &&
                                                              sto.STO_PROF_ID == model.STO_PROF_ID && sto.STO_TYPE.ToLower() == "level installation")
                                                    .OrderByDescending(sto => sto.STO_DATE).FirstOrDefaultAsync();
                    if (isProfId == null)
                    {
                        return (int)Globals.ErrorValidationCodes.STO_LEVEL_REMOVAL_PROF_ID_NOT_FOUND;
                    }
                    break;
                case "variable map":

                    var subUiVarMap = UiOperations.Where(ui => ui.Variable.Contains("STO_GA_VARIABLE") || ui.Variable.Contains("STO_LOGGER") ||
                                                        ui.Variable.Contains("STO_FILE") || ui.Variable.Contains("STO_GA_MODEL") || ui.Variable.Contains("STO_GA_SN")
                                                        || ui.Variable.Contains("STO_DATE"));
                    if (ValidationUtils.FindMandatoryNull<GRP_STO>(model, subUiVarMap.Select(x => x.Variable).ToArray()))
                    {
                        return (int)Globals.ErrorValidationCodes.STO_VARIABLE_MAP_MANDATORY;
                    }
                    if (model.STO_GA_SAMPLING_INT == null)
                    {
                        model.STO_GA_SAMPLING_INT = 1;
                    }
                    //check if the logger  / file couple exists for STO in File table
                    //check if GA is still installed
                    if (!await IsGaInstalledAsync(model.STO_GA_MODEL, model.STO_GA_SN, model.STO_DATE, siteId))
                    {
                        return (int)Globals.ErrorValidationCodes.STO_GA_REMOVAL_GA_NOT_FOUND;
                    }
                    //check if GA_SAMPLING_INT has changed: if so, must be changed also logger AND file?
                    break;
                case "maintenance":
                    int GaProfCombo = GaProfNotAllowed(model);
                    if (GaProfCombo == 3)
                    {
                        return (int)Globals.ErrorValidationCodes.STO_GA_PROF_NOT_ALLOWED;
                    }
                    else if (GaProfCombo == 2)
                    {
                        if (model.STO_PROF_SAMPLING_TIME == null)
                        {
                            //return (int)Globals.ErrorValidationCodes.STO_MAINTENANCE_PROF_SAMPLING_INT;
                        }
                        else
                        {
                            if (stoConfig != "sequential")
                            {
                                return (int)Globals.ErrorValidationCodes.STO_SEQ_SAMPLING_INT;
                            }
                        }
                        if(model.STO_PROF_BUFFER_FLOWRATE!=null  && model.STO_PROF_TUBE_THERM != null)
                        {
                            if(stoConfig== "simultaneous")
                            {
                                if (model.STO_PROF_BUFFER_FLOWRATE != 0)
                                {
                                    return (int)Globals.ErrorValidationCodes.STO_MAINTENANCE_BUFFER_FR_NON_ZERO;
                                }
                            }
                            if (model.STO_PROF_LEVEL == null)
                            {
                                return (int)Globals.ErrorValidationCodes.STO_MAINTENANCE_PROF_LEVEL_NULL;
                            }
                        }
                        
                    }
                    else if (GaProfCombo == 1)
                    {
                        if (ValidationUtils.XORNull<String>(model.STO_GA_MODEL, model.STO_GA_SN))
                        {
                            return (int)Globals.ErrorValidationCodes.STO_GA_MODEL_SN_ERROR;
                        }
                        if(model.STO_GA_FLOW_RATE!=null || model.STO_GA_TUBE_THERM != null)
                        {
                            if (model.STO_GA_MODEL.ToLower().StartsWith("ga_op"))
                            {
                                return (int)Globals.ErrorValidationCodes.STO_GA_FLOW_TUBE_ERROR;
                            }
                        }
                    }
                        break;
                case "field calibration":
                case "field calibration check":
                case "firmware update":
                    if (String.IsNullOrEmpty(model.STO_GA_MODEL) || String.IsNullOrEmpty(model.STO_GA_SN))
                    {
                        return (int)Globals.ErrorValidationCodes.STO_FIELD_FIRM_MANDATORY_ERROR;
                        
                    }
                    //check if sensor ga is still installed
                    if (!await IsGaInstalledAsync(model.STO_GA_MODEL, model.STO_GA_SN, String.IsNullOrEmpty(model.STO_DATE) ? model.STO_DATE_START : model.STO_DATE, siteId))
                    {
                        return (int)Globals.ErrorValidationCodes.STO_GA_REMOVAL_GA_NOT_FOUND;
                    }

                    if (!ValidationUtils.CountBoundedProps<GRP_STO>(model, 3, "STO_GA_CAL_VARIABLE", "STO_GA_CAL_VALUE", "STO_GA_CAL_REF"))
                    {
                        return (int)Globals.ErrorValidationCodes.STO_GA_CAL_BOUND;
                    }

                    break;
                case "disturbance":
                case "field cleaning":
                case "parts substitution":
                case "filter change":
                case "tube change":
                    if(!String.IsNullOrEmpty(model.STO_GA_MODEL) && !String.IsNullOrEmpty(model.STO_GA_SN))
                    {
                        if (!await IsGaInstalledAsync(model.STO_GA_MODEL, model.STO_GA_SN, (String.IsNullOrEmpty(model.STO_DATE) ? model.STO_DATE_START : model.STO_DATE), siteId))
                        {
                            return (int)Globals.ErrorValidationCodes.STO_GA_REMOVAL_GA_NOT_FOUND;
                        }
                    }
                    else
                    {
                       if(ValidationUtils.XORNull<String>(model.STO_GA_MODEL, model.STO_GA_SN))
                        {
                            return (int)Globals.ErrorValidationCodes.STO_GA_MODEL_SN_ERROR;
                        }
                    }

                    if (GaProfNotAllowed(model)==3)
                    {
                        return (int)Globals.ErrorValidationCodes.STO_GA_PROF_NOT_ALLOWED;
                    }
                    if((model.STO_PROF_ID==null && model.STO_PROF_LEVEL!=null)||(model.STO_PROF_ID != null && model.STO_PROF_LEVEL == null))
                    {
                        return (int)Globals.ErrorValidationCodes.STO_ALL_PROF_VARS_MANDATORY;
                    }
                    break;
                case "connection failure":
                    if (!String.IsNullOrEmpty(model.STO_GA_MODEL) && !String.IsNullOrEmpty(model.STO_GA_SN))
                    {
                        if (!await IsGaInstalledAsync(model.STO_GA_MODEL, model.STO_GA_SN, (String.IsNullOrEmpty(model.STO_DATE) ? model.STO_DATE_START : model.STO_DATE), siteId))
                        {
                            return (int)Globals.ErrorValidationCodes.STO_GA_REMOVAL_GA_NOT_FOUND;
                        }
                    }
                    else
                    {
                        if (ValidationUtils.XORNull<String>(model.STO_GA_MODEL, model.STO_GA_SN))
                        {
                            return (int)Globals.ErrorValidationCodes.STO_GA_MODEL_SN_ERROR;
                        }
                    }
                    if (!String.IsNullOrEmpty(model.STO_DATE))
                    {
                        return (int)Globals.ErrorValidationCodes.STO_DATE_NOT_ALLOWED;
                    }
                    if (ValidationUtils.XORNull<String>(model.STO_DATE_START, model.STO_DATE_END))
                    {
                        return (int)Globals.ErrorValidationCodes.STO_DATE_START_END_ERROR;
                    }
                    break;
                case "general comment":
                    if (ValidationUtils.XORNull<String>(model.STO_DATE_START, model.STO_DATE_END))
                    {
                        return (int)Globals.ErrorValidationCodes.STO_DATE_START_END_ERROR;
                    }
                    break;
            }


            return 0;
        }

        public async Task<int> InstrumentInGrpInst(GRP_STO model, int siteId)
        {
            int resp = 0;

            var inst = await _context.GRP_INST.Where(md => md.INST_MODEL == model.STO_GA_MODEL && md.INST_SN == model.STO_GA_SN && md.SiteId == siteId && md.INST_FACTORY.ToLower() == "purchase")
                                              .OrderBy(md => md.INST_DATE).FirstOrDefaultAsync();
            if (inst == null)
            {
                resp = (int)Globals.ErrorValidationCodes.NOT_IN_GRP_INST;
            }
            else
            {
                string _iDate = inst.INST_DATE;
                string stoDate = String.IsNullOrEmpty(model.STO_DATE) ? model.STO_DATE_START : model.STO_DATE;
                if (String.Compare(_iDate, stoDate) > 0)
                {
                    resp = (int)Globals.ErrorValidationCodes.INST_PURCHASE_DATE_GREATER_THAN_INST_OP_DATE;
                }
            }
            return resp;
        }

        public async Task<int> LastExpectedOpByDateAsync(GRP_STO model, int siteId)
        {
            int result = 0;
            string stoType = model.STO_TYPE.ToLower();
            string stoDate = (String.IsNullOrEmpty(model.STO_DATE)) ? model.STO_DATE_START : model.STO_DATE;
            string lastOpToSearch = "";
            if(stoType=="ga installation")
            {
                lastOpToSearch = "ga removal";
            }
            else if(stoType == "level installation")
            {
                lastOpToSearch = "level removal";
            }
            else
            {

                lastOpToSearch = "ga installation";
            }
            /////step 0
            ///check if passed op date is prior to purchase inst date
            var beforePurchase = await _context.GRP_INST.Where(inst => inst.INST_DATE.CompareTo(stoDate) < 0).FirstOrDefaultAsync();
            if (beforePurchase == null)
            {
                return (int)Globals.ErrorValidationCodes.INST_PURCHASE_DATE_GREATER_THAN_INST_OP_DATE;
            }

            ////First check if sensor has been first installed (for any op != installation)
            ///or removed or empty (if op==installation)
            if (!String.IsNullOrEmpty(model.STO_GA_MODEL) && !String.IsNullOrEmpty(model.STO_GA_SN))
            {
                var foundSto = await _context.GRP_STO.Where(bm => ((bm.STO_TYPE.ToLower() == "ga installation") || (bm.STO_TYPE.ToLower() == "ga removal"))
                                                                       && bm.DataStatus == 0 && bm.SiteId == siteId
                                                                       && model.STO_GA_MODEL == bm.STO_GA_MODEL && model.STO_GA_SN == bm.STO_GA_SN)
                                                   .OrderByDescending(bm => bm.STO_DATE)
                                                   .ToListAsync();
                GRP_STO _stoItem = null;
                if (foundSto != null)
                {
                    _stoItem = foundSto.FirstOrDefault(item => String.Compare(item.STO_DATE, stoDate) < 0);
                }

                if (_stoItem == null)
                {
                    if (stoType == "ga installation")
                    {
                        result = 0;
                    }
                    else
                    {
                        result = (int)Globals.ErrorValidationCodes.STO_SENSOR_NOT_INSTALLED;
                    }
                }
                else
                {
                    if (stoType == "ga installation")
                    {
                        if (_stoItem.STO_TYPE.ToLower().CompareTo("ga removal") != 0)
                        {
                            result = (int)Globals.ErrorValidationCodes.STO_SENSOR_NOT_REMOVED;
                        }
                    }
                    else
                    {
                        if (_stoItem.STO_TYPE.ToLower().CompareTo("ga installation") != 0)
                        {
                            result = (int)Globals.ErrorValidationCodes.STO_SENSOR_NOT_INSTALLED;
                        }
                    }
                }
            }
            else
            {
                //probably level installation....
                var foundLevel = await _context.GRP_STO.Where(bm => ((bm.STO_TYPE.ToLower() == "level installation") || (bm.STO_TYPE.ToLower() == "level removal"))
                                                                       && bm.DataStatus == 0 && bm.SiteId == siteId
                                                                       && model.STO_PROF_ID == bm.STO_PROF_ID)
                                                        .OrderByDescending(bm => bm.STO_DATE)
                                                        .ToListAsync();
                GRP_STO _stoLevel = null;
                if (foundLevel != null)
                {
                    _stoLevel = foundLevel.FirstOrDefault(item => String.Compare(item.STO_DATE, stoDate) < 0);
                }

                if (_stoLevel == null)
                {
                    if (stoType == "level installation")
                    {
                        result = 0;
                    }
                    else
                    {
                        result = (int)Globals.ErrorValidationCodes.STO_LEVEL_NOT_INSTALLED; 
                    }
                }
                else
                {
                    if (stoType == "level installation")
                    {
                        if (_stoLevel.STO_TYPE.ToLower().CompareTo("level removal") != 0)
                        {
                            result = (int)Globals.ErrorValidationCodes.STO_LEVEL_NOT_REMOVED;
                        }
                    }
                    else
                    {
                        if (_stoLevel.STO_TYPE.ToLower().CompareTo("level installation") != 0)
                        {
                            result = (int)Globals.ErrorValidationCodes.STO_LEVEL_NOT_INSTALLED;
                        }
                    }
                }
            }
            ///add a check also for the future
            ///for example, if an installation or a removal is sent,
            ///check if there are ops for dates >= passed date
            ///raise error for this
            if (new string[] { "ga installation", "ga removal"/*, "level installation", "level removal"*/ }.Contains(stoType.ToLower()))
            {
                var noFuture = await _context.GRP_STO.Where(sto => sto.DataStatus == 0 && sto.SiteId == siteId
                                                && model.STO_GA_MODEL == sto.STO_GA_MODEL && model.STO_GA_SN == sto.STO_GA_SN &&
                                                (String.Compare(sto.STO_DATE, stoDate) > 0 || String.Compare(sto.STO_DATE_START, stoDate) > 0))
                                               .OrderBy(sto => sto.STO_DATE)
                                               .FirstOrDefaultAsync();
                if (noFuture != null)
                {
                    result = (int)Globals.ErrorValidationCodes.STO_SENSOR_FOLLOWING_OPERATION;
                }
            }
            if (new string[] { "level installation", "level removal" }.Contains(stoType.ToLower()))
            {
                var noFuture = await _context.GRP_STO.Where(sto => sto.DataStatus == 0 && sto.SiteId == siteId
                                                && model.STO_PROF_ID == sto.STO_PROF_ID && model.STO_PROF_LEVEL == sto.STO_PROF_LEVEL &&
                                                (String.Compare(sto.STO_DATE, stoDate) > 0 || String.Compare(sto.STO_DATE_START, stoDate) > 0))
                                               .OrderBy(sto => sto.STO_DATE)
                                               .FirstOrDefaultAsync();
                if (noFuture != null)
                {
                    result = (int)Globals.ErrorValidationCodes.STO_LEVEL_FOLLOWING_OPERATION;
                }
            }
            return result;
        }

        public int LoggerFileCheck(GRP_STO model, int siteId)
        {
            string stoDateToCompare = String.IsNullOrEmpty(model.STO_DATE) ? model.STO_DATE_START : model.STO_DATE;
            var exist = _context.GRP_FILE.Where(file => file.FILE_ID == model.STO_FILE &&
                                                      file.FILE_LOGGER_ID == model.STO_LOGGER &&
                                                      file.SiteId == siteId && file.DataStatus == 0 &&
                                                      file.FILE_DATE.CompareTo(stoDateToCompare) <= 0 &&
                                                      file.FILE_TYPE == "STO").Any();
            if (!exist)
            {
                return (int)Globals.ErrorValidationCodes.STO_LOGGER_FILE_MISSING_IN_GRP_FILE;
            }
            return 0;
        }

        //check if the passed STO_SAMPLING_INT is consistent with STO_SAMPLING_INT, STO_FILE and STO_LOGGER existing in the GRP_STO table for a given site
        //it is not possible to have two different sampling int mapped on a same file_id!!!
        public int LoggerFileSamplingIntCheck(GRP_STO model, int siteId)
        {
            string stoDateToCompare = String.IsNullOrEmpty(model.STO_DATE) ? model.STO_DATE_START : model.STO_DATE;
            var lastRecord = _context.GRP_STO.Where(bm => bm.STO_FILE == model.STO_FILE &&
                                                             //DIEGO:: FILE_ID must be different for different sampling int, but LOGGER_ID??? bm.STO_LOGGER == model.STO_LOGGER &&
                                                             bm.STO_DATE.CompareTo(stoDateToCompare) <= 0 &&
                                                             (bm.STO_TYPE.ToLower() == "ga installation" || bm.STO_TYPE.ToLower() == "variable map") &&
                                                             bm.SiteId == siteId && bm.DataStatus == 0).
                                                             OrderByDescending(bm => bm.STO_DATE).
                                                             FirstOrDefault();
            if (lastRecord != null)
            {
                if (lastRecord.STO_GA_SAMPLING_INT != model.STO_GA_SAMPLING_INT)
                {
                    return (int)Globals.ErrorValidationCodes.STO_DIFFERENT_SAMPLING_INT;
                }
            }
            return 0;
        }

        private async Task GetAvailableOpsByTypeAsync(string stoType)
        {
            //perhaps move to a web service....
            UiOperations.Clear();
            UiOperations = await _context.StoOps.Where(op => String.Compare(op.Sto_OP.ToLower(), stoType.ToLower()) == 0).ToListAsync();
        }

        private async Task<bool> IsGaInstalledAsync(string gaModel, string gaSn, string date, int siteId)
        {
            var isGa = await _context.GRP_STO.Where(sto => sto.DataStatus == 0 && sto.SiteId == siteId
                                                              && String.Compare(sto.STO_DATE, date) < 0 &&
                                                              sto.STO_GA_MODEL == gaModel && sto.STO_GA_SN == gaSn
                                                              && sto.STO_TYPE.ToLower() == "ga installation")
                                              .OrderByDescending(sto => sto.STO_DATE).FirstOrDefaultAsync();
            if (isGa == null)
            {
                return false;
            }
            return true;
        }

        private int GaProfNotAllowed(GRP_STO model)
        {
            var subProf = UiOperations.Where(ui => ui.Variable.Contains("STO_PROF"));
            var subGa = UiOperations.Where(ui => ui.Variable.Contains("STO_GA"));
            var isProf = (ValidationUtils.IsAnyPropNotNull<GRP_STO>(model, subProf.Select(x => x.Variable).ToArray()));
            var isGA = (ValidationUtils.IsAnyPropNotNull<GRP_STO>(model, subGa.Select(x => x.Variable).ToArray()));
            if (isProf && isGA) return 3;
            if (isProf && !isGA) return 2;
            if (!isProf && isGA) return 1;
            return 0;
        }
    }
}
