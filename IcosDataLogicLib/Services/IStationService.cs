﻿using IcosModelLib.DTO;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosDataLogicLib.Services
{
    public interface IStationService
    {
        Task<List<Station>> GetIcosSitesAsync();
        Task<IEnumerable<WGUsers>> GetWorkingGroupUsers(int WGId);
        Task<List<WorkGroups>> GetWorkGroupsList();
        Task<WGUsers> SaveWGUser(WGUsers WGuser, List<int> wgs);
    }
}
