﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosModelLib.Utils
{
    public class RequiredIfDatesAttribute : ValidationAttribute
    {
        public string _dependentProperty { get; set; }
        //public object _targetValue { get; set; }
        public string MyMessage { get; set; }
        //public string ErrorMessage { get; set; }
        public RequiredIfDatesAttribute(string dependentProperty/*, object targetValue*/)
        {
            this._dependentProperty = dependentProperty;
            //this._targetValue = targetValue;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            bool valid = true;
           
            string[] _depDatesArray;
           // string[] _depDatesArrayValues;
            //string name = validationContext.DisplayName;
            _depDatesArray = _dependentProperty.Split(",");
            string[] _depDatesArrayValues=new string[_depDatesArray.Length];
            for(int i=0; i< _depDatesArray.Length; ++i)
            {
                var field = validationContext.ObjectType.GetProperty(_depDatesArray[i]);
                if (field == null) continue;
                var what = field.GetValue(validationContext.ObjectInstance, null);
                _depDatesArrayValues[i] = (what!=null)? field.GetValue(validationContext.ObjectInstance, null).ToString():"";
            }
            //All dates submitted
            if(_depDatesArrayValues.All(date => !String.IsNullOrEmpty(date)))
            {
                valid = false;
                ErrorMessage = String.Format("Error. Found {0} together with {1} and/or {2} variables. Please, enter either {0} or {1}/{2}",
                                            _depDatesArray[0], _depDatesArray[1], _depDatesArray[2]);
                return new ValidationResult(FormatErrorMessage(ErrorMessage));
            }
            //No dates submitted
            else if (_depDatesArrayValues.All(date => String.IsNullOrEmpty(date)))
            {
                valid = false;
                ErrorMessage = String.Format("Error: no date submitted. Please, enter either {0} or {1}/{2}",
                                            _depDatesArray[0], _depDatesArray[1], _depDatesArray[2]);
                return new ValidationResult(FormatErrorMessage(ErrorMessage));
            }
            //_DATE and (_DATE_START OR _DATE_END): error!
            else if (!String.IsNullOrEmpty(_depDatesArrayValues[0]) && (!String.IsNullOrEmpty(_depDatesArrayValues[1]) || !String.IsNullOrEmpty(_depDatesArrayValues[2])))
            {
                valid = false;
                ErrorMessage = String.Format("Error. Please, enter either {0} or {1}/{2}",
                                            _depDatesArray[0], _depDatesArray[1], _depDatesArray[2]);
                return new ValidationResult(FormatErrorMessage(ErrorMessage));
            }
            //No _DATE and (_DATE_START is empty OR _DATE_END is empty): error!
            else if (String.IsNullOrEmpty(_depDatesArrayValues[0]) && (String.IsNullOrEmpty(_depDatesArrayValues[1]) || String.IsNullOrEmpty(_depDatesArrayValues[2])))
            {
                valid = false;
                ErrorMessage = String.Format("Error. {0} or {1} must be entered together",
                                            _depDatesArray[1], _depDatesArray[2]);
                return new ValidationResult(FormatErrorMessage(ErrorMessage));
            }
            else if (String.IsNullOrEmpty(_depDatesArrayValues[0]) && (!String.IsNullOrEmpty(_depDatesArrayValues[1]) && !String.IsNullOrEmpty(_depDatesArrayValues[2])))
            {
                if(String.Compare(_depDatesArrayValues[2], _depDatesArrayValues[1]) < 0)
                {
                    valid = false;
                    ErrorMessage = String.Format("Error. {0} must be greater than {1}", _depDatesArray[1], _depDatesArray[2]);
                    return new ValidationResult(FormatErrorMessage(ErrorMessage));
                }
                
            }
            else 
            { 
                valid = (!String.IsNullOrEmpty(_depDatesArrayValues[0]) && String.IsNullOrEmpty(_depDatesArrayValues[1]) && String.IsNullOrEmpty(_depDatesArrayValues[2])) ||
                        (String.IsNullOrEmpty(_depDatesArrayValues[0]) && !String.IsNullOrEmpty(_depDatesArrayValues[1]) && !String.IsNullOrEmpty(_depDatesArrayValues[2]));
                if (!valid)
                {
                    ErrorMessage = "Error in date submission. Please check data";
                    return new ValidationResult(FormatErrorMessage(ErrorMessage));
                }
            }

          //  if (valid)
            {
                return ValidationResult.Success;
            }
           /* else
            {
                //ErrorMessage = "Too many or too few dates!";
                return new ValidationResult(FormatErrorMessage(ErrorMessage));
            }*/
        }
    }
}
