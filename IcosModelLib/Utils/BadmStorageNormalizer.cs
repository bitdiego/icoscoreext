﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosModelLib.Utils
{
    public class BadmStorageNormalizer
    {

        // solo un esempio.... 
        public static void daStorageANormale()
        {
                int zgrid = 7;

                string CONN_STRING = "Server=tcp:icossql.database.windows.net,1433;Initial Catalog=IcosExp;Persist Security Info=False;User ID=icosetcsql;Password=asfas7!!3412;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                SqlConnection cn = new SqlConnection();
                cn.ConnectionString = CONN_STRING;
                cn.Open();

                SqlConnection cn1 = new SqlConnection("Data Source=NEW-GAIA\\NEWGAIASQL;Initial Catalog=ICOS;Integrated Security=True;Connect Timeout=300; MultipleActiveResultSets=True");
                cn1.Open();
                SqlCommand cmd1 = new SqlCommand();
                cmd1.Connection = cn1;
                ///////////
                cmd1.CommandText = "SelectDataGeneralExport";
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@siteId", 0);
                cmd1.Parameters.AddWithValue("@grId", zgrid);
                //   MetaData md = null;
                SqlDataReader rdx = cmd1.ExecuteReader();


                if (rdx.HasRows)
                {
                    String newcommand = "";
                    String listcampi = "";
                    String listval = "";
                    String xuserid = "";
                    String xsiteID = "";
                    String xdataStatus = "";
                    String xinsertDate = "";
                    String xinsertUserId = "";
                    String xdeleteUserId = "";
                    String xdeletedDate = "";
                    //          siteID,insertDate,insertUserId,deleteUserId,deletedDate, dataStatus
                    while (rdx.Read())
                    {
                        //  xuserid = rdx["xuserid"].ToString();
                        xsiteID = rdx["siteID"].ToString();
                        xinsertDate = mynordate(rdx["insertDate"].ToString());
                        xinsertUserId = rdx["insertUserId"].ToString();
                        xdeleteUserId = rdx["deleteUserId"].ToString();
                        xdataStatus = rdx["dataStatus"].ToString();
                        xdeletedDate = (xdeletedDate.Length > 0) ? "'" + mynordate(rdx["deletedDate"].ToString()) + "'" : "null";

                        newcommand = "insert into " + rdx["groupName"].ToString().Replace("GRP_", "");
                        for (int i = 8; i < rdx.FieldCount; i++)
                        {

                            if (rdx[i] != null && rdx[i].ToString().Length > 0)
                            {
                                string varname = rdx.GetName(i);
                                listcampi += varname + ",";
                                SqlCommand cmdv = new SqlCommand();
                                cmdv.CommandText = "select * from Variables where group_id=" + zgrid.ToString() + " and varIndex IS NOT NULL and Name='" + varname + "'";
                                cmdv.Connection = cn1;
                                SqlDataReader rdv = cmdv.ExecuteReader();
                                string addap = "";
                                bool isadate = false;
                                while (rdv.Read())
                                {
                                    string xte = rdv["unit_type"].ToString();
                                    if (xte == "1" || xte == "5")
                                    {
                                        addap = "'";
                                        if (xte == "5")
                                        {
                                            isadate = true;
                                        }
                                    }

                                }
                                rdv.Close();
                                String xval = rdx[i].ToString();
                                if (isadate)
                                {
                                    xval = mynordate(rdx[i].ToString());
                                }
                                listval += addap + xval + addap + ",";
                            }

                            //  [unit_type] 1 text 2 number 5 data 
                        }


                        // campi fissi
                        listcampi += " siteID,insertDate,insertUserId,deleteUserId,deletedDate, dataStatus";
                        listval += xsiteID + ",'" + xinsertDate + "'," + xinsertUserId + "," + xdeleteUserId + "," + xdeletedDate + "," + xdataStatus;
                        newcommand += " (" + listcampi + ") VALUES (" + listval + ")";  //.Substring(0, listval.Length - 1)
                        SqlCommand Commandxa = new SqlCommand(newcommand, cn);
                        int retValue = Commandxa.ExecuteNonQuery();
                    }
                }


                cn.Close();
                cn1.Close();
            }
        private static string mynordate(string p)
        {
            DateTime xd = Convert.ToDateTime(p);
            return xd.ToString("yyyyMMdd");
        }
    }
}
