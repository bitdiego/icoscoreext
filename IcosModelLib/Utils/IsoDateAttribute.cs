﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IcosModelLib.Utils
{
    public class IsoDateAttribute : ValidationAttribute, IClientModelValidator
    {
        
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null || value.ToString() == "")
            {
                return ValidationResult.Success;
            }
            bool valid = ValidateIsoDate(value.ToString());

            if (valid)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(FormatErrorMessage(ErrorMessage));
            }
        }

        public void AddValidation(ClientModelValidationContext context)
        {
            string target = context.ModelMetadata.PropertyName.Replace("_", "-");
            string to = "data-val-isodate";
            context.Attributes.Add("data-val", "true");
            context.Attributes.Add(to, "Invalid Iso date format");
        }

        public bool ValidateIsoDate(string input)
        {
            int currentYear = DateTime.Now.Year;
            /* try
             {
                 DateTime dt = Convert.ToDateTime(input);
                // return true;
             }
             catch (Exception e)
             {
                 return false; 
             }*/
            string numReg = "^[0-9]+$";
            int[] daysInMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

            if (input == "")
            {
                ErrorMessage = "Iso date is not valid";
                return false;
            }
            if ((input.Length) % 2 > 0 || input.Length > 12)
            {
                ErrorMessage = "Iso date is not valid";
                return false;
            }
            Match match = Regex.Match(input, numReg, RegexOptions.IgnoreCase);
            if (!match.Success) return false;
            else
            {
                string subYear = input.Substring(0, 4);
                string subMonth = "";
                string subDay = "";
                string subHour = "";
                string subMins = "";
                ////////////////
                int iYear = 0;
                iYear = int.Parse(subYear);

                var iMonth = 0;
                var iDay = 0;
                var iHour = 0;
                var iMins = 0;
                if (iYear < 1800 || iYear > (currentYear + 2))
                {
                    ErrorMessage = "Iso date is outside a valid range";
                    return false;
                }
                if (input.Length > 4)
                {
                    subMonth = input.Substring(4, 2);
                    iMonth = int.Parse(subMonth);
                    if (iMonth < 1 || iMonth > 12)
                    {
                        ErrorMessage = "Iso date has an invalid MONTH value";
                        return false;
                    }
                }
                if (input.Length > 6)
                {
                    subDay = input.Substring(6, 2);
                    iDay = int.Parse(subDay);
                    if (iMonth != 2)
                    {

                        if (iDay > daysInMonth[iMonth - 1])
                        {
                            ErrorMessage = "Iso date has an invalid DAY value";
                            return false;
                        }

                    }
                    else
                    {
                        if (isLeap(iYear))
                        {
                            if (iDay > 29)
                            {
                                ErrorMessage = "Iso date has an invalid DAY value";
                                return false;
                            }
                        }
                        else
                        {
                            if (iDay > 28)
                            {
                                ErrorMessage = "Iso date has an invalid DAY value";
                                return false;
                            }
                        }
                    }
                }
                if (input.Length > 8)
                {
                    subHour = input.Substring(8, 2);
                    iHour = int.Parse(subHour);
                    if (iHour >= 24)
                    {
                        ErrorMessage = "Iso date has an invalid HOUR value";
                        return false;
                    }
                }
                if (input.Length > 10)
                {
                    subMins = input.Substring(10, 2);
                    iMins = int.Parse(subMins);
                    if (iMins > 59)
                    {
                        ErrorMessage = "Iso date has an invalid MINUTE value";
                        return false;
                    }
                }

            }
            return true;
        }

        private bool isLeap(int yy)
        {
            if ((yy % 400 == 0 || yy % 100 != 0) && (yy % 4 == 0))
            {
                return true;

            }
            else
            {
                return false;
            }

        }
    }
}
