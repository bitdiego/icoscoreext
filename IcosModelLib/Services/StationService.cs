﻿using IcosDataLib.Data;
using IcosModelLib.DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosModelLib.Services
{
    public class StationService : IStationService
    {
        private readonly IcosDbContext _context;

        public StationService(IcosDbContext ctx)
        {
            _context = ctx;
        }
        public async Task<List<Station>> GetIcosSitesAsync()
        {
           /* try 
            {
                List<Station> stations = await _context.Stations.ToListAsync();
                return stations;
            }
            catch(Exception e)
            {
                return null;
            }*/

            List<Station> stations = await _context.Stations.ToListAsync();
            return stations;
            /*new List<Station>();
        stations.Add(new Station
                                {
                                    site_name = "False",
                                    site_code = "FA-Lso",
                                    NLat = "44.098745",
                                    NLon = "13.123456"
        });
        stations.Add(new Station
        {
            site_name = "EuTest",
            site_code = "EU-Tst",
            NLat = "42.427",
            NLon = "12.0883"
        });*/

        }
    }
}
