﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosModelLib.Services
{
    public interface IStationService
    {
        Task<List<Station>> GetIcosSitesAsync();
    }
}
