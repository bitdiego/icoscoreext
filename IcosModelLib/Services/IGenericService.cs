﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace IcosModelLib.Services
{
    public interface IGenericService<T>
    {
        Task<T> GetItemAsync();
        Task<T> GetItemValueAsync(uint siteId);
        Task<IEnumerable<T>> GetItemValuesAsync(uint siteId);
        Task<bool> SaveItemAsync(T t, uint insertUserId, uint siteId);
        Task<bool> DeleteItemAsync(int? id, uint siteId, uint userId);
        Task<bool> UpdateItemAsync(int? id, uint siteId, uint userId, T t);
        Task<bool> SetItemInvalidAsync(uint siteId, uint userId, T t);
    }
}
