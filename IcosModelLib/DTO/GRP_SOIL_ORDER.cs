using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_SOIL_ORDER : BaseClass
{
public GRP_SOIL_ORDER(){ GroupId = (int)Globals.Groups.GRP_SOIL_ORDER;}

	public string SOIL_ORDER { get; set; }

	public string SOIL_ORDER_APPROACH { get; set; }

	public string SOIL_ORDER_COMMENT { get; set; }

}
}