﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IcosModelLib.DTO
{
    public class Site
    {
        public int Id { get; set; }

        public string SiteName { get; set; }

        public string SiteCode { get; set; }

        //public string Ecosystem { get; set; }

        public int? SiteClass { get; set; }
    }
}
