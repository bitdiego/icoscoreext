using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_SITE_NAME : BaseClass
{
public GRP_SITE_NAME(){ GroupId = (int)Globals.Groups.GRP_SITE_NAME;}

	public string SITE_NAME { get; set; }

}
}