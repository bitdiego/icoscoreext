using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_ICOS : BaseClass
{
public GRP_ICOS(){ GroupId = (int)Globals.Groups.GRP_ICOS;}

	public string DATE_LABEL { get; set; }

	public string URL_REPORT { get; set; }

	public string CLASS_ICOS { get; set; }

	public string SUPPORT_COUNTRY { get; set; }

}
}