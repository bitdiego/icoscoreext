using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_SITE_CHAR1 : BaseClass
{
public GRP_SITE_CHAR1(){ GroupId = (int)Globals.Groups.GRP_SITE_CHAR1;}

	public string TERRAIN { get; set; }

}
}