using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_SOIL_WRB_GROUP : BaseClass
{
public GRP_SOIL_WRB_GROUP(){ GroupId = (int)Globals.Groups.GRP_SOIL_WRB_GROUP;}

	public string SOIL_WRB_GROUP { get; set; }

	public string SOIL_WRB_GROUP_APPROACH { get; set; }

	public string SOIL_WRB_GROUP_COMMENT { get; set; }

}
}