using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_HEADER : BaseClass
{
public GRP_HEADER(){ GroupId = (int)Globals.Groups.GRP_HEADER;}

	public string SITE_ID { get; set; }

	public string SITE_NAME { get; set; }

	public string SUBMISSION_CONTACT_NAME { get; set; }

	public string SUBMISSION_CONTACT_EMAIL { get; set; }

	public string SUBMISSION_DATE { get; set; }

}
}