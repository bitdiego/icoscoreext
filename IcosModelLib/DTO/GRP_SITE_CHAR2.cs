using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_SITE_CHAR2 : BaseClass
{
public GRP_SITE_CHAR2(){ GroupId = (int)Globals.Groups.GRP_SITE_CHAR2;}

	public string ASPECT { get; set; }

}
}