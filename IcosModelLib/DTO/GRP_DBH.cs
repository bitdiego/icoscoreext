using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_DBH : BaseClass
{
public GRP_DBH(){ GroupId = (int)Globals.Groups.GRP_DBH;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal DBH { get; set; }

	public string DBH_SPP { get; set; }

	public string DBH_VEGTYPE { get; set; }

	public string DBH_STATISTIC { get; set; }

	public string DBH_STATISTIC_METHOD { get; set; }

	public int DBH_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal DBH_HEIGHT { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal DBH_MIN { get; set; }

	public string DBH_APPROACH { get; set; }

	public string DBH_DATE { get; set; }

	public string DBH_DATE_START { get; set; }

	public string DBH_DATE_END { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal DBH_DATE_UNC { get; set; }

	public string DBH_COMMENT { get; set; }

}
}