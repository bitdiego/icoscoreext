using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_SOIL_SERIES : BaseClass
{
public GRP_SOIL_SERIES(){ GroupId = (int)Globals.Groups.GRP_SOIL_SERIES;}

	public string SOIL_SERIES { get; set; }

	public string SOIL_SERIES_APPROACH { get; set; }

	public string SOIL_SERIES_COMMENT { get; set; }

}
}