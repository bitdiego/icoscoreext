using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO
{

	public class GRP_TEAM : BaseClass
	{
		public GRP_TEAM()
		{
			GroupId = (int)Globals.Groups.GRP_TEAM;
		}

		public string TEAM_MEMBER_FIRSTNAME { get; set; }

		public string TEAM_MEMBER_LASTNAME { get; set; }

		public string TEAM_MEMBER_TITLE { get; set; }

		public string TEAM_MEMBER_ROLE { get; set; }

		public string TEAM_MEMBER_MAIN_EXPERT { get; set; }

		public string TEAM_MEMBER_PERC_ICOS { get; set; }

		public string TEAM_MEMBER_CONTRACT { get; set; }

		public string TEAM_MEMBER_EMAIL { get; set; }

		public string TEAM_MEMBER_ORCID { get; set; }

		public string TEAM_MEMBER_SOCIALMEDIA { get; set; }

		public string TEAM_MEMBER_PHONE { get; set; }

		public string TEAM_MEMBER_INSTITUTION { get; set; }

		public string TEAM_MEMBER_INST_STREET { get; set; }

		public string TEAM_MEMBER_INST_POSTCODE { get; set; }

		public string TEAM_MEMBER_INST_CITY { get; set; }

		public string TEAM_MEMBER_INST_COUNTRY { get; set; }

		public string TEAM_MEMBER_WORKEND { get; set; }

		public string TEAM_MEMBER_COMMENT { get; set; }

		public string TEAM_MEMBER_DATE { get; set; }

		public int? TEAM_MEMBER_AUTHORDER { get; set; }

	}
}