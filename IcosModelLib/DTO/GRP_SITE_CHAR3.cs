using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_SITE_CHAR3 : BaseClass
{
public GRP_SITE_CHAR3(){ GroupId = (int)Globals.Groups.GRP_SITE_CHAR3;}

	public string WIND_DIRECTION { get; set; }

}
}