using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_IGBP : BaseClass
{
public GRP_IGBP(){ GroupId = (int)Globals.Groups.GRP_IGBP;}

	public string IGBP { get; set; }

	public string IGBP_DATE_START { get; set; }

	public string IGBP_COMMENT { get; set; }

}
}