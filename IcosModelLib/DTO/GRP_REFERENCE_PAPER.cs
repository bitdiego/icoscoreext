using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_REFERENCE_PAPER : BaseClass
{
public GRP_REFERENCE_PAPER(){ GroupId = (int)Globals.Groups.GRP_REFERENCE_PAPER;}

	public string REFERENCE_PAPER { get; set; }

	public string REFERENCE_DOI { get; set; }

	public string REFERENCE_USAGE { get; set; }

	public string REFERENCE_COMMENT { get; set; }

}
}