using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_SPP_ass : BaseClass
{
public GRP_SPP_ass(){ GroupId = (int)Globals.Groups.GRP_SPPS;}

	public string SPP_O { get; set; }

	public string SPP_O_VEGTYPE { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SPP_O_PERC { get; set; }

	public string SPP_O_PERC_STATISTIC { get; set; }

	public string SPP_O_PERC_STATISTIC_METHOD { get; set; }

	public int SPP_O_PERC_STATISTIC_NUMBER { get; set; }

	public string SPP_U { get; set; }

	public string SPP_U_VEGTYPE { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SPP_U_PERC { get; set; }

	public string SPP_U_PERC_STATISTIC { get; set; }

	public string SPP_U_PERC_STATISTIC_METHOD { get; set; }

	public int SPP_U_PERC_STATISTIC_NUMBER { get; set; }

	public string SPP_PERC_UNIT { get; set; }

	public string SPP_APPROACH { get; set; }

	public string SPP_DATE { get; set; }

	public string SPP_DATE_START { get; set; }

	public string SPP_DATE_END { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SPP_DATE_UNC { get; set; }

	public string SPP_COMMENT { get; set; }

	public string SPP_O_PREVALENCE { get; set; }

	public string SPP_U_PREVALENCE { get; set; }

}
}