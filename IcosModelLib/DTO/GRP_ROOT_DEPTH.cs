using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_ROOT_DEPTH : BaseClass
{
public GRP_ROOT_DEPTH(){ GroupId = (int)Globals.Groups.GRP_ROOT_DEPTH;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal ROOT_DEPTH { get; set; }

	public string ROOT_DEPTH_SPP { get; set; }

	public string ROOT_DEPTH_VEGTYPE { get; set; }

	public string ROOT_DEPTH_STATISTIC { get; set; }

	public string ROOT_DEPTH_STATISTIC_METHOD { get; set; }

	public int ROOT_DEPTH_STATISTIC_NUMBER { get; set; }

	public string ROOT_DEPTH_APPROACH { get; set; }

	public string ROOT_DEPTH_DATE { get; set; }

	public string ROOT_DEPTH_DATE_START { get; set; }

	public string ROOT_DEPTH_DATE_END { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal ROOT_DEPTH_DATE_UNC { get; set; }

	public string ROOT_DEPTH_COMMENT { get; set; }

}
}