using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_SOIL_DEPTH : BaseClass
{
public GRP_SOIL_DEPTH(){ GroupId = (int)Globals.Groups.GRP_SOIL_DEPTH;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_DEPTH { get; set; }

	public string SOIL_DEPTH_STATISTIC { get; set; }

	public string SOIL_DEPTH_STATISTIC_METHOD { get; set; }

	public int SOIL_DEPTH_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_DEPTH_ORG { get; set; }

	public string SOIL_DEPTH_ORG_STATISTIC { get; set; }

	public string SOIL_DEPTH_ORG_STATISTIC_METHOD { get; set; }

	public int SOIL_DEPTH_ORG_STATISTIC_NUMBER { get; set; }

	public string SOIL_DEPTH_APPROACH { get; set; }

	public string SOIL_DEPTH_DATE { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_DEPTH_DATE_UNC { get; set; }

	public string SOIL_DEPTH_COMMENT { get; set; }

}
}