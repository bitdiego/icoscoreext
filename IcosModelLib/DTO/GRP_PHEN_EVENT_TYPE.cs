using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_PHEN_EVENT_TYPE : BaseClass
{
public GRP_PHEN_EVENT_TYPE(){ GroupId = (int)Globals.Groups.GRP_PHEN_EVENT_TYPE;}

	public string PHEN_EVENT_TYPE { get; set; }

	public string PHEN_EVENT_STATUS { get; set; }

	public string PHEN_EVENT_SPP { get; set; }

	public string PHEN_EVENT_VEGTYPE { get; set; }

	public string PHEN_EVENT_DATE { get; set; }

	public string PHEN_EVENT_DATE_STATISTIC { get; set; }

	public string PHEN_EVENT_DATE_STATISTIC_METHOD { get; set; }

	public int PHEN_EVENT_DATE_STATISTIC_NUMBER { get; set; }

	public string PHEN_EVENT_APPROACH { get; set; }

	public string PHEN_EVENT_COMMENT { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal PHEN_EVENT_DATE_STD_DEV { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal PHEN_EVENT_DATE_MEAS_UNC { get; set; }

}
}