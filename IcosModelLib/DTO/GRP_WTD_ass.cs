using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_WTD_ass : BaseClass
{
public GRP_WTD_ass(){ GroupId = (int)Globals.Groups.GRP_WTDPNT;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal WTD { get; set; }

	public string WTD_STATISTIC { get; set; }

	public string WTD_STATISTIC_METHOD { get; set; }

	public int WTD_STATISTIC_NUMBER { get; set; }

	public string WTD_APPROACH { get; set; }

	public string WTD_DATE { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal WTD_DATE_UNC { get; set; }

	public string WTD_COMMENT { get; set; }

}
}