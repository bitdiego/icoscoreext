using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_NETWORK : BaseClass
{
public GRP_NETWORK(){ GroupId = (int)Globals.Groups.GRP_NETWORK;}

	public string NETWORK { get; set; }

}
}