using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_BIOMASS : BaseClass
{
public GRP_BIOMASS(){ GroupId = (int)Globals.Groups.GRP_BIOMASS;}

	public string BIOMASS { get; set; }

	public string BIOMASS_UNIT { get; set; }

	public string BIOMASS_SPP { get; set; }

	public string BIOMASS_VEGTYPE { get; set; }

	public string BIOMASS_STATISTIC { get; set; }

	public string BIOMASS_STATISTIC_METHOD { get; set; }

	public int BIOMASS_STATISTIC_NUMBER { get; set; }

	public string BIOMASS_ORGAN { get; set; }

	public string BIOMASS_PHEN { get; set; }

	public string BIOMASS_APPROACH { get; set; }

	public string BIOMASS_DATE { get; set; }

	public string BIOMASS_DATE_START { get; set; }

	public string BIOMASS_DATE_END { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal BIOMASS_DATE_UNC { get; set; }

	public string BIOMASS_COMMENT { get; set; }

	public string BIOMASS_LIFESTAGE { get; set; }

	public string BIOMASS_VEG_STATUS { get; set; }

}
}