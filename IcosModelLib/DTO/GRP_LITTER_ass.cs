using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_LITTER_ass : BaseClass
{
public GRP_LITTER_ass(){ GroupId = (int)Globals.Groups.GRP_LITTERPNT;}

	public string LITTER { get; set; }

	public string LITTER_UNIT { get; set; }

	public string LITTER_SPP { get; set; }

	public string LITTER_VEGTYPE { get; set; }

	public string LITTER_STATISTIC { get; set; }

	public string LITTER_STATISTIC_METHOD { get; set; }

	public int LITTER_STATISTIC_NUMBER { get; set; }

	public string LITTER_ORGAN { get; set; }

	public string LITTER_APPROACH { get; set; }

	public string LITTER_DATE { get; set; }

	public string LITTER_DATE_START { get; set; }

	public string LITTER_DATE_END { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal LITTER_DATE_UNC { get; set; }

	public string LITTER_COMMENT { get; set; }

}
}