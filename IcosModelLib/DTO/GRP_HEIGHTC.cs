using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_HEIGHTC : BaseClass
{
public GRP_HEIGHTC(){ GroupId = (int)Globals.Groups.GRP_HEIGHTC;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal HEIGHTC { get; set; }

	public string HEIGHTC_SPP { get; set; }

	public string HEIGHTC_VEGTYPE { get; set; }

	public string HEIGHTC_STATISTIC { get; set; }

	public int HEIGHTC_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal HEIGHTC_U { get; set; }

	public string HEIGHTC_U_SPP { get; set; }

	public string HEIGHTC_U_VEGTYPE { get; set; }

	public string HEIGHTC_U_STATISTIC { get; set; }

	public string HEIGHTC_U_STATISTIC_METHOD { get; set; }

	public int HEIGHTC_U_STATISTIC_NUMBER { get; set; }

	public string HEIGHTC_APPROACH { get; set; }

	public string HEIGHTC_DATE { get; set; }

	public string HEIGHTC_DATE_START { get; set; }

	public string HEIGHTC_DATE_END { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal HEIGHTC_DATE_UNC { get; set; }

	public string HEIGHTC_COMMENT { get; set; }

	public string HEIGHTC_STATISTIC_METHOD { get; set; }

}
}