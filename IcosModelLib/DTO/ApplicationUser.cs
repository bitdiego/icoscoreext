using Microsoft.AspNetCore.Identity;
using System;

namespace IdentitySample.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public int? LegacyID { get; set; }
        public string? Name { get; set; }
        public string? Type { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? FirstAccess { get; set; }
    }
}
