using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_FLUX_MEASUREMENTS : BaseClass
{
public GRP_FLUX_MEASUREMENTS(){ GroupId = (int)Globals.Groups.GRP_FLUX_MEASUREMENTS;}

	public string FLUX_MEASUREMENTS_VARIABLE { get; set; }

	public string FLUX_MEASUREMENTS_METHOD { get; set; }

	public string FLUX_MEASUREMENTS_OPERATIONS { get; set; }

	public string FLUX_MEASUREMENTS_DATE_START { get; set; }

	public string FLUX_MEASUREMENTS_DATE_END { get; set; }

	public string FLUX_MEASUREMENTS_COMMENT { get; set; }

}
}