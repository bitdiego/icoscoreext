using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_SWC : BaseClass
{
public GRP_SWC(){ GroupId = (int)Globals.Groups.GRP_SWC;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal SWC { get; set; }

	public string SWC_STATISTIC { get; set; }

	public string SWC_STATISTIC_METHOD { get; set; }

	public int SWC_STATISTIC_NUMBER { get; set; }

	public string SWC_UNIT { get; set; }

	public string SWC_PROFILE_ZERO_REF { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SWC_PROFILE_MIN { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SWC_PROFILE_MAX { get; set; }

	public string SWC_HORIZON { get; set; }

	public string SWC_APPROACH { get; set; }

	public string SWC_DATE { get; set; }

	public string SWC_DATE_START { get; set; }

	public string SWC_DATE_END { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SWC_DATE_UNC { get; set; }

	public string SWC_COMMENT { get; set; }

}
}