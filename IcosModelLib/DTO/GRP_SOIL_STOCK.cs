using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_SOIL_STOCK : BaseClass
{
public GRP_SOIL_STOCK(){ GroupId = (int)Globals.Groups.GRP_SOIL_STOCK;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_STOCK_C_ORG { get; set; }

	public string SOIL_STOCK_C_ORG_STATISTIC { get; set; }

	public string SOIL_STOCK_C_ORG_STATISTIC_METHOD { get; set; }

	public int SOIL_STOCK_C_ORG_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_STOCK_N_TOT { get; set; }

	public string SOIL_STOCK_N_TOT_STATISTIC { get; set; }

	public string SOIL_STOCK_N_TOT_STATISTIC_METHOD { get; set; }

	public int SOIL_STOCK_N_TOT_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_STOCK_NH4 { get; set; }

	public string SOIL_STOCK_NH4_STATISTIC { get; set; }

	public string SOIL_STOCK_NH4_STATISTIC_METHOD { get; set; }

	public int SOIL_STOCK_NH4_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_STOCK_NO3 { get; set; }

	public string SOIL_STOCK_NO3_STATISTIC { get; set; }

	public string SOIL_STOCK_NO3_STATISTIC_METHOD { get; set; }

	public int SOIL_STOCK_NO3_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_STOCK_K { get; set; }

	public string SOIL_STOCK_K_STATISTIC { get; set; }

	public string SOIL_STOCK_K_STATISTIC_METHOD { get; set; }

	public int SOIL_STOCK_K_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_STOCK_P { get; set; }

	public string SOIL_STOCK_P_STATISTIC { get; set; }

	public string SOIL_STOCK_P_STATISTIC_METHOD { get; set; }

	public int SOIL_STOCK_P_STATISTIC_NUMBER { get; set; }

	public string SOIL_STOCK_PROFILE_ZERO_REF { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_STOCK_PROFILE_MIN { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_STOCK_PROFILE_MAX { get; set; }

	public string SOIL_STOCK_HORIZON { get; set; }

	public string SOIL_STOCK_APPROACH { get; set; }

	public string SOIL_STOCK_DATE { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_STOCK_DATE_UNC { get; set; }

	public string SOIL_STOCK_COMMENT { get; set; }

}
}