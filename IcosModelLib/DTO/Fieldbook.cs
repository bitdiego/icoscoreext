﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosModelLib.DTO
{
    public class Fieldbook
    {
        [Key]
        public int Id { get; set; }
        public string OriginalName { get; set; }
        public string DataInfo { get; set; }
        public int SiteId { get; set; }
        public int UserId { get; set; }
        public int DataTypeId { get; set; }
        public string Date { get; set; }
        public DateTime ImportDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public short Status { get; set; }

    }
}
