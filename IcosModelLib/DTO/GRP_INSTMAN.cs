using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO
{

	public class GRP_INSTMAN : BaseClass
	{
		public GRP_INSTMAN(){ GroupId = (int)Globals.Groups.GRP_INSTMAN;}

		public string INSTMAN_MODEL { get; set; }

		public string INSTMAN_SN { get; set; }

		public string INSTMAN_TYPE { get; set; }

		public string INSTMAN_VARIABLE_H_V_R { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_HEIGHT { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_EASTWARD_DIST { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_NORTHWARD_DIST { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_SAMPLING_INT { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_AVERAGING_INT { get; set; }

		public string INSTMAN_HEAT { get; set; }

		public string INSTMAN_SHIELDING { get; set; }

		public string INSTMAN_ASPIRATION { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_SA_OFFSET_NORTH { get; set; }

		public string INSTMAN_SA_WIND_FORMAT { get; set; }

		public string INSTMAN_SA_GILL_ALIGN { get; set; }

		public string INSTMAN_GA_CP_FILTERS { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_GA_CP_TUBE_LENGTH { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_GA_CP_TUBE_IN_DIAM { get; set; }

		public string INSTMAN_GA_CP_TUBE_MAT { get; set; }

		public string INSTMAN_GA_CP_TUBE_THERM { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_GA_CP_FLOW_RATE { get; set; }

		public string INSTMAN_GA_CP_MFC { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_GA_OP_AZIM_MOUNT { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_GA_OP_VERT_MOUNT { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_GA_CO2_ZERO { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_GA_CO2_OFFSET { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_GA_CO2_REF { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_GA_H2O_ZERO { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_GA_H2O_OFFSET { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_GA_H2O_REF { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_GA_CAL_TA { get; set; }

		public string INSTMAN_DATE { get; set; }

		public string INSTMAN_DATE_START { get; set; }

		public string INSTMAN_DATE_END { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? INSTMAN_DATE_UNC { get; set; }

		public string INSTMAN_COMMENT { get; set; }

		public string INSTMAN_FIRMWARE { get; set; }

	}
}