using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO
{

	public class GRP_FUNDING : BaseClass
	{
		public GRP_FUNDING(){ GroupId = (int)Globals.Groups.GRP_FUNDING;}

		public string FUNDING_ORGANIZATION { get; set; }

		public string FUNDING_GRANT { get; set; }

		public string FUNDING_GRANT_URL { get; set; }

		public string FUNDING_TITLE { get; set; }

		public string FUNDING_COMMENT { get; set; }

		public string FUNDING_DATE_START { get; set; }

		public string FUNDING_DATE_END { get; set; }

	}
}