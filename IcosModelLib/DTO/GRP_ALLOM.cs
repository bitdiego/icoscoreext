using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_ALLOM : BaseClass
{
public GRP_ALLOM(){ GroupId = (int)Globals.Groups.GRP_ALLOM;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal ALLOM_DBH { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal ALLOM_HEIGHT { get; set; }

	public string ALLOM_SPP { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal ALLOM_STEM_BIOM { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal ALLOM_BRANCHES_BIOM { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal ALLOM_LEAVES_BIOM { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal ALLOM_NDLE_CONUM { get; set; }

	public string ALLOM_COMMENT { get; set; }

	public string ALLOM_DATE { get; set; }

}
}