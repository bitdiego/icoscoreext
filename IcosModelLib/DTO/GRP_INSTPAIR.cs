using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO
{

	public class GRP_INSTPAIR : BaseClass
	{
		public GRP_INSTPAIR(){ GroupId = (int)Globals.Groups.GRP_INSTPAIR;}

		public string INSTPAIR_MODEL_1 { get; set; }

		public string INSTPAIR_SN_1 { get; set; }

		public string INSTPAIR_MODEL_2 { get; set; }

		public string INSTPAIR_SN_2 { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal INSTPAIR_HEIGHT_SEP { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal INSTPAIR_EASTWARD_SEP { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal INSTPAIR_NORTHWARD_SEP { get; set; }

		public string INSTPAIR_DATE { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal INSTPAIR_DATE_UNC { get; set; }

		public string INSTPAIR_COMMENT { get; set; }

	}
}