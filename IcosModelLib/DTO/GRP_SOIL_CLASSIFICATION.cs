using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_SOIL_CLASSIFICATION : BaseClass
{
public GRP_SOIL_CLASSIFICATION(){ GroupId = (int)Globals.Groups.GRP_SOIL_CLASSIFICATION;}

	public string SOIL_CLASSIFICATION { get; set; }

	public string SOIL_CLASSIFICATION_TAXONOMY { get; set; }

	public string SOIL_CLASSIFICATION_APPROACH { get; set; }

	public string SOIL_CLASSIFICATION_COMMENT { get; set; }

}
}