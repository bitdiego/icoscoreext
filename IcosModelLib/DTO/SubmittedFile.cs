﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosModelLib.DTO
{
    public class SubmittedFile
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string OriginalName { get; set; }
        public string DataInfo { get; set; }
        public bool Exclusion { get; set; }
        public string Status { get; set; }
        public int UserId { get; set; }
        public int FileTypeId { get; set; }
        public int SiteId { get; set; }
        public DateTime Date { get; set; }

        /////////////
        ///
        public FileType FileType { get; set; }
        public Site Site { get; set; }
    }
}
