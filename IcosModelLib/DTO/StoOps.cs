﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosModelLib.DTO
{
    public class StoOps
    {
        public int Id { get; set; }
        public string Sto_OP { get; set; }
        public string Variable { get; set; }
        public bool Mandatory { get; set; }
        public string Constraints { get; set; }
    }
}
