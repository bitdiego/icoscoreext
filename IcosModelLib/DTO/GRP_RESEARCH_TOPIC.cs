using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_RESEARCH_TOPIC : BaseClass
{
public GRP_RESEARCH_TOPIC(){ GroupId = (int)Globals.Groups.GRP_RESEARCH_TOPIC;}

	public string RESEARCH_TOPIC { get; set; }

}
}