using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_TREES_NUM : BaseClass
{
public GRP_TREES_NUM(){ GroupId = (int)Globals.Groups.GRP_TREES_NUM;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal TREES_NUM { get; set; }

	public string TREES_NUM_SPP { get; set; }

	public string TREES_NUM_VEGTYPE { get; set; }

	public string TREES_NUM_STATISTIC { get; set; }

	public string TREES_NUM_STATISTIC_METHOD { get; set; }

	public int TREES_NUM_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal TREES_NUM_DBH_MIN { get; set; }

	public string TREES_NUM_APPROACH { get; set; }

	public string TREES_NUM_DATE { get; set; }

	public string TREES_NUM_DATE_START { get; set; }

	public string TREES_NUM_DATE_END { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal TREES_NUM_DATE_UNC { get; set; }

	public string TREES_NUM_COMMENT { get; set; }

	public string TREES_NUM_LIFESTAGE { get; set; }

	public string TREES_NUM_VEG_STATUS { get; set; }

}
}