﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosModelLib.DTO
{
    public class WGUsers
    {   [Key]
        public int Id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string institution { get; set; }
        public string mail { get; set; }
        public bool auto { get; set; }
    }

    public class UsersToWG
    {
        [Key]
        public int id { get; set; }
        public int idUser { get; set; }
        public int idWG { get; set; }
        public DateTime date { get; set; }
    }

    public class WorkGroups {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
}
