using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_ACKNOWLEDGEMENT : BaseClass
{
public GRP_ACKNOWLEDGEMENT(){ GroupId = (int)Globals.Groups.GRP_ACKNOWLEDGEMENT;}

	public string ACKNOWLEDGEMENT { get; set; }

	public string ACKNOWLEDGEMENT_COMMENT { get; set; }

}
}