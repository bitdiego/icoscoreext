using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_SITE_ICOS_CLASS : BaseClass
{
public GRP_SITE_ICOS_CLASS(){ GroupId = (int)Globals.Groups.GRP_SITE_ICOS_CLASS;}

	public string SITE_ICOS_CLASS { get; set; }

	public string SITE_ICOS_LABDATE { get; set; }

}
}