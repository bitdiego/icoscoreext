using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_VAR_INFO : BaseClass
{
public GRP_VAR_INFO(){ GroupId = (int)Globals.Groups.GRP_VAR_INFO;}

	public string VAR_INFO_VARNAME { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal VAR_INFO_HEIGHT { get; set; }

	public string VAR_INFO_INST_MODEL { get; set; }

	public string VAR_INFO_UNIT { get; set; }

	public int VAR_INFO_AGG_NUMLOC { get; set; }

	public string VAR_INFO_DATE { get; set; }

	public string VAR_INFO_COMMENT { get; set; }

	public string VAR_INFO_VARNAME_ROOT { get; set; }

	public string VAR_INFO_VARNAME_PI { get; set; }

	public string VAR_INFO_VARNAME_GAP_FILL { get; set; }

	public int VAR_INFO_VARNAME_POSN_HOR_INDEX { get; set; }

	public int VAR_INFO_VARNAME_POSN_VERT_INDEX { get; set; }

	public int VAR_INFO_VARNAME_REPLICATE_INDEX { get; set; }

	public string VAR_INFO_VARNAME_AGG_REPLICATE { get; set; }

	public int VAR_INFO_VARNAME_AGG_LAYER_INDEX { get; set; }

	public string VAR_INFO_VARNAME_AGG_STDDEV { get; set; }

	public string VAR_INFO_VARNAME_AGG_NUM_SAMPLE { get; set; }

	public string VAR_INFO_VARNAME_INST_UNIT { get; set; }

	public string VAR_INFO_VARNAME_QC_FLAG { get; set; }

}
}