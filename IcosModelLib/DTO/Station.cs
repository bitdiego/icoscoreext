﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosModelLib.DTO
{
    public class Station
    {
        [Key]
        public int Id { get; set; }
        public string SiteName { get; set; }
        public string SiteCode { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }
        public decimal? Elevation { get; set; }

        [NotMapped]
        public string Date { get; set; }

        [NotMapped]
        public int DataStatus { get; set; }
    }
}
