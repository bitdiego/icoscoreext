using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_SA : BaseClass
{
public GRP_SA(){ GroupId = (int)Globals.Groups.GRP_SA;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal SA { get; set; }

	public string SA_STATISTIC { get; set; }

	public string SA_STATISTIC_METHOD { get; set; }

	public int SA_STATISTIC_NUMBER { get; set; }

	public string SA_APPROACH { get; set; }

	public string SA_DATE { get; set; }

	public string SA_DATE_START { get; set; }

	public string SA_DATE_END { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SA_DATE_UNC { get; set; }

	public string SA_COMMENT { get; set; }

}
}