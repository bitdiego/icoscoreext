using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_SOIL_CHEM : BaseClass
{
public GRP_SOIL_CHEM(){ GroupId = (int)Globals.Groups.GRP_SOIL_CHEM;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_CHEM_C_ORG { get; set; }

	public string SOIL_CHEM_C_ORG_STATISTIC { get; set; }

	public string SOIL_CHEM_C_ORG_STATISTIC_METHOD { get; set; }

	public int SOIL_CHEM_C_ORG_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_CHEM_N_TOT { get; set; }

	public string SOIL_CHEM_N_TOT_STATISTIC { get; set; }

	public string SOIL_CHEM_N_TOT_STATISTIC_METHOD { get; set; }

	public int SOIL_CHEM_N_TOT_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_CHEM_NH4 { get; set; }

	public string SOIL_CHEM_NH4_STATISTIC { get; set; }

	public string SOIL_CHEM_NH4_STATISTIC_METHOD { get; set; }

	public int SOIL_CHEM_NH4_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_CHEM_NO3 { get; set; }

	public string SOIL_CHEM_NO3_STATISTIC { get; set; }

	public string SOIL_CHEM_NO3_STATISTIC_METHOD { get; set; }

	public int SOIL_CHEM_NO3_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_CHEM_K { get; set; }

	public string SOIL_CHEM_K_STATISTIC { get; set; }

	public string SOIL_CHEM_K_STATISTIC_METHOD { get; set; }

	public int SOIL_CHEM_K_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_CHEM_P { get; set; }

	public string SOIL_CHEM_P_STATISTIC { get; set; }

	public string SOIL_CHEM_P_STATISTIC_METHOD { get; set; }

	public int SOIL_CHEM_P_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_CHEM_CN_RATIO { get; set; }

	public string SOIL_CHEM_CN_RATIO_STATISTIC { get; set; }

	public string SOIL_CHEM_CN_RATIO_STATISTIC_METHOD { get; set; }

	public int SOIL_CHEM_CN_RATIO_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_CHEM_PH_SALT { get; set; }

	public string SOIL_CHEM_PH_SALT_STATISTIC { get; set; }

	public string SOIL_CHEM_PH_SALT_STATISTIC_METHOD { get; set; }

	public int SOIL_CHEM_PH_SALT_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_CHEM_PH_H2O { get; set; }

	public string SOIL_CHEM_PH_H2O_STATISTIC { get; set; }

	public string SOIL_CHEM_PH_H2O_STATISTIC_METHOD { get; set; }

	public int SOIL_CHEM_PH_H2O_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_CHEM_BD { get; set; }

	public string SOIL_CHEM_BD_STATISTIC { get; set; }

	public string SOIL_CHEM_BD_STATISTIC_METHOD { get; set; }

	public int SOIL_CHEM_BD_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_CHEM_PROFILE_ZERO_REF { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_CHEM_PROFILE_MIN { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_CHEM_PROFILE_MAX { get; set; }

	public string SOIL_CHEM_HORIZON { get; set; }

	public string SOIL_CHEM_APPROACH { get; set; }

	public string SOIL_CHEM_DATE { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal SOIL_CHEM_DATE_UNC { get; set; }

	public string SOIL_CHEM_COMMENT { get; set; }

}
}