﻿using IcosModelLib.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Models.PIAreaModels
{
    public class SitesToUser
    {
        public int UserId { get; set; }
        public IEnumerable<Site> SitesPiList;
    }
}
