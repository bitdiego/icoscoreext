﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IcosModelLib.DTO.PIAreaModels
{
    // need partial?? it's a view..
    public class IcosSitePI
    {
        //public int id_user_site { get; set; }
        public Nullable<int> id_user { get; set; }
        public Nullable<int> id_site { get; set; }
        public string type { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string site_code { get; set; }
    }
}
