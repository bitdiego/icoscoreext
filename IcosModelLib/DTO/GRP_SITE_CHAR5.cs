using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_SITE_CHAR5 : BaseClass
{
public GRP_SITE_CHAR5(){ GroupId = (int)Globals.Groups.GRP_SITE_CHAR5;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal SITE_SNOW_COVER_DAYS { get; set; }

}
}