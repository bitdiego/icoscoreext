using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO
{

    public class GRP_LOCATION : BaseClass
    {
        public GRP_LOCATION() { GroupId = (int)Globals.Groups.GRP_LOCATION; }
        [Column(TypeName = "decimal(18, 8)")]

        public decimal LOCATION_LAT { get; set; }
        [Column(TypeName = "decimal(18, 8)")]


        public decimal LOCATION_LONG { get; set; }
        [Column(TypeName = "decimal(18, 8)")]
        public decimal? LOCATION_ELEV { get; set; }

        public string LOCATION_DATE { get; set; }

        public string LOCATION_COMMENT { get; set; }

    }
}