using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_PFCURVE : BaseClass
{
public GRP_PFCURVE(){ GroupId = (int)Globals.Groups.GRP_PFCURVE;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal PFCURVE_MP { get; set; }

	public string PFCURVE_MP_STATISTIC { get; set; }

	public string PFCURVE_MP_STATISTIC_METHOD { get; set; }

	public int PFCURVE_MP_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal PFCURVE_SWC { get; set; }

	public string PFCURVE_SWC_STATISTIC { get; set; }

	public string PFCURVE_SWC_STATISTIC_METHOD { get; set; }

	public int PFCURVE_SWC_STATISTIC_NUMBER { get; set; }

	public string PFCURVE_SWC_UNIT { get; set; }

	public string PFCURVE_APPROACH { get; set; }

	public string PFCURVE_DATE { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal PFCURVE_DATE_UNC { get; set; }

	public string PFCURVE_COMMENT { get; set; }

}
}