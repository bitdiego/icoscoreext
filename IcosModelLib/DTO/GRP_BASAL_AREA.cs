using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_BASAL_AREA : BaseClass
{
public GRP_BASAL_AREA(){ GroupId = (int)Globals.Groups.GRP_BASAL_AREA;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal BASAL_AREA { get; set; }

	public string BASAL_AREA_SPP { get; set; }

	public string BASAL_AREA_VEGTYPE { get; set; }

	public string BASAL_AREA_STATISTIC { get; set; }

	public string BASAL_AREA_STATISTIC_METHOD { get; set; }

	public int BASAL_AREA_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal BASAL_AREA_DBH_MIN { get; set; }

	public string BASAL_AREA_APPROACH { get; set; }

	public string BASAL_AREA_DATE { get; set; }

	public string BASAL_AREA_DATE_START { get; set; }

	public string BASAL_AREA_DATE_END { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal BASAL_AREA_DATE_UNC { get; set; }

	public string BASAL_AREA_COMMENT { get; set; }

}
}