using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_VAR_AGG : BaseClass
{
public GRP_VAR_AGG(){ GroupId = (int)Globals.Groups.GRP_VAR_AGG;}

	public string VAR_AGG_VARNAME { get; set; }

	public string VAR_AGG_MEMBERS { get; set; }

	public string VAR_AGG_STATISTIC { get; set; }

	public string VAR_AGG_DATE { get; set; }

	public string VAR_AGG_COMMENT { get; set; }

}
}