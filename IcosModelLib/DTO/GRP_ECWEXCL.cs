using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;
namespace IcosModelLib.DTO
{
	public class GRP_ECWEXCL : BaseClass
	{
        public GRP_ECWEXCL()
        {
			GroupId = (int)Globals.Groups.GRP_ECWEXCL;
		}
		[Column(TypeName = "decimal(18, 8)")]
		public decimal ECWEXCL { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal ECWEXCL_RANGE { get; set; }

		public string ECWEXCL_ACTION { get; set; }

		public string ECWEXCL_DATE { get; set; }

        public string ECWEXCL_COMMENT { get; set; }

    }
}