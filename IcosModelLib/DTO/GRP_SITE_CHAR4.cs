using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_SITE_CHAR4 : BaseClass
{
public GRP_SITE_CHAR4(){ GroupId = (int)Globals.Groups.GRP_SITE_CHAR4;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal SURFACE_HOMOGENEITY { get; set; }

}
}