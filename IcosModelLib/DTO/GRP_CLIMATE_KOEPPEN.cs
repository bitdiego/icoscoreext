using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_CLIMATE_KOEPPEN : BaseClass
{
public GRP_CLIMATE_KOEPPEN(){ GroupId = (int)Globals.Groups.GRP_CLIMATE_KOEPPEN;}

	public string CLIMATE_KOEPPEN { get; set; }

}
}