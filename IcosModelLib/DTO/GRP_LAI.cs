using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosModelLib.DTO{

public class GRP_LAI : BaseClass
{
public GRP_LAI(){ GroupId = (int)Globals.Groups.GRP_LAI;}

	[Column(TypeName = "decimal(18, 8)")]
public decimal LAI { get; set; }

	public string LAI_TYPE { get; set; }

	public string LAI_CANOPY_TYPE { get; set; }

	public string LAI_SPP { get; set; }

	public string LAI_VEGTYPE { get; set; }

	public string LAI_STATISTIC { get; set; }

	public string LAI_STATISTIC_METHOD { get; set; }

	public int LAI_STATISTIC_NUMBER { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal LAI_CLUMP { get; set; }

	public string LAI_METHOD { get; set; }

	public string LAI_APPROACH { get; set; }

	public string LAI_DATE { get; set; }

	public string LAI_DATE_START { get; set; }

	public string LAI_DATE_END { get; set; }

	[Column(TypeName = "decimal(18, 8)")]
public decimal LAI_DATE_UNC { get; set; }

	public string LAI_COMMENT { get; set; }

}
}