using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_URL : BaseClass
{
public GRP_URL(){ GroupId = (int)Globals.Groups.GRP_URL;}

	public string URL { get; set; }

}
}