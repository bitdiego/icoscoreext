using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosModelLib.DTO{

public class GRP_SITE_DESC : BaseClass
{
public GRP_SITE_DESC(){ GroupId = (int)Globals.Groups.GRP_SITE_DESC;}

	public string SITE_DESC_SHORT { get; set; }

	public string URL_PICTURE { get; set; }

	public string URL_KMZ { get; set; }

	public string SITE_DESC { get; set; }

}
}