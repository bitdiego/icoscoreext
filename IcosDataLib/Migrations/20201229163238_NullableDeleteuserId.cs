﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IcosDataLib.Migrations
{
    public partial class NullableDeleteuserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           /* migrationBuilder.CreateTable(
                name: "CLIM_AVG",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MAT = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MAP = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MAR = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MAC_YEARS = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    MAS = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CLIMATE_KOEPPEN = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MAC_COMMENTS = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MAC_DATE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataStatus = table.Column<byte>(type: "tinyint", nullable: false),
                    InsertUserId = table.Column<long>(type: "bigint", nullable: false),
                    InsertDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeleteUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SiteId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CLIM_AVG", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LOCATION",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LOCATION_LAT = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    LOCATION_LONG = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    LOCATION_ELEV = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    LOCATION_DATE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LOCATION_COMMENT = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataStatus = table.Column<byte>(type: "tinyint", nullable: false),
                    InsertUserId = table.Column<long>(type: "bigint", nullable: false),
                    InsertDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeleteUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SiteId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LOCATION", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TEAM",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TEAM_MEMBER_FIRSTNAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_LASTNAME = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_TITLE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_ROLE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_MAIN_EXPERT = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_PERC_ICOS = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_CONTRACT = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_EMAIL = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_ORCID = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_SOCIALMEDIA = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_PHONE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_INSTITUTION = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_INST_STREET = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_INST_POSTCODE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_INST_CITY = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_INST_COUNTRY = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_WORKEND = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_COMMENT = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TEAM_MEMBER_DATE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataStatus = table.Column<byte>(type: "tinyint", nullable: false),
                    InsertUserId = table.Column<long>(type: "bigint", nullable: false),
                    InsertDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeleteUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SiteId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TEAM", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UTC",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UTC_OFFSET = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    UTC_OFFSET_DATE_START = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UTC_OFFSET_COMMENT = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataStatus = table.Column<byte>(type: "tinyint", nullable: false),
                    InsertUserId = table.Column<long>(type: "bigint", nullable: false),
                    InsertDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeleteUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SiteId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UTC", x => x.Id);
                });*/
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
           /* migrationBuilder.DropTable(
                name: "CLIM_AVG");

            migrationBuilder.DropTable(
                name: "LOCATION");

            migrationBuilder.DropTable(
                name: "TEAM");

            migrationBuilder.DropTable(
                name: "UTC");*/
        }
    }
}
