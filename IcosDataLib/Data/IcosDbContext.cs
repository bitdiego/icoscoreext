﻿//using Microsoft.Analytics.Interfaces;
//using Microsoft.Analytics.Types.Sql;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using IcosModelLib.DTO;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.ModelConfiguration;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using IdentitySample.Models;
using IcosModelLib.DTO.PIAreaModels;

namespace IcosDataLib.Data
{
    public class IcosDbContext : IdentityDbContext<ApplicationUser>
    {
        public IcosDbContext(DbContextOptions<IcosDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BADMList>().HasKey(j => j.id_badmlist);
            modelBuilder
    .Entity<IcosSitePI>(builder =>
    {
        builder.HasNoKey();
        //builder.ToTable("MY_ENTITY");
    });
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<BADMList> BADMList { get; set; }
       // public  DbSet<AspNetUsers> GeneralUsers { get; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<SubmittedFile> SubmittedFiles { get; set; }
        public DbSet<FileType> FileType { get; set; }
        public DbSet<Station> Stations { get; set; }
        public DbSet<GRP_TEAM> GRP_TEAM { get; set; }
        public DbSet<GRP_UTC_OFFSET> GRP_UTC_OFFSET { get; set; }
        public DbSet<GRP_CLIM_AVG> GRP_CLIM_AVG { get; set; }
        public DbSet<GRP_DM> GRP_DM { get; set; }
        public DbSet<GRP_PLOT> GRP_PLOT { get; set; }
        public DbSet<GRP_INST> GRP_INST { get; set; }
        public DbSet<GRP_LOGGER> GRP_LOGGER { get; set; }
        public DbSet<GRP_FILE> GRP_FILE { get; set; }
        public DbSet<GRP_EC> GRP_EC { get; set; }
        public DbSet<GRP_ECSYS> GRP_ECSYS { get; set; }
        public DbSet<GRP_ECWEXCL> GRP_ECWEXCL { get; set; }
        public DbSet<GRP_BM> GRP_BM { get; set; }
        public DbSet<GRP_LOCATION> GRP_LOCATION { get; set; }
        public DbSet<GRP_TOWER> GRP_TOWER { get; set; }
        public DbSet<GRP_LAND_OWNERSHIP> GRP_LAND_OWNERSHIP { get; set; }
        public DbSet<GRP_DHP> GRP_DHP { get; set; }
        public DbSet<GRP_CEPT> GRP_CEPT { get; set; }
        public DbSet<GRP_TREE> GRP_TREE { get; set; }
        public DbSet<GRP_FLSM> GRP_FLSM { get; set; }
        public DbSet<GRP_SOSM> GRP_SOSM { get; set; }
        public DbSet<GRP_SPPS> GRP_SPPS { get; set; }
        public DbSet<GRP_AGB> GRP_AGB { get; set; }
        public DbSet<GRP_LITTERPNT> GRP_LITTERPNT { get; set; }
        public DbSet<GRP_WTDPNT> GRP_WTDPNT { get; set; }
        public DbSet<GRP_D_SNOW> GRP_D_SNOW { get; set; }
        public DbSet<GRP_HEADER> GRP_HEADER { get; set; }
        public DbSet<GRP_BULKH> GRP_BULKH { get; set; }
        public DbSet<GRP_GAI> GRP_GAI { get; set; }
        public DbSet<GRP_STO> GRP_STO { get; set; }
        public DbSet<GRP_INSTMAN> GRP_INSTMAN { get; set; }
        public DbSet<GRP_INSTPAIR> GRP_INSTPAIR { get; set; }
        public DbSet<GRP_FUNDING> GRP_FUNDING { get; set; }
        public DbSet<IcosSitePI> IcosSitePIS { get; set; }
        public DbSet<WGUsers> WGUsers { get; set; }
        public DbSet<UsersToWG> UsersToWG { get; set; }
        public DbSet<WorkGroups> WorkGroups { get; set; }
        public DbSet<Fieldbook> Fieldbooks { get; set; }


        /////////////////////////////////
        ///for UI validation facilities
        ///
        public DbSet<StoOps> StoOps { get; set; }
    } 

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<IcosDbContext>
    {
        public IcosDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile(@Directory.GetCurrentDirectory() + "/../IcosCoreExt/appsettings.json").Build();
            var builder = new DbContextOptionsBuilder<IcosDbContext>();
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            builder.UseSqlServer(connectionString);
            return new IcosDbContext(builder.Options);
        }
    }

}