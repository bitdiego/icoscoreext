using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosDataLogicLib.Services.Validators;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class SppsController : Controller
    {
        private readonly IGenericService<GRP_SPPS> _service;
        private readonly IMapper _mapper;
        private readonly ISppsValidator _sppsValidator;

        public SppsController(IGenericService<GRP_SPPS> service, IMapper mapper, ISppsValidator sppsValidator)
        {
            _service = service;
            _mapper = mapper;
            _sppsValidator = sppsValidator;
        }

        // GET: SppsController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_SPPS> Sppss = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_SPPSViewModel> SppssVM = new List<GRP_SPPSViewModel>();
            foreach (var tow in Sppss)
            {
                var towVM = _mapper.Map<GRP_SPPSViewModel>(tow);
                SppssVM.Add(towVM);
            }
            return View(SppssVM);
        }

        // GET: SppsController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_SPPSViewModel towVM = _mapper.Map<GRP_SPPS, GRP_SPPSViewModel>(item);

            return View(towVM);
        }

        // GET: SppsController/Create
        public ActionResult Create()
        {
            GRP_SPPSViewModel SppsViewModel = new GRP_SPPSViewModel();
   
         ModelUtils.FillSelectItemLists(SppsViewModel.SPPS_TWSP_PCTTypes, _service.GetBadmListData((int)Globals.CvIndexes.TWSP)); 
         ModelUtils.FillSelectItemLists(SppsViewModel.SPPS_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.SPPPTYPE)); 


            return View(SppsViewModel);
        }

        // POST: SppsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_SPPSViewModel SppsViewModel)
        {
            bool reload = false;
            if (!ModelState.IsValid)
            {
                reload = true;
            }
            else
            {
                GRP_SPPS model = _mapper.Map<GRP_SPPSViewModel, GRP_SPPS>(SppsViewModel);
                int errorCode = 0, globalError = 0;
                string errorString = "";
                errorCode = await _sppsValidator.SppsPlotExistsAsync(model.SPPS_PLOT, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (errorCode > 0)
                {
                    globalError += errorCode;
                    ModelState.AddModelError("SPPS_PLOT", ErrorCodes.ErrorsList[errorCode]);
                    errorCode = 0;
                }
                errorCode = _sppsValidator.ValidateSpps(model, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (errorCode > 0)
                {
                    ModelState.AddModelError("All", ErrorCodes.ErrorsList[errorCode]);
                    globalError += errorCode;
                }
                if (globalError > 0)
                {
                    reload = true;
                }
                else
                {
                    bool b = await _service.SaveItemAsync(model, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                    if (!b)
                    {
                        reload = true;
                    }
                    
                }
            }


            if (reload)
            {
                ModelUtils.FillSelectItemLists(SppsViewModel.SPPS_TWSP_PCTTypes, _service.GetBadmListData((int)Globals.CvIndexes.TWSP));
                ModelUtils.FillSelectItemLists(SppsViewModel.SPPS_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.SPPPTYPE));
                return View(SppsViewModel);
            }
            return RedirectToAction("Index");
        }

        // GET: SppsController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_SPPSViewModel SppsViewModel = _mapper.Map<GRP_SPPS, GRP_SPPSViewModel>(item);
            ModelUtils.FillSelectItemLists(SppsViewModel.SPPS_TWSP_PCTTypes, _service.GetBadmListData((int)Globals.CvIndexes.TWSP));
            ModelUtils.FillSelectItemLists(SppsViewModel.SPPS_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.SPPPTYPE));


            return View(SppsViewModel);
        }

        // POST: SppsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_SPPSViewModel SppsViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_SPPS loc = _mapper.Map<GRP_SPPSViewModel, GRP_SPPS>(SppsViewModel);
                bool b = await _service.UpdateItemAsync(SppsViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
            ModelUtils.FillSelectItemLists(SppsViewModel.SPPS_TWSP_PCTTypes, _service.GetBadmListData((int)Globals.CvIndexes.TWSP));
            ModelUtils.FillSelectItemLists(SppsViewModel.SPPS_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.SPPPTYPE));

            return View(SppsViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_SPPSViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
