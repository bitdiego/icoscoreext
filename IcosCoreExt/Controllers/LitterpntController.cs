using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosDataLogicLib.Services.Validators;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class LitterpntController : Controller
    {
        private readonly IGenericService<GRP_LITTERPNT> _service;
        private readonly IMapper _mapper;
        private readonly ILitterPntValidator _litterPntValidator;

        public LitterpntController(IGenericService<GRP_LITTERPNT> service, IMapper mapper, ILitterPntValidator litValidator)
        {
            _service = service;
            _mapper = mapper;
            _litterPntValidator = litValidator;
        }

        // GET: LitterpntController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_LITTERPNT> Litterpnts = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_LITTERPNTViewModel> LitterpntsVM = new List<GRP_LITTERPNTViewModel>();
            foreach (var tow in Litterpnts)
            {
                var towVM = _mapper.Map<GRP_LITTERPNTViewModel>(tow);
                LitterpntsVM.Add(towVM);
            }
            return View(LitterpntsVM);
        }

        // GET: LitterpntController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_LITTERPNTViewModel towVM = _mapper.Map<GRP_LITTERPNT, GRP_LITTERPNTViewModel>(item);

            return View(towVM);
        }

        // GET: LitterpntController/Create
        public ActionResult Create()
        {
            GRP_LITTERPNTViewModel LitterpntViewModel = new GRP_LITTERPNTViewModel();
   
             ModelUtils.FillSelectItemLists(LitterpntViewModel.LITTERPNT_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.LITTYPE)); 
             ModelUtils.FillSelectItemLists(LitterpntViewModel.LITTERPNT_FRACTIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.LITFRAC));
            
            return View(LitterpntViewModel);
        }

        // POST: LitterpntController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_LITTERPNTViewModel LitterpntViewModel)
        {
            bool reload = false;
            if (!ModelState.IsValid)
            {
                reload = true;
            }
            else
            {
                GRP_LITTERPNT tow = _mapper.Map<GRP_LITTERPNTViewModel, GRP_LITTERPNT>(LitterpntViewModel);
                int error = 0, globalError = 0;
                string errorString = "";

                error = await _litterPntValidator.ItemInSamplingPointGroupAsync(tow, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    globalError += error;
                    errorString = ErrorCodes.ErrorsList[error];
                    ModelState.AddModelError("LITTERPNT_PLOT", errorString);
                }
                error = _litterPntValidator.ValidateLitterPnt(tow, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    globalError += error;
                    errorString = ErrorCodes.ErrorsList[error];
                    ModelState.AddModelError("All", errorString);
                }
                if (globalError > 0)
                {
                    reload = true;
                }
                else
                {
                    bool b = await _service.SaveItemAsync(tow, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                    reload = !b;
                }
            }
            if (reload)
            {
                ModelUtils.FillSelectItemLists(LitterpntViewModel.LITTERPNT_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.LITTYPE));
                ModelUtils.FillSelectItemLists(LitterpntViewModel.LITTERPNT_FRACTIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.LITFRAC));

                return View(LitterpntViewModel);
            }

            return RedirectToAction("Index");
        }

        // GET: LitterpntController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_LITTERPNTViewModel LitterpntViewModel = _mapper.Map<GRP_LITTERPNT, GRP_LITTERPNTViewModel>(item);
            ModelUtils.FillSelectItemLists(LitterpntViewModel.LITTERPNT_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.LITTYPE));
            ModelUtils.FillSelectItemLists(LitterpntViewModel.LITTERPNT_FRACTIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.LITFRAC));


            return View(LitterpntViewModel);
        }

        // POST: LitterpntController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_LITTERPNTViewModel LitterpntViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_LITTERPNT loc = _mapper.Map<GRP_LITTERPNTViewModel, GRP_LITTERPNT>(LitterpntViewModel);
                bool b = await _service.UpdateItemAsync(LitterpntViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
            ModelUtils.FillSelectItemLists(LitterpntViewModel.LITTERPNT_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.LITTYPE));
            ModelUtils.FillSelectItemLists(LitterpntViewModel.LITTERPNT_FRACTIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.LITFRAC));

            return View(LitterpntViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_LITTERPNTViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
