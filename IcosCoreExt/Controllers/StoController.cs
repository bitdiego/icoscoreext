using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosDataLogicLib.Services.Validators;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class StoController : Controller
    {
        private readonly IGenericService<GRP_STO> _service;
        private readonly IMapper _mapper;
        private readonly IStoValidator<GRP_STO> _iStoValidator;

        public StoController(IGenericService<GRP_STO> service, IMapper mapper, IStoValidator<GRP_STO> stoValidator)
        {
            _service = service;
            _mapper = mapper;
            _iStoValidator = stoValidator;
        }

        // GET: StoController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_STO> Stos = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_STOViewModel> StosVM = new List<GRP_STOViewModel>();
            foreach (var tow in Stos)
            {
                var towVM = _mapper.Map<GRP_STOViewModel>(tow);
                StosVM.Add(towVM);
            }
            return View(StosVM);
        }

        // GET: StoController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_STOViewModel towVM = _mapper.Map<GRP_STO, GRP_STOViewModel>(item);

            return View(towVM);
        }

        // GET: StoController/Create
        public ActionResult Create()
        {
            GRP_STOViewModel StoViewModel = new GRP_STOViewModel();
   
            ModelUtils.FillSelectItemLists(StoViewModel.STO_CONFIGTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_CONFIG)); 
            ModelUtils.FillSelectItemLists(StoViewModel.STO_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TYPE)); 
            ModelUtils.FillSelectItemLists(StoViewModel.STO_PROF_TUBE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBEMAT)); 
            ModelUtils.FillSelectItemLists(StoViewModel.STO_PROF_TUBE_THERMTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBETHERM)); 
            //ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_MODELTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_INST)); 
            ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_VARIABLETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_VAR)); 
            ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_TUBE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBEMAT)); 
            ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_TUBE_THERMTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBETHERM)); 
            ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_CAL_VARIABLETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_VAR)); 

            return View(StoViewModel);
        }

        // POST: StoController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_STOViewModel StoViewModel)
        {
            bool reload = false;
            if (!ModelState.IsValid)
            {
                reload = true;
            }
            else
            {
                GRP_STO model = _mapper.Map<GRP_STOViewModel, GRP_STO>(StoViewModel);
                int error = 0, globalError = 0;
                error = await _iStoValidator.ValidateStoByOpAsync(model, Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {

                    string errorString = ErrorCodes.ErrorsList[error];
                    ModelState.AddModelError("All", errorString);
                    reload = true;
                }
                else
                {
                    bool b = await _service.SaveItemAsync(model,IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                    if (b)
                        return RedirectToAction("Index");
                    else
                        reload = true;
                }
                /*
                if (globalError > 0)
                {
                    reload = true;
                }*/
            }

            if (reload)
            {

                ViewData["sent"] = 1;
                ModelUtils.FillSelectItemLists(StoViewModel.STO_CONFIGTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_CONFIG));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TYPE));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_PROF_TUBE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBEMAT));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_PROF_TUBE_THERMTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBETHERM));
                //ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_MODELTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_INST));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_VARIABLETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_VAR));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_TUBE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBEMAT));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_TUBE_THERMTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBETHERM));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_CAL_VARIABLETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_VAR));

                return View(StoViewModel);
            }
            else
            {
                return RedirectToAction("Index");
            }
            /*
            if (ModelState.IsValid)
            {
                GRP_STO tow = _mapper.Map<GRP_STOViewModel, GRP_STO>(StoViewModel);
                bool b = await _service.SaveItemAsync(tow, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                return RedirectToAction("Index");
            }
            else
            {
                ModelUtils.FillSelectItemLists(StoViewModel.STO_CONFIGTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_CONFIG));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TYPE));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_PROF_TUBE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBEMAT));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_PROF_TUBE_THERMTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBETHERM));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_MODELTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_INST));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_VARIABLETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_VAR));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_TUBE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBEMAT)));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_TUBE_THERMTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBETHERM));
                ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_CAL_VARIABLETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_VAR));

                return View(StoViewModel);
            }*/
        }

        // GET: StoController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_STOViewModel StoViewModel = _mapper.Map<GRP_STO, GRP_STOViewModel>(item);
            ModelUtils.FillSelectItemLists(StoViewModel.STO_CONFIGTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_CONFIG));
            ModelUtils.FillSelectItemLists(StoViewModel.STO_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TYPE));
            ModelUtils.FillSelectItemLists(StoViewModel.STO_PROF_TUBE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBEMAT));
            ModelUtils.FillSelectItemLists(StoViewModel.STO_PROF_TUBE_THERMTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBETHERM));
            //ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_MODELTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_INST));
            ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_VARIABLETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_VAR));
            ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_TUBE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBEMAT));
            ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_TUBE_THERMTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBETHERM));
            ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_CAL_VARIABLETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_VAR));


            return View(StoViewModel);
        }

        // POST: StoController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_STOViewModel StoViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_STO loc = _mapper.Map<GRP_STOViewModel, GRP_STO>(StoViewModel);
                bool b = await _service.UpdateItemAsync(StoViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
            ModelUtils.FillSelectItemLists(StoViewModel.STO_CONFIGTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_CONFIG));
            ModelUtils.FillSelectItemLists(StoViewModel.STO_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TYPE));
            ModelUtils.FillSelectItemLists(StoViewModel.STO_PROF_TUBE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBEMAT));
            ModelUtils.FillSelectItemLists(StoViewModel.STO_PROF_TUBE_THERMTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBETHERM));
            //ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_MODELTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_INST));
            ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_VARIABLETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_VAR));
            ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_TUBE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBEMAT));
            ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_TUBE_THERMTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBETHERM));
            ModelUtils.FillSelectItemLists(StoViewModel.STO_GA_CAL_VARIABLETypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_VAR));

            return View(StoViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_STOViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
