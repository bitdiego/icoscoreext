﻿using IcosDataLogicLib.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IcosModelLib.DTO;
using AutoMapper;
using IcosCoreExt.Models.ViewModels;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
    [Authorize]
    public class ClimateAverageController : Controller
    {
        private readonly IGenericService<GRP_CLIM_AVG> _service;
        private readonly IMapper _mapper;

        public ClimateAverageController(IGenericService<GRP_CLIM_AVG> service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }
        // GET: ClimateAverageController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_CLIM_AVG> climAvgs = await _service.GetItemValuesAsync(Utils.Utils.GetSite(HttpContext));
            List<GRP_CLIM_AVGViewModel> climAvgsVM = new List<GRP_CLIM_AVGViewModel>();
            foreach (var climAvg in climAvgs)
            {
                var climAvgVM = _mapper.Map<GRP_CLIM_AVGViewModel>(climAvg);
                climAvgsVM.Add(climAvgVM);
            }
            return View(climAvgsVM);
        }

        // GET: ClimateAverageController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_CLIM_AVGViewModel climVM = _mapper.Map<GRP_CLIM_AVG, GRP_CLIM_AVGViewModel>(item);

            return View(climVM);
        }

        // GET: ClimateAverageController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ClimateAverageController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_CLIM_AVGViewModel climAvgVM)
        {
            if (ModelState.IsValid)
            {
                GRP_CLIM_AVG climAvg = _mapper.Map<GRP_CLIM_AVGViewModel, GRP_CLIM_AVG>(climAvgVM);
                bool b = await _service.SaveItemAsync(climAvg, Utils.Utils.GetUser(HttpContext), Utils.Utils.GetSite(HttpContext));
                return RedirectToAction("Index");
            }
            return View();
        }

        // GET: ClimateAverageController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_CLIM_AVGViewModel climVM = _mapper.Map<GRP_CLIM_AVG, GRP_CLIM_AVGViewModel>(item);
            return View(climVM);
        }

        // POST: ClimateAverageController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_CLIM_AVGViewModel climVM)
        {
            if (ModelState.IsValid)
            {
                GRP_CLIM_AVG clim = _mapper.Map<GRP_CLIM_AVGViewModel, GRP_CLIM_AVG>(climVM);
                bool b = await _service.UpdateItemAsync(climVM.Id, Utils.Utils.GetSite(HttpContext), Utils.Utils.GetUser(HttpContext), clim);
                if (b)
                    return RedirectToAction("Index");
            }
            return View(climVM);
        }

        // GET: DHPController/Delete/5

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_CLIM_AVGViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
