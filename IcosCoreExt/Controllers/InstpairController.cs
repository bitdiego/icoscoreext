using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosDataLogicLib.Services.Validators;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class InstpairController : Controller
    {
        private readonly IGenericService<GRP_INSTPAIR> _service;
        private readonly IMapper _mapper;
        private readonly IInstPairValidator _instPairValidator;
        public InstpairController(IGenericService<GRP_INSTPAIR> service, IMapper mapper, IInstPairValidator instPairValidator)
        {
            _service = service;
            _mapper = mapper;
            _instPairValidator = instPairValidator;
        }

        // GET: InstpairController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_INSTPAIR> Instpairs = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_INSTPAIRViewModel> InstpairsVM = new List<GRP_INSTPAIRViewModel>();
            foreach (var tow in Instpairs)
            {
                var towVM = _mapper.Map<GRP_INSTPAIRViewModel>(tow);
                InstpairsVM.Add(towVM);
            }
            return View(InstpairsVM);
        }

        // GET: InstpairController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_INSTPAIRViewModel towVM = _mapper.Map<GRP_INSTPAIR, GRP_INSTPAIRViewModel>(item);

            return View(towVM);
        }

        // GET: InstpairController/Create
        public ActionResult Create()
        {
            GRP_INSTPAIRViewModel InstpairViewModel = new GRP_INSTPAIRViewModel();
   
         ModelUtils.FillSelectItemLists(InstpairViewModel.INSTPAIR_MODEL_1Types, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_MODEL)); 
         ModelUtils.FillSelectItemLists(InstpairViewModel.INSTPAIR_MODEL_2Types, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_MODEL)); 


            return View(InstpairViewModel);
        }

        // POST: InstpairController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_INSTPAIRViewModel InstpairViewModel)
        {
            bool reload = false;
            if (!ModelState.IsValid)
            {
                reload = true;
            }
            else
            {
                GRP_INSTPAIR model = _mapper.Map<GRP_INSTPAIRViewModel, GRP_INSTPAIR>(InstpairViewModel);
                int error = 0, globalError = 0;
                string errorString = "";

                error = await _instPairValidator.InstrumentInGrpInst(model, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    globalError += error;
                    errorString = ErrorCodes.ErrorsList[error];
                    ModelState.AddModelError("All", errorString);
                    error = 0;
                }
                error = await _instPairValidator.ValidateInstPairAsync(model, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    globalError += error;
                    errorString = ErrorCodes.ErrorsList[error];
                    ModelState.AddModelError("All", errorString);
                    error = 0;
                }
                if (globalError > 0)
                {
                    reload = true;
                }
                else
                {
                    bool b = await _service.SaveItemAsync(model, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                    if (!b) reload = true;
                }
            }
            if(reload)
            {
                ViewData["sent"] = 1;
                ModelUtils.FillSelectItemLists(InstpairViewModel.INSTPAIR_MODEL_1Types, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_MODEL)); 
                ModelUtils.FillSelectItemLists(InstpairViewModel.INSTPAIR_MODEL_2Types, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_MODEL)); 

                return View(InstpairViewModel);
            }
            return RedirectToAction("Index");
        }

        // GET: InstpairController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_INSTPAIRViewModel InstpairViewModel = _mapper.Map<GRP_INSTPAIR, GRP_INSTPAIRViewModel>(item);
         ModelUtils.FillSelectItemLists(InstpairViewModel.INSTPAIR_MODEL_1Types, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_MODEL)); 
         ModelUtils.FillSelectItemLists(InstpairViewModel.INSTPAIR_MODEL_2Types, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_MODEL)); 

            
            return View(InstpairViewModel);
        }

        // POST: InstpairController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_INSTPAIRViewModel InstpairViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_INSTPAIR loc = _mapper.Map<GRP_INSTPAIRViewModel, GRP_INSTPAIR>(InstpairViewModel);
                bool b = await _service.UpdateItemAsync(InstpairViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
         ModelUtils.FillSelectItemLists(InstpairViewModel.INSTPAIR_MODEL_1Types, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_MODEL)); 
         ModelUtils.FillSelectItemLists(InstpairViewModel.INSTPAIR_MODEL_2Types, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_MODEL)); 

            return View(InstpairViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_INSTPAIRViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
