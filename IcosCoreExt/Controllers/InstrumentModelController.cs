﻿using AutoMapper;
using IcosCoreExt.Models.ViewModels;
using IcosDataLogicLib.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using IcosCoreExt.Models;
using Microsoft.AspNetCore.Authorization;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class InstrumentModelController : Controller
    {
        private readonly IGenericService<GRP_INST> _service;
        private readonly IMapper _mapper;
        private readonly IGeneralInstModelValidator _instGeneral;
        private readonly IBasicInstrumentValidator<GRP_INST> _instService;
        public InstrumentModelController(IGenericService<GRP_INST> service, IMapper mapper, IGeneralInstModelValidator instGeneral, IBasicInstrumentValidator<GRP_INST> instService)
        {
            _service = service;
            _mapper = mapper;
            _instService = instService;
            _instGeneral = instGeneral;
        }
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_INST> instruments = await _service.GetItemValuesAsync(Utils.Utils.GetSite(HttpContext));
            List<GRP_INSTViewModel> instrumentsVM = new List<GRP_INSTViewModel>();
            foreach (var inst in instruments)
            {
                var instVM = _mapper.Map<GRP_INSTViewModel>(inst);
                instrumentsVM.Add(instVM);
            }
            return View(instrumentsVM);
        }

        // GET: Controllers/Team/Create
        public IActionResult Create()
        {
            GRP_INSTViewModel instViewModel = new GRP_INSTViewModel();
            //IQueryable<string> iFactory = _service.GetBadmListData((int)Globals.CvIndexes.INST_FACTORY);
            //ModelUtils.SetSelectItemList(iFactory, instViewModel.InstFactory);
            CreateItemsList(instViewModel);
            return View(instViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create( GRP_INSTViewModel instVM)
        {
            GRP_INST model = _mapper.Map<GRP_INSTViewModel, GRP_INST>(instVM);
            model.SiteId = Utils.Utils.GetSite(HttpContext);
            if (ModelState.IsValid)
            {
                if(String.Compare( model.INST_FACTORY, "purchase", true)!=0)
                { 
                    int inDb  = await _instGeneral.InstrumentInGrpInst(model.INST_MODEL, model.INST_SN, model.INST_DATE, Utils.Utils.GetSite(HttpContext));
                    if (inDb > 0)
                    {
                        ModelState.AddModelError("INST_FACTORY", ErrorCodes.ErrorsList[inDb]);
                        CreateItemsList(instVM);
                        return View(instVM);
                    }
                }
                int errOpCode  =  await _instService.LastExpectedOpByDateAsync(model, Utils.Utils.GetSite(HttpContext));
                int generalError = 0;
                if (errOpCode > 0)
                {
                    ModelState.AddModelError("INST_FACTORY", ErrorCodes.ErrorsList[errOpCode]);
                    generalError += errOpCode;
                }
                errOpCode = _instGeneral.SerialNumberCheck(model.INST_MODEL, model.INST_SN);
                if (errOpCode > 0)
                {
                    string _errorStr = ErrorCodes.ErrorsList[errOpCode];
                    ModelUtils.FormatError(ref _errorStr, "$V0$", instVM.INST_MODEL.ToLower().StartsWith("ga_cp") ? "72H-XXXX" :"HXXXXXX", "$V1$", instVM.INST_SN);
                    ModelState.AddModelError("INST_SN", _errorStr);
                    generalError += errOpCode;
                }
                if (generalError>0)
                {
                    //Why TF is null????
                    CreateItemsList(instVM);
                    return View(instVM);
                }
                else
                {
                    bool b = await _service.SaveItemAsync(model, Utils.Utils.GetUser(HttpContext), Utils.Utils.GetSite(HttpContext));
                }
                return RedirectToAction("Index");
                
            }
            
            return View(instVM);
        }

        public async Task<IActionResult> Details(int? id)
        {
             if (id == null)
             {
                 return NotFound();
             }

            var item = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
             if (item == null)
             {
                 return NotFound();
             }
            
            GRP_INSTViewModel instVM = _mapper.Map<GRP_INST, GRP_INSTViewModel>(item);
            return View(instVM);
        }

        public async Task<IActionResult> AddOperation(int id)
        {
            var item = await _service.GetInstMainValuesByRecordIdAsync((int)Globals.Groups.GRP_INST, Utils.Utils.GetSite(HttpContext), id);
            GRP_INSTViewModel instVM = _mapper.Map<GRP_INST, GRP_INSTViewModel>(item);
            CreateItemsList(instVM);
            instVM.InstFactory.Remove(instVM.InstFactory.Where(c => c.Value.ToLower() == "purchase").Single());
            
            return View("Create", instVM);
        }

        private async Task CreateInstVmListAsync(IGenericService<GRP_INST> _service, List<GRP_INSTViewModel> instrumentsVM)
        {
            IEnumerable<GRP_INST> instruments = await _service.GetItemValuesAsync(Utils.Utils.GetSite(HttpContext));
            foreach (var inst in instruments)
            {
                var instVM = _mapper.Map<GRP_INSTViewModel>(inst);
                instrumentsVM.Add(instVM);
            }
        }


        private void CreateItemsList(GRP_INSTViewModel instViewModel)
        {
            if (instViewModel.InstFactory.Count == 0) 
            { 
                IQueryable<string> iFactory = _service.GetBadmListData((int)Globals.CvIndexes.INST_FACTORY);
                ModelUtils.SetSelectItemList(iFactory, instViewModel.InstFactory);
            }
        }
    }
}
