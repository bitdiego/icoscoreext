using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class HeaderController : Controller
    {
        private readonly IGenericService<GRP_HEADER> _service;
        private readonly IMapper _mapper;
        private readonly IcosDbContext _context;

        public HeaderController(IGenericService<GRP_HEADER> service, IMapper mapper, IcosDbContext context)
        {
            _service = service;
            _mapper = mapper;
            _context = context;
        }

        // GET: HeaderController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_HEADER> Headers = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_HEADERViewModel> HeadersVM = new List<GRP_HEADERViewModel>();
            foreach (var tow in Headers)
            {
                var towVM = _mapper.Map<GRP_HEADERViewModel>(tow);
                HeadersVM.Add(towVM);
            }
            return View(HeadersVM);
        }

        // GET: HeaderController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_HEADERViewModel towVM = _mapper.Map<GRP_HEADER, GRP_HEADERViewModel>(item);

            return View(towVM);
        }

        // GET: HeaderController/Create
        public ActionResult Create()
        {
            GRP_HEADERViewModel HeaderViewModel = new GRP_HEADERViewModel();
   


            return View(HeaderViewModel);
        }

        // POST: HeaderController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_HEADERViewModel HeaderViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_HEADER tow = _mapper.Map<GRP_HEADERViewModel, GRP_HEADER>(HeaderViewModel);
                bool b = await _service.SaveItemAsync(tow, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                return RedirectToAction("Index");
            }
            else
            {

                return View(HeaderViewModel);
            }
        }

        // GET: HeaderController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_HEADERViewModel HeaderViewModel = _mapper.Map<GRP_HEADER, GRP_HEADERViewModel>(item);

            
            return View(HeaderViewModel);
        }

        // POST: HeaderController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_HEADERViewModel HeaderViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_HEADER loc = _mapper.Map<GRP_HEADERViewModel, GRP_HEADER>(HeaderViewModel);
                bool b = await _service.UpdateItemAsync(HeaderViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }

            return View(HeaderViewModel);
        }
    }
}
