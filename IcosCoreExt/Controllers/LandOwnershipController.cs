using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt
{

    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class LandOwnershipController : Controller
{


        private readonly IGenericService<IcosModelLib.DTO.GRP_LAND_OWNERSHIP> _service;
        private readonly IMapper _mapper;
        private readonly IcosDbContext _context;

        public LandOwnershipController(IGenericService<IcosModelLib.DTO.GRP_LAND_OWNERSHIP> service, IMapper mapper, IcosDbContext context)
        {
            _service = service;
            _mapper = mapper;
            _context = context;
        }

        // GET: LAND_OWNERSHIP
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_LAND_OWNERSHIP> lands = await _service.GetItemValuesAsync(Utils.Utils.GetSite(HttpContext));
            List<GRP_LAND_OWNERSHIPViewModel> landsVM = new List<GRP_LAND_OWNERSHIPViewModel>();
            foreach (var land in lands)
            {
                var landVM = _mapper.Map<GRP_LAND_OWNERSHIPViewModel>(land);
                landsVM.Add(landVM);
            }
            return View(landsVM);

        }

        // GET: LAND_OWNERSHIP/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(70, id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_LAND_OWNERSHIPViewModel landVM = _mapper.Map<GRP_LAND_OWNERSHIP, GRP_LAND_OWNERSHIPViewModel>(item);

            return View(landVM);
        }

        // GET: LAND_OWNERSHIP/Create
        public IActionResult Create()
        {
            GRP_LAND_OWNERSHIPViewModel LAND_OWNERSHIPViewModel = new GRP_LAND_OWNERSHIPViewModel();
            ModelUtils.FillSelectItemLists(LAND_OWNERSHIPViewModel.LANDOWNERSHIP, _service.GetBadmListData((int)Globals.CvIndexes.LAND_OWNERSHIP));

            return View(LAND_OWNERSHIPViewModel);
        }

        // POST: LAND_OWNERSHIP/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken] 
        public async Task<IActionResult> Create(GRP_LAND_OWNERSHIPViewModel gRP_LAND_OWNERSHIP) //[Bind("LAND_OWNERSHIP,LAND_OWNER,LAND_DATE,LAND_COMMENT,Id,DataStatus,InsertUserId,InsertDate,DeleteUserId,DeletedDate,SiteId")]
        {
            if (ModelState.IsValid)
            {
                GRP_LAND_OWNERSHIP land = _mapper.Map<GRP_LAND_OWNERSHIPViewModel, GRP_LAND_OWNERSHIP>(gRP_LAND_OWNERSHIP);
                bool b = await _service.SaveItemAsync(land, Utils.Utils.GetUser(HttpContext), Utils.Utils.GetSite(HttpContext));
                return RedirectToAction("Index");
            }
            else
            {
                ModelUtils.FillSelectItemLists(gRP_LAND_OWNERSHIP.LANDOWNERSHIP, _service.GetBadmListData((int)Globals.CvIndexes.LAND_OWNERSHIP));
            }
                return View(gRP_LAND_OWNERSHIP);
        }

        // GET: LAND_OWNERSHIP/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_LAND_OWNERSHIPViewModel LAND_OWNERSHIPViewModel = _mapper.Map<GRP_LAND_OWNERSHIP, GRP_LAND_OWNERSHIPViewModel>(item);
            ModelUtils.FillSelectItemLists(LAND_OWNERSHIPViewModel.LANDOWNERSHIP, _service.GetBadmListData((int)Globals.CvIndexes.LAND_OWNERSHIP));

            return View(LAND_OWNERSHIPViewModel);
        }

        // POST: LAND_OWNERSHIP/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_LAND_OWNERSHIPViewModel gRP_LAND_OWNERSHIP)
        {

            if (ModelState.IsValid)
            { 
                    GRP_LAND_OWNERSHIP loc = _mapper.Map<GRP_LAND_OWNERSHIPViewModel, GRP_LAND_OWNERSHIP>(gRP_LAND_OWNERSHIP);
                    bool b = await _service.UpdateItemAsync(gRP_LAND_OWNERSHIP.Id, Utils.Utils.GetSite(HttpContext), Utils.Utils.GetUser(HttpContext), loc);
                    if (b)
                        return RedirectToAction("Index");
            }
            ModelUtils.FillSelectItemLists(gRP_LAND_OWNERSHIP.LANDOWNERSHIP, _service.GetBadmListData((int)Globals.CvIndexes.LAND_OWNERSHIP));
            return View(gRP_LAND_OWNERSHIP);
        }

        // GET: DHPController/Delete/5

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_LAND_OWNERSHIPViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }


    }
}
