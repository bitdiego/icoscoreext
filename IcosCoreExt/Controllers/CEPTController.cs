using System;
using IcosCoreExt.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosDataLogicLib.Services;
using AutoMapper;
using IcosCoreExt.Models;
using IcosModelLib.Utils;
using IcosDataLogicLib.Services.Validators;
using Microsoft.AspNetCore.Authorization;

namespace IcosCoreExt
{
   [Authorize]
	 public class CEPTController : Controller
    {
        private string Ecosystem { get; set; } = "Grassland";
        private readonly IGenericService<GRP_CEPT> _service;
        private readonly IMapper _mapper;
        private readonly ICeptValidator _ceptValidator;

        public CEPTController(IGenericService<GRP_CEPT> service, IMapper mapper, ICeptValidator ceptVal)
        {
            _service = service;
            _mapper = mapper;
            _ceptValidator = ceptVal;
        }

        // GET: CEPT
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_CEPT> cept = await _service.GetItemValuesAsync(Utils.Utils.GetSite(HttpContext));
            List<GRP_CEPTViewModel> ceptVM = new List<GRP_CEPTViewModel>();
            foreach (var tow in cept)
            {
                var towVM = _mapper.Map<GRP_CEPTViewModel>(tow);
                ceptVM.Add(towVM);
            }
            return View(ceptVM);
            //return View(await _service.GetItemValuesAsync<GRP_CEPT>(70));
        }

        // GET: CEPT/Details/5
        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_CEPTViewModel towVM = _mapper.Map<GRP_CEPT, GRP_CEPTViewModel>(item);

            return View(towVM);
        }

        // GET: CEPT/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CEPT/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CEPT_ELADP,CEPT_ABSORP,CEPT_FIRST,CEPT_LAST,CEPT_COMMENT,CEPT_DATE,Id,DataStatus,InsertUserId,InsertDate,DeleteUserId,DeletedDate,SiteId")] GRP_CEPTViewModel ceptVM)
        {
            GRP_CEPT model = _mapper.Map<GRP_CEPTViewModel, GRP_CEPT>(ceptVM);
            if (ModelState.IsValid)
            {
                bool isAny = ModelUtils.IsAnyPropNotNull<GRP_CEPT>(model);
                if (!isAny)
                {
                    //raise error
                    //return View(vmModel);
                    ModelState.AddModelError("All", ErrorCodes.ErrorsList[(int)Globals.ErrorValidationCodes.NO_VALUE_SUBMITTED]);
                    return View(ceptVM);
                }
                //DIEGO:::
                /*
                 * The grp is very simple: seems to be unnecessary to create a reserved validator 
                 * Anyway, thinking on a final goal to have a bunch of validators that can be called from anywhere,
                 * I decided to develop a CeptValidator
                 * */
                int error = _ceptValidator.ValidateCeptModel(model, Ecosystem);
                if (error > 0)
                {
                    ModelState.AddModelError("All", ErrorCodes.ErrorsList[error]);
                    return View(ceptVM);
                }
                bool b = await _service.SaveItemAsync(model, Utils.Utils.GetUser(HttpContext), Utils.Utils.GetSite(HttpContext));
                return RedirectToAction(nameof(Index));
            }
            return View(ceptVM);
        }

        // GET: CEPT/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_CEPTViewModel ceptVM = _mapper.Map<GRP_CEPT, GRP_CEPTViewModel>(item);

            return View(ceptVM);
        }

        // POST: CEPT/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CEPT_ELADP,CEPT_ABSORP,CEPT_FIRST,CEPT_LAST,CEPT_COMMENT,CEPT_DATE,Id,DataStatus,InsertUserId,InsertDate,DeleteUserId,DeletedDate,SiteId")] GRP_CEPTViewModel ceptVM)
        {
            if (id != ceptVM.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {

                GRP_CEPT cept = _mapper.Map<GRP_CEPTViewModel, GRP_CEPT>(ceptVM);
                bool b = await _service.UpdateItemAsync(ceptVM.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), cept);
                if (b)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(ceptVM);
        }

        // GET: CEPT/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || id <= 0)
            {
                return NotFound();
            }

            var cept = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (cept == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_CEPTViewModel>(cept));
        }

        // POST: CEPT/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            /*var gRP_CEPT = await _context.GRP_CEPT.FindAsync(id);
            _context.GRP_CEPT.Remove(gRP_CEPT);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));*/
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                //raise error....
                return RedirectToAction(nameof(Index));
            }
        }

        /*private bool GRP_CEPTExists(int id)
        {
            return _context.GRP_CEPT.Any(e => e.Id == id);
        }*/
    }
}
