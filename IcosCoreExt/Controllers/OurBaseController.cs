﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace IcosCoreExt.Controllers
{
   [Authorize]
	 public class OurBaseController : Controller
    {
        private  IHttpContextAccessor _httpContextAccessor;

        public OurBaseController()
        {
           
        }

        //[InjectionMethod]
        //public void Initialize(IHttpContextAccessor dep) => this._httpContextAccessor = dep;

        public int Siteid
        {
            get {  _httpContextAccessor = HttpContext.RequestServices.GetService<IHttpContextAccessor>();
                return Utils.Utils.GetSite(_httpContextAccessor.HttpContext); }
        }
    }
}
