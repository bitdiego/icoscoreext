using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosDataLogicLib.Services.Validators;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class InstmanController : Controller
    {
        private readonly IGenericService<GRP_INSTMAN> _service;
        private readonly IMapper _mapper;
        private readonly IInstmanValidator _instmanValidator;
        public InstmanController(IGenericService<GRP_INSTMAN> service, IMapper mapper, IInstmanValidator instmanValidator)
        {
            _service = service;
            _mapper = mapper;
            _instmanValidator = instmanValidator;
        }

        // GET: InstmanController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_INSTMAN> Instmans = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_INSTMANViewModel> InstmansVM = new List<GRP_INSTMANViewModel>();
            foreach (var tow in Instmans)
            {
                var towVM = _mapper.Map<GRP_INSTMANViewModel>(tow);
                InstmansVM.Add(towVM);
            }
            return View(InstmansVM);
        }

        // GET: InstmanController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_INSTMANViewModel towVM = _mapper.Map<GRP_INSTMAN, GRP_INSTMANViewModel>(item);

            return View(towVM);
        }

        // GET: InstmanController/Create
        public ActionResult Create()
        {
            GRP_INSTMANViewModel InstmanViewModel = new GRP_INSTMANViewModel();
   
             ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_MODELTypes, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_MODEL)); 
             ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_TYPE)); 
             ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_HEATTypes, _service.GetBadmListData((int)Globals.CvIndexes.EC_YN)); 
             ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_SHIELDINGTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_SHIELDING)); 
             ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_ASPIRATIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_ASPIRATION)); 
             ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_SA_WIND_FORMATTypes, _service.GetBadmListData((int)Globals.CvIndexes.SA_FORMAT)); 
             ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_SA_GILL_ALIGNTypes, _service.GetBadmListData((int)Globals.CvIndexes.SA_ALIGN)); 
             ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_GA_CP_TUBE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.GA_CP_MATERIAL)); 
             ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_GA_CP_TUBE_THERMTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBETHERM)); 
             ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_GA_CP_MFCTypes, _service.GetBadmListData((int)Globals.CvIndexes.EC_YN)); 

            return View(InstmanViewModel);
        }

        // POST: InstmanController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_INSTMANViewModel InstmanViewModel)
        {
            bool reload = false;
            if (!ModelState.IsValid)
            {
                reload = true;
            }
            else
            {
                //validactions
                int error = 0, globalError = 0;
                string errorString = "";
                GRP_INSTMAN model = _mapper.Map<GRP_INSTMANViewModel, GRP_INSTMAN>(InstmanViewModel);
                error = await _instmanValidator.LastExpectedOpByDateAsync(model, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    globalError += error;
                    errorString = ErrorCodes.ErrorsList[error];
                    ModelState.AddModelError("All", errorString);
                    error = 0;
                }
                error = _instmanValidator.ValidateByInstManType(model, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    globalError += error;
                    errorString = ErrorCodes.ErrorsList[error];
                    ModelState.AddModelError("All", errorString);
                    error = 0;
                }
                if (globalError > 0)
                {
                    
                    reload = true;
                }
                else
                {
                    bool b = await _service.SaveItemAsync(model, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                    if (!b)
                    {
                        reload = true;
                    }
                }
                
                
            }
            if(reload)
            {
                ViewData["sent"] = 1;
                ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_MODELTypes, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_MODEL));
                ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_TYPE));
                ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_HEATTypes, _service.GetBadmListData((int)Globals.CvIndexes.EC_YN));
                ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_SHIELDINGTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_SHIELDING));
                ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_ASPIRATIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_ASPIRATION));
                ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_SA_WIND_FORMATTypes, _service.GetBadmListData((int)Globals.CvIndexes.SA_FORMAT));
                ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_SA_GILL_ALIGNTypes, _service.GetBadmListData((int)Globals.CvIndexes.SA_ALIGN));
                ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_GA_CP_TUBE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.GA_CP_MATERIAL));
                ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_GA_CP_TUBE_THERMTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBETHERM));
                ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_GA_CP_MFCTypes, _service.GetBadmListData((int)Globals.CvIndexes.EC_YN));

                return View(InstmanViewModel);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        // GET: InstmanController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_INSTMANViewModel InstmanViewModel = _mapper.Map<GRP_INSTMAN, GRP_INSTMANViewModel>(item);
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_MODELTypes, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_MODEL));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_TYPE));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_HEATTypes, _service.GetBadmListData((int)Globals.CvIndexes.EC_YN));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_SHIELDINGTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_SHIELDING));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_ASPIRATIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_ASPIRATION));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_SA_WIND_FORMATTypes, _service.GetBadmListData((int)Globals.CvIndexes.SA_FORMAT));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_SA_GILL_ALIGNTypes, _service.GetBadmListData((int)Globals.CvIndexes.SA_ALIGN));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_GA_CP_TUBE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.GA_CP_MATERIAL));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_GA_CP_TUBE_THERMTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBETHERM));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_GA_CP_MFCTypes, _service.GetBadmListData((int)Globals.CvIndexes.EC_YN));


            return View(InstmanViewModel);
        }

        // POST: InstmanController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_INSTMANViewModel InstmanViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_INSTMAN loc = _mapper.Map<GRP_INSTMANViewModel, GRP_INSTMAN>(InstmanViewModel);
                bool b = await _service.UpdateItemAsync(InstmanViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_MODELTypes, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_MODEL));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.INSTMAN_TYPE));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_HEATTypes, _service.GetBadmListData((int)Globals.CvIndexes.EC_YN));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_SHIELDINGTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_SHIELDING));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_ASPIRATIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_ASPIRATION));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_SA_WIND_FORMATTypes, _service.GetBadmListData((int)Globals.CvIndexes.SA_FORMAT));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_SA_GILL_ALIGNTypes, _service.GetBadmListData((int)Globals.CvIndexes.SA_ALIGN));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_GA_CP_TUBE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.GA_CP_MATERIAL));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_GA_CP_TUBE_THERMTypes, _service.GetBadmListData((int)Globals.CvIndexes.STO_TUBETHERM));
            ModelUtils.FillSelectItemLists(InstmanViewModel.INSTMAN_GA_CP_MFCTypes, _service.GetBadmListData((int)Globals.CvIndexes.EC_YN));

            return View(InstmanViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_INSTMANViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
