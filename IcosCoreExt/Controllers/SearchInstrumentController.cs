﻿using IcosDataLib.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace IcosCoreExt.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
   [Authorize]
	 public class SearchInstrumentController : ControllerBase
    {
        private IcosDbContext db;

        public SearchInstrumentController(IcosDbContext _db)
        {
            db = _db;
        }

        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [Produces("application/json")]
        [HttpGet("search")]
        public async Task<IEnumerable<string>> Search()
        {
            try
            {
                string term = HttpContext.Request.Query["query"].ToString();
                string extra = HttpContext.Request.Query["extra"].ToString();
                var names = await db.BADMList.Where(cv => cv.cv_index == int.Parse(extra) && cv.shortname.Contains(term))
                                             .Select(n => n.shortname).ToListAsync();
                
                return names;
                    //Ok(names);

            }
            catch
            {
                return null;// BadRequest();
            }
        }

        /*[Produces("application/json")]
        [HttpGet("searchsnbymodelandsite")]
        public async Task<IEnumerable<string>> SearchSnByModelAndSite()
        {
            try
            {
                string model = HttpContext.Request.Query["model"].ToString();
                string siteId = HttpContext.Request.Query["siteId"].ToString();
                var names = await db.GRP_INST.Where(inst=>inst.DataStatus==0 && inst.INST_MODEL == model && inst.SiteId==int.Parse(siteId) && inst.INST_SN.Contains(term))
                                             .Select(n => n.INST_SN).ToListAsync();

                return names;
                //Ok(names);

            }
            catch
            {
                return null;// BadRequest();
            }
        }*/
    }

}
