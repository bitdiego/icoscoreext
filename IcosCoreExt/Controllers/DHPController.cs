﻿using AutoMapper;
using IcosDataLogicLib.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IcosModelLib.DTO;
using IcosCoreExt.Models.ViewModels;
using IcosCoreExt.Models;
using IcosModelLib.Utils;

namespace IcosCoreExt.Controllers
{
   [Authorize]
	 public class DHPController : Controller
    {
        private readonly IGenericService<GRP_DHP> _service;
        private readonly IMapper _mapper;

        public DHPController(IGenericService<GRP_DHP> service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }
        // GET: DHPController
        public async Task<ActionResult> Index()
        {
            IEnumerable<GRP_DHP> dhps = await _service.GetItemValuesAsync(Utils.Utils.GetSite(HttpContext));
            List<GRP_DHPViewModel> dhpVMs = new List<GRP_DHPViewModel>();
            foreach (var dhp in dhps)
            {
                var dhpVM = _mapper.Map<GRP_DHPViewModel>(dhp);
                dhpVMs.Add(dhpVM);
            }
            return View(dhpVMs);
        }

        // GET: DHPController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_DHPViewModel dhpVM = _mapper.Map<GRP_DHP, GRP_DHPViewModel>(item);

            return View(dhpVM);
        }

        // GET: DHPController/Create
        public ActionResult Create()
        {
            GRP_DHPViewModel dhpVM = new GRP_DHPViewModel();
            ModelUtils.FillSelectItemLists(dhpVM.Cameras, _service.GetBadmListData((int)Globals.CvIndexes.CAMERA));
            ModelUtils.FillSelectItemLists(dhpVM.Lenses, _service.GetBadmListData((int)Globals.CvIndexes.LENS));
            return View(dhpVM);
        }

        // POST: DHPController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_DHPViewModel dhpVM)
        {
            if (ModelState.IsValid)
            {
                GRP_DHP dhp = _mapper.Map<GRP_DHPViewModel, GRP_DHP>(dhpVM);
                bool b = await _service.SaveItemAsync(dhp, Utils.Utils.GetUser(HttpContext), Utils.Utils.GetSite(HttpContext));
                return RedirectToAction("Index");
            }
            else{
                ModelUtils.FillSelectItemLists(dhpVM.Cameras, _service.GetBadmListData((int)Globals.CvIndexes.CAMERA));
                ModelUtils.FillSelectItemLists(dhpVM.Lenses, _service.GetBadmListData((int)Globals.CvIndexes.LENS));
                return View(dhpVM);
            }
        }

        // GET: DHPController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_DHPViewModel dhpVM = _mapper.Map<GRP_DHP, GRP_DHPViewModel>(item);
            ModelUtils.FillSelectItemLists(dhpVM.Cameras, _service.GetBadmListData((int)Globals.CvIndexes.CAMERA));
            ModelUtils.FillSelectItemLists(dhpVM.Lenses, _service.GetBadmListData((int)Globals.CvIndexes.LENS));
            return View(dhpVM);
        }

        // POST: DHPController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_DHPViewModel dhpVM)
        {
            if (ModelState.IsValid)
            {
                GRP_DHP dhp = _mapper.Map<GRP_DHPViewModel, GRP_DHP>(dhpVM);
                bool b = await _service.UpdateItemAsync(dhpVM.Id, Utils.Utils.GetSite(HttpContext), Utils.Utils.GetUser(HttpContext), dhp);
                if (b)
                    return RedirectToAction("Index");
            }
            ModelUtils.FillSelectItemLists(dhpVM.Cameras, _service.GetBadmListData((int)Globals.CvIndexes.CAMERA));
            ModelUtils.FillSelectItemLists(dhpVM.Lenses, _service.GetBadmListData((int)Globals.CvIndexes.LENS));
            return View(dhpVM);
        }

        // GET: DHPController/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_DHPViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, Utils.Utils.GetSite(HttpContext), Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
