using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosDataLogicLib.Services.Validators;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
    [Authorize]
	 public class WtdpntController : Controller
    {
        private readonly IGenericService<GRP_WTDPNT> _service;
        private readonly IMapper _mapper;
        private readonly IWtdPntValidator _wtdPntValidator;
        public WtdpntController(IGenericService<GRP_WTDPNT> service, IMapper mapper, IWtdPntValidator wtdValidator)
        {
            _service = service;
            _mapper = mapper;
            _wtdPntValidator = wtdValidator;
        }

        // GET: WtdpntController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_WTDPNT> Wtdpnts = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_WTDPNTViewModel> WtdpntsVM = new List<GRP_WTDPNTViewModel>();
            foreach (var tow in Wtdpnts)
            {
                var towVM = _mapper.Map<GRP_WTDPNTViewModel>(tow);
                WtdpntsVM.Add(towVM);
            }
            return View(WtdpntsVM);
        }

        // GET: WtdpntController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_WTDPNTViewModel towVM = _mapper.Map<GRP_WTDPNT, GRP_WTDPNTViewModel>(item);

            return View(towVM);
        }

        // GET: WtdpntController/Create
        public ActionResult Create()
        {
            GRP_WTDPNTViewModel WtdpntViewModel = new GRP_WTDPNTViewModel();
   


            return View(WtdpntViewModel);
        }

        // POST: WtdpntController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_WTDPNTViewModel WtdpntViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_WTDPNT model = _mapper.Map<GRP_WTDPNTViewModel, GRP_WTDPNT>(WtdpntViewModel);
                string errorString = "";
                int globalError = 0;
                int error = await _wtdPntValidator.ItemInSamplingPointGroupAsync(model, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    errorString = ErrorCodes.ErrorsList[error];
                    ModelState.AddModelError("WTDPNT_PLOT", errorString);
                    globalError += error;
                }

                error = _wtdPntValidator.ValidateWtdPnt(model, IcosCoreExt.Utils.Utils.GetSite(HttpContext));

                if (error > 0)
                {
                    globalError += error;
                    errorString = ErrorCodes.ErrorsList[error];

                    ModelState.AddModelError("All", errorString);

                }
                if (globalError > 0)
                {
                    return View(WtdpntViewModel);
                }
                else
                {
                    bool b = await _service.SaveItemAsync(model, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                    return RedirectToAction("Index");
                }
            }
            else
            {

                return View(WtdpntViewModel);
            }
        }

        // GET: WtdpntController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_WTDPNTViewModel WtdpntViewModel = _mapper.Map<GRP_WTDPNT, GRP_WTDPNTViewModel>(item);

            
            return View(WtdpntViewModel);
        }

        // POST: WtdpntController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_WTDPNTViewModel WtdpntViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_WTDPNT loc = _mapper.Map<GRP_WTDPNTViewModel, GRP_WTDPNT>(WtdpntViewModel);
                bool b = await _service.UpdateItemAsync(WtdpntViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }

            return View(WtdpntViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_WTDPNTViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
