using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class EcwexclController : Controller
    {
        private readonly IGenericService<GRP_ECWEXCL> _service;
        private readonly IMapper _mapper;

        public EcwexclController(IGenericService<GRP_ECWEXCL> service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: EcwexclController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_ECWEXCL> Ecwexcls = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_ECWEXCLViewModel> EcwexclsVM = new List<GRP_ECWEXCLViewModel>();
            foreach (var tow in Ecwexcls)
            {
                var towVM = _mapper.Map<GRP_ECWEXCLViewModel>(tow);
                EcwexclsVM.Add(towVM);
            }
            return View(EcwexclsVM);
        }

        // GET: EcwexclController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_ECWEXCLViewModel towVM = _mapper.Map<GRP_ECWEXCL, GRP_ECWEXCLViewModel>(item);

            return View(towVM);
        }

        // GET: EcwexclController/Create
        public ActionResult Create()
        {
            GRP_ECWEXCLViewModel EcwexclViewModel = new GRP_ECWEXCLViewModel();
   
         ModelUtils.FillSelectItemLists(EcwexclViewModel.ECWEXCL_ACTIONTypes, _service.GetBadmListData(130)); 


            return View(EcwexclViewModel);
        }

        // POST: EcwexclController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_ECWEXCLViewModel EcwexclViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_ECWEXCL tow = _mapper.Map<GRP_ECWEXCLViewModel, GRP_ECWEXCL>(EcwexclViewModel);
                bool b = await _service.SaveItemAsync(tow, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                return RedirectToAction("Index");
            }
            else
            {
         ModelUtils.FillSelectItemLists(EcwexclViewModel.ECWEXCL_ACTIONTypes, _service.GetBadmListData(130)); 

                return View(EcwexclViewModel);
            }
        }

        // GET: EcwexclController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_ECWEXCLViewModel EcwexclViewModel = _mapper.Map<GRP_ECWEXCL, GRP_ECWEXCLViewModel>(item);
         ModelUtils.FillSelectItemLists(EcwexclViewModel.ECWEXCL_ACTIONTypes, _service.GetBadmListData(130)); 

            
            return View(EcwexclViewModel);
        }

        // POST: EcwexclController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_ECWEXCLViewModel EcwexclViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_ECWEXCL loc = _mapper.Map<GRP_ECWEXCLViewModel, GRP_ECWEXCL>(EcwexclViewModel);
                bool b = await _service.UpdateItemAsync(EcwexclViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
            ModelUtils.FillSelectItemLists(EcwexclViewModel.ECWEXCL_ACTIONTypes, _service.GetBadmListData(130)); 

            return View(EcwexclViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_ECWEXCLViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
