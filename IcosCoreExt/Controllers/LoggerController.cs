using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class LoggerController : Controller
    {
        private readonly IGenericService<GRP_LOGGER> _service;
        private readonly IMapper _mapper;
        private readonly IGeneralInstModelValidator _instModelValidator;

        public LoggerController(IGenericService<GRP_LOGGER> service, IMapper mapper, IGeneralInstModelValidator instModelValidator)
        {
            _service = service;
            _mapper = mapper;
            _instModelValidator = instModelValidator;
        }

        // GET: LoggerController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_LOGGER> Loggers = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_LOGGERViewModel> LoggersVM = new List<GRP_LOGGERViewModel>();
            foreach (var tow in Loggers)
            {
                var towVM = _mapper.Map<GRP_LOGGERViewModel>(tow);
                LoggersVM.Add(towVM);
            }
            return View(LoggersVM);
        }

        // GET: LoggerController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            if (id <= 0)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_LOGGERViewModel towVM = _mapper.Map<GRP_LOGGER, GRP_LOGGERViewModel>(item);

            return View(towVM);
        }

        // GET: LoggerController/Create
        public ActionResult Create()
        {
            GRP_LOGGERViewModel LoggerViewModel = new GRP_LOGGERViewModel();
   
          //  ModelUtils.FillSelectItemLists(LoggerViewModel.LOGGER_MODEL, _service.GetBadmListData((int)Globals.CvIndexes.LOGGER_MODEL)); 


            return View(LoggerViewModel);
        }

        // POST: LoggerController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_LOGGERViewModel LoggerViewModel)
        {
            GRP_LOGGER tow = _mapper.Map<GRP_LOGGERViewModel, GRP_LOGGER>(LoggerViewModel);
            if (ModelState.IsValid)
            {
                //validation steps:
                //is instrument in GRP_INST could be unnecessary, as api returns sensors list available
                //logger_id, logger_date, logger_model and logger_sn are all required: are dataannotation enough??
                //
                int iLoggerValid = await _instModelValidator.UniqueLoggerIdAsync(tow.LOGGER_MODEL, tow.LOGGER_SN, tow.LOGGER_ID, tow.LOGGER_DATE, 70);
                if (iLoggerValid > 0)
                {
                    ViewData["sent"] = 1;
                    return View(LoggerViewModel);
                }
                bool b = await _service.SaveItemAsync(tow, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                return RedirectToAction("Index");
            }
            else
            {
            //ModelUtils.FillSelectItemLists(LOGGERViewModel.LOGGER_MODEL, _service.GetBadmListData((int)Globals.CvIndexes.LOGGER_MODEL)); 

                return View(LoggerViewModel);
            }
        }

        // GET: LoggerController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_LOGGERViewModel LoggerViewModel = _mapper.Map<GRP_LOGGER, GRP_LOGGERViewModel>(item);
           // ModelUtils.FillSelectItemLists(LOGGERViewModel.LOGGER_MODEL, _service.GetBadmListData((int)Globals.CvIndexes.LOGGER_MODEL)); 

            
            return View(LoggerViewModel);
        }

        // POST: LoggerController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_LOGGERViewModel LoggerViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_LOGGER loc = _mapper.Map<GRP_LOGGERViewModel, GRP_LOGGER>(LoggerViewModel);
                bool b = await _service.UpdateItemAsync(LoggerViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
           // ModelUtils.FillSelectItemLists(LOGGERViewModel.LOGGER_MODEL, _service.GetBadmListData((int)Globals.CvIndexes.LOGGER_MODEL)); 

            return View(LoggerViewModel);
        }

        private int IsUniqueLoggerId(GRP_LOCATIONViewModel loggerVM, int siteId)
        {

            return 0;
        }
    }
}
