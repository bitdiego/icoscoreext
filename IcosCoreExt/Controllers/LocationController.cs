using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosDataLogicLib.Services;
using AutoMapper;
using IcosCoreExt.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class LocationController : Controller
    {
        private readonly IGenericService<GRP_LOCATION> _service;
        private readonly IMapper _mapper;
        private readonly IcosDbContext _context;

        public LocationController(IGenericService<GRP_LOCATION> service, IMapper mapper, IcosDbContext context)
        {
            _service = service;
            _mapper = mapper;
            _context = context;
        }

        // GET: Location
        public async Task<IActionResult> Index()
        {   
            IEnumerable<GRP_LOCATION> locations = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_LOCATIONViewModel> locationsVM = new List<GRP_LOCATIONViewModel>();
            foreach (var loc in locations)
            {
                var locVM = _mapper.Map<GRP_LOCATIONViewModel>(loc);
                locationsVM.Add(locVM);
            }
            ViewBag.SessionSite = IcosCoreExt.Utils.Utils.GetSite(HttpContext);
            return View(locationsVM);
        }

        // GET: Location/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_LOCATIONViewModel locVM = _mapper.Map<GRP_LOCATION, GRP_LOCATIONViewModel>(item);

            return View(locVM);
        }

        // GET: Location/Create
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_LOCATIONViewModel locVM)
        {
            if (ModelState.IsValid)
            {
                GRP_LOCATION loc = _mapper.Map<GRP_LOCATIONViewModel, GRP_LOCATION>(locVM);
                bool b = await _service.SaveItemAsync(loc, Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                return RedirectToAction("Index");
            }
            return View();
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_LOCATIONViewModel locVM = _mapper.Map<GRP_LOCATION, GRP_LOCATIONViewModel>(item);
            return View(locVM);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_LOCATIONViewModel locVM)
        {
            if (ModelState.IsValid)
            {
                GRP_LOCATION loc = _mapper.Map<GRP_LOCATIONViewModel, GRP_LOCATION>(locVM);
                bool b = await _service.UpdateItemAsync(locVM.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), Utils.Utils.GetUser(HttpContext), loc);
                if(b)
                    return RedirectToAction("Index");
            }
            return View(locVM);
        }

        // GET: DHPController/Delete/5

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_LOCATIONViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
