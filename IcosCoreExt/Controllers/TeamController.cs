using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using IcosDataLogicLib.Services;
using AutoMapper;
using IcosCoreExt.Models.ViewModels;
using IcosCoreExt.Models;
using IcosDataLogicLib.Services.Validators;
using Microsoft.AspNetCore.Authorization;
using IdentitySample.Models;
using IcosCoreExt.Controllers;
using Microsoft.AspNetCore.Identity;
using System;

namespace IcosCoreExt
{
    [Route("/PI/{controller}/{action}")]
    [Authorize]
    public class TeamController : Controller
    {
        private readonly IGenericService<GRP_TEAM> _service;
        private readonly IMapper _mapper;
        private readonly ITeamValidator _teamValidator;
        private readonly UserManager<ApplicationUser> _userManager;
        public TeamController(IGenericService<GRP_TEAM> service, IMapper mapper, ITeamValidator validator, UserManager<ApplicationUser> userManager)
        {
            _service = service;
            _mapper = mapper;
            _teamValidator = validator;
            _userManager = userManager;
        }

        // GET: Controllers/Team
        public async Task<IActionResult> Index()
        {

            // check if existing email
            var xx = await _userManager.FindByEmailAsync("aciaccia@unitus.it");
            if (xx != null)
            {
                xx.Email = "aciaccia2@unitus.it";
                string password = Utils.Utils.GenerateRandomPassword(); // "Pis192021";
                xx.UserName = "aciaccia2@unitus.it";
                
                var result = await _userManager.UpdateAsync(xx);

            }

            IEnumerable<GRP_TEAM> teamMembers = await _service.GetItemValuesAsync(Utils.Utils.GetSite(HttpContext));
            List<GRP_TEAMViewModel> teamMembersVM = new List<GRP_TEAMViewModel>();
            foreach (var tm in teamMembers)
            {
                var tmVM = _mapper.Map<GRP_TEAMViewModel>(tm);
                teamMembersVM.Add(tmVM);
            }

            return View(teamMembersVM);
        }

        // GET: Controllers/Team/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var team = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (team == null)
            {
                return NotFound();
            }
            GRP_TEAMViewModel teamVM = _mapper.Map<GRP_TEAM, GRP_TEAMViewModel>(team);

            return View(teamVM);
        }

        // GET: Controllers/Team/Create
        public IActionResult Create()
        {
            GRP_TEAMViewModel teamViewModel = new GRP_TEAMViewModel();

            ModelUtils.FillSelectItemLists(teamViewModel.Roles, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_ROLE));
            ModelUtils.FillSelectItemLists(teamViewModel.Percs, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_PERC));
            ModelUtils.FillSelectItemLists(teamViewModel.Contracts, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_CONTR));
            ModelUtils.FillSelectItemLists(teamViewModel.Experts, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_EXP));

            return View(teamViewModel);
        }

        // POST: Controllers/Team/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(/*[Bind("TEAM_MEMBER_FIRSTNAME,TEAM_MEMBER_LASTNAME,TEAM_MEMBER_TITLE,TEAM_MEMBER_ROLE,TEAM_MEMBER_MAIN_EXPERT,TEAM_MEMBER_PERC_ICOS,TEAM_MEMBER_CONTRACT,TEAM_MEMBER_EMAIL,TEAM_MEMBER_ORCID,TEAM_MEMBER_SOCIALMEDIA,TEAM_MEMBER_PHONE,TEAM_MEMBER_INSTITUTION,TEAM_MEMBER_INST_STREET,TEAM_MEMBER_INST_POSTCODE,TEAM_MEMBER_INST_CITY,TEAM_MEMBER_INST_COUNTRY,TEAM_MEMBER_WORKEND,TEAM_MEMBER_COMMENT,TEAM_MEMBER_DATE,TEAM_MEMBER_AUTHORDER,Id,DataStatus,InsertUserId,InsertDate,DeleteUserId,DeletedDate,SiteId")]*/ GRP_TEAMViewModel teamVM)
        {
            bool reload = false;
            GRP_TEAM model = _mapper.Map<GRP_TEAMViewModel, GRP_TEAM>(teamVM);
            if (!ModelState.IsValid)
            {
                reload = true;
            }
            else
            {
                var cb = await _teamValidator.WorkEndValidatorAsync(model, Utils.Utils.GetSite(HttpContext));
                if (cb > 0)
                {
                    reload = true;
                    //return View(teamVM);
                }

                if (!await _service.SaveItemAsync(model, Utils.Utils.GetUser(HttpContext), Utils.Utils.GetSite(HttpContext)))
                {
                    reload = true;
                }
                else
                {
                    // check if existing email
                    var xx = await _userManager.FindByEmailAsync(model.TEAM_MEMBER_EMAIL);
                    if (xx == null)
                    {
                        var xu = new ApplicationUser();
                        xu.Email = model.TEAM_MEMBER_EMAIL;
                        string password = Utils.Utils.GenerateRandomPassword(); // "Pis192021";
                        xu.UserName = "info@icos-etc.eu";
                        xu.LegacyID = 0;
                        var result = await _userManager.CreateAsync(xu, password);

                    }

                }
                //return RedirectToAction(nameof(Index));
            }
            if (reload)
            {
                ModelUtils.FillSelectItemLists(teamVM.Roles, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_ROLE));
                ModelUtils.FillSelectItemLists(teamVM.Percs, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_PERC));
                ModelUtils.FillSelectItemLists(teamVM.Contracts, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_CONTR));
                ModelUtils.FillSelectItemLists(teamVM.Experts, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_EXP));
                return View(teamVM);
            }
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> AddWorkend(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var team = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (team == null)
            {
                return NotFound();
            }
            GRP_TEAMViewModel teamVM = _mapper.Map<GRP_TEAM, GRP_TEAMViewModel>(team);

            return View(teamVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddWorkend(GRP_TEAMViewModel teamVM)
        {
            bool reload = false;
            GRP_TEAM model = _mapper.Map<GRP_TEAMViewModel, GRP_TEAM>(teamVM);
            if (!ModelState.IsValid)
            {
                reload = true;
            }
            else
            {
                var cb = await _teamValidator.WorkEndValidatorAsync(model, Utils.Utils.GetSite(HttpContext));
                if (cb > 0)
                {
                    reload = true;
                }

                if (!await _service.SaveItemAsync(model, Utils.Utils.GetUser(HttpContext), Utils.Utils.GetSite(HttpContext)))
                {
                    reload = true;
                }
                //return RedirectToAction(nameof(Index));
            }
            if (reload)
            {
                return View(teamVM);
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: Controllers/Team/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var team = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (team == null)
            {
                return NotFound();
            }
            GRP_TEAMViewModel teamViewModel = _mapper.Map<GRP_TEAM, GRP_TEAMViewModel>(team);
            ModelUtils.FillSelectItemLists(teamViewModel.Roles, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_ROLE));
            ModelUtils.FillSelectItemLists(teamViewModel.Percs, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_PERC));
            ModelUtils.FillSelectItemLists(teamViewModel.Contracts, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_CONTR));
            ModelUtils.FillSelectItemLists(teamViewModel.Experts, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_EXP));
            teamViewModel.Original_Email = teamViewModel.TEAM_MEMBER_EMAIL;
            return View(teamViewModel);
        }

        // POST: Controllers/Team/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TEAM_MEMBER_FIRSTNAME,TEAM_MEMBER_LASTNAME,TEAM_MEMBER_TITLE,TEAM_MEMBER_ROLE,TEAM_MEMBER_MAIN_EXPERT,TEAM_MEMBER_PERC_ICOS,TEAM_MEMBER_CONTRACT,TEAM_MEMBER_EMAIL,TEAM_MEMBER_ORCID,TEAM_MEMBER_SOCIALMEDIA,TEAM_MEMBER_PHONE,TEAM_MEMBER_INSTITUTION,TEAM_MEMBER_INST_STREET,TEAM_MEMBER_INST_POSTCODE,TEAM_MEMBER_INST_CITY,TEAM_MEMBER_INST_COUNTRY,TEAM_MEMBER_WORKEND,TEAM_MEMBER_COMMENT,TEAM_MEMBER_DATE,TEAM_MEMBER_AUTHORDER,Id,DataStatus,InsertUserId,InsertDate,DeleteUserId,DeletedDate,SiteId")] GRP_TEAMViewModel teamVM)
        {
            bool reload = false;
            GRP_TEAM model = _mapper.Map<GRP_TEAMViewModel, GRP_TEAM>(teamVM);
            if (!ModelState.IsValid)
            {
                reload = true;
            }
            else
            {
                var cb = await _teamValidator.WorkEndValidatorAsync(model, Utils.Utils.GetSite(HttpContext));
                if (cb > 0)
                {
                    reload = true;
                    //return View(teamVM);
                }

                if (!await _service.UpdateItemAsync(id, Utils.Utils.GetSite(HttpContext), Utils.Utils.GetUser(HttpContext), model))
                {
                    reload = true;
                }
                else  // update identity
                {
                    var xuser = await _userManager.FindByEmailAsync(teamVM.Original_Email);
                    if (xuser != null && (teamVM.Original_Email!=teamVM.TEAM_MEMBER_EMAIL || xuser.Name != teamVM.TEAM_MEMBER_FIRSTNAME +" " + teamVM.TEAM_MEMBER_LASTNAME))
                    {
                        xuser.Email = teamVM.TEAM_MEMBER_EMAIL;
                      //  string password = Utils.Utils.GenerateRandomPassword(); // "Pis192021";
                        xuser.UserName = teamVM.TEAM_MEMBER_EMAIL;
                        xuser.Name = teamVM.TEAM_MEMBER_FIRSTNAME + " " + teamVM.TEAM_MEMBER_LASTNAME;
                        if (teamVM.TEAM_MEMBER_WORKEND!=null && teamVM.TEAM_MEMBER_WORKEND.Length>0)
                        {
                            DateTime workend;
                            if ( DateTime.TryParse(teamVM.TEAM_MEMBER_WORKEND,out workend))
                            {
                                xuser.EndDate = workend;
                            }  
                        }
                        var result = await _userManager.UpdateAsync(xuser);
                        // TODO : check the result state!!
                    }

                }
                //return RedirectToAction(nameof(Index));
            }
            if (reload)
            {
                ModelUtils.FillSelectItemLists(teamVM.Roles, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_ROLE));
                ModelUtils.FillSelectItemLists(teamVM.Percs, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_PERC));
                ModelUtils.FillSelectItemLists(teamVM.Contracts, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_CONTR));
                ModelUtils.FillSelectItemLists(teamVM.Experts, _service.GetBadmListData((int)Globals.CvIndexes.TEAM_EXP));
                return View(teamVM);
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: Controllers/Team/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            /* if (id == null)
             {
                 return NotFound();
             }

             var tEAM = await _context.TEAM
                 .FirstOrDefaultAsync(m => m.Id == id);
             if (tEAM == null)
             {
                 return NotFound();
             }
            */
            GRP_TEAMViewModel team = null;
            return View(team);
        }

        // POST: Controllers/Team/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            /* var tEAM = await _context.TEAM.FindAsync(id);
             _context.TEAM.Remove(tEAM);
             await _context.SaveChangesAsync();*/
            return RedirectToAction(nameof(Index));
        }

        private bool TEAMExists(int id)
        {
            return true;// _context.TEAM.Any(e => e.Id == id);
        }

        /********************************/
        private void SetSelectItemList(IQueryable<string> iq, List<SelectListItem> list)
        {
            foreach (var item in iq)
            {
                list.Add(new SelectListItem(item, item));
            }
        }
    }
}
