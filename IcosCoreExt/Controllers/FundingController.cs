using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class FundingController : Controller
    {
        private readonly IGenericService<GRP_FUNDING> _service;
        private readonly IMapper _mapper;

        public FundingController(IGenericService<GRP_FUNDING> service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: FundingController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_FUNDING> Fundings = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_FUNDINGViewModel> FundingsVM = new List<GRP_FUNDINGViewModel>();
            foreach (var tow in Fundings)
            {
                var towVM = _mapper.Map<GRP_FUNDINGViewModel>(tow);
                FundingsVM.Add(towVM);
            }
            return View(FundingsVM);
        }

        // GET: FundingController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_FUNDINGViewModel towVM = _mapper.Map<GRP_FUNDING, GRP_FUNDINGViewModel>(item);

            return View(towVM);
        }

        // GET: FundingController/Create
        public ActionResult Create()
        {
            GRP_FUNDINGViewModel FundingViewModel = new GRP_FUNDINGViewModel();
   


            return View(FundingViewModel);
        }

        // POST: FundingController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_FUNDINGViewModel FundingViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_FUNDING tow = _mapper.Map<GRP_FUNDINGViewModel, GRP_FUNDING>(FundingViewModel);
                bool b = await _service.SaveItemAsync(tow, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                return RedirectToAction("Index");
            }
            else
            {

                return View(FundingViewModel);
            }
        }

        // GET: FundingController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_FUNDINGViewModel FundingViewModel = _mapper.Map<GRP_FUNDING, GRP_FUNDINGViewModel>(item);

            
            return View(FundingViewModel);
        }

        // POST: FundingController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_FUNDINGViewModel FundingViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_FUNDING loc = _mapper.Map<GRP_FUNDINGViewModel, GRP_FUNDING>(FundingViewModel);
                bool b = await _service.UpdateItemAsync(FundingViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }

            return View(FundingViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_FUNDINGViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
