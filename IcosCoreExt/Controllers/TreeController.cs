using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosDataLogicLib.Services.Validators;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class TreeController : Controller
    {
        private readonly IGenericService<GRP_TREE> _service;
        private readonly IMapper _mapper;
        private readonly ITreeValidator _treeValidator;

        //DIEGO:: to retrieve...
        public string Ecosystem { get; set; } = "Forest";

        public TreeController(IGenericService<GRP_TREE> service, IMapper mapper, ITreeValidator treeValidator)
        {
            _service = service;
            _mapper = mapper;
            _treeValidator = treeValidator;
        }

        // GET: TreeController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_TREE> Trees = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_TREEViewModel> TreesVM = new List<GRP_TREEViewModel>();
            foreach (var tow in Trees)
            {
                var towVM = _mapper.Map<GRP_TREEViewModel>(tow);
                TreesVM.Add(towVM);
            }
            return View(TreesVM);
        }

        // GET: TreeController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || id <= 0)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_TREEViewModel towVM = _mapper.Map<GRP_TREE, GRP_TREEViewModel>(item);

            return View(towVM);
        }

        // GET: TreeController/Create
        public ActionResult Create()
        {
            GRP_TREEViewModel TreeViewModel = new GRP_TREEViewModel();
            ModelUtils.FillSelectItemLists(TreeViewModel.TREE_STATUSTypes, _service.GetBadmListData((int)Globals.CvIndexes.TREE_STATUS)); 

            return View(TreeViewModel);
        }

        // POST: TreeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_TREEViewModel TreeViewModel)
        {
            GRP_TREE model = _mapper.Map<GRP_TREEViewModel, GRP_TREE>(TreeViewModel);
            bool reload = false;
            if (ModelState.IsValid)
            {
                int error = 0, globalError=0;
                string errorString = "";
                error = await _treeValidator.ItemInSamplingPointGroupAsync(model, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    errorString = ErrorCodes.ErrorsList[error];
                    ModelState.AddModelError("TREE_PLOT", errorString);
                    globalError += error;
                    error = 0;
                }
                error = _treeValidator.ValidateTree(model, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    errorString = ErrorCodes.ErrorsList[error];
                    ModelState.AddModelError("All", errorString);
                    globalError += error;
                    error = 0;
                }
                if (globalError > 0)
                {
                    reload = true;
                    ViewData["sent"] = 1;
                }
                else 
                { 
                    bool b = await _service.SaveItemAsync(model, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                    if(!b)
                    {
                        reload = true;
                        ViewData["sent"] = 1;
                    }
                }
            }
            else
            {
                ViewData["sent"] = 1;
                reload = true;
            }
            if (reload)
            {
                ModelUtils.FillSelectItemLists(TreeViewModel.TREE_STATUSTypes, _service.GetBadmListData((int)Globals.CvIndexes.TREE_STATUS));
                return View(TreeViewModel);
            }
            return RedirectToAction("Index");
        }

        // GET: TreeController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_TREEViewModel TreeViewModel = _mapper.Map<GRP_TREE, GRP_TREEViewModel>(item);
            ModelUtils.FillSelectItemLists(TreeViewModel.TREE_STATUSTypes, _service.GetBadmListData((int)Globals.CvIndexes.TREE_STATUS)); 

            
            return View(TreeViewModel);
        }

        // POST: TreeController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_TREEViewModel TreeViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_TREE loc = _mapper.Map<GRP_TREEViewModel, GRP_TREE>(TreeViewModel);
                bool b = await _service.UpdateItemAsync(TreeViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
         ModelUtils.FillSelectItemLists(TreeViewModel.TREE_STATUSTypes, _service.GetBadmListData((int)Globals.CvIndexes.TREE_STATUS)); 

            return View(TreeViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_TREEViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
