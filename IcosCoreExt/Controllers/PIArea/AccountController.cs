﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers.PIArea
{
    public class AccountController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public IActionResult Login(LoginModel model)
        public async Task<IActionResult> CheckLogin(/*LoginModel model*/)
        {
            //95->SuperUser ICOS Etc with email info@icos-etc.eu
            HttpContext.Session.SetInt32("UserId", 95);
            return RedirectToAction("Dashboard", "PI");
            /*if (ModelState.IsValid)
             {
                 var user = await iUserloginServices.CheckLoginAsync(model);
                 if (user is null)
                 {
                     ModelState.AddModelError("", "Username and password do not match. Please try again or use the password forgotten function.");
                     return View(model);
                 }
                 else
                 {
                     if (user.first_acc == 0)
                     {
                         //Session["ICOS"] = 1;
                         return RedirectToAction("Dashboard", "PI");
                     }
                     else
                     {
                         // Session["ICOS"] = 0;
                         return RedirectToAction("ChangePassword", "Account");
                     }
                 }
             }*/

            //return View(/*model*/);
        }
    }
}
