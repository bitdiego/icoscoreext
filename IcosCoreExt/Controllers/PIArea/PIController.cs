﻿using Azure;
using Azure.Storage.Files.Shares;
using Azure.Storage.Files.Shares.Models;
using IcosCoreExt.Models.PIAreaModels;
using IcosCoreExt.Models.PiAreaViewModels;
using IcosCoreExt.Models.ViewModels;
using IcosDataLogicLib.Services;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.File;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using IcosCoreExt.Utils;
using Microsoft.AspNetCore.Authorization;
using IdentitySample.Services;

namespace IcosCoreExt.Controllers.PIArea
{
   // [Authorize]
    public class PIController : Controller
    {
        int? SessionUserId;
       // private readonly IGenericService<SubmittedFile> _service;
        private readonly IPiAreaService _piAreaService;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _environment;
        private readonly IEmailSender _emailSender;
        private string DataPath = "";
        private string FieldbookFolder = "";
        private string baseUri = "icosshare/icos/";        // "icos/";

        public PIController(IPiAreaService piAreaService, IConfiguration configuration, IWebHostEnvironment environment, IEmailSender emailSender)
        {
            this._piAreaService = piAreaService;
            this._configuration = configuration;
            this._environment = environment;
            _emailSender = emailSender;
            DataPath = Path.Combine(_environment.WebRootPath, "data");
            FieldbookFolder = Path.Combine(_environment.WebRootPath, "fieldbook");
            //DIEGO:: brute forced...
            //SessionUserId = 95;
        }
        public IActionResult Index()
        { 
            return View();
        }

        // GET: CEPT/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || id <= 0)
            {
                return NotFound();
            }
            //consider to delete the share file!
            bool success = await _piAreaService.DeleteFieldBookFileAsync((int)id, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            if (!success)
            {

            }
            return RedirectToAction("Fieldbook");
        }

        public async Task<IActionResult> DashboardAsync()
        {
            /* insert here all the login management stuff: sessions, caching and so on*/
            /*
             * DIEGO
             * it seems that in every controller must read the session vars. a better solution???
             * */
            ViewBag.HtmlStr = await Utils.Utils.GetHtmlFromStorageAsync("Areas", "1");
            SessionUserId = HttpContext.Session.GetInt32("UserId");
            if(SessionUserId == null)
            {
                //Diego: why the fuck does it not work any more????????????????????????????
               // return RedirectToAction("Login", "Account");
            }
            
            return View();
        }

        [Authorize]
        //public async Task<IActionResult> Fileupload()
        public IActionResult Fileupload()
        {
            /*
             * DIEGO
             * it seems that in every controller must read the session vars. a better solution???
            */

            /*
             SessionUserId = HttpContext.Session.GetInt32("UserId");
             if (SessionUserId == null)
             {
                 return RedirectToAction("Login", "Account");
             }
            */
            //SessionUserId = 95;
            /*
            var sitesToUser = await _piAreaService.GetSitesToUserListAsync(SessionUserId);
            
            //viewModel = await GetPISitesListAsync();
            SitesToUserViewModel sitesToUserViewModel = new SitesToUserViewModel();
            foreach (var item in sitesToUser.SitesPiList)
            {
                sitesToUserViewModel.SitesPiList.Add(new SelectListItem(item.SiteCode, item.Id.ToString()));
            }
            return View(sitesToUserViewModel);
            */
            return View();
        }

        //Save submitted files
        [HttpPost]
        public async Task<string> Save(List<IFormFile> files, string fileType, string site, int siteId, string dataInfo, bool sampleCheck = false, bool isFieldbook=false)
        {
            bool isAllOk = true;
            string message = "";
            string mimeType = "";
            string emailSubject = "ICOS page data submission - " + site;
            string bodyMail = "New $FF$ file(s) submitted from the ICOS Upload web page<br />";
            string submittedFileType = "";

            //containing the files to attach to mail recipients
            List<string> filesToPost = new List<string>();
            //containing the mail recipients to send in CC
            List<string> lcc = new List<string>();
            List<Attachment> lAtch = new List<Attachment>();
            if (!ModelState.IsValid)
            {
                return "An error occurred. Please contact administrators<br />info@icos-etc.eu";
            }
            else
            {
                int fType = int.Parse(fileType);
                short? siteClass = 0;
                string newFileName = "";
                foreach (IFormFile file in files)
                {
                    if (file != null)
                    {
                        //mimeType = file.ContentType;
                        //ALL THE SAVEAS...ACTIONS FOR FILES I THINK NEED TO BE REWRITTEN IN ORDER TO save AS BLOB DATA TYPE....
                        //PROBLEM: SOME FILES NEED TO BE SENT TO RECIPIENTS IN ATTACHMENTS: SEE BELOW ON fType switch
                        //DIEGO:: end 

                        switch (fType)
                        {
                            case 12:
                                //BADM file: save to storage
                                submittedFileType = "BADM";
                                newFileName = SetNewFileName(file.FileName);

                                mimeType = (file.FileName.EndsWith("csv")) ? "text/csv" : "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                break;
                            case 13:
                                newFileName = SetNewFileName(file.FileName);
                                //kml file: send attached to g.nicolini@unitus.it and create CP_SP file
                                if (sampleCheck)
                                {
                                    //check if CP points have been submitted
                                    bool isCP = await _piAreaService.CPRecordsExistAsync(siteId);
                                    if (!isCP)
                                    {
                                        return "WARNING! CP points coordinates are missing. Please submit by BADM Sampling Scheme Template or by the Web UI";
                                    }

                                    //* create a txt file with ec-cp coordinates
                                    //fToJack = @"F:\icos\" + sitesList.SelectedItem.Text + "\\" + sitesList.SelectedItem.Text + "_EC-CP_coordinates.txt";
                                    // getCooFile(sitesList.SelectedItem.Text, true, fToJack);

                                    bodyMail += "<br /><br />Submitter asked to treat the CPs as Exclusion Areas";
                                }

                                //file.SaveAs(path);
                                // string fToJack= Path.Combine(Server.MapPath("~/App_Data"), site + "_EC-CP_coordinates.txt");
                                string fToJack = Path.Combine(@"C:\Users\DiegoPolidori\source\repos\icoscore\ICOSCore\wwwroot\upload", site + "_EC-CP_coordinates.txt");
                                filesToPost.Add(fToJack);
                                lAtch.Add(new Attachment
                                {
                                    Name= site + "_EC-CP_coordinates.txt",
                                    Path= Path.Combine(@"C:\Users\DiegoPolidori\source\repos\icoscore\ICOSCore\wwwroot\upload", site + "_EC-CP_coordinates.txt")
                                });
                                lcc.Add("g.nicolini@unitus.it");
                                submittedFileType = "KML Target/Exclusion area";
                                mimeType = "application/vnd.google-earth.kml+xml";
                                break;
                            case 14:
                                //shape

                                submittedFileType = "KML Target/Exclusion area (shape files)";
                                newFileName = SetNewFileName(file.FileName);
                                mimeType = "application/zip";
                                break;
                            case 15:
                                //Other files
                                submittedFileType = "Other files";
                                newFileName = file.FileName;
                                mimeType = file.ContentType;
                                //bodyMail += Environment.NewLine + dangerRemove(TextBoxinfo.Text.Trim());
                                break;
                            case 16:
                                //Field map
                                lcc.Add("maarten.opdebeeck@uantwerpen.be");
                                lcc.Add("bert.gielen@uantwerpen.be");
                                submittedFileType = "Field Map";
                                mimeType = file.ContentType;
                                break;
                            case 17:
                                // Ceptometer
                                lcc.Add("maarten.opdebeeck@uantwerpen.be");
                                lcc.Add("bert.gielen@uantwerpen.be");
                                submittedFileType = "Ceptometer data";
                                mimeType = file.ContentType;
                                break;
                            case 18:
                                // Header file
                                submittedFileType = "Header file";
                                newFileName = file.FileName;
                                mimeType = "text/csv";
                                
                                if (file.FileName.ToLower().IndexOf("bm") >= 0)
                                {
                                    //checkBM(sitesList.SelectedItem.Text, file.FileName, 70);
                                }
                                lcc.Add("simone.sabbatini@unitus.it");
                                break;
                            case 19:
                                submittedFileType = "Continuous data file";
                                if (siteClass != 0)
                                {
                                    message += "Warning! Only associated stations can upload continuous data files<br />";
                                    return message;
                                }
                                break;
                            case 20:
                                submittedFileType = "DHP image";
                                if (file.FileName.EndsWith(".zip", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    newFileName = SetNewFileName(file.FileName);
                                    message += "Processing " + file.FileName + "<br />";
                                    mimeType = "application/zip";
                                    
                                }
                                else
                                {
                                    newFileName = SetNewFileName(file.FileName);
                                    mimeType = "image/x-raw";
                                }
                               
                                break;
                            case 22:
                                //General images
                                submittedFileType = "General image file";
                                newFileName = SetNewFileName(file.FileName);
                                mimeType = file.ContentType;
                                break;
                            case 23:
                                //raw data
                                break;

                        }
                        bodyMail = bodyMail.Replace("$FF$", submittedFileType);
                        bodyMail += "<br />File name: " + file.FileName;

                        string destPath = Path.Combine(DataPath, newFileName);
                        message += "Processing " + file.FileName + "<br />";
                        
                        try
                        {
                            using (FileStream DestinationStream = new FileStream(destPath, FileMode.Create))
                            {
                                await file.CopyToAsync(DestinationStream);
                                DestinationStream.Close();
                                DestinationStream.Dispose();
                            }
                            if (fType == 14 || fType==20)
                            {
                                string checkString = CheckFileConsistency(destPath, fType, site);
                                if (!String.IsNullOrEmpty(checkString))
                                {
                                    DeleteUploadedData();
                                    return checkString;
                                }
                            }
                                
                        }
                        catch(Exception wwe)
                        {
                            string hy = wwe.ToString();
                        }
                        //int response = await UploadFileToBlobAsync(site, newFileName, new byte[file.Length], mimeType);//newFileName
                        int response = await UploadFileToFileShare(site, newFileName, mimeType);

                        if (response != 0)
                        {
                            //return
                            message = "An error occurred in saving file to cloud storage. Please contact administration (info@icos-etc.eu)";
                        }


                        //DIEGO::: TO SAVE IN DB!!!!!!!!!!
                        SubmittedFile subFile = new SubmittedFile
                        {
                            Name = newFileName,
                            OriginalName = file.FileName,
                            Status = "Uploaded",
                            DataInfo = dataInfo,
                            Exclusion = sampleCheck,
                            FileTypeId = fType,
                            SiteId = siteId,
                            UserId = (int)Utils.Utils.GetUser(HttpContext),
                            Date = DateTime.Now
                        };
                        bool res = await _piAreaService.SaveSubmittedFileAsync(subFile);
                        if (!res)
                        {
                            isAllOk = false;
                            message = "An error occurred in saving file to database. Please contact administration (info@icos-etc.eu)";
                            break;
                        }

                        //Diego:: files to attach for email services 
                        //Do not attach DHP, continuous data or Image files
                        if (fType != 20 && fType != 22 && fType != 19)
                        {
                            filesToPost.Add(baseUri + newFileName);
                            lAtch.Add(new Attachment
                            {
                                Name=file.FileName,
                                Path =  baseUri + site + "/"+ newFileName
                            });
                        }

                    }
                }
                try
                {
                    /*DirectoryInfo dInfo = new DirectoryInfo(DataPath);
                    FileInfo[] fi = dInfo.GetFiles();
                    foreach (var item in fi)
                    {
                        item.Delete();
                    }*/
                    DeleteUploadedData();
                }
                catch (Exception de)
                {
                    string des = de.ToString();
                }
                
            }
            //send mails...add submitter credentials to body mail
            if (isAllOk)
            {
                //get submitter credentials: name, email and role for site

                bodyMail += await _piAreaService.GetSubmitterCredentialsAsync(siteId, (int)Utils.Utils.GetUser(HttpContext));

                await _emailSender.SendEmailWithCcAsync("diego.p@unitus.it", emailSubject, bodyMail, lAtch, lcc);
            }
            else
            {
                //error message
            }
            return message;
        }

        private string CheckFileConsistency(string destPath, int fType, string site)
        {
            string message = "";
            if (fType == 14)
            {
                string[] extensions = { "shp", "shx", "dbf", "prj" };
                int[] _cExt = { 0, 0, 0, 0 };
                try
                {
                    using (ZipArchive archive = ZipFile.OpenRead(destPath))
                    {
                        foreach (ZipArchiveEntry entry in archive.Entries)
                        {
                            if (entry.Name.Length > 0)
                            {
                                string ext = entry.Name.Substring(entry.Name.LastIndexOf(".") + 1);
                                if (extensions.Contains(ext))
                                {
                                    ++_cExt[extensions.ToList().IndexOf(ext)];
                                }

                                Match m1 = Regex.Match(entry.Name, Globals.taReg, RegexOptions.IgnoreCase);
                                Match m2 = Regex.Match(entry.Name, Globals.eaReg, RegexOptions.IgnoreCase);
                                if (!m1.Success && !m2.Success)
                                {
                                    message += "<br />Warning: file name " + entry.Name + " has a wrong format!<br />";
                                    return message;
                                }

                            }

                        }
                    }
                    if (_cExt.Any(item => item == 0))
                    {
                        message = "Wrong SD files. Missing " + _cExt.FirstOrDefault(item => item == 0);
                        return message;
                    }
                }
                catch (Exception ze)
                {
                    string h = ze.ToString();
                }
            }
            else
            {
                string dhpReg = @"[a-zA-Z]{2}\-[a-zA-Z0-9]{3}_(DHP)_(S)[1-9]_(CP|SP)([0][1-9]|[1][0-9]|20)_(L)([0][1-9]|[1][0-9]|20)_[0-9]{8}.[a-zA-Z0-9]{3,4}$";
                using (ZipArchive archive = ZipFile.OpenRead(destPath))
                {
                    foreach (var entry in archive.Entries)
                    {

                        if (entry.FullName.LastIndexOf(".") < 0)
                        {
                            continue;
                        }
                        else
                        {

                            string prefix = entry.Name.Substring(0, 6);
                            if (String.Compare(prefix, site, true) != 0)
                            {
                                message = "Error: file " + entry.Name + " does not match with selected site code<br />";
                                return message;
                            }

                            Match match = Regex.Match(entry.Name, dhpReg, RegexOptions.IgnoreCase);
                            if (!match.Success)
                            {
                                message = "Error:: " + entry.Name + " has a wrong name!!!<br />";
                                return message;
                            }

                            //Unzip to a DHP folder to process for CP submitting
                            string fullPath = Path.GetFullPath("data", entry.Name);
                            entry.ExtractToFile(fullPath);
                        }
                    }
                }
            }

            return message;

        }

        private string SetNewFileName(string fname)
        {
            string newName = fname.Substring(0, fname.LastIndexOf(".")) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + fname.Substring(fname.LastIndexOf("."));
            return newName;
        }

        private async Task<int> UploadFileToFileShare(string site, string strFileName, string fileMimeType, bool isFieldbbok=false)
        {
            int res = 0;
            try 
            {
                var shFolder = _configuration.GetValue<string>("AzureFileShare:ShareFolder");
                ShareClient share = new ShareClient(_configuration.GetValue<string>("AzureConnectionStrings:AccessKey"), "icosshare");
                ShareDirectoryClient directory = share.GetDirectoryClient("icos");
                directory = directory.GetSubdirectoryClient(site);
                if (isFieldbbok)
                {
                    directory = directory.GetSubdirectoryClient("fieldbook");
                    //cloudFileDirectory.CreateIfNotExists();
                    await directory.CreateIfNotExistsAsync();
                    //directory = directory.GetSubdirectoryClient("fieldbook");
                }
                ShareFileClient file = directory.GetFileClient(strFileName);
                using (FileStream stream = System.IO.File.OpenRead(Path.Combine(DataPath, strFileName)))
                {
                    file.Create(stream.Length);
                    await file.UploadRangeAsync(new HttpRange(0, stream.Length), stream);
                }
                
            }
            catch(Exception ge)
            {
                string htr = ge.ToString();
                res = -1;
            }
            
            return res;
        }

        private async Task<int> UploadFileToBlobAsync(string site, string strFileName, byte[] fileData, string fileMimeType)
        {
            int res = 0;
            try
            {
                CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(_configuration.GetSection("AzureConnectionStrings").GetValue<string>("AccessKey"));
                CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
                string strContainerName = "icos";
                CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(strContainerName);

                if (await cloudBlobContainer.CreateIfNotExistsAsync())
                {
                    await cloudBlobContainer.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
                }

                if (strFileName != null && fileData != null)
                {
                    using (var fs = new FileStream(Path.Combine(DataPath, strFileName), FileMode.Open))
                    {
                        fs.Read(fileData, 0, (int)fs.Length);
                        fs.Close();
                        fs.Dispose();
                    }
                    string blobDest = Path.Combine(site, strFileName);
                    //strFileName = site + "/" + strFileName;
                    CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(blobDest);
                    cloudBlockBlob.Properties.ContentType = fileMimeType;
                    //await cloudBlockBlob.UploadFromFileAsync(strFileName);
                    await cloudBlockBlob.UploadFromByteArrayAsync(fileData, 0, fileData.Length);
                    if (String.IsNullOrEmpty(cloudBlockBlob.Uri.AbsoluteUri))
                    {
                        res = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                res = 1;
            }
            return res;
        }

        private async Task<int> xup2Async(string fileName, string site, string nn, bool isFieldbook)
        {
            var _root = _configuration.GetSection("DefaultUploadFolder").Value;
            //var prefix = "icos/" + site + "/";
            var prefix = site + "/";
            if (isFieldbook)
            {
                prefix += "fieldbook/";
            }
            try
            {

                string blobName = "";// prefix + SetNewFileName(file.FileName);
                if (!String.IsNullOrEmpty(nn))
                {
                    blobName = prefix + nn;
                }
                else
                {
                    blobName = prefix + fileName;//SetNewFileName(fileName);
                }
                var storageCredentials = new StorageCredentials(_configuration.GetSection("AzureFileShare").GetValue<string>("user"),
                                                                _configuration.GetSection("AzureFileShare").GetValue<string>("password"));
                CloudStorageAccount cloudStorageAccount = new CloudStorageAccount(storageCredentials, true);
                CloudFileClient fileClient = cloudStorageAccount.CreateCloudFileClient();
                CloudFileShare fileShare = fileClient.GetShareReference(_configuration.GetSection("AzureFileShare").GetValue<string>("shareName"));
                await fileShare.CreateIfNotExistsAsync();
                CloudFile cloudFile = fileShare.GetRootDirectoryReference().GetFileReference(blobName);

                string ftest = Path.Combine(_root, fileName);

                await cloudFile.UploadFromFileAsync(ftest);
                //await cloudFile.UploadFromFileAsync(Path.Combine(_root, fileName));

                return 0;
            }
            catch (Exception e)
            {
                return 1;
            }
        }

        [Authorize]
        public async Task<IActionResult> SubmittedFiles()
        {
            var subFileslist = await _piAreaService.GetSubmittedFilesAsync(Utils.Utils.GetSite(HttpContext));
            List<SubmittedFilesViewModel> subFileslistVM=new List<SubmittedFilesViewModel>();
            foreach (var sf in subFileslist)
            {
                SubmittedFilesViewModel subVM = new SubmittedFilesViewModel();
                subVM.Id = sf.Id;
                subVM.Name = sf.Name;
                subVM.OriginalName = sf.OriginalName;
                //subVM.SiteId = sf.SiteId;
                subVM.Status = sf.Status;
                subVM.UserId = sf.UserId;
                subVM.FType = sf.FileType;
                subVM.Site = sf.Site;
                subFileslistVM.Add(subVM);
            }
            return View(subFileslistVM);
        }


        [HttpGet]
        public ActionResult SetSite(int? siteId)
        {
         //   HttpContext.Current.Session["PiSite"] = siteId.ToString();
            HttpContext.Session.SetInt32("PiSite", siteId.Value);

            return Content("");
        }

        [Produces("application/json")]
        [HttpGet("/pi/downloadsubmittedfiles")]
        public async Task<ActionResult> DownloadSubmittedFileAsync(int fileId, string fileName)
        {
            /*var shFolder = _configuration.GetValue<string>("AzureFileShare:ShareFolder");
            ShareClient share = new ShareClient(_configuration.GetValue<string>("AzureConnectionStrings:AccessKey"), "icosshare");
            ShareDirectoryClient directory = share.GetDirectoryClient("icos");
            directory = directory.GetSubdirectoryClient(site);
            ShareFileClient file = directory.GetFileClient(strFileName);*/

            /*CloudStorageAccount account = CloudStorageAccount.Parse(_configuration.GetValue<string>("AzureConnectionStrings:AccessKey"));
            CloudFileClient client = account.CreateCloudFileClient();


            //get File Share
            CloudFileShare cloudFileShare = client.GetShareReference("icosshare");

            //get the related directory
            CloudFileDirectory root = cloudFileShare.GetRootDirectoryReference();
            CloudFileDirectory dir = root.GetDirectoryReference("FA-Lso");

            //get the file reference
            CloudFile file = dir.GetFileReference(fileName);*/
            ShareClient share = new ShareClient(_configuration.GetValue<string>("AzureConnectionStrings:AccessKey"), "icosshare");
            ShareDirectoryClient directory = share.GetDirectoryClient("icos");

            directory = directory.GetSubdirectoryClient("FA-Lso");

            ShareFileClient file = directory.GetFileClient(fileName);
            
            // Download the file

            //download file to local disk
            try
            {
                string dlF = DataPath +"\\"+ fileName;
                ShareFileDownloadInfo download = file.Download();
                using (FileStream stream = System.IO.File.OpenWrite(dlF))
                {
                    download.Content.CopyTo(stream);
                }
                string url = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}" + "/data/" + fileName;
                return Ok(url);
            }
            catch(Exception ee)
            {
                string hy = ee.ToString();
                return BadRequest("what happened?");
            }
            
            /*if (AccountDA.CurrentUserId != null && AccountDA.CurrentUserId != 0)
            {
                CloudFile cloudFile;

                using (ICOSEntities context = new newIcos.ICOSEntities())
                {
                    var file = (from f in context.icosfiles
                                join s in context.ICOS_site on f.site equals s.id_icos_site
                                where f.ID == ID
                                select new
                                {
                                    f.nome,
                                    s.site_code
                                }).ToList();

                    if (file.Count > 0)
                    {
                        CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
                        CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
                        CloudFileShare share = fileClient.GetShareReference("icosshare");

                        if (share.Exists())
                        {
                            CloudFileDirectory rootDir = share.GetRootDirectoryReference();
                            CloudFileDirectory icosDir = rootDir.GetDirectoryReference("icos");
                            if (icosDir.Exists())
                            {
                                CloudFileDirectory siteDir = icosDir.GetDirectoryReference(file[0].site_code);
                                if (siteDir.Exists())
                                {
                                    cloudFile = siteDir.GetFileReference(file[0].nome);
                                    if (cloudFile.Exists())
                                    {
                                        var policy = new SharedAccessFilePolicy
                                        {
                                            Permissions = SharedAccessFilePermissions.Read,
                                            SharedAccessStartTime = DateTime.UtcNow.AddMinutes(-15),
                                            SharedAccessExpiryTime = DateTime.UtcNow.AddHours(1)
                                        };
                                        var url = cloudFile.Uri.AbsoluteUri + cloudFile.GetSharedAccessSignature(policy);

                                        return Json(url, JsonRequestBehavior.AllowGet);

  
                                    }

                                }
                            }
                        }
                    }
                }



                return null;
            }
            else
                return RedirectToAction("Login", "Account");*/
        }

        [Authorize]
        public async Task<IActionResult> Fieldbook()
        {
            string siteFolder = "FA-Lso";//DIEGO:: Caution, to retrieve from select list
            string localPath = Path.Combine(FieldbookFolder, siteFolder);
            if (!Directory.Exists(localPath))
            {
                Directory.CreateDirectory(localPath);
            }
            var fieldBookList = await _piAreaService.GetFieldbooksAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<FieldbookViewModel> fbViewModelList = new List<FieldbookViewModel>();
            foreach(var fb in fieldBookList)
            {
                FieldbookViewModel fbVM = new FieldbookViewModel();
                fbVM.Id = fb.Id;
                fbVM.ImportDate = fb.ImportDate;
                fbVM.OriginalName = fb.OriginalName;
                fbVM.SiteId = fb.SiteId;
                fbVM.DataInfo = fb.DataInfo;
                fbVM.UserId = fb.UserId;
                fbVM.DataTypeId = fb.DataTypeId;

                var shFolder = _configuration.GetValue<string>("AzureFileShare:ShareFolder");
                ShareClient share = new ShareClient(_configuration.GetValue<string>("AzureConnectionStrings:AccessKey"), "icosshare");
                ShareDirectoryClient directory = share.GetDirectoryClient("icos");
                directory = directory.GetSubdirectoryClient(siteFolder);
                directory = directory.GetSubdirectoryClient("fieldbook");
                await directory.CreateIfNotExistsAsync();
                ShareFileClient file = directory.GetFileClient(fb.OriginalName);
                
                if (fb.DataTypeId == 15)
                {
                    //txt file
                    //read txt content from file share

                    using (var stream = file.OpenRead())
                    {
                        /*file.Create(stream.Length);
                        await file.UploadRangeAsync(new HttpRange(0, stream.Length), stream);*/
                        using (var reader = new StreamReader(stream))
                        {
                            // read csv file one line by line 
                            while (!reader.EndOfStream)
                            {
                                var line = await reader.ReadLineAsync().ConfigureAwait(false);
                                fbVM.HtmlContent += line;
                            }
                        }
                    }
                }
                else if(fb.DataTypeId == 21)
                {
                    //Audio file
                    ShareFileDownloadInfo download = file.Download();
                    using (FileStream stream = System.IO.File.OpenWrite(localPath + "\\" + file.Name))
                    {
                        await download.Content.CopyToAsync(stream);
                        fbVM.HtmlContent = "/fieldbook/" + siteFolder+"//" + file.Name;
                    }
                }
                else if (fb.DataTypeId == 22)
                {
                    //image file
                    ShareFileDownloadInfo download = file.Download();
                    using (FileStream stream = System.IO.File.OpenWrite(localPath + "\\" + file.Name))
                    {
                        await download.Content.CopyToAsync(stream);
                        fbVM.HtmlContent = "/fieldbook/" + siteFolder + "//" + file.Name;
                    }
                }

                fbViewModelList.Add(fbVM);
            }
            return View(fbViewModelList);
        }

        [HttpPost]
        public async Task<string> Fbook(List<IFormFile> files, string fileType, string data_info, string site, int siteId, string note_info)
        {
            //var _root = configuration.GetSection("DefaultUploadFolder").Value;
           // int piSession = AccountDA.CurrentUserID;
            string message = "", fullPath = "", emailTypeMessage="";
            DateTime dt = DateTime.Now;
            string prefix = "";
            // context = new ICOSEntities();
            int fType = int.Parse(fileType);
            string mimeType = "";
            if (files == null || files.Count == 0)
            {
                //Process text file: create, save and submit
                mimeType = "text/plain";
            }
            else
            {
                mimeType = files[0].ContentType;
            }
            string fileName = "";// files[0].FileName;
            switch (fType)
            {

                case 15:
                    emailTypeMessage = "text message";
                    message += "Text file ";
                    prefix = site + "_FBT_";
                    fileName = prefix + dt.ToString("yyyyMMddHHmmss") + ".txt";
                    fullPath = Path.Combine(DataPath, fileName);
                    try
                    { 
                        using (var wrFile = new StreamWriter(fullPath))
                        {
                            wrFile.WriteLine(note_info);
                        }
                    }
                    catch(Exception ex)
                    {
                        int ho = 0;
                    }
                    break;
                case 21:
                    emailTypeMessage = "audio file";
                    prefix = site + "_FBA_";
                    fileName = prefix + dt.ToString("yyyyMMddHHmmss") + files[0].FileName.Substring(files[0].FileName.LastIndexOf("."));
                    message += "Audio file ";
                    fullPath = Path.Combine(DataPath, fileName);
                    using (FileStream fs = System.IO.File.Create(fullPath))
                    {
                        files[0].CopyTo(fs);
                        fs.Flush();
                    }
                    break;
                case 22:
                    emailTypeMessage = "image file";
                    prefix = site + "_FBP_";
                    fileName = prefix + dt.ToString("yyyyMMddHHmmss") + files[0].FileName.Substring(files[0].FileName.LastIndexOf("."));
                    fullPath = Path.Combine(DataPath, fileName);

                    using (FileStream fs = System.IO.File.Create(fullPath))
                    {
                        files[0].CopyTo(fs);
                        fs.Flush();
                    }

                    message += "Image file " + files[0].FileName;
                    break;
            }

            //await Save(files, fileType, site, siteId, data_info, false, true);
            int response = await UploadFileToFileShare(site, fileName, mimeType, true);


            bool iRes = false;
            try
            {
                iRes= await _piAreaService.SaveFieldBookFileAsync(new Fieldbook
                {
                    OriginalName = fileName,
                    DataInfo = data_info,
                    UserId = (int)Utils.Utils.GetUser(HttpContext), //MIND!!!
                    SiteId=siteId,
                    DataTypeId = fType,
                    Date = DateTime.Now.ToString("yyyyMMddHHmm"),
                    Status = 0
                });
                
            }
            catch(Exception e)
            {
                message = "An error occurred in Fieldbook saving. Please contact administration (info@icos-etc.eu) " +e.ToString();
            }

            if (!iRes)
            {
                message = "An error occurred in Fieldbook saving. Please contact administration (info@icos-etc.eu)";
            }
            else
            {
                message += " correctly uploaded and renamed in " + fileName;
                string emailBody = "A new fieldbook item has been created and submitted for site "+site+"<br />";
                emailBody += "File type: "+ emailTypeMessage + "<br />";
                await _emailSender.SendEmailAsync("diego.p@unitus.it", "[ICOS ETC] New fieldbook uploaded for site " + site, emailBody);
            }

            DeleteUploadedData();

            return message;
        }

        private void DeleteUploadedData()
        {
            DirectoryInfo dInfo = new DirectoryInfo(DataPath);
            FileInfo[] fi = dInfo.GetFiles();
            foreach (var item in fi)
            {
                item.Delete();
            }

        }
    }
}
