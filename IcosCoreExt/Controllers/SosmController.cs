using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosDataLogicLib.Services.Validators;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class SosmController : Controller
    {
        private readonly IGenericService<GRP_SOSM> _service;
        private readonly IMapper _mapper;
        private readonly ISosmValidator _sosmValidator;

        public SosmController(IGenericService<GRP_SOSM> service, IMapper mapper, ISosmValidator sosmValidator)
        {
            _service = service;
            _mapper = mapper;
            _sosmValidator = sosmValidator;
        }

        // GET: SosmController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_SOSM> Sosms = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_SOSMViewModel> SosmsVM = new List<GRP_SOSMViewModel>();
            foreach (var tow in Sosms)
            {
                var towVM = _mapper.Map<GRP_SOSMViewModel>(tow);
                SosmsVM.Add(towVM);
            }
            return View(SosmsVM);
        }

        // GET: SosmController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_SOSMViewModel towVM = _mapper.Map<GRP_SOSM, GRP_SOSMViewModel>(item);

            return View(towVM);
        }

        // GET: SosmController/Create
        public ActionResult Create()
        {
            GRP_SOSMViewModel SosmViewModel = new GRP_SOSMViewModel();
   
            ModelUtils.FillSelectItemLists(SosmViewModel.SOSM_SAMPLE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.SAMPLEMAT)); 

            return View(SosmViewModel);
        }

        // POST: SosmController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_SOSMViewModel SosmViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_SOSM model = _mapper.Map<GRP_SOSMViewModel, GRP_SOSM>(SosmViewModel);
                string errorString = "";
                int globalError = 0;
                int error = await _sosmValidator.ItemInSamplingPointGroupAsync(model, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    errorString = ErrorCodes.ErrorsList[error];
                    ModelState.AddModelError("SOSM_PLOT_ID", errorString);
                    globalError += error;
                }
                
                error = _sosmValidator.ValidateSosm(model, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                
                if (error > 0)
                {
                    globalError += error;
                    errorString = ErrorCodes.ErrorsList[error];
                    
                    ModelState.AddModelError("All", errorString);
                    
                }
                if (globalError > 0)
                {
                    ModelUtils.FillSelectItemLists(SosmViewModel.SOSM_SAMPLE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.SAMPLEMAT));
                    return View(SosmViewModel);
                }
                else
                {
                    bool b = await _service.SaveItemAsync(model, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                    return RedirectToAction("Index");
                }
            }
            else
            {
                ModelUtils.FillSelectItemLists(SosmViewModel.SOSM_SAMPLE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.SAMPLEMAT)); 

                return View(SosmViewModel);
            }
        }

        // GET: SosmController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_SOSMViewModel SosmViewModel = _mapper.Map<GRP_SOSM, GRP_SOSMViewModel>(item);
         ModelUtils.FillSelectItemLists(SosmViewModel.SOSM_SAMPLE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.SAMPLEMAT)); 

            
            return View(SosmViewModel);
        }

        // POST: SosmController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_SOSMViewModel SosmViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_SOSM loc = _mapper.Map<GRP_SOSMViewModel, GRP_SOSM>(SosmViewModel);
                bool b = await _service.UpdateItemAsync(SosmViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
         ModelUtils.FillSelectItemLists(SosmViewModel.SOSM_SAMPLE_MATTypes, _service.GetBadmListData((int)Globals.CvIndexes.SAMPLEMAT)); 

            return View(SosmViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_SOSMViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
