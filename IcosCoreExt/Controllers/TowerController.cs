﻿using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
    [Authorize]
    public class TowerController : OurBaseController
    {
        private readonly IGenericService<GRP_TOWER> _service;
        private readonly IMapper _mapper;
        private readonly IcosDbContext _context;
        //       private readonly IHttpContextAccessor _httpContextAccessor;

        public TowerController(IGenericService<GRP_TOWER> service, IMapper mapper, IcosDbContext context) //, IHttpContextAccessor httpContextAccessor)
        {
            _service = service;
            _mapper = mapper;
            _context = context;
            //          _httpContextAccessor = httpContextAccessor;
        }

        // GET: TowerController

        public async Task<IActionResult> Index()
        {
            var xtest = new Utils.Attachment();
            // this worked!!
            //  xtest.Name = "FA-Lso_20180731_BADM_XXXX1.xlsx";
            //  xtest.Path = "/icos/FA-Lso_20180731_BADM_XXXX1.xlsx";

            xtest.Name = "BE-Lcr_20171121_BADM_1_20180130111115.xlsx";
            xtest.Path = "/icosshare/icos/BE-Lcr/BE-Lcr_20171121_BADM_1_20180130111115.xlsx";
            var xtest2 = new List<Utils.Attachment>();
            xtest2.Add(xtest);
            //{ "to":"aciaccia@unitus.it","html":"test body","subject":"test sub","cc":"ciaccias@gmail.com;diego.p@unitus.it","attachments":[{ "path":"/icos/FA-Lso_20180731_BADM_XXXX1.xlsx","name":"FA-Lso_20180731_BADM_XXXX1.xlsx"}]}
            Utils.Utils.SendMailwithAttachments("aciaccia@unitus.it", "test sub", "test body", xtest2, new List<string> { "ciaccias@gmail.com", "diego.p@unitus.it" });
            IEnumerable<GRP_TOWER> towers = await _service.GetItemValuesAsync(Siteid);
            List<GRP_TOWERViewModel> towersVM = new List<GRP_TOWERViewModel>();
            foreach (var tow in towers)
            {
                var towVM = _mapper.Map<GRP_TOWERViewModel>(tow);
                towersVM.Add(towVM);
            }
            return View(towersVM);
        }

        // GET: TowerController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || id <= 0)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(Siteid, id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_TOWERViewModel towVM = _mapper.Map<GRP_TOWER, GRP_TOWERViewModel>(item);

            return View(towVM);
        }

        // GET: TowerController/Create
        public ActionResult Create()
        {
            GRP_TOWERViewModel towerViewModel = new GRP_TOWERViewModel();
            ModelUtils.FillSelectItemLists(towerViewModel.TowerAccess, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_ACCESS));
            ModelUtils.FillSelectItemLists(towerViewModel.TowerDatatrans, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_DATATRAN));
            ModelUtils.FillSelectItemLists(towerViewModel.TowerPowers, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_POWER));
            ModelUtils.FillSelectItemLists(towerViewModel.TowerTypes, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_TYPE));
            return View(towerViewModel);
        }

        // POST: TowerController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_TOWERViewModel towerViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_TOWER tow = _mapper.Map<GRP_TOWERViewModel, GRP_TOWER>(towerViewModel);
                bool b = await _service.SaveItemAsync(tow, IcosCoreExt.Utils.Utils.GetUser(HttpContext), Siteid);
                return RedirectToAction("Index");
            }
            else
            {
                ModelUtils.FillSelectItemLists(towerViewModel.TowerAccess, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_ACCESS));
                ModelUtils.FillSelectItemLists(towerViewModel.TowerDatatrans, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_DATATRAN));
                ModelUtils.FillSelectItemLists(towerViewModel.TowerPowers, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_POWER));
                ModelUtils.FillSelectItemLists(towerViewModel.TowerTypes, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_TYPE));
                return View(towerViewModel);
            }
        }

        // GET: TowerController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(Siteid, id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_TOWERViewModel towerViewModel = _mapper.Map<GRP_TOWER, GRP_TOWERViewModel>(item);
            ModelUtils.FillSelectItemLists(towerViewModel.TowerAccess, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_ACCESS));
            ModelUtils.FillSelectItemLists(towerViewModel.TowerDatatrans, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_DATATRAN));
            ModelUtils.FillSelectItemLists(towerViewModel.TowerPowers, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_POWER));
            ModelUtils.FillSelectItemLists(towerViewModel.TowerTypes, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_TYPE));

            return View(towerViewModel);
        }

        // POST: TowerController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_TOWERViewModel towerViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_TOWER loc = _mapper.Map<GRP_TOWERViewModel, GRP_TOWER>(towerViewModel);
                bool b = await _service.UpdateItemAsync(towerViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
            ModelUtils.FillSelectItemLists(towerViewModel.TowerAccess, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_ACCESS));
            ModelUtils.FillSelectItemLists(towerViewModel.TowerDatatrans, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_DATATRAN));
            ModelUtils.FillSelectItemLists(towerViewModel.TowerPowers, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_POWER));
            ModelUtils.FillSelectItemLists(towerViewModel.TowerTypes, _service.GetBadmListData((int)Globals.CvIndexes.TOWER_TYPE));
            return View(towerViewModel);
        }
        // GET: DHPController/Delete/5

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_TOWERViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
