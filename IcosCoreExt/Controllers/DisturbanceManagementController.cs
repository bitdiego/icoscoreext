﻿using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLogicLib.Services;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class DisturbanceManagementController : Controller
    {
        // GET: DisturbanceManagementController
        private readonly IGenericService<GRP_DM> _service;
        private readonly IMapper _mapper;

        public DisturbanceManagementController(IGenericService<GRP_DM> service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }
        public async Task<ActionResult> Index()
        {
            IEnumerable<GRP_DM> dmList = await _service.GetItemValuesAsync(Utils.Utils.GetSite(HttpContext));
            List<GRP_DMViewModel> dmListVM = new List<GRP_DMViewModel>();
            foreach (var dm in dmList)
            {
                var dmVM = _mapper.Map<GRP_DMViewModel>(dm);
                dmListVM.Add(dmVM);
            }
            return View(dmListVM);
        }

        // GET: DisturbanceManagementController/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_DMViewModel dhpVM = _mapper.Map<GRP_DM, GRP_DMViewModel>(item);

            return View(dhpVM);
        }

        // GET: DisturbanceManagementController/Create
        public ActionResult Create()
        {
            GRP_DMViewModel dmViewModel = new GRP_DMViewModel();
            ModelUtils.FillSelectItemLists(dmViewModel.DmAgriculture, _service.GetBadmListData((int)Globals.CvIndexes.DM_AGRICULTURE));
            ModelUtils.FillSelectItemLists(dmViewModel.DmEncroach, _service.GetBadmListData((int)Globals.CvIndexes.DM_ENCROACH));
            ModelUtils.FillSelectItemLists(dmViewModel.DmExtWeather, _service.GetBadmListData((int)Globals.CvIndexes.DM_EXT_WEATHER));
            ModelUtils.FillSelectItemLists(dmViewModel.DmFertM, _service.GetBadmListData((int)Globals.CvIndexes.DM_FERT_M));
            ModelUtils.FillSelectItemLists(dmViewModel.DmFertO, _service.GetBadmListData((int)Globals.CvIndexes.DM_FERT_O));
            ModelUtils.FillSelectItemLists(dmViewModel.DmFire, _service.GetBadmListData((int)Globals.CvIndexes.DM_FIRE));
            ModelUtils.FillSelectItemLists(dmViewModel.DmForestry, _service.GetBadmListData((int)Globals.CvIndexes.DM_FORESTRY));
            ModelUtils.FillSelectItemLists(dmViewModel.DmGeneral, _service.GetBadmListData((int)Globals.CvIndexes.DM_GENERAL));
            ModelUtils.FillSelectItemLists(dmViewModel.DmGraze, _service.GetBadmListData((int)Globals.CvIndexes.DM_GRAZE));
            ModelUtils.FillSelectItemLists(dmViewModel.DmInspath, _service.GetBadmListData((int)Globals.CvIndexes.DM_INS_PATH));
            ModelUtils.FillSelectItemLists(dmViewModel.DmPesticide, _service.GetBadmListData((int)Globals.CvIndexes.DM_PESTICIDE));
            ModelUtils.FillSelectItemLists(dmViewModel.DmPlanting, _service.GetBadmListData((int)Globals.CvIndexes.DM_PLANTING));
            ModelUtils.FillSelectItemLists(dmViewModel.DmTill, _service.GetBadmListData((int)Globals.CvIndexes.DM_TILL));
            ModelUtils.FillSelectItemLists(dmViewModel.DmWater, _service.GetBadmListData((int)Globals.CvIndexes.DM_WATER));
            return View(dmViewModel);
        }

        // POST: DisturbanceManagementController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([FromForm]GRP_DMViewModel vmModel)
        {
            bool toReload = false;
            
            GRP_DM dmModel = _mapper.Map<GRP_DMViewModel, GRP_DM>(vmModel);
            if (ModelState.IsValid)
            {
                //lots of variables to check for validation:
                //but the most are input from a select list, so it could be unnecessary to write 
                //a control if inputs are in controlled vocabulary.
                //other checks done by data annotations, so take care if a duplicate is being generated
                //anyway, there are no andatory vars apart _DATE_; so check if a var
                //different from _DATE_ and comments are present
                //bool isAny = vmModel.
                bool isAny = ModelUtils.IsAnyPropNotNull<GRP_DM>(dmModel);
                if (!isAny)
                {
                    //raise error
                    //return View(vmModel);
                    ModelState.AddModelError("All", ErrorCodes.ErrorsList[(int)Globals.ErrorValidationCodes.NO_VALUE_SUBMITTED]);
                    toReload = true;
                }
                else 
                { 
                    //there are probably two variables bound together...
                    if(dmModel.DM_SURF_MEAS_PRECISION != null && dmModel.DM_SURF == null)
                    {
                        //surf_meas_precision must be submitted with surf
                        toReload = true;
                        ModelState.AddModelError("All", ErrorCodes.ErrorsList[(int)Globals.ErrorValidationCodes.DM_SURF_DM_SURF_PRECISION]);
                    }
                }
                if (!toReload)
                { 
                    try
                    {
                        await _service.SaveItemAsync(dmModel, Utils.Utils.GetUser(HttpContext), Utils.Utils.GetSite(HttpContext));
                    }
                    catch
                    {
                        toReload = true;
                    }
                }
            }
            else
            {
                toReload = true;
            }
            if (toReload)
            {
                ModelUtils.FillSelectItemLists(vmModel.DmAgriculture, _service.GetBadmListData((int)Globals.CvIndexes.DM_AGRICULTURE));
                ModelUtils.FillSelectItemLists(vmModel.DmEncroach, _service.GetBadmListData((int)Globals.CvIndexes.DM_ENCROACH));
                ModelUtils.FillSelectItemLists(vmModel.DmExtWeather, _service.GetBadmListData((int)Globals.CvIndexes.DM_EXT_WEATHER));
                ModelUtils.FillSelectItemLists(vmModel.DmFertM, _service.GetBadmListData((int)Globals.CvIndexes.DM_FERT_M));
                ModelUtils.FillSelectItemLists(vmModel.DmFertO, _service.GetBadmListData((int)Globals.CvIndexes.DM_FERT_O));
                ModelUtils.FillSelectItemLists(vmModel.DmFire, _service.GetBadmListData((int)Globals.CvIndexes.DM_FIRE));
                ModelUtils.FillSelectItemLists(vmModel.DmForestry, _service.GetBadmListData((int)Globals.CvIndexes.DM_FORESTRY));
                ModelUtils.FillSelectItemLists(vmModel.DmGeneral, _service.GetBadmListData((int)Globals.CvIndexes.DM_GENERAL));
                ModelUtils.FillSelectItemLists(vmModel.DmGraze, _service.GetBadmListData((int)Globals.CvIndexes.DM_GRAZE));
                ModelUtils.FillSelectItemLists(vmModel.DmInspath, _service.GetBadmListData((int)Globals.CvIndexes.DM_INS_PATH));
                ModelUtils.FillSelectItemLists(vmModel.DmPesticide, _service.GetBadmListData((int)Globals.CvIndexes.DM_PESTICIDE));
                ModelUtils.FillSelectItemLists(vmModel.DmPlanting, _service.GetBadmListData((int)Globals.CvIndexes.DM_PLANTING));
                ModelUtils.FillSelectItemLists(vmModel.DmTill, _service.GetBadmListData((int)Globals.CvIndexes.DM_TILL));
                ModelUtils.FillSelectItemLists(vmModel.DmWater, _service.GetBadmListData((int)Globals.CvIndexes.DM_WATER));

                return View(vmModel);
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }

        // GET: DisturbanceManagementController/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }
            GRP_DM model = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (model == null)
            {
                return BadRequest();
            }
            GRP_DMViewModel dmViewModel = _mapper.Map<GRP_DM, GRP_DMViewModel>(model);

            ModelUtils.FillSelectItemLists(dmViewModel.DmAgriculture, _service.GetBadmListData((int)Globals.CvIndexes.DM_AGRICULTURE));
            ModelUtils.FillSelectItemLists(dmViewModel.DmEncroach, _service.GetBadmListData((int)Globals.CvIndexes.DM_ENCROACH));
            ModelUtils.FillSelectItemLists(dmViewModel.DmExtWeather, _service.GetBadmListData((int)Globals.CvIndexes.DM_EXT_WEATHER));
            ModelUtils.FillSelectItemLists(dmViewModel.DmFertM, _service.GetBadmListData((int)Globals.CvIndexes.DM_FERT_M));
            ModelUtils.FillSelectItemLists(dmViewModel.DmFertO, _service.GetBadmListData((int)Globals.CvIndexes.DM_FERT_O));
            ModelUtils.FillSelectItemLists(dmViewModel.DmFire, _service.GetBadmListData((int)Globals.CvIndexes.DM_FIRE));
            ModelUtils.FillSelectItemLists(dmViewModel.DmForestry, _service.GetBadmListData((int)Globals.CvIndexes.DM_FORESTRY));
            ModelUtils.FillSelectItemLists(dmViewModel.DmGeneral, _service.GetBadmListData((int)Globals.CvIndexes.DM_GENERAL));
            ModelUtils.FillSelectItemLists(dmViewModel.DmGraze, _service.GetBadmListData((int)Globals.CvIndexes.DM_GRAZE));
            ModelUtils.FillSelectItemLists(dmViewModel.DmInspath, _service.GetBadmListData((int)Globals.CvIndexes.DM_INS_PATH));
            ModelUtils.FillSelectItemLists(dmViewModel.DmPesticide, _service.GetBadmListData((int)Globals.CvIndexes.DM_PESTICIDE));
            ModelUtils.FillSelectItemLists(dmViewModel.DmPlanting, _service.GetBadmListData((int)Globals.CvIndexes.DM_PLANTING));
            ModelUtils.FillSelectItemLists(dmViewModel.DmTill, _service.GetBadmListData((int)Globals.CvIndexes.DM_TILL));
            ModelUtils.FillSelectItemLists(dmViewModel.DmWater, _service.GetBadmListData((int)Globals.CvIndexes.DM_WATER));

            return View(dmViewModel);
        }

        // POST: DisturbanceManagementController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, GRP_DMViewModel vmModel)
        {
            //The same steps in the Create Action???
            bool toReload = false;

            GRP_DM dmModel = _mapper.Map<GRP_DMViewModel, GRP_DM>(vmModel);
            if (ModelState.IsValid)
            {
                //lots of variables to check for validation:
                //but the most are input from a select list, so it could be unnecessary to write 
                //a control if inputs are in controlled vocabulary.
                //other checks done by data annotations, so take care if a duplicate is being generated
                //anyway, there are no andatory vars apart _DATE_; so check if a var
                //different from _DATE_ and comments are present
                //bool isAny = vmModel.
                bool isAny = ModelUtils.IsAnyPropNotNull<GRP_DM>(dmModel);
                if (!isAny)
                {
                    //raise error
                    //return View(vmModel);
                    ModelState.AddModelError("All", ErrorCodes.ErrorsList[(int)Globals.ErrorValidationCodes.NO_VALUE_SUBMITTED]);
                    toReload = true;
                }
                else
                {
                    //there are probably two variables bound together...
                    if (dmModel.DM_SURF_MEAS_PRECISION != null && dmModel.DM_SURF == null)
                    {
                        //surf_meas_precision must be submitted with surf
                        toReload = true;
                        ModelState.AddModelError("All", ErrorCodes.ErrorsList[(int)Globals.ErrorValidationCodes.DM_SURF_DM_SURF_PRECISION]);
                    }
                }
                if (!toReload)
                {
                    try
                    {
                        await _service.UpdateItemAsync(id,Utils.Utils.GetSite(HttpContext), Utils.Utils.GetUser(HttpContext), dmModel);
                    }
                    catch
                    {
                        toReload = true;
                    }
                }
            }
            else
            {
                toReload = true;
            }
            if (toReload)
            {
                ModelUtils.FillSelectItemLists(vmModel.DmAgriculture, _service.GetBadmListData((int)Globals.CvIndexes.DM_AGRICULTURE));
                ModelUtils.FillSelectItemLists(vmModel.DmEncroach, _service.GetBadmListData((int)Globals.CvIndexes.DM_ENCROACH));
                ModelUtils.FillSelectItemLists(vmModel.DmExtWeather, _service.GetBadmListData((int)Globals.CvIndexes.DM_EXT_WEATHER));
                ModelUtils.FillSelectItemLists(vmModel.DmFertM, _service.GetBadmListData((int)Globals.CvIndexes.DM_FERT_M));
                ModelUtils.FillSelectItemLists(vmModel.DmFertO, _service.GetBadmListData((int)Globals.CvIndexes.DM_FERT_O));
                ModelUtils.FillSelectItemLists(vmModel.DmFire, _service.GetBadmListData((int)Globals.CvIndexes.DM_FIRE));
                ModelUtils.FillSelectItemLists(vmModel.DmForestry, _service.GetBadmListData((int)Globals.CvIndexes.DM_FORESTRY));
                ModelUtils.FillSelectItemLists(vmModel.DmGeneral, _service.GetBadmListData((int)Globals.CvIndexes.DM_GENERAL));
                ModelUtils.FillSelectItemLists(vmModel.DmGraze, _service.GetBadmListData((int)Globals.CvIndexes.DM_GRAZE));
                ModelUtils.FillSelectItemLists(vmModel.DmInspath, _service.GetBadmListData((int)Globals.CvIndexes.DM_INS_PATH));
                ModelUtils.FillSelectItemLists(vmModel.DmPesticide, _service.GetBadmListData((int)Globals.CvIndexes.DM_PESTICIDE));
                ModelUtils.FillSelectItemLists(vmModel.DmPlanting, _service.GetBadmListData((int)Globals.CvIndexes.DM_PLANTING));
                ModelUtils.FillSelectItemLists(vmModel.DmTill, _service.GetBadmListData((int)Globals.CvIndexes.DM_TILL));
                ModelUtils.FillSelectItemLists(vmModel.DmWater, _service.GetBadmListData((int)Globals.CvIndexes.DM_WATER));

                return View(vmModel);
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }

        // GET: DHPController/Delete/5

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_DMViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
