using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosDataLogicLib.Services.Validators;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
    [Authorize]
    public class AgbController : OurBaseController
    {
        private readonly IGenericService<GRP_AGB> _service;
        private readonly IMapper _mapper;
        private readonly IAgbValidator _agbValidator;

        public AgbController(IGenericService<GRP_AGB> service, IMapper mapper, IAgbValidator agbValidator)
        {
            _service = service;
            _mapper = mapper;
            _agbValidator = agbValidator;
        }

        // GET: AgbController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_AGB> Agbs = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_AGBViewModel> AgbsVM = new List<GRP_AGBViewModel>();
            foreach (var tow in Agbs)
            {
                var towVM = _mapper.Map<GRP_AGBViewModel>(tow);
                AgbsVM.Add(towVM);
            }
            return View(AgbsVM);
        }

        // GET: AgbController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_AGBViewModel towVM = _mapper.Map<GRP_AGB, GRP_AGBViewModel>(item);

            return View(towVM);
        }

        // GET: AgbController/Create
        public ActionResult Create()
        {
            GRP_AGBViewModel AgbViewModel = new GRP_AGBViewModel();
   
         ModelUtils.FillSelectItemLists(AgbViewModel.AGB_PLOT_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.PLOTTYPE)); 
         ModelUtils.FillSelectItemLists(AgbViewModel.AGB_PHENTypes, _service.GetBadmListData((int)Globals.CvIndexes.PHEN)); 
         ModelUtils.FillSelectItemLists(AgbViewModel.AGB_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.AGBPTYPE)); 
         ModelUtils.FillSelectItemLists(AgbViewModel.AGB_ORGANTypes, _service.GetBadmListData((int)Globals.CvIndexes.AGBORGAN)); 


            return View(AgbViewModel);
        }

        // POST: AgbController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_AGBViewModel AgbViewModel)
        {
            bool reload = false;
            if (!ModelState.IsValid)
            {
                reload = true;
            }
            else
            {
                int error = 0, globalError = 0;
                GRP_AGB tow = _mapper.Map<GRP_AGBViewModel, GRP_AGB>(AgbViewModel);
                string errorString = "";
                error = await _agbValidator.ItemInSamplingPointGroupAsync(tow, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    globalError += error;
                    errorString = ErrorCodes.ErrorsList[error];
                    ModelState.AddModelError("AGB_PLOT", errorString);
                }
                error = _agbValidator.ValidateAgb(tow, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    globalError += error;
                    errorString = ErrorCodes.ErrorsList[error];
                    ModelState.AddModelError("All", errorString);
                }
                if (globalError > 0)
                {
                    reload = true;
                }
                else
                {
                    bool b = await _service.SaveItemAsync(tow, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                    if (!b)
                    {
                        return RedirectToAction("Index"); //raise error...
                    }
                    
                }
                
            }
            
            if(reload)
            {
                ModelUtils.FillSelectItemLists(AgbViewModel.AGB_PLOT_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.PLOTTYPE));
                ModelUtils.FillSelectItemLists(AgbViewModel.AGB_PHENTypes, _service.GetBadmListData((int)Globals.CvIndexes.PHEN));
                ModelUtils.FillSelectItemLists(AgbViewModel.AGB_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.AGBPTYPE));
                ModelUtils.FillSelectItemLists(AgbViewModel.AGB_ORGANTypes, _service.GetBadmListData((int)Globals.CvIndexes.AGBORGAN));

                return View(AgbViewModel);
            }
            return RedirectToAction("Index");
        }

        // GET: AgbController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_AGBViewModel AgbViewModel = _mapper.Map<GRP_AGB, GRP_AGBViewModel>(item);
            ModelUtils.FillSelectItemLists(AgbViewModel.AGB_PLOT_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.PLOTTYPE));
            ModelUtils.FillSelectItemLists(AgbViewModel.AGB_PHENTypes, _service.GetBadmListData((int)Globals.CvIndexes.PHEN));
            ModelUtils.FillSelectItemLists(AgbViewModel.AGB_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.AGBPTYPE));
            ModelUtils.FillSelectItemLists(AgbViewModel.AGB_ORGANTypes, _service.GetBadmListData((int)Globals.CvIndexes.AGBORGAN));


            return View(AgbViewModel);
        }

        // POST: AgbController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_AGBViewModel AgbViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_AGB loc = _mapper.Map<GRP_AGBViewModel, GRP_AGB>(AgbViewModel);
                bool b = await _service.UpdateItemAsync(AgbViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
            ModelUtils.FillSelectItemLists(AgbViewModel.AGB_PLOT_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.PLOTTYPE));
            ModelUtils.FillSelectItemLists(AgbViewModel.AGB_PHENTypes, _service.GetBadmListData((int)Globals.CvIndexes.PHEN));
            ModelUtils.FillSelectItemLists(AgbViewModel.AGB_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.AGBPTYPE));
            ModelUtils.FillSelectItemLists(AgbViewModel.AGB_ORGANTypes, _service.GetBadmListData((int)Globals.CvIndexes.AGBORGAN));

            return View(AgbViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_AGBViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
