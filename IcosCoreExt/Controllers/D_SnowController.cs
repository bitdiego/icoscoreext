using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosDataLogicLib.Services.Validators;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class D_SnowController : Controller
    {
        private readonly IGenericService<GRP_D_SNOW> _service;
        private readonly IMapper _mapper;
        private readonly IDSnowValidator _dSnowValidator;

        public D_SnowController(IGenericService<GRP_D_SNOW> service, IMapper mapper, IDSnowValidator dSnowVal)
        {
            _service = service;
            _mapper = mapper;
            _dSnowValidator = dSnowVal;
        }

        // GET: D_SnowController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_D_SNOW> D_Snows = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_D_SNOWViewModel> D_SnowsVM = new List<GRP_D_SNOWViewModel>();
            foreach (var tow in D_Snows)
            {
                var towVM = _mapper.Map<GRP_D_SNOWViewModel>(tow);
                D_SnowsVM.Add(towVM);
            }
            return View(D_SnowsVM);
        }

        // GET: D_SnowController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_D_SNOWViewModel towVM = _mapper.Map<GRP_D_SNOW, GRP_D_SNOWViewModel>(item);

            return View(towVM);
        }

        // GET: D_SnowController/Create
        public ActionResult Create()
        {
            GRP_D_SNOWViewModel D_SnowViewModel = new GRP_D_SNOWViewModel();
   


            return View(D_SnowViewModel);
        }

        // POST: D_SnowController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_D_SNOWViewModel D_SnowViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_D_SNOW tow = _mapper.Map<GRP_D_SNOWViewModel, GRP_D_SNOW>(D_SnowViewModel);
                bool b = await _service.SaveItemAsync(tow, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                return RedirectToAction("Index");
            }
            else
            {

                return View(D_SnowViewModel);
            }
        }

        // GET: D_SnowController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_D_SNOWViewModel D_SnowViewModel = _mapper.Map<GRP_D_SNOW, GRP_D_SNOWViewModel>(item);

            
            return View(D_SnowViewModel);
        }

        // POST: D_SnowController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_D_SNOWViewModel D_SnowViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_D_SNOW loc = _mapper.Map<GRP_D_SNOWViewModel, GRP_D_SNOW>(D_SnowViewModel);
                bool b = await _service.UpdateItemAsync(D_SnowViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }

            return View(D_SnowViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_D_SNOWViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
