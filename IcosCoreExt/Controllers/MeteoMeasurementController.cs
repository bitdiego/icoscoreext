﻿using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLogicLib.Services;
using IcosDataLogicLib.Services.Validators;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class MeteoMeasurementController : Controller
    {
        private readonly IGenericService<GRP_BM> _service;
        private readonly IMapper _mapper;
        private readonly IBmValidator<GRP_BM> _bmValidator;

        public MeteoMeasurementController(IGenericService<GRP_BM> service, IMapper mapper, IBmValidator<GRP_BM> bmValidator)
        {
            _mapper = mapper;
            _service = service;
            _bmValidator = bmValidator;
        }
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_BM> bmModels = await _service.GetItemValuesAsync(Utils.Utils.GetSite(HttpContext));
            List<GRP_BMViewModel> bmModelsVM = new List<GRP_BMViewModel>();
            foreach (var inst in bmModels)
            {
                var instVM = _mapper.Map<GRP_BMViewModel>(inst);
                bmModelsVM.Add(instVM);
            }
            return View(bmModelsVM);
        }

        public ActionResult Create()
        {
            GRP_BMViewModel bmViewModel = new GRP_BMViewModel();
            ModelUtils.FillSelectItemLists(bmViewModel.BM_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_TYPE));
            ModelUtils.FillSelectItemLists(bmViewModel.BM_VARIABLE_H_V_RTypes, _service.GetBadmListData((int)Globals.CvIndexes.VAR_CODE));
            ModelUtils.FillSelectItemLists(bmViewModel.BM_INST_HEATTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_YN));
            ModelUtils.FillSelectItemLists(bmViewModel.BM_INST_SHIELDINGTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_SHIELDING));
            ModelUtils.FillSelectItemLists(bmViewModel.BM_INST_ASPIRATIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_ASPIRATION));
            return View(bmViewModel);
        }

        public async Task<IActionResult> AddBmOperation(int id)
        {
            var item = await _service.GetInstMainValuesByRecordIdAsync((int)Globals.Groups.GRP_BM, Utils.Utils.GetSite(HttpContext), id);
            GRP_BMViewModel instVM = _mapper.Map<GRP_BM, GRP_BMViewModel>(item);
            ModelUtils.FillSelectItemLists(instVM.BM_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_TYPE));
            ModelUtils.FillSelectItemLists(instVM.BM_VARIABLE_H_V_RTypes, _service.GetBadmListData((int)Globals.CvIndexes.VAR_CODE));
            ModelUtils.FillSelectItemLists(instVM.BM_INST_HEATTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_YN));
            ModelUtils.FillSelectItemLists(instVM.BM_INST_SHIELDINGTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_SHIELDING));
            ModelUtils.FillSelectItemLists(instVM.BM_INST_ASPIRATIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_ASPIRATION));
            return View("Create", instVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(GRP_BMViewModel bmViewModel)
        {
            GRP_BM model = _mapper.Map<GRP_BMViewModel, GRP_BM>(bmViewModel);
            if (ModelState.IsValid)
            {
                int error = 0, globalError=0;
                string errorString = "";
                /*if (!String.IsNullOrEmpty(model.BM_VARIABLE_H_V_R))
                {
                    error = _bmValidator.VarHVRCorrect(model.BM_VARIABLE_H_V_R);
                    if (error > 0) 
                    { 
                        globalError += error;
                        errorString = ErrorCodes.ErrorsList[error];
                        ModelState.AddModelError("All", errorString);
                        globalError += error;
                        error = 0;
                    }
                }*/
                error = await _bmValidator.LastExpectedOpByDateAsync(model, Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    globalError += error;
                    errorString = ErrorCodes.ErrorsList[error].Replace("$OP$", model.BM_TYPE);
                    ModelState.AddModelError("All", errorString);
                    globalError += error;
                    error = 0;
                }
                //add other validation checks....
                error = _bmValidator.ValidateByBmType(model, Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    globalError += error;
                    errorString = ErrorCodes.ErrorsList[error].Replace("$OP$", model.BM_TYPE);
                    ModelState.AddModelError("All", errorString);
                    globalError += error;
                    error = 0;
                }
                if (globalError > 0)
                {
                    ViewData["sent"] = 1;
                    ModelUtils.FillSelectItemLists(bmViewModel.BM_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_TYPE));
                    ModelUtils.FillSelectItemLists(bmViewModel.BM_VARIABLE_H_V_RTypes, _service.GetBadmListData((int)Globals.CvIndexes.VAR_CODE));
                    ModelUtils.FillSelectItemLists(bmViewModel.BM_INST_HEATTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_YN));
                    ModelUtils.FillSelectItemLists(bmViewModel.BM_INST_SHIELDINGTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_SHIELDING));
                    ModelUtils.FillSelectItemLists(bmViewModel.BM_INST_ASPIRATIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_ASPIRATION));
                    return View(bmViewModel);
                }
                bool b = await _service.SaveItemAsync(model, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["sent"] = 1;
                // ModelUtils.FillSelectItemLists(bmViewModel.BM_MODELTypes, _service.GetBadmListData(84));
                ModelUtils.FillSelectItemLists(bmViewModel.BM_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_TYPE));
                ModelUtils.FillSelectItemLists(bmViewModel.BM_VARIABLE_H_V_RTypes, _service.GetBadmListData((int)Globals.CvIndexes.VAR_CODE));
                ModelUtils.FillSelectItemLists(bmViewModel.BM_INST_HEATTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_YN));
                ModelUtils.FillSelectItemLists(bmViewModel.BM_INST_SHIELDINGTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_SHIELDING));
                ModelUtils.FillSelectItemLists(bmViewModel.BM_INST_ASPIRATIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.BM_ASPIRATION));

                return View(bmViewModel);
            }

          //  return View(bmViewModel);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_BMViewModel towVM = _mapper.Map<GRP_BM, GRP_BMViewModel>(item);

            return View(towVM);
        }
    }
}
