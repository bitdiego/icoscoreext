using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosDataLogicLib.Services.Validators;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class FlsmController : Controller
    {
        private readonly IGenericService<GRP_FLSM> _service;
        private readonly IMapper _mapper;
        private readonly IFlsmValidator _flsmValidator;

        public FlsmController(IGenericService<GRP_FLSM> service, IMapper mapper, IFlsmValidator validator)
        {
            _service = service;
            _mapper = mapper;
            _flsmValidator = validator;
        }

        // GET: FlsmController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_FLSM> Flsms = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_FLSMViewModel> FlsmsVM = new List<GRP_FLSMViewModel>();
            foreach (var tow in Flsms)
            {
                var towVM = _mapper.Map<GRP_FLSMViewModel>(tow);
                FlsmsVM.Add(towVM);
            }
            return View(FlsmsVM);
        }

        // GET: FlsmController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_FLSMViewModel towVM = _mapper.Map<GRP_FLSM, GRP_FLSMViewModel>(item);

            return View(towVM);
        }

        // GET: FlsmController/Create
        public ActionResult Create()
        {
            GRP_FLSMViewModel FlsmViewModel = new GRP_FLSMViewModel();
   
            ModelUtils.FillSelectItemLists(FlsmViewModel.FLSM_SAMPLE_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.FLSM_STYPE));    
            ModelUtils.FillSelectItemLists(FlsmViewModel.FLSM_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.FLSM_PTYPE));          


            return View(FlsmViewModel);
        }

        // POST: FlsmController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_FLSMViewModel FlsmViewModel)
        {
            bool reload = false;
            if(!ModelState.IsValid)
            {
                reload = true;
            }
            else
            {
                GRP_FLSM tow = _mapper.Map<GRP_FLSMViewModel, GRP_FLSM>(FlsmViewModel);
                int globalError = 0;
                int error = await _flsmValidator.ItemInSamplingPointGroupAsync(tow, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    ModelState.AddModelError("FLSM_PLOT_ID", ErrorCodes.ErrorsList[error]);
                    globalError += error;
                }    
                    
                error = _flsmValidator.ValidateFlsm(tow, IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    globalError += error;
                    ModelState.AddModelError("All", ErrorCodes.ErrorsList[error]);
                    
                }
                if (globalError > 0)
                {
                    reload = true;
                }
                else
                {
                    bool b = await _service.SaveItemAsync(tow, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                    if (!b)
                    {
                        reload = true;
                    }
                }
            }

            if (reload)
            {
                ModelUtils.FillSelectItemLists(FlsmViewModel.FLSM_SAMPLE_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.FLSM_STYPE));
                ModelUtils.FillSelectItemLists(FlsmViewModel.FLSM_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.FLSM_PTYPE));
                return View(FlsmViewModel);
            }

            return RedirectToAction("Index");
        }

        // GET: FlsmController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_FLSMViewModel FlsmViewModel = _mapper.Map<GRP_FLSM, GRP_FLSMViewModel>(item);
            ModelUtils.FillSelectItemLists(FlsmViewModel.FLSM_SAMPLE_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.FLSM_STYPE));
            ModelUtils.FillSelectItemLists(FlsmViewModel.FLSM_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.FLSM_PTYPE));


            return View(FlsmViewModel);
        }

        // POST: FlsmController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_FLSMViewModel FlsmViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_FLSM loc = _mapper.Map<GRP_FLSMViewModel, GRP_FLSM>(FlsmViewModel);
                bool b = await _service.UpdateItemAsync(FlsmViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
            ModelUtils.FillSelectItemLists(FlsmViewModel.FLSM_SAMPLE_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.FLSM_STYPE));
            ModelUtils.FillSelectItemLists(FlsmViewModel.FLSM_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.FLSM_PTYPE));
            return View(FlsmViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_FLSMViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
