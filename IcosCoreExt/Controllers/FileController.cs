using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosDataLogicLib.Services.Validators;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class FileController : Controller
    {
        private readonly IGenericService<GRP_FILE> _service;
        private readonly IMapper _mapper;
        private readonly IcosDbContext _context;
        private readonly ILoggerValidator _loggerVal;

        public FileController(IGenericService<GRP_FILE> service, IMapper mapper, IcosDbContext context, ILoggerValidator loggerVal)
        {
            _service = service;
            _mapper = mapper;
            _context = context;
            _loggerVal = loggerVal;
        }

        // GET: FileController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_FILE> Files = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_FILEViewModel> FilesVM = new List<GRP_FILEViewModel>();
            foreach (var tow in Files)
            {
                var towVM = _mapper.Map<GRP_FILEViewModel>(tow);
                FilesVM.Add(towVM);
            }
            return View(FilesVM);
        }

        // GET: FileController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_FILEViewModel towVM = _mapper.Map<GRP_FILE, GRP_FILEViewModel>(item);

            return View(towVM);
        }

        // GET: FileController/Create
        public ActionResult Create()
        {
            GRP_FILEViewModel FileViewModel = new GRP_FILEViewModel();
   
            ModelUtils.FillSelectItemLists(FileViewModel.FILE_FORMATTypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_FORMAT)); 
            ModelUtils.FillSelectItemLists(FileViewModel.FILE_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_TYPE)); 
            ModelUtils.FillSelectItemLists(FileViewModel.FILE_EXTENSIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_EXT)); 
            ModelUtils.FillSelectItemLists(FileViewModel.FILE_MISSING_VALUETypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_MISSING)); 
            ModelUtils.FillSelectItemLists(FileViewModel.FILE_TIMESTAMPTypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_TIME)); 
            ModelUtils.FillSelectItemLists(FileViewModel.FILE_COMPRESSTypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_COMPR)); 


            return View(FileViewModel);
        }

        // POST: FileController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_FILEViewModel FileViewModel)
        {
            //VAlidations to perform:
            //1. mandatory fields and some format (by data annotations) 
            //2. logger_id must be contained in GRP_LOGGER table for a given site
            //3. check constraints between file format, header variables, header number
           
            GRP_FILE tow = _mapper.Map<GRP_FILEViewModel, GRP_FILE>(FileViewModel);
            bool gotoView = false;
            if (ModelState.IsValid)
            {
                int globalErr = 0, error=0;
               
                
                error = await _loggerVal.IsLoggerIdAsync(tow.FILE_LOGGER_ID, Utils.Utils.GetSite(HttpContext));
                globalErr += error;
                if (error > 0)
                {
                    ModelState.AddModelError("FILE_LOGGER_ID", ErrorCodes.ErrorsList[error]);
                    error = 0;
                }
                error = _loggerVal.CheckFileFormat(tow.FILE_FORMAT, tow.FILE_EXTENSION, Utils.Utils.GetSite(HttpContext));
                globalErr += error;
                if (error > 0)
                {
                    ModelState.AddModelError("FILE_FORMAT", ErrorCodes.ErrorsList[error]);
                    error = 0;
                }
                error = _loggerVal.CheckHeadConstraints(tow.FILE_FORMAT, tow.FILE_HEAD_NUM, tow.FILE_HEAD_VARS, tow.FILE_HEAD_TYPE);
                globalErr += error;
                if (error > 0)
                {
                    ModelState.AddModelError("FILE_HEAD_NUM", ErrorCodes.ErrorsList[error]);
                    error = 0;
                }
                if (globalErr > 0)
                {
                    gotoView = true;
                }
                else
                {
                    bool b = await _service.SaveItemAsync(tow, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                    if (!b)
                    {
                        gotoView = true;
                    }
                }
            }
            else
            {
                gotoView = true;
            }
            if (gotoView)
            {
                ModelUtils.FillSelectItemLists(FileViewModel.FILE_FORMATTypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_FORMAT));
                ModelUtils.FillSelectItemLists(FileViewModel.FILE_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_TYPE));
                ModelUtils.FillSelectItemLists(FileViewModel.FILE_EXTENSIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_EXT));
                ModelUtils.FillSelectItemLists(FileViewModel.FILE_MISSING_VALUETypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_MISSING));
                ModelUtils.FillSelectItemLists(FileViewModel.FILE_TIMESTAMPTypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_TIME));
                ModelUtils.FillSelectItemLists(FileViewModel.FILE_COMPRESSTypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_COMPR));

                return View(FileViewModel);
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

    // GET: FileController/Edit/5
    public async Task<IActionResult> Edit(int id)
    {
        if (id <= 0)
        {
            return NotFound();
        }

        var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
        if (item == null)
        {
            return NotFound();
        }
        GRP_FILEViewModel FileViewModel = _mapper.Map<GRP_FILE, GRP_FILEViewModel>(item);
            
        
        ModelUtils.FillSelectItemLists(FileViewModel.FILE_FORMATTypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_FORMAT));
        ModelUtils.FillSelectItemLists(FileViewModel.FILE_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_TYPE));
        ModelUtils.FillSelectItemLists(FileViewModel.FILE_EXTENSIONTypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_EXT));
        ModelUtils.FillSelectItemLists(FileViewModel.FILE_MISSING_VALUETypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_MISSING));
        ModelUtils.FillSelectItemLists(FileViewModel.FILE_TIMESTAMPTypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_TIME));
        ModelUtils.FillSelectItemLists(FileViewModel.FILE_COMPRESSTypes, _service.GetBadmListData((int)Globals.CvIndexes.FILE_COMPR));

        return View(FileViewModel);
    }

    // POST: FileController/Edit/5
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(GRP_FILEViewModel FileViewModel)
    {
        if (ModelState.IsValid)
        {
            GRP_FILE loc = _mapper.Map<GRP_FILEViewModel, GRP_FILE>(FileViewModel);
            bool b = await _service.UpdateItemAsync(FileViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
            if (b)
            return RedirectToAction("Index");
        }
        //same validation functs as in Create???
        ModelUtils.FillSelectItemLists(FileViewModel.FILE_FORMATTypes, _service.GetBadmListData(66)); 
        ModelUtils.FillSelectItemLists(FileViewModel.FILE_TYPETypes, _service.GetBadmListData(67)); 
        ModelUtils.FillSelectItemLists(FileViewModel.FILE_EXTENSIONTypes, _service.GetBadmListData(88)); 
        ModelUtils.FillSelectItemLists(FileViewModel.FILE_MISSING_VALUETypes, _service.GetBadmListData(90)); 
        ModelUtils.FillSelectItemLists(FileViewModel.FILE_TIMESTAMPTypes, _service.GetBadmListData(91)); 
        ModelUtils.FillSelectItemLists(FileViewModel.FILE_COMPRESSTypes, _service.GetBadmListData(89)); 

        return View(FileViewModel);
    }

    // GET: DHPController/Delete/5

    public async Task<IActionResult> Delete(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
        if (dhp == null)
        {
            return NotFound();
        }

        return View(_mapper.Map<GRP_FILEViewModel>(dhp));
    }

    // POST: DHPController/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Delete(int id)
    {
        bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
        if (deleted)
        {
            return RedirectToAction(nameof(Index));
        }
        else
        {
            return RedirectToAction(nameof(Index));
        }
    }
}
}
