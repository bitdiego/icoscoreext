﻿using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLogicLib.Services;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class EcModelController : Controller
    {
        private readonly IGenericService<GRP_EC> _service;
        private readonly IMapper _mapper;
        private readonly IBasicInstrumentValidator<GRP_EC> _ecValidator;

        public EcModelController(IGenericService<GRP_EC> service, IMapper mapper, IBasicInstrumentValidator<GRP_EC> ecValidator)
        {
            _service = service;
            _mapper = mapper;
            _ecValidator = ecValidator;
        }
        // GET: EcModelController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_EC> ecModels = await _service.GetItemValuesAsync(Utils.Utils.GetSite(HttpContext));
            List<GRP_ECViewModel> ecModelsVM = new List<GRP_ECViewModel>();
            foreach (var inst in ecModels)
            {
                var instVM = _mapper.Map<GRP_ECViewModel>(inst);
                ecModelsVM.Add(instVM);
            }
            return View(ecModelsVM);
        }

       
        // GET: EcModelController/Create
        public ActionResult Create()
        {
            GRP_ECViewModel ecViewModel = new GRP_ECViewModel();
            FillEcSelectLists(ecViewModel);
            return View(ecViewModel);
        }

        // POST: EcModelController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(GRP_ECViewModel ecViewModel)
        {
            string _errorStr = "";
            GRP_EC model = _mapper.Map<GRP_ECViewModel, GRP_EC>(ecViewModel);
            if (ModelState.IsValid)
            {
                /*
                 * All sensors are pre-checked: they are valid sensors coming from former submission in GRP_INST:
                 * no need to check about serial number correctness.
                 * Only EC constraints 
                 * 0. check if sensor is in GRP_INST (maybe unnecessary: all available sensors are returned from a web api)
                 * 1. if ec_type!='Installation', check if a former installation is in
                 * 2. if ec_type=='Installation', check if no item is in GRP_EC or if last op is removal
                 * */
                int errOpCode = await _ecValidator.LastExpectedOpByDateAsync(model, Utils.Utils.GetSite(HttpContext));
                int generalError = 0;
                if (errOpCode > 0)
                {
                   // ModelUtils.FillSelectItemLists(ecViewModel.EcTypes, _service.GetBadmListData((int)Globals.CvIndexes.EC_TYPE));
                   // ViewData["sent"] = 1;
                   // return View(ecViewModel);
                    generalError += errOpCode;
                    _errorStr = ErrorCodes.ErrorsList[errOpCode];
                    ModelUtils.FormatError(ref _errorStr, "$V0$", model.EC_MODEL, "$V1$", model.EC_SN, "$OP$", model.EC_TYPE);
                    ModelState.AddModelError("EC_TYPE", _errorStr);
                }
                if (generalError > 0)
                {
                    ModelUtils.FillSelectItemLists(ecViewModel.EcTypes, _service.GetBadmListData((int)Globals.CvIndexes.EC_TYPE));
                    ViewData["sent"] = 1;
                    FillEcSelectLists(ecViewModel);
                    return View(ecViewModel);
                }
                else
                {
                    bool b = await _service.SaveItemAsync(model, Utils.Utils.GetUser(HttpContext), Utils.Utils.GetSite(HttpContext));
                    if (b)
                    {
                        return RedirectToAction("Index");// View("Index");
                    }
                }
               
            }
            else
            {
                //ModelUtils.FillSelectItemLists(ecViewModel.EcTypes, _service.GetBadmListData((int)Globals.CvIndexes.EC_TYPE));
                FillEcSelectLists(ecViewModel);
                ViewData["sent"] = 1;
                return View(ecViewModel);
            }

            return View(ecViewModel);
        }

        public async Task<IActionResult> AddEcOperation(int id)
        {
            var item = await _service.GetInstMainValuesByRecordIdAsync((int)Globals.Groups.GRP_EC, Utils.Utils.GetSite(HttpContext), id);
            GRP_ECViewModel instVM = _mapper.Map<GRP_EC, GRP_ECViewModel>(item);
            //CreateItemsList(instVM);
            //instVM.EcTypes.Remove(instVM.EcTypes.Where(c => c.Value.ToLower() == "installation").Single());
           
            return View("Create", instVM);
        }

        // GET: EcModelController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: EcModelController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: EcModelController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: EcModelController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: EcModelController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }

            GRP_ECViewModel ecVM = _mapper.Map<GRP_EC, GRP_ECViewModel>(item);
            return View(ecVM);
        }

        private void FillEcSelectLists(GRP_ECViewModel ecViewModel)
        {
            ModelUtils.FillSelectItemLists(ecViewModel.EcTypes, _service.GetBadmListData((int)Globals.CvIndexes.EC_TYPE));
            ModelUtils.FillSelectItemLists(ecViewModel.EcYn, _service.GetBadmListData((int)Globals.CvIndexes.EC_YN));
            ModelUtils.FillSelectItemLists(ecViewModel.EcSaAlign, _service.GetBadmListData((int)Globals.CvIndexes.SA_ALIGN));
            ModelUtils.FillSelectItemLists(ecViewModel.EcSaFormat, _service.GetBadmListData((int)Globals.CvIndexes.SA_FORMAT));
        }
    }
}
