using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IcosDataLib.Data;
using IcosModelLib.DTO;
using IcosDataLogicLib.Services;
using AutoMapper;
using IcosCoreExt.Models.ViewModels;
using IcosModelLib.Utils;
using IcosCoreExt.Models;
using System.Text;
using Microsoft.AspNetCore.Authorization;

namespace IcosCoreExt
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class UTCController : Controller
    {
        private readonly IGenericService<GRP_UTC_OFFSET> _service;
        private readonly IMapper _mapper;

        public UTCController(IGenericService<GRP_UTC_OFFSET> service, IMapper mapper)
        {
            this._mapper = mapper;
            this._service = service;
        }

        // GET: UTC
        public async Task<IActionResult> Index()
        {
            List<GRP_UTC_OFFSETViewModel> utcVM = new List<GRP_UTC_OFFSETViewModel>();
            //hard coded FA-Lso site
            IEnumerable<GRP_UTC_OFFSET> utcList = await _service.GetItemValuesAsync(Utils.Utils.GetSite(HttpContext));
            if(utcList != null)
            {
                
                foreach(var utc in utcList)
                {
                    utcVM.Add(_mapper.Map<GRP_UTC_OFFSETViewModel>(utc));
                }
            }
            return View(utcVM);
        }

        // GET: UTC/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var utc = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (utc == null)
            {
                return NotFound();
            }
            
            return View(_mapper.Map<GRP_UTC_OFFSETViewModel>(utc));
        }

        // GET: UTC/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: UTC/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UTC_OFFSET,UTC_OFFSET_DATE_START,UTC_OFFSET_COMMENT,Id,DataStatus,InsertUserId,InsertDate,DeleteUserId,DeletedDate,SiteId")] GRP_UTC_OFFSETViewModel utcVM)
        {
            if (ModelState.IsValid)
            {
                /*_context.Add(uTC);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));*/
                GRP_UTC_OFFSET utc = _mapper.Map<GRP_UTC_OFFSET>(utcVM);
                bool res = await _service.SaveItemAsync(utc, Utils.Utils.GetUser(HttpContext), Utils.Utils.GetSite(HttpContext));
                return RedirectToAction(nameof(Index));
            }
            return View(utcVM);
        }

        // GET: UTC/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var utc = await _service.GetItemValueAsync(70);
            if (utc == null)
            {
                return NotFound();
            }
            var utcVM = _mapper.Map<GRP_UTC_OFFSETViewModel>(utc);
            return View(utcVM);
        }

        // POST: UTC/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("UTC_OFFSET,UTC_OFFSET_DATE_START,UTC_OFFSET_COMMENT,Id,DataStatus")] GRP_UTC_OFFSETViewModel utcVM)
        {
            if (id != utcVM.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var utcModel = _mapper.Map<GRP_UTC_OFFSET>(utcVM);
                    bool res = await _service.UpdateItemAsync(id, Utils.Utils.GetSite(HttpContext), Utils.Utils.GetUser(HttpContext), utcModel);
                    if (res)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    /*if (!LOCATIONViewModelExists(lOCATIONViewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }*/
                    return RedirectToAction(nameof(Index)); //set error message...
                }

            }
            return View(utcVM);
        }

        // GET: UTC/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var utc = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (utc == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_UTC_OFFSETViewModel>(utc));
        }

        // POST: UTC/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var utc = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            //Hard coded: 70 is index f FA-Lso site
            //95 is id for superuser
            //To be substituted with id of choosen site and id of logged user 
            bool deleted = await _service.DeleteItemAsync(id, Utils.Utils.GetSite(HttpContext), Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }

        /*private bool UTCExists(int id)
        {
            // return _context.UTC.Any(e => e.Id == id);
            return true;
        }*/
    }
}
