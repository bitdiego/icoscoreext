﻿using AutoMapper;
using IcosCoreExt.Models.ViewModels;
using IcosDataLogicLib.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using IcosCoreExt.Models;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using IcosDataLogicLib.Services.Validators;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class SamplingPlotsController : Controller
    {
        private readonly IGenericService<GRP_PLOT> _service;
        private readonly IMapper _mapper;
        private readonly ISamplingSchemeValidator _validator;
        private int? SiteId = 0;

        public SamplingPlotsController(IGenericService<GRP_PLOT> service, IMapper mapper, ISamplingSchemeValidator validator)
        {
            _service = service;
            _mapper = mapper;
            _validator = validator;
        }
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_PLOT> samplingPlots = await _service.GetItemValuesAsync(Utils.Utils.GetSite(HttpContext));
            List<GRP_PLOTViewModel> samplingPlotsVM = new List<GRP_PLOTViewModel>();
            foreach (var plot in samplingPlots)
            {
                var plotVM = _mapper.Map<GRP_PLOTViewModel>(plot);
                samplingPlotsVM.Add(plotVM);
            }
            return View(samplingPlotsVM);
        }

        // GET: Controllers/Team/Create
        public IActionResult Create()
        {
            GRP_PLOTViewModel plotViewModel = new GRP_PLOTViewModel();
            ModelUtils.FillSelectItemLists(plotViewModel.PlotReference, _service.GetBadmListData((int)Globals.CvIndexes.PLOTREF));
            ModelUtils.FillSelectItemLists(plotViewModel.PlotType, _service.GetBadmListData((int)Globals.CvIndexes.PLOTYPE));
            //CreateItemsList(plotViewModel);
            return View(plotViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_PLOTViewModel plotVM)
        {
            GRP_PLOT model = _mapper.Map<GRP_PLOTViewModel, GRP_PLOT>(plotVM);
            if (ModelState.IsValid)
            {
                int vRes = _validator.SamplingSchemeValidate(model);
                if (vRes > 0)
                {
                    ModelState.AddModelError("PLOT_ID", ErrorCodes.ErrorsList[vRes]);
                    ModelUtils.FillSelectItemLists(plotVM.PlotReference, _service.GetBadmListData((int)Globals.CvIndexes.PLOTREF));
                    ModelUtils.FillSelectItemLists(plotVM.PlotType, _service.GetBadmListData((int)Globals.CvIndexes.PLOTYPE));
                    return View(plotVM);
                }
                bool res = await _service.SaveItemAsync(model, Utils.Utils.GetUser(HttpContext), Utils.Utils.GetSite(HttpContext));
                if (res)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return NotFound();
                }
            }
            ModelUtils.FillSelectItemLists(plotVM.PlotReference, _service.GetBadmListData((int)Globals.CvIndexes.PLOTREF));
            ModelUtils.FillSelectItemLists(plotVM.PlotType, _service.GetBadmListData((int)Globals.CvIndexes.PLOTYPE));
            return View(plotVM);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var item = await _service.GetItemValueByIdAsync(Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }

           GRP_PLOTViewModel instVM = _mapper.Map<GRP_PLOT, GRP_PLOTViewModel>(item);
           return View(instVM);
        }


        private void CreateItemsList(GRP_PLOTViewModel plotVM)
        {
            if (plotVM.PlotType.Count == 0)
            {
                IQueryable<string> ipType = _service.GetBadmListData((int)Globals.CvIndexes.PLOTYPE);
                ModelUtils.SetSelectItemList(ipType, plotVM.PlotType);
            }
            if (plotVM.PlotReference.Count == 0)
            {
                IQueryable<string> ipref = _service.GetBadmListData((int)Globals.CvIndexes.PLOTREF);
                ModelUtils.SetSelectItemList(ipref, plotVM.PlotReference);
            }
        }
        // GET: PlotController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_PLOTViewModel PlotViewModel = _mapper.Map<GRP_PLOT, GRP_PLOTViewModel>(item); 
            ModelUtils.FillSelectItemLists(PlotViewModel.PlotReference, _service.GetBadmListData((int)Globals.CvIndexes.PLOTREF));
            ModelUtils.FillSelectItemLists(PlotViewModel.PlotType, _service.GetBadmListData((int)Globals.CvIndexes.PLOTYPE));

            return View(PlotViewModel);
        }

        // POST: PlotController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_PLOTViewModel PlotViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_PLOT loc = _mapper.Map<GRP_PLOTViewModel, GRP_PLOT>(PlotViewModel);
                bool b = await _service.UpdateItemAsync(PlotViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
            ModelUtils.FillSelectItemLists(PlotViewModel.PlotReference, _service.GetBadmListData((int)Globals.CvIndexes.PLOTREF));
            ModelUtils.FillSelectItemLists(PlotViewModel.PlotType, _service.GetBadmListData((int)Globals.CvIndexes.PLOTYPE));

            return View(PlotViewModel);
        }

        // GET: DHPController/Delete/5

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_PLOTViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
