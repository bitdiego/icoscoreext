using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosDataLogicLib.Services.Validators;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class GaiController : Controller
    {
        private readonly IGenericService<GRP_GAI> _service;
        private readonly IMapper _mapper;
        private readonly IGaiValidator _gaiValidator;

        private readonly string Ecosystem="Grassland";

        public GaiController(IGenericService<GRP_GAI> service, IMapper mapper, IGaiValidator gaiValidator)
        {
            _service = service;
            _mapper = mapper;
            _gaiValidator = gaiValidator;
        }

        // GET: GaiController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_GAI> Gais = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_GAIViewModel> GaisVM = new List<GRP_GAIViewModel>();
            foreach (var tow in Gais)
            {
                var towVM = _mapper.Map<GRP_GAIViewModel>(tow);
                GaisVM.Add(towVM);
            }
            return View(GaisVM);
        }

        // GET: GaiController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_GAIViewModel towVM = _mapper.Map<GRP_GAI, GRP_GAIViewModel>(item);

            return View(towVM);
        }

        // GET: GaiController/Create
        public ActionResult Create()
        {
            GRP_GAIViewModel GaiViewModel = new GRP_GAIViewModel();
   
         ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PLOT_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.PLOTTYPE)); 
         ModelUtils.FillSelectItemLists(GaiViewModel.GAI_METHODTypes, _service.GetBadmListData((int)Globals.CvIndexes.GAIMETHOD)); 
         ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PHENTypes, _service.GetBadmListData((int)Globals.CvIndexes.PHEN)); 
         ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.GAIPTYPE)); 
         //ModelUtils.FillSelectItemLists(GaiViewModel.GAI_DHP_QCTypes, _service.GetBadmListData(122)); 
         //ModelUtils.FillSelectItemLists(GaiViewModel.GAI_DHP_QC_EXTTypes, _service.GetBadmListData(124)); 


            return View(GaiViewModel);
        }

        // POST: GaiController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_GAIViewModel GaiViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_GAI model = _mapper.Map<GRP_GAIViewModel, GRP_GAI>(GaiViewModel);
                int error = 0, globalError = 0;
                error = _gaiValidator.GaiPlotInDb(model.GAI_PLOT, 70);
                if (error > 0)
                {
                    string errorString = ErrorCodes.ErrorsList[error].Replace("$V0$", "GAI_PLOT");
                    ModelState.AddModelError("All", errorString);
                    globalError += error;
                    error = 0;
                }
                error = _gaiValidator.ValidateGaiByMethod(model, 70, Ecosystem);
                if (error > 0)
                {
                    string errorString = ErrorCodes.ErrorsList[error];
                    ModelState.AddModelError("All", errorString);
                    globalError += error;
                    error = 0;
                }
                if (globalError > 0)
                {
                    ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PLOT_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.PLOTTYPE));
                    ModelUtils.FillSelectItemLists(GaiViewModel.GAI_METHODTypes, _service.GetBadmListData((int)Globals.CvIndexes.GAIMETHOD));
                    ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PHENTypes, _service.GetBadmListData((int)Globals.CvIndexes.PHEN));
                    ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.GAIPTYPE));
                    ViewData["sent"] = 1;
                    return View(GaiViewModel);
                }
                else
                {
                    bool b = await _service.SaveItemAsync(model, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                    return RedirectToAction("Index");
                }
                
            }
            else
            {
                ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PLOT_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.PLOTTYPE));
                ModelUtils.FillSelectItemLists(GaiViewModel.GAI_METHODTypes, _service.GetBadmListData((int)Globals.CvIndexes.GAIMETHOD));
                ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PHENTypes, _service.GetBadmListData((int)Globals.CvIndexes.PHEN));
                ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.GAIPTYPE)); 

                return View(GaiViewModel);
            }
        }

        // GET: GaiController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_GAIViewModel GaiViewModel = _mapper.Map<GRP_GAI, GRP_GAIViewModel>(item);
            ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PLOT_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.PLOTTYPE));
            ModelUtils.FillSelectItemLists(GaiViewModel.GAI_METHODTypes, _service.GetBadmListData((int)Globals.CvIndexes.GAIMETHOD));
            ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PHENTypes, _service.GetBadmListData((int)Globals.CvIndexes.PHEN));
            ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.GAIPTYPE));
            //ModelUtils.FillSelectItemLists(GaiViewModel.GAI_DHP_QCTypes, _service.GetBadmListData(122)); 
            //ModelUtils.FillSelectItemLists(GaiViewModel.GAI_DHP_QC_EXTTypes, _service.GetBadmListData(124)); 


            return View(GaiViewModel);
        }

        // POST: GaiController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_GAIViewModel GaiViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_GAI loc = _mapper.Map<GRP_GAIViewModel, GRP_GAI>(GaiViewModel);
                bool b = await _service.UpdateItemAsync(GaiViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
            ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PLOT_TYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.PLOTTYPE));
            ModelUtils.FillSelectItemLists(GaiViewModel.GAI_METHODTypes, _service.GetBadmListData((int)Globals.CvIndexes.GAIMETHOD));
            ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PHENTypes, _service.GetBadmListData((int)Globals.CvIndexes.PHEN));
            ModelUtils.FillSelectItemLists(GaiViewModel.GAI_PTYPETypes, _service.GetBadmListData((int)Globals.CvIndexes.GAIPTYPE));
            ///ModelUtils.FillSelectItemLists(GaiViewModel.GAI_DHP_QCTypes, _service.GetBadmListData(122)); 
            //ModelUtils.FillSelectItemLists(GaiViewModel.GAI_DHP_QC_EXTTypes, _service.GetBadmListData(124)); 

            return View(GaiViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || id<=0)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_GAIViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
