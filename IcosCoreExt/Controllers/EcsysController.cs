using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
   [Authorize]
	 public class EcsysController : Controller
    {
        private readonly IGenericService<GRP_ECSYS> _service;
        private readonly IMapper _mapper;
        private readonly IGeneralInstModelValidator _ecSysValidator;

        public EcsysController(IGenericService<GRP_ECSYS> service, IMapper mapper, IGeneralInstModelValidator ecSysValidator)
        {
            _service = service;
            _mapper = mapper;
            _ecSysValidator = ecSysValidator;
        }

        // GET: EcsysController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_ECSYS> Ecsyss = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_ECSYSViewModel> EcsyssVM = new List<GRP_ECSYSViewModel>();
            foreach (var tow in Ecsyss)
            {
                var towVM = _mapper.Map<GRP_ECSYSViewModel>(tow);
                EcsyssVM.Add(towVM);
            }
            return View(EcsyssVM);
        }

        // GET: EcsysController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            if (id <= 0)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_ECSYSViewModel towVM = _mapper.Map<GRP_ECSYS, GRP_ECSYSViewModel>(item);

            return View(towVM);
        }

        // GET: EcsysController/Create
        public ActionResult Create()
        {
            GRP_ECSYSViewModel EcsysViewModel = new GRP_ECSYSViewModel();

            return View(EcsysViewModel);
        }

        // POST: EcsysController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_ECSYSViewModel EcsysViewModel)
        {
            //validation rules:
            //1. GA / SA come from select list, they should not contain robbish: check anyway???
            //2. the ECSYS date must be >= the date of GA / SA in the GRP_EC and must be bound to installed and not removed sensors
            if (ModelState.IsValid)
            {
                GRP_ECSYS model = _mapper.Map<GRP_ECSYSViewModel, GRP_ECSYS>(EcsysViewModel);
                int globalErr = 0;
                int error = await _ecSysValidator.InstrumentInGrpEcAsync(model.ECSYS_GA_MODEL, model.ECSYS_GA_SN, model.ECSYS_DATE, Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    globalErr += error;
                    ModelState.AddModelError("ECSYS_GA_MODEL", ErrorCodes.ErrorsList[error]);
                    error = 0;
                }
                error = await _ecSysValidator.InstrumentInGrpEcAsync(model.ECSYS_SA_MODEL, model.ECSYS_SA_SN, model.ECSYS_DATE, Utils.Utils.GetSite(HttpContext));
                if (error > 0)
                {
                    globalErr += error;
                    ModelState.AddModelError("ECSYS_SA_MODEL", ErrorCodes.ErrorsList[error]);
                    error = 0;
                }
                if (globalErr > 0)
                {
                    ViewData["sent"] = 1;
                    return View(EcsysViewModel);
                }

                bool b = await _service.SaveItemAsync(model, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["sent"] = 1;
                return View(EcsysViewModel);
            }
        }

        // GET: EcsysController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_ECSYSViewModel EcsysViewModel = _mapper.Map<GRP_ECSYS, GRP_ECSYSViewModel>(item);
            
            return View(EcsysViewModel);
        }

        // POST: EcsysController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_ECSYSViewModel EcsysViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_ECSYS loc = _mapper.Map<GRP_ECSYSViewModel, GRP_ECSYS>(EcsysViewModel);
                bool b = await _service.UpdateItemAsync(EcsysViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }

            return View(EcsysViewModel);
        }

	// GET: DHPController/Delete/5
        
       public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dhp = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (dhp == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_ECSYSViewModel>(dhp));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
