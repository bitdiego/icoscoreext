﻿using IcosCoreExt.Controllers.api.ApiObjects;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class UIFacilitiesController : ControllerBase
    {
        public IConfiguration Configuration { get; }
        public UIFacilitiesController(IConfiguration _conf)
        {
            Configuration = _conf;
        }

        [Produces("application/json")]
        [HttpGet("ecops")]
        public IEnumerable<string> EcOps()
        {
            try
            {
                List<string> EcOps = new List<string>();
                SqlConnection _conn = new SqlConnection();
                _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
                _conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _conn;
                cmd.CommandText = @"select Name from Variables where group_id=2003 and name not in ('EC_MODEL', 'EC_SN', 'EC_TYPE') order by id_variable";
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    EcOps.Add(reader["Name"].ToString());
                }
                reader.Close();
                _conn.Close();
                return EcOps;

            }
            catch
            {
                return null;// BadRequest();
            }
        }

        [Produces("application/json")]
        [HttpGet("bmops")]
        public IEnumerable<string> BmOps()
        {
            try
            {
                List<string> BmOps = new List<string>();
                SqlConnection _conn = new SqlConnection();
                _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
                _conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _conn;
                cmd.CommandText = @"select Name from Variables where group_id=2005 and name not in ('BM_MODEL', 'BM_SN', 'BM_TYPE','BM_COMMENT') order by id_variable";
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    BmOps.Add(reader["Name"].ToString());
                }
                reader.Close();
                _conn.Close();
                return BmOps;

            }
            catch
            {
                return null;// BadRequest();
            }
        }

        [Produces("application/json")]
        [HttpGet("getbmops")]
        public IEnumerable<BadmOpsObj> GetBmOps()
        {
            List<BadmOpsObj> badmOps = new List<BadmOpsObj>();

            SqlConnection _conn = new SqlConnection();
            _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            _conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = _conn;
            cmd.CommandText = @"select Bm_OP, Variable, Mandatory, Constraints from [dbo].[BmOps] order by Bm_OP";
            SqlDataReader reader = cmd.ExecuteReader();
            string preOp = "";
            BadmOpsObj obj;
            BadmOpsObj preObj = null;
            while (reader.Read())
            {
                string actOp = reader["Bm_OP"].ToString();
                if (String.Compare(preOp, actOp, true) != 0)
                {
                    preOp = actOp;
                    obj = new BadmOpsObj();
                    obj.Type = actOp;
                    obj.OpList = new List<Operation>();
                    badmOps.Add(obj);
                    preObj = badmOps.Last();
                }
                Operation op = new Operation
                {
                    Variable = reader["Variable"].ToString(),
                    Mandatory = (reader["Mandatory"].ToString() == "1") ? true : false,
                    Constraints = (String.IsNullOrEmpty(reader["Constraints"].ToString())) ? null : (char?)reader["Constraints"].ToString().ToCharArray()[0]
                };
                preObj.OpList.Add(op);
            }
            reader.Close();
            _conn.Close();
            return badmOps;
        }

        [Produces("application/json")]
        [HttpGet("getecops")]
        public IEnumerable<BadmOpsObj> GetEcOps()
        {
            List<BadmOpsObj> badmOps = new List<BadmOpsObj>();

            SqlConnection _conn = new SqlConnection();
            _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            _conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = _conn;
            cmd.CommandText = @"SELECT [id] ,[EC_OP] ,[Variable] ,[Mandatory]  ,[Constraints]  ,[Model]  FROM [EcOps] order by EC_OP";
            SqlDataReader reader = cmd.ExecuteReader();
            string preOp = "";
            BadmOpsObj obj;
            BadmOpsObj preObj = null;
            while (reader.Read())
            {
                string actOp = reader["EC_OP"].ToString();
                if (String.Compare(preOp, actOp, true) != 0)
                {
                    preOp = actOp;
                    obj = new BadmOpsObj();
                    obj.Type = actOp;
                    obj.OpList = new List<Operation>();
                    badmOps.Add(obj);
                    preObj = badmOps.Last();
                }
                Operation op = new Operation
                {
                    Variable = reader["Variable"].ToString(),
                    Mandatory = (reader["Mandatory"].ToString() == "1") ? true : false,
                    Constraints = (String.IsNullOrEmpty(reader["Constraints"].ToString())) ? null : (char?)reader["Constraints"].ToString().ToCharArray()[0],
                    Model = (String.IsNullOrEmpty(reader["Model"].ToString())) ? null : reader["Model"].ToString()
                };
                preObj.OpList.Add(op);
            }
            reader.Close();
            _conn.Close();
            return badmOps;
        }

        [Produces("application/json")]
        [HttpGet("getecavinst")]
        public IEnumerable<AvailableEcInstrument> GetEcAvInst(int siteId)
        {
            List<AvailableEcInstrument> ecInstList = new List<AvailableEcInstrument>();
            IEnumerable<string> ops = GetListOfEcOps();
            //select GA_CP, Ga_pump, SA_Gill from GRP_INST but not in GRP_EC (not installed) + select GA_CP, Ga_pump, SA_Gill from GRP_EC getting last op

            SqlConnection _conn = new SqlConnection();
            _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            _conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = _conn;
            cmd.CommandText = @"SELECT [INST_MODEL] ,[INST_SN]  FROM [GRP_INST] where siteId="+siteId+ " and INST_FACTORY='Purchase' And (INST_MODEL Like 'GA_CP%' OR INST_MODEL Like 'GA_P%' OR INST_MODEL Like 'SA%') order by INST_MODEL";
            SqlDataReader reader = cmd.ExecuteReader();
          
            int _id = 1;
            while (reader.Read())
            {
                AvailableEcInstrument ec = new AvailableEcInstrument()
                {
                    Model=reader["INST_MODEL"].ToString(),
                    Sn = reader["INST_SN"].ToString(),
                    Id=_id,
                    PossibleOps = ops.ToList()
                };
                _id++;
                //DIEGO: perhaps not feasible
                /*SqlCommand _ecCmd = new SqlCommand();
                _ecCmd.CommandText = "SELECT Top 1 * FROM GRP_EC Where EC_MODEL='"+ec.Model+"' AND EC_SN = '"+ec.Sn+"' And siteId=70 and EC_TYPE in ('Installation', 'Removal') order by EC_DATE desc";
                _ecCmd.Connection = _conn;
                SqlDataReader _ecReader = _ecCmd.ExecuteReader();
                if (_ecReader.HasRows)
                {
                    _ecReader.Read();
                    string lastOp = _ecReader["EC_TYPE"].ToString();
                    
                    if (String.Compare(lastOp, "installation", true) == 0)
                    {
                        ec.PossibleOps.Remove( "Installation");
                    }
                    else  //last op is removal 
                    {
                        ec.PossibleOps.Remove("Removal");
                    }
                }
                _ecReader.Close();*/
                
                ecInstList.Add(ec);
            }
            reader.Close();
            _conn.Close();/**/
            return ecInstList;
        }

        [Produces("application/json")]
        [HttpGet("getstoops")]
        public IEnumerable<BadmOpsObj> GetStoOps()
        {
            List<BadmOpsObj> badmOps = new List<BadmOpsObj>();

            SqlConnection _conn = new SqlConnection();
            _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            _conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = _conn;
            cmd.CommandText = @"select Sto_OP, Variable, Mandatory, Constraints from [dbo].[StoOps] order by Sto_OP";
            SqlDataReader reader = cmd.ExecuteReader();
            string preOp = "";
            BadmOpsObj obj;
            BadmOpsObj preObj = null;
            while (reader.Read())
            {
                string actOp = reader["Sto_OP"].ToString();
                if (String.Compare(preOp, actOp, true) != 0)
                {
                    preOp = actOp;
                    obj = new BadmOpsObj();
                    obj.Type = actOp;
                    obj.OpList = new List<Operation>();
                    badmOps.Add(obj);
                    preObj = badmOps.Last();
                }
                Operation op = new Operation
                {
                    Variable = reader["Variable"].ToString(),
                    Mandatory = (reader["Mandatory"].ToString() == "1") ? true : false,
                    Constraints = (String.IsNullOrEmpty(reader["Constraints"].ToString())) ? null : (char?)reader["Constraints"].ToString().ToCharArray()[0]
                };
                preObj.OpList.Add(op);
            }
            reader.Close();
            _conn.Close();
            return badmOps;
        }


        [HttpGet("gettooltip")]
        public ContentResult Gettooltip(string varname)
        {
            string content = "";
            try
            {
                List<string> EcOps = new List<string>();
                SqlConnection _conn = new SqlConnection();
                _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
                _conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _conn;
                cmd.CommandText = @"select Description from Variables where name='"+ varname.Substring(1) + "'";
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    content = reader["Description"].ToString();
                }
                reader.Close();
                _conn.Close();
            }
            catch
            {
            }

            return new ContentResult
            {
                ContentType = "text/html",
                StatusCode = (int)HttpStatusCode.OK,
                Content = content
            };
            
        }

        [Produces("application/json")]
        [HttpGet("getavailableloggers")]
        public IEnumerable<AvailableEcInstrument> GetAvailableLoggers(int siteId)
        {
            List<AvailableEcInstrument> loggerList = new List<AvailableEcInstrument>();
            List<AvailableEcInstrument> justIn = new List<AvailableEcInstrument>();

            //select GA_CP, Ga_pump, SA_Gill from GRP_INST but not in GRP_EC (not installed) + select GA_CP, Ga_pump, SA_Gill from GRP_EC getting last op

            SqlConnection _conn = new SqlConnection();
            _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            _conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = _conn;
            cmd.CommandText = @"SELECT t1.INST_MODEL, t1.INST_SN
FROM GRP_INST t1
LEFT JOIN GRP_LOGGER t2 ON t2.LOGGER_MODEL = t1.INST_MODEL AND t2.LOGGER_SN = t1.INST_SN
WHERE t2.LOGGER_MODEL IS NULL AND t1.SiteId=@siteId and t1.DataStatus=0 
and t1.INST_FACTORY='Purchase' AND t1.INST_MODEL like 'DL%'";
            SqlParameter sitePar = new SqlParameter("siteId", (int)siteId);
            cmd.Parameters.Add(sitePar);
            SqlDataReader reader = cmd.ExecuteReader();

            int _id = 1;
            
            while (reader.Read())
            {
                AvailableEcInstrument logger = new AvailableEcInstrument()
                {
                    Model = reader["INST_MODEL"].ToString(),
                    Sn = reader["INST_SN"].ToString(),
                    Id = _id
                };
                _id++;
                loggerList.Add(logger);
                
            }
            cmd.Parameters.Clear();
            reader.Close();
            cmd.CommandText = @"SELECT * FROM GRP_LOGGER WHERE siteId=@lsiteId and datastatus=0 order by LOGGER_ID, LOGGER_DATE DESC";
            SqlParameter lSitePar = new SqlParameter("lsiteId", (int)siteId);
            cmd.Parameters.Add(lSitePar);
            reader = cmd.ExecuteReader();
            int _prevLoggerId = 0;
            while (reader.Read())
            {
                string _tm = reader["LOGGER_MODEL"].ToString();
                string _ts = reader["LOGGER_SN"].ToString();
                int loggerId = Convert.ToInt32(reader["LOGGER_ID"]);
                if (_prevLoggerId == 0)
                {
                    _prevLoggerId = loggerId;
                    justIn.Add(new AvailableEcInstrument {
                        Model = reader["LOGGER_MODEL"].ToString(),
                        Sn = reader["LOGGER_SN"].ToString(),
                        Id = 0
                    });
                }
                else
                {
                    if (_prevLoggerId == loggerId)
                    {
                        AvailableEcInstrument logger = new AvailableEcInstrument()
                        {
                            Model = reader["LOGGER_MODEL"].ToString(),
                            Sn = reader["LOGGER_SN"].ToString(),
                            Id = _id
                        };
                        if (!loggerList.Contains(logger) && !justIn.Any(lg=>lg.Model== reader["LOGGER_MODEL"].ToString() && lg.Sn== reader["LOGGER_SN"].ToString()))
                        {
                            _id++;
                            loggerList.Add(logger);
                        }
                    }
                    else
                    {
                        _prevLoggerId = loggerId;
                        justIn.Add(new AvailableEcInstrument
                        {
                            Model = reader["LOGGER_MODEL"].ToString(),
                            Sn = reader["LOGGER_SN"].ToString(),
                            Id = 0
                        });
                    }
                }
            }
            cmd.Parameters.Clear();
            reader.Close();
            _conn.Close();
            return loggerList;
        }

        [Produces("application/json")]
        [HttpGet("getavailablebminst")]
        public IEnumerable<AvailableEcInstrument> GetAvailableBmInst(int siteId)
        {
            List<AvailableEcInstrument> bmList = new List<AvailableEcInstrument>();
            List<AvailableEcInstrument> justIn = new List<AvailableEcInstrument>();

            //select GA_CP, Ga_pump, SA_Gill from GRP_INST but not in GRP_EC (not installed) + select GA_CP, Ga_pump, SA_Gill from GRP_EC getting last op

            SqlConnection _conn = new SqlConnection();
            _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            _conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = _conn;
            cmd.CommandText = @"SELECT t1.INST_MODEL, t1.INST_SN
FROM GRP_INST t1
LEFT JOIN GRP_BM t2 ON t2.BM_MODEL = t1.INST_MODEL AND t2.BM_SN = t1.INST_SN
WHERE t2.BM_MODEL IS NULL AND t1.SiteId=@siteId and t1.DataStatus=0 
and t1.INST_FACTORY='Purchase' AND t1.INST_MODEL NOT like 'DL%' AND t1.INST_MODEL NOT like 'GA_CP%'";

            cmd.CommandText = @"SELECT [INST_MODEL] ,[INST_SN]  FROM [GRP_INST] where siteId=@siteId and INST_FACTORY = 'Purchase' 
And (INST_MODEL NOT Like 'GA_CP%' AND INST_MODEL NOT Like 'DL%' AND INST_MODEL NOT LIKE 'SA-G%')  and DataStatus=0 order by INST_MODEL, INST_SN";

            SqlParameter sitePar = new SqlParameter("siteId", (int)siteId);
            cmd.Parameters.Add(sitePar);
            SqlDataReader reader = cmd.ExecuteReader();

            int _id = 1;

            while (reader.Read())
            {
                AvailableEcInstrument bmInst = new AvailableEcInstrument()
                {
                    Model = reader["INST_MODEL"].ToString(),
                    Sn = reader["INST_SN"].ToString(),
                    Id = _id++
                };
                
                bmList.Add(bmInst);

            }
            cmd.Parameters.Clear();
            reader.Close();
           // cmd.CommandText = @"select distinct [BM_MODEL], [BM_SN] from GRP_BM where siteID=@lsiteId and datastatus = 0  order by [BM_MODEL], [BM_SN] ";
          //  SqlParameter lSitePar = new SqlParameter("lsiteId", (int)siteId);
         //   cmd.Parameters.Add(lSitePar);
          //  reader = cmd.ExecuteReader();
           /* int _prevLoggerId = 0;
            while (reader.Read())
            {
                string _tm = reader["BM_MODEL"].ToString();
                string _ts = reader["BM_SN"].ToString();
                if (_prevLoggerId == 0)
                {
                    justIn.Add(new AvailableEcInstrument
                    {
                        Model = reader["BM_MODEL"].ToString(),
                        Sn = reader["BM_SN"].ToString(),
                        Id = 0
                    });
                }
                else
                {
                    if (_prevLoggerId == loggerId)
                    {
                        AvailableEcInstrument logger = new AvailableEcInstrument()
                        {
                            Model = reader["LOGGER_MODEL"].ToString(),
                            Sn = reader["LOGGER_SN"].ToString(),
                            Id = _id
                        };
                        if (!loggerList.Contains(logger) && !justIn.Any(lg => lg.Model == reader["LOGGER_MODEL"].ToString() && lg.Sn == reader["LOGGER_SN"].ToString()))
                        {
                            _id++;
                            loggerList.Add(logger);
                        }
                    }
                    else
                    {
                        _prevLoggerId = loggerId;
                        justIn.Add(new AvailableEcInstrument
                        {
                            Model = reader["LOGGER_MODEL"].ToString(),
                            Sn = reader["LOGGER_SN"].ToString(),
                            Id = 0
                        });
                    }
                }
            }*/
            
            cmd.Parameters.Clear();
            reader.Close();
            _conn.Close();
            return bmList;
        }

        [Produces("application/json")]
        [HttpGet("getavailablestoinst")]
        public IEnumerable<AvailableEcInstrument> GetAvailableStoInst(int siteId)
        {
            List<AvailableEcInstrument> stoList = new List<AvailableEcInstrument>();

            //select GA_CP, Ga_pump, SA_Gill from GRP_INST but not in GRP_EC (not installed) + select GA_CP, Ga_pump, SA_Gill from GRP_EC getting last op

            SqlConnection _conn = new SqlConnection();
            _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            _conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = _conn;

            cmd.CommandText = @"SELECT [INST_MODEL] ,[INST_SN]  FROM[GRP_INST] where siteId=@siteId and INST_FACTORY = 'Purchase' 
And (INST_MODEL Like 'GA_CP%' OR INST_MODEL Like 'GA_OP%' OR INST_MODEL Like 'GA_SR%' OR INST_MODEL Like 'GA-Oth%') and DataStatus=0 order by INST_MODEL, INST_SN";

            SqlParameter sitePar = new SqlParameter("siteId", (int)siteId);
            cmd.Parameters.Add(sitePar);
            SqlDataReader reader = cmd.ExecuteReader();

            int _id = 1;

            while (reader.Read())
            {
                AvailableEcInstrument bmInst = new AvailableEcInstrument()
                {
                    Model = reader["INST_MODEL"].ToString(),
                    Sn = reader["INST_SN"].ToString(),
                    Id = _id++
                };

                stoList.Add(bmInst);

            }
            cmd.Parameters.Clear();
            reader.Close();
            //cmd.CommandText = @"select distinct [BM_MODEL], [BM_SN] from GRP_BM where siteID=@lsiteId and datastatus = 0  order by [BM_MODEL], [BM_SN] ";
            //SqlParameter lSitePar = new SqlParameter("lsiteId", (int)siteId);
            //cmd.Parameters.Add(lSitePar);
           // reader = cmd.ExecuteReader();
           

            cmd.Parameters.Clear();
            //reader.Close();
            _conn.Close();
            return stoList;
        }

        [Produces("application/json")]
        [HttpGet("getavailableecsys")]
        public IEnumerable<AvailableEcInstrument> GetAvailableEcSys(string prefix, int siteId)
        {
            List<AvailableEcInstrument> ecSysList = new List<AvailableEcInstrument>();
            List<AvailableEcInstrument> justIn = new List<AvailableEcInstrument>();

            //select GA_CP, Ga_pump, SA_Gill from GRP_INST but not in GRP_EC (not installed) + select GA_CP, Ga_pump, SA_Gill from GRP_EC getting last op

            SqlConnection _conn = new SqlConnection();
            _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            _conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = _conn;

            cmd.CommandText = @"SELECT t1.INST_MODEL, t1.INST_SN
FROM GRP_INST t1
LEFT JOIN GRP_EC t2 ON t2.EC_MODEL = t1.INST_MODEL AND t2.EC_SN = t1.INST_SN
WHERE t2.EC_MODEL IS NULL AND t1.SiteId=@siteId and t1.DataStatus=0 
and t1.INST_FACTORY='Purchase' AND t1.INST_MODEL like @prefix +'%'";
            SqlParameter sitePar = new SqlParameter("siteId", (int)siteId);
            SqlParameter prefixPar = new SqlParameter("prefix", (string)prefix);
            cmd.Parameters.Add(sitePar);
            cmd.Parameters.Add(prefixPar);
            SqlDataReader reader = cmd.ExecuteReader();

            int _id = 1;

            while (reader.Read())
            {
                string model = reader["INST_MODEL"].ToString().ToLower();
                if(!model.StartsWith("ga_pump") && !model.StartsWith("ga_cp"))
                {
                    continue;
                }
                AvailableEcInstrument ecSys = new AvailableEcInstrument()
                {
                    Model = reader["INST_MODEL"].ToString(),
                    Sn = reader["INST_SN"].ToString(),
                    Id = _id
                };
                _id++;
                ecSysList.Add(ecSys);

            }
            cmd.Parameters.Clear();
            reader.Close();
            cmd.CommandText = @"select * from GRP_EC where siteId=@lsiteId and EC_MODEL like @ecprefix + '%' and EC_TYPE in ('installation','removal') and datastatus=0 order by EC_MODEL, EC_SN, EC_DATE desc";
            SqlParameter lSitePar = new SqlParameter("lsiteId", (int)siteId);
            SqlParameter ecprefixPar = new SqlParameter("ecprefix", (string)prefix);
            cmd.Parameters.Add(lSitePar);
            cmd.Parameters.Add(ecprefixPar);
            reader = cmd.ExecuteReader();
            string _removedModel = "", _removedSn = "";
            while (reader.Read())
            {
                string _tm = reader["EC_MODEL"].ToString();
                string _ts = reader["EC_SN"].ToString();
                string _ectype = reader["EC_TYPE"].ToString();
                if(String.Compare(_ectype,"installation", true) == 0)
                {
                    if(String.Compare(_tm, _removedModel, true)==0 && String.Compare(_ts, _removedSn, true) == 0)
                    {
                        continue;
                    }
                    if(!ecSysList.Any(ecsys=>ecsys.Model==_tm && ecsys.Sn == _ts))
                    {
                        ecSysList.Add(new AvailableEcInstrument()
                        {
                            Model = _tm,
                            Sn = _ts,
                            Id = _id
                        });
                        _id++;
                    }
                }
                else
                {
                    _removedModel = _tm;
                    _removedSn = _ts;
                }
                //int loggerId = Convert.ToInt32(reader["LOGGER_ID"]);
                /*if (_prevLoggerId == 0)
                {
                    _prevLoggerId = loggerId;
                    justIn.Add(new AvailableEcInstrument
                    {
                        Model = reader["LOGGER_MODEL"].ToString(),
                        Sn = reader["LOGGER_SN"].ToString(),
                        Id = 0
                    });
                }
                else
                {
                    
                }*/
            }
            cmd.Parameters.Clear();
            reader.Close();
            _conn.Close();
            return ecSysList;
        }

        [Produces("application/json")]
        [HttpGet("getgaimethods")]
        public IEnumerable<string> GetGaiMethods(string ecosystem)
        {
            List<string> gaiMethods = new List<string>();

            SqlConnection _conn = new SqlConnection();
            _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            _conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = _conn;
            cmd.CommandText = @"SELECT distinct method from [dbo].[GaiOps] where ecosystem=@ecosys or ecosystem is null";
            SqlParameter ecoParam = new SqlParameter("ecosys", (string)ecosystem);
            cmd.Parameters.Add(ecoParam);
            SqlDataReader reader = cmd.ExecuteReader();
            
            while (reader.Read())
            {

                gaiMethods.Add(reader["method"].ToString());
            }
            reader.Close();
            _conn.Close();
            return gaiMethods;
        }

        [Produces("application/json")]
        [HttpGet("getgaiuiops")]
        public IEnumerable<GaiOps> GetGaiUIOps(string ecosystem)
        {
            List<GaiOps> gaiOps = new List<GaiOps>();

            SqlConnection _conn = new SqlConnection();
            _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            _conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = _conn;
            cmd.CommandText = @"SELECT Method, Plot, Ecosystem, Variable, Mandatory,Constraints FROM [dbo].[GaiOps] Where (ecosystem=@ecosys or ecosystem is null) order by method";
            SqlParameter ecoParam = new SqlParameter("ecosys", (string)ecosystem);
            cmd.Parameters.Add(ecoParam);
            SqlDataReader reader = cmd.ExecuteReader();
            string preMethod = "";
            GaiOps obj;
            GaiOps preObj = null;
            while (reader.Read())
            {
                string actMethod = reader["Method"].ToString();
                if (String.Compare(preMethod, actMethod, true) != 0)
                {
                    preMethod = actMethod;
                    obj = new GaiOps();
                    obj.Method = actMethod;
                    obj.GaiOpsList= new List<GaiUI>();
                    gaiOps.Add(obj);
                    preObj = gaiOps.Last();
                }
                GaiUI gaiUI = new GaiUI
                {
                    Variable = reader["Variable"].ToString(),
                    Mandatory = (reader["Mandatory"].ToString() == "1") ? true : false,
                    Constraints = (String.IsNullOrEmpty(reader["Constraints"].ToString())) ? null : reader["Constraints"].ToString(),
                    Plot= reader["Plot"].ToString(),
                    Ecosystem= reader["Ecosystem"].ToString()
                    //Model = (String.IsNullOrEmpty(reader["Model"].ToString())) ? null : reader["Model"].ToString()
                };
                preObj.GaiOpsList.Add(gaiUI);
            }
            reader.Close();
            _conn.Close();
            return gaiOps;
        }

        private IEnumerable<string> GetListOfEcOps()
        {
            List<string> ecOpsList = new List<string>();

            SqlConnection _conn = new SqlConnection();
            _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            _conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = _conn;
            cmd.CommandText = @"select shortname from BADMList where cv_index = 68 ";

            SqlDataReader reader = cmd.ExecuteReader();
            while(reader.Read())
            {
                ecOpsList.Add(reader["shortname"].ToString());
            }
            reader.Close();
            _conn.Close();/**/
            return ecOpsList;
        }

        #region INSTMAN
        [Produces("application/json")]
        [HttpGet("getinstmanops")]
        public IEnumerable<BadmOpsObj> GetInstmanOps()
        {
            List<BadmOpsObj> badmOps = new List<BadmOpsObj>();

            SqlConnection _conn = new SqlConnection();
            _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            _conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = _conn;
            cmd.CommandText = @"select Instman_OP, Variable, Mandatory, Constraints, Model from InstManOps order by Instman_OP";
            SqlDataReader reader = cmd.ExecuteReader();
            string preOp = "";
            BadmOpsObj obj;
            BadmOpsObj preObj = null;
            while (reader.Read())
            {
                string actOp = reader["Instman_OP"].ToString();
                if (String.Compare(preOp, actOp, true) != 0)
                {
                    preOp = actOp;
                    obj = new BadmOpsObj();
                    obj.Type = actOp;
                    obj.OpList = new List<Operation>();
                    badmOps.Add(obj);
                    preObj = badmOps.Last();
                }
                Operation op = new Operation
                {
                    Variable = reader["Variable"].ToString(),
                    Mandatory = (reader["Mandatory"].ToString() == "1") ? true : false,
                    Constraints = (String.IsNullOrEmpty(reader["Constraints"].ToString())) ? null : (char?)reader["Constraints"].ToString().ToCharArray()[0],
                    Model = (String.IsNullOrEmpty(reader["Model"].ToString())) ? null : reader["Model"].ToString()
                };
                preObj.OpList.Add(op);
            }
            reader.Close();
            _conn.Close();
            return badmOps;
        }

        [Produces("application/json")]
        [HttpGet("getavailableinstmaninst")]
        public IEnumerable<AvailableEcInstrument> GetAvailableInstmanInst(int siteId)
        {
            List<AvailableEcInstrument> bmList = new List<AvailableEcInstrument>();
            List<AvailableEcInstrument> justIn = new List<AvailableEcInstrument>();

            //select GA_CP, Ga_pump, SA_Gill from GRP_INST but not in GRP_EC (not installed) + select GA_CP, Ga_pump, SA_Gill from GRP_EC getting last op

            SqlConnection _conn = new SqlConnection();
            _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            _conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = _conn;
            cmd.CommandText = @"SELECT t1.INST_MODEL, t1.INST_SN
FROM GRP_INST t1
LEFT JOIN GRP_BM t2 ON t2.BM_MODEL = t1.INST_MODEL AND t2.BM_SN = t1.INST_SN
WHERE t2.BM_MODEL IS NULL AND t1.SiteId=@siteId and t1.DataStatus=0 
and t1.INST_FACTORY='Purchase' AND t1.INST_MODEL NOT like 'DL%' AND t1.INST_MODEL NOT like 'GA_CP%'";

            cmd.CommandText = @"SELECT [INST_MODEL] ,[INST_SN]  FROM [GRP_INST] where siteId=@siteId and INST_FACTORY = 'Purchase'  AND INST_MODEL NOT like 'DL%' and DataStatus=0 order by INST_MODEL, INST_SN";

            SqlParameter sitePar = new SqlParameter("siteId", (int)siteId);
            cmd.Parameters.Add(sitePar);
            SqlDataReader reader = cmd.ExecuteReader();

            int _id = 1;

            while (reader.Read())
            {
                AvailableEcInstrument bmInst = new AvailableEcInstrument()
                {
                    Model = reader["INST_MODEL"].ToString(),
                    Sn = reader["INST_SN"].ToString(),
                    Id = _id++
                };

                bmList.Add(bmInst);

            }
            cmd.Parameters.Clear();
            reader.Close();
            

            cmd.Parameters.Clear();
            reader.Close();
            _conn.Close();
            return bmList;
        }

        [Produces("application/json")]
        [HttpGet("getavailableinstpairinst")]
        public IEnumerable<AvailableEcInstrument> GetAvailableInstPairInst(int siteId)
        {
            List<AvailableEcInstrument> bmList = new List<AvailableEcInstrument>();
            List<AvailableEcInstrument> justIn = new List<AvailableEcInstrument>();

            //select GA_CP, Ga_pump, SA_Gill from GRP_INST but not in GRP_EC (not installed) + select GA_CP, Ga_pump, SA_Gill from GRP_EC getting last op

            SqlConnection _conn = new SqlConnection();
            _conn.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            _conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = _conn;
           
            cmd.CommandText = @"SELECT [INST_MODEL] ,[INST_SN]  FROM [GRP_INST] where siteId=@siteId and INST_FACTORY = 'Purchase'  AND (INST_MODEL like 'SA-%' OR INST_MODEL like 'GA_C%' OR INST_MODEL like 'GA_O%')
and DataStatus=0 order by INST_MODEL, INST_SN";

            SqlParameter sitePar = new SqlParameter("siteId", (int)siteId);
            cmd.Parameters.Add(sitePar);
            SqlDataReader reader = cmd.ExecuteReader();

            int _id = 1;

            while (reader.Read())
            {
                AvailableEcInstrument bmInst = new AvailableEcInstrument()
                {
                    Model = reader["INST_MODEL"].ToString(),
                    Sn = reader["INST_SN"].ToString(),
                    Id = _id++
                };

                bmList.Add(bmInst);

            }
            cmd.Parameters.Clear();
            reader.Close();


            cmd.Parameters.Clear();
            reader.Close();
            _conn.Close();
            return bmList;
        }

        #endregion

    }

    public class BadmOpsObj
    {
        public string Type { get; set; }
        public List<Operation> OpList { get; set; }
    }


    public class Operation
    {
        public string Variable { get; set; }
        public bool Mandatory { get; set; }
        public char? Constraints { get; set; }
        public string Model { get; set; }
    }

    public class AvailableEcInstrument
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public string Sn { get; set; }
        public List<string> PossibleOps { get; set; }
    }
}


