﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers.api.ApiObjects
{
    public class GaiOps
    {
        public string Method { get; set; }
        public List<GaiUI> GaiOpsList { get; set; }
    }

    public class GaiUI
    {
        //public string Method { get; set; }
        public string Plot { get; set; }
        public string Ecosystem { get; set; }
        public string Variable { get; set; }
        public bool Mandatory { get; set; }
        public string Constraints { get; set; }
    }
}
