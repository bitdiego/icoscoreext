﻿using IcosDataLib.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace IcosCoreExt.Controllers.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchPlotController : ControllerBase
    {
        private IcosDbContext db;

        public SearchPlotController(IcosDbContext _db)
        {
            db = _db;
        }

        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [Produces("application/json")]
        [HttpGet("Search")]
        public async Task<IEnumerable<string>> Search()
        {
            try
            {
                string term = HttpContext.Request.Query["query"].ToString();
                string siteId = HttpContext.Request.Query["siteID"].ToString();
                var names = await db.GRP_PLOT.Where(pl => pl.SiteId == int.Parse(siteId) && pl.PLOT_ID.Contains(term) && pl.DataStatus==0)
                    .Select(pn => pn.PLOT_ID).ToListAsync();

                return names;

            }
            catch
            {
                return null;
            }
        }

        [Produces("application/json")]
        [HttpGet("searchsp")]
        public async Task<IEnumerable<string>> SearchSp()
        {
            try
            {
                string term = HttpContext.Request.Query["query"].ToString();
                string siteId = HttpContext.Request.Query["siteID"].ToString();
                var names = await db.GRP_PLOT.Where(pl => pl.SiteId == int.Parse(siteId) && pl.PLOT_ID.Contains(term) && pl.DataStatus == 0 && pl.PLOT_ID.ToLower().StartsWith("sp"))
                    .Select(pn => pn.PLOT_ID).ToListAsync();

                return names;

            }
            catch
            {
                return null;
            }
        }

        [Produces("application/json")]
        [HttpGet("searchcpspi")]
        public async Task<IEnumerable<string>> SearchCpSpI()
        {
            try
            {
                string term = HttpContext.Request.Query["query"].ToString();
                string siteId = HttpContext.Request.Query["siteID"].ToString();
                var names = await db.GRP_PLOT.Where(pl => pl.SiteId == int.Parse(siteId) && pl.PLOT_ID.Contains(term) && pl.DataStatus == 0 
                                                    && (pl.PLOT_ID.ToLower().StartsWith("cp") || pl.PLOT_ID.ToLower().StartsWith("sp-i")) && !pl.PLOT_ID.ToLower().StartsWith("sp-ii"))
                                              .Select(pn => pn.PLOT_ID).ToListAsync();

                return names;

            }
            catch
            {
                return null;
            }
        }

        [Produces("application/json")]
        [HttpGet("getbmvars")]
        public async Task<IEnumerable<string>> GetBmVars()
        {
            try
            {
                string term = HttpContext.Request.Query["query"].ToString();
                var names = await db.BADMList.Where(pl =>  pl.shortname.Contains(term) && pl.cv_index == 73)
                    .Select(pn => pn.shortname).ToListAsync();

                return names;

            }
            catch
            {
                return null;
            }
        }

    }
}
