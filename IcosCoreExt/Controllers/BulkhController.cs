using AutoMapper;
using IcosCoreExt.Models;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Controllers
{
    [Route("PI/{controller}/{action}")]
    public class BulkhController : Controller
    {
        private readonly IGenericService<GRP_BULKH> _service;
        private readonly IMapper _mapper;
        private readonly IcosDbContext _context;

        public BulkhController(IGenericService<GRP_BULKH> service, IMapper mapper, IcosDbContext context)
        {
            _service = service;
            _mapper = mapper;
            _context = context;
        }

        // GET: BulkhController
        public async Task<IActionResult> Index()
        {
            IEnumerable<GRP_BULKH> Bulkhs = await _service.GetItemValuesAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext));
            List<GRP_BULKHViewModel> BulkhsVM = new List<GRP_BULKHViewModel>();
            foreach (var tow in Bulkhs)
            {
                var towVM = _mapper.Map<GRP_BULKHViewModel>(tow);
                BulkhsVM.Add(towVM);
            }
            return View(BulkhsVM);
        }

        // GET: BulkhController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || id <= 0)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_BULKHViewModel towVM = _mapper.Map<GRP_BULKH, GRP_BULKHViewModel>(item);

            return View(towVM);
        }

        // GET: BulkhController/Create
        public ActionResult Create()
        {
            GRP_BULKHViewModel BulkhViewModel = new GRP_BULKHViewModel();

            ModelUtils.FillSelectItemLists(BulkhViewModel.BULKH_PLOT_TYPETypes, _service.GetBadmListData(94));


            return View(BulkhViewModel);
        }

        // POST: BulkhController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GRP_BULKHViewModel BulkhViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_BULKH tow = _mapper.Map<GRP_BULKHViewModel, GRP_BULKH>(BulkhViewModel);
                bool b = await _service.SaveItemAsync(tow, IcosCoreExt.Utils.Utils.GetUser(HttpContext), IcosCoreExt.Utils.Utils.GetSite(HttpContext));
                return RedirectToAction("Index");
            }
            else
            {
                ModelUtils.FillSelectItemLists(BulkhViewModel.BULKH_PLOT_TYPETypes, _service.GetBadmListData(94));

                return View(BulkhViewModel);
            }
        }

        // GET: BulkhController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (item == null)
            {
                return NotFound();
            }
            GRP_BULKHViewModel BulkhViewModel = _mapper.Map<GRP_BULKH, GRP_BULKHViewModel>(item);
            ModelUtils.FillSelectItemLists(BulkhViewModel.BULKH_PLOT_TYPETypes, _service.GetBadmListData(94));


            return View(BulkhViewModel);
        }

        // POST: BulkhController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GRP_BULKHViewModel BulkhViewModel)
        {
            if (ModelState.IsValid)
            {
                GRP_BULKH loc = _mapper.Map<GRP_BULKHViewModel, GRP_BULKH>(BulkhViewModel);
                bool b = await _service.UpdateItemAsync(BulkhViewModel.Id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext), loc);
                if (b)
                    return RedirectToAction("Index");
            }
            ModelUtils.FillSelectItemLists(BulkhViewModel.BULKH_PLOT_TYPETypes, _service.GetBadmListData(94));

            return View(BulkhViewModel);
        }

        // GET: DHPController/Delete/5

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bulkh = await _service.GetItemValueByIdAsync(IcosCoreExt.Utils.Utils.GetSite(HttpContext), id);
            if (bulkh == null)
            {
                return NotFound();
            }

            return View(_mapper.Map<GRP_BULKHViewModel>(bulkh));
        }

        // POST: DHPController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            bool deleted = await _service.DeleteItemAsync(id, IcosCoreExt.Utils.Utils.GetSite(HttpContext), IcosCoreExt.Utils.Utils.GetUser(HttpContext));
            if (deleted)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
