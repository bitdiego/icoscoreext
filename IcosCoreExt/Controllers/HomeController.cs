﻿using IcosCoreExt.Models;
using IcosDataLogicLib.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using IcosCoreExt.Models.ViewModels;
using IcosDataLib.Data;
using IcosCoreExt.Controllers.api;
using IcosModelLib.DTO;
using System.Collections.Generic;
using AutoMapper;
using System.Text;
using IdentitySample.Services;

namespace IcosCoreExt.Controllers
{
   //[Authorize]
	 public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IStationService _service;
        private readonly IMapper _mapper;
        private readonly IEmailSender _emailSender;

        public HomeController(ILogger<HomeController> logger, IStationService service, IMapper mapper, IEmailSender emailSender)
        {
            _logger = logger;
            this._service = service;
            _mapper = mapper;
            _emailSender = emailSender;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Documents()
        {
            return View();
        }
        public IActionResult WorkingGroups()
        {
            return View();
        }
        public async Task<ActionResult> WorkingGroup(int WGId)
        {
            WorkingGroupViewModel WG = new WorkingGroupViewModel();

            WG.id = WGId;
            IEnumerable<WGUsers> users = await _service.GetWorkingGroupUsers(WGId);
            List<WGUserViewModel> usersVM = new List<WGUserViewModel>();
            string usersEmail = null;
            foreach (var u in users)
            {
                var uVM = _mapper.Map<WGUserViewModel>(u);
                usersVM.Add(uVM);
                usersEmail += uVM.Mail + ";";
            }
            WG.WGUsers = usersVM;
            WG.usersEmails = usersEmail;
            return View(WG);
        }

        public async Task<ActionResult> DownloadWGUsers(int WGId)
        {
            IEnumerable<WGUsers> users = await _service.GetWorkingGroupUsers(WGId);
            List<string> data = new List<string>();
            data.Add("Surname;Name;Institution;Email");

            foreach (var u in users)
            {
                data.Add(String.Format("{0};{1};{2};{3}", u.surname, u.name, u.institution, u.mail));
            }

            string fileName = String.Format("WorkingGroup{0}Users.csv",WGId);
            string fileContent = String.Join("\n", data);
            var byteArray = Encoding.UTF32.GetBytes(fileContent);

            return File(byteArray, "text/csv", fileName);
        }

        public async Task<ActionResult> WGRegister()
        {
            WGUserViewModel model = new WGUserViewModel();
            IEnumerable<WorkGroups> wgs = await _service.GetWorkGroupsList();
            List<WorkingGroupViewModel> wgVMs = new List<WorkingGroupViewModel>();
            foreach (var w in wgs)
            {
                var wgVM = _mapper.Map<WorkingGroupViewModel>(w);
                wgVMs.Add(wgVM);
            }
            model.workingGroups = wgVMs;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> WGRegister(WGUserViewModel wgu)
        {
            if (ModelState.IsValid)
            {
                WGUsers wGUser = _mapper.Map<WGUserViewModel, WGUsers>(wgu);
                List<int> WGids = new List<int>();
                string WGnames = "";
                foreach (WorkingGroupViewModel wg in wgu.workingGroups)
                {
                    if (wg.selected) { 
                        WGids.Add(wg.id);
                        WGnames += wg.description + Environment.NewLine;
                    }
                }
                WGUsers res = await _service.SaveWGUser(wGUser, WGids);

                if (res != null)
                {
                    string sTextBody = "Dear User," + Environment.NewLine;
                    sTextBody = sTextBody + "We received a registration from user " + res.name + " " + res.surname + " (" + res.institution + ", " + res.mail + ") for following ICOS Working Groups:" + Environment.NewLine + Environment.NewLine + WGnames+ Environment.NewLine;
                    sTextBody = sTextBody + "Thank you." + Environment.NewLine + Environment.NewLine;
                    await _emailSender.SendEmailAsync(res.mail, "ICOS Working Groups Registration", sTextBody);
                }

                return RedirectToAction("WorkingGroups");
            }
            else {
                return View(wgu);
            }
        }
        public IActionResult People()
        {
            return View();
        }

        public IActionResult Templates()
        {
            return View();
        }

        #region ajax

        [HttpGet]
        public async Task<JsonResult> GetIcosSites()
        {
            //result.Add(station);
            var result = await _service.GetIcosSitesAsync();
            return Json(result);
        }

        #endregion

        [HttpGet]
        [Route("/write-to-session")]
        public IActionResult TestWriteSession()
        {
            var value = $"Session written at {DateTime.UtcNow.ToString()}";
            HttpContext.Session.SetString("Test", value);
            return Content($"Wrote: {value}");
        }

        [HttpGet]
        [Route("/read-from-session")]
        public IActionResult TestReadSession()
        {
            var value = HttpContext.Session.GetString("Test");
            return Content($"Read: {value}");
        }
    }

}
