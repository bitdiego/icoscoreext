using IcosDataLib.Data;
using IcosDataLogicLib.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using IcosModelLib.DTO;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using IcosDataLogicLib.Services.Validators;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using IdentitySample.Models;
using IdentitySample.Services;

namespace IcosCoreExt
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container. TEST
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor(); // this too... really??

        //    services.AddDistributedMemoryCache();

            services.AddControllersWithViews();
            //services.AddHttpContextAccessor();
            services.AddDbContext<IcosDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false; // terrible, blocking the session !!!! was  true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // services.AddSession();
            //   services.AddCors(); // Make sure you call this previous to AddMvc
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest)
                .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });
           

            services.AddAutoMapper(typeof(Startup));
            /*
                                .AddJsonOptions(options => {
                                    var resolver = options.SerializerSettings.ContractResolver;
                                    if (resolver != null)
                                        (resolver as DefaultContractResolver).NamingStrategy = null;
                                });*/
            //Injections
            services.AddScoped<IStationService, StationService>();
            services.AddScoped<IGeneralInstModelValidator, GeneralInstModelValidator>();
            services.AddScoped<IBasicInstrumentValidator<GRP_INST>, InstrumentModelValidator>();
            services.AddScoped<IBasicInstrumentValidator<GRP_EC>, ECModelValidator>();
            services.AddScoped<IBmValidator<GRP_BM>, BmValidator>();
            services.AddScoped<ISamplingSchemeValidator, SamplingSchemeValidator>();
            services.AddScoped<ILoggerValidator, LoggerValidator>();
            services.AddScoped<IGaiValidator, GaiValidator>();
            services.AddScoped<ICeptValidator, CeptValidator>();
            services.AddScoped<ISosmValidator, SosmValidator>();
            services.AddScoped<IFlsmValidator, FlsmValidator>();
            services.AddScoped<ISppsValidator, SppsValidator>();
            services.AddScoped<IAgbValidator, AgbValidator>();
            services.AddScoped<ILitterPntValidator, LitterPntValidator>();
            services.AddScoped<IWtdPntValidator, WtdPntValidator>();
            services.AddScoped<IDSnowValidator, DSnowValidator>();
            services.AddScoped(typeof(IGenericService<>), typeof(GenericService<>));
            services.AddScoped<IPiAreaService, PiAreaService>();
            services.AddScoped<ITeamValidator, TeamValidator>();
            services.AddScoped<ITreeValidator, TreeValidator>();
            services.AddScoped<IStoValidator<GRP_STO>, StoValidator>();
            services.AddScoped<IInstmanValidator, InstmanValidator>();
            services.AddScoped<IInstPairValidator, InstPairValidator>();

            services.AddRazorPages().AddRazorRuntimeCompilation();

            #region redis cache for session
               services.AddSession(options => {
                //options.Cookie.IsEssential = true;
                options.Cookie.Name = "myapp_session";
                options.IdleTimeout = TimeSpan.FromMinutes(60 * 24);
            });
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = Configuration.GetSection("redis")["redisConnection"];
                options.InstanceName = "IcosInstance";
            });
            //services.AddDistributedRedisCache(o =>
            //{
            //    o.Configuration = Configuration.GetConnectionString("Redis");
            //});
            //Add services needed for sessions


            #endregion

            #region identity

            //         services.AddIdentity<IdentityUser, IdentityRole>(); // should register usermanager,etc..

            services.AddIdentityCore<ApplicationUser>()
                .AddEntityFrameworkStores<IcosDbContext>()
                                .AddSignInManager()
                .AddDefaultTokenProviders();
            services.AddAuthentication(o =>
            {
                o.DefaultScheme = IdentityConstants.ApplicationScheme;
                o.DefaultSignInScheme = IdentityConstants.ExternalScheme;
            })
.AddIdentityCookies(o => { });

            // was working-ish
            //services.AddDefaultIdentity<ApplicationUser>()
            //    .AddDefaultUI()
            //    .AddEntityFrameworkStores<IcosDbContext>()
            //       .AddDefaultTokenProviders(); 

            //https://stackoverflow.com/questions/52089864/unable-to-resolve-service-for-type-iemailsender-while-attempting-to-activate-reg
            //services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
            //.AddEntityFrameworkStores<IcosDbContext>()
            //.AddDefaultTokenProviders();

            services.AddTransient<IdentitySample.Services.IEmailSender, AuthMessageSender>();

            services.AddTransient<IdentitySample.Services.ISmsSender, AuthMessageSender>();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 1;

                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = true;
            }
            );

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(5);

         //       options.LoginPath = "/Identity/Account/Login";
//                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.SlidingExpiration = true;
            });

            //services.AddRazorPages(options =>
            //{
            //    options.Conventions.AddAreaPageRoute("Identity", "/Account/Register", "/Register");
            //    options.Conventions.AddAreaPageRoute("Identity", "/Account/Login", "/zLogin");
            //});

            #endregion

            /* #region max_allowed_upload_size
             services.Configure<FormOptions>(x =>
             {
                 x.ValueLengthLimit = 5000; // Limit on individual form values
                 x.MultipartBodyLengthLimit = 737280000; // Limit on form body size
                 x.MultipartHeadersLengthLimit = 737280000; // Limit on form header size
             });
             services.Configure<IISServerOptions>(options =>
             {
                 options.MaxRequestBodySize = 837280000; // Limit on request body size
             });
             #endregion
            */
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {     
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            //app.UseCors(
            //    options => options.WithOrigins("http://http://localhost:53194/").AllowAnyMethod()
            //);
            // added for Identity
            app.UseHttpsRedirection();
            app.UseCookiePolicy();
            app.UseAuthentication();
            // added for Identity

            app.UseRouting();

            app.UseAuthorization();
            app.UseSession();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

            // Make sure you call this before calling app.UseMvc()

      //      app.UseMvc();

        }
    }
}
