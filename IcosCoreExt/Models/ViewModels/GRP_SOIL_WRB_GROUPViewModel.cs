using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_SOIL_WRB_GROUP 
{
public GRP_SOIL_WRB_GROUP(){ }

	public string SOIL_WRB_GROUP { get; set; }

	public string SOIL_WRB_GROUP_APPROACH { get; set; }

	public string SOIL_WRB_GROUP_COMMENT { get; set; }

}
}