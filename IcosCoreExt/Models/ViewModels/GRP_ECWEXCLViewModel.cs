using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IcosCoreExt.Models.ViewModels
{

    public class GRP_ECWEXCLViewModel
    {
        public GRP_ECWEXCLViewModel() 
		{
			ECWEXCL_ACTIONTypes= new List<SelectListItem>();
        }

		public int Id { get; set; }
		public int DataStatus { get; set; }
	
		[Required]
		[Range(0.0, 360.0, ErrorMessage = "Please enter valid decimal Number")]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal ECWEXCL { get; set; }
	
		[Required]
		[Range(0.0, 360.0, ErrorMessage = "Please enter valid decimal Number")]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal ECWEXCL_RANGE { get; set; }
	
		[Required]
		public string ECWEXCL_ACTION { get; set; }
	
		[Required]
		[IsoDate]
		public string ECWEXCL_DATE { get; set; }
		public string ECWEXCL_COMMENT { get; set; }

		/// <summary>
		/// //navigation property
		/// </summary>
		public List<SelectListItem> ECWEXCL_ACTIONTypes { get; set; }


    }
}
