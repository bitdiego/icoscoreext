using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_IGBP 
{
public GRP_IGBP(){ }

	public string IGBP { get; set; }

	[IsoDate]
	public string IGBP_DATE_START { get; set; }

	public string IGBP_COMMENT { get; set; }

}
}