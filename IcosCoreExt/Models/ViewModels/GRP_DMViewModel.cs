using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_DMViewModel 
	{
		public GRP_DMViewModel()
		{
			DmAgriculture = new List<SelectListItem>();
			DmEncroach  = new List<SelectListItem>();
			DmExtWeather  = new List<SelectListItem>();
			DmFertM  = new List<SelectListItem>();
			DmFertO  = new List<SelectListItem>();
			DmFire  = new List<SelectListItem>();
			DmForestry  = new List<SelectListItem>();
			DmGraze  = new List<SelectListItem>();
			DmInspath  = new List<SelectListItem>();
			DmPesticide  = new List<SelectListItem>();
			DmPlanting  = new List<SelectListItem>();
			DmTill  = new List<SelectListItem>();
			DmWater  = new List<SelectListItem>();
			DmGeneral  = new List<SelectListItem>();
	}

        public int Id { get; set; }
		public int DataStatus { get; set; }

		public string DM_AGRICULTURE { get; set; }

		public string DM_ENCROACH { get; set; }

		public string DM_EXT_WEATHER { get; set; }

		public string DM_FERT_M { get; set; }

		public string DM_FERT_O { get; set; }

		public string DM_FIRE { get; set; }

		public string DM_FORESTRY { get; set; }

		public string DM_GRAZE { get; set; }

		public string DM_INS_PATH { get; set; }

		public string DM_PESTICIDE { get; set; }

		public string DM_PLANTING { get; set; }

		public string DM_TILL { get; set; }

		public string DM_WATER { get; set; }

		public string DM_GENERAL { get; set; }

		[Range(0.0, 100.0, ErrorMessage ="Error: percentage value must be positive and less or equal than 100")]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? DM_SURF { get; set; }

		[Range(0.0, 100.0, ErrorMessage = "Error: percentage value must be positive and less or equal than 100")]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? DM_SURF_MEAS_PRECISION { get; set; }

		[IsoDate]
		[RequiredIfDates("DM_DATE,DM_DATE_START,DM_DATE_END")]
		public string DM_DATE { get; set; }

		[IsoDate]
		[RequiredIfDates("DM_DATE,DM_DATE_START,DM_DATE_END")]
		public string DM_DATE_START { get; set; }

		[IsoDate]
		[RequiredIfDates("DM_DATE,DM_DATE_START,DM_DATE_END")]
		public string DM_DATE_END { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? DM_DATE_UNC { get; set; }

		public string DM_COMMENT { get; set; }
		
		///////////Nav Properties
		public List<SelectListItem> DmAgriculture { get; set; }
		public List<SelectListItem> DmEncroach { get; set; }
		public List<SelectListItem> DmExtWeather { get; set; }
		public List<SelectListItem> DmFertM { get; set; }
		public List<SelectListItem> DmFertO { get; set; }
		public List<SelectListItem> DmFire { get; set; }
		public List<SelectListItem> DmForestry { get; set; }
		public List<SelectListItem> DmGraze { get; set; }
		public List<SelectListItem> DmInspath { get; set; }
		public List<SelectListItem> DmPesticide { get; set; }
		public List<SelectListItem> DmPlanting { get; set; }
		public List<SelectListItem> DmTill { get; set; }
		public List<SelectListItem> DmWater { get; set; }
		public List<SelectListItem> DmGeneral { get; set; }

	}
}