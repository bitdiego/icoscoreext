using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;


namespace IcosCoreExt.Models.ViewModels{

public class GRP_HEADERViewModel
	{
public GRP_HEADERViewModel(){ }

		public int Id { get; set; }
		public int DataStatus { get; set; }

		[Required]
	public string SITE_ID { get; set; }

	[Required]
	public string SITE_NAME { get; set; }

	[Required]
	public string SUBMISSION_CONTACT_NAME { get; set; }

	[Required]
	public string SUBMISSION_CONTACT_EMAIL { get; set; }

	[Required]
	[IsoDate]
	public string SUBMISSION_DATE { get; set; }

}
}