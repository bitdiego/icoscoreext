using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_ICOS 
{
public GRP_ICOS(){ }

	[IsoDate]
	public string DATE_LABEL { get; set; }

	public string URL_REPORT { get; set; }

	public string CLASS_ICOS { get; set; }

	public string SUPPORT_COUNTRY { get; set; }

}
}