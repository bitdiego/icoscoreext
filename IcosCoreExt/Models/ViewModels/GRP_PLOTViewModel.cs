using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IcosCoreExt.Models.ViewModels{

	public class GRP_PLOTViewModel 
	{
		public GRP_PLOTViewModel()
		{
			PlotReference = new List<SelectListItem>();
			PlotType = new List<SelectListItem>();
		}
		public int Id { get; set; }
		public int DataStatus { get; set; }

		[Required]
		[RegularExpression("^((CP)_([0][1-9]|[1][0-9]|[2][0-9]|[3][0-9]|[4][0-9]|50)|((SP-I)_([0][1-9]|[1][0-9]|20))|((SP-II)_([0][1-9]|[1][0-9]|20)-([0][1-9]|[1][0-9]|20)))$", ErrorMessage ="Invalid format for PLOT_ID")] // | ^(SP-I)_([0][1-9]|[1][0-9]|20)$ | ^(SP-II)_([0][1-9]|[1][0-9]|20)-([0][1-9]|[1][0-9]|20)$
		public string PLOT_ID { get; set; }

		[Required]
		public string PLOT_TYPE { get; set; }

		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? PLOT_EASTWARD_DIST { get; set; }

		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? PLOT_NORTHWARD_DIST { get; set; }

		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? PLOT_DISTANCE_POLAR { get; set; }

		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? PLOT_ANGLE_POLAR { get; set; }

		public string PLOT_REFERENCE_POINT { get; set; }

		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? PLOT_LOCATION_LAT { get; set; }

		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? PLOT_LOCATION_LONG { get; set; }

		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? PLOT_HEIGHT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? PLOT_RECT_X { get; set; }

		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? PLOT_RECT_Y { get; set; }

		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? PLOT_RECT_DIR { get; set; }

		[Required]
		[IsoDate]
		public string PLOT_DATE { get; set; }
		public string PLOT_COMMENT { get; set; }

		///////////Nav Properties
		public List<SelectListItem> PlotType { get; set; }
		public List<SelectListItem> PlotReference { get; set; }
	}
}