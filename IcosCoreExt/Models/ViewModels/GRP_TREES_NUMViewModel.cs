using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_TREES_NUM 
{
public GRP_TREES_NUM(){ }

	public decimal TREES_NUM { get; set; }

	[Required]
	public string TREES_NUM_SPP { get; set; }

	public string TREES_NUM_VEGTYPE { get; set; }

	[Required]
	public string TREES_NUM_STATISTIC { get; set; }

	public string TREES_NUM_STATISTIC_METHOD { get; set; }

	public int TREES_NUM_STATISTIC_NUMBER { get; set; }

	public decimal TREES_NUM_DBH_MIN { get; set; }

	public string TREES_NUM_APPROACH { get; set; }

	[Required]
	[IsoDate]
	public string TREES_NUM_DATE { get; set; }

	[Required]
	[IsoDate]
	public string TREES_NUM_DATE_START { get; set; }

	[Required]
	[IsoDate]
	public string TREES_NUM_DATE_END { get; set; }

	public decimal TREES_NUM_DATE_UNC { get; set; }

	public string TREES_NUM_COMMENT { get; set; }

	public string TREES_NUM_LIFESTAGE { get; set; }

	public string TREES_NUM_VEG_STATUS { get; set; }

}
}