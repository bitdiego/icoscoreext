using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_LITTER_ass 
{
public GRP_LITTER_ass(){ }

	[Required]
	public string LITTER { get; set; }

	[Required]
	public string LITTER_UNIT { get; set; }

	public string LITTER_SPP { get; set; }

	public string LITTER_VEGTYPE { get; set; }

	[Required]
	public string LITTER_STATISTIC { get; set; }

	public string LITTER_STATISTIC_METHOD { get; set; }

	public int LITTER_STATISTIC_NUMBER { get; set; }

	public string LITTER_ORGAN { get; set; }

	public string LITTER_APPROACH { get; set; }

	[Required]
	[IsoDate]
	public string LITTER_DATE { get; set; }

	[Required]
	[IsoDate]
	public string LITTER_DATE_START { get; set; }

	[Required]
	[IsoDate]
	public string LITTER_DATE_END { get; set; }

	public decimal LITTER_DATE_UNC { get; set; }

	public string LITTER_COMMENT { get; set; }

}
}