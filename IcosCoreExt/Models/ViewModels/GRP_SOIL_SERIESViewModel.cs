using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_SOIL_SERIES 
{
public GRP_SOIL_SERIES(){ }

	public string SOIL_SERIES { get; set; }

	public string SOIL_SERIES_APPROACH { get; set; }

	public string SOIL_SERIES_COMMENT { get; set; }

}
}