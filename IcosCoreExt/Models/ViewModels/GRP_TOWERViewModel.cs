using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IcosCoreExt.Models.ViewModels
{

    public class GRP_TOWERViewModel
    {
        public GRP_TOWERViewModel() {
            TowerAccess = new List<SelectListItem>();
            TowerDatatrans = new List<SelectListItem>();
            TowerPowers = new List<SelectListItem>();
            TowerTypes = new List<SelectListItem>();
        }

        public int Id { get; set; }
        public int DataStatus { get; set; }
        public string TOWER_TYPE { get; set; }
        [Range(0.0, Double.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
        public decimal? TOWER_HEIGHT { get; set; }

        public string TOWER_ACCESS { get; set; }

        public string TOWER_POWER { get; set; }
        [Range(0.0, Double.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
        public decimal? TOWER_POWER_AVAIL { get; set; }

        public string TOWER_DATATRAN { get; set; }
        [Range(0.0, Double.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
        public decimal? TOWER_DATATRAN_SPEED { get; set; }
        [Required]
        [IsoDate]
        public string TOWER_DATE { get; set; }

        public string TOWER_COMMENT { get; set; }

        public List<SelectListItem> TowerAccess { get; set; }
        public List<SelectListItem> TowerDatatrans { get; set; }
        public List<SelectListItem> TowerPowers { get; set; }
        public List<SelectListItem> TowerTypes { get; set; }

    }
}