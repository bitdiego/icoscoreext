using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_SOIL_CLASSIFICATION 
{
public GRP_SOIL_CLASSIFICATION(){ }

	public string SOIL_CLASSIFICATION { get; set; }

	public string SOIL_CLASSIFICATION_TAXONOMY { get; set; }

	public string SOIL_CLASSIFICATION_APPROACH { get; set; }

	public string SOIL_CLASSIFICATION_COMMENT { get; set; }

}
}