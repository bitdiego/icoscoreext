using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_ALLOM
	{
public GRP_ALLOM(){ }

	[Required]
	public decimal ALLOM_DBH { get; set; }

	[Required]
	public decimal ALLOM_HEIGHT { get; set; }

	[Required]
	public string ALLOM_SPP { get; set; }

	[Required]
	public decimal ALLOM_STEM_BIOM { get; set; }

	[Required]
	public decimal ALLOM_BRANCHES_BIOM { get; set; }

	public decimal ALLOM_LEAVES_BIOM { get; set; }

	public decimal ALLOM_NDLE_CONUM { get; set; }

	public string ALLOM_COMMENT { get; set; }

	[Required]
	[IsoDate]
	public string ALLOM_DATE { get; set; }

}
}