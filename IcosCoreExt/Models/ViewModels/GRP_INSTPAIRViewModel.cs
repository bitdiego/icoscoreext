using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_INSTPAIRViewModel 
	{
		public GRP_INSTPAIRViewModel()
		{
			INSTPAIR_MODEL_1Types = new List<SelectListItem>();
			INSTPAIR_MODEL_2Types = new List<SelectListItem>();
		}
        public int Id { get; set; }
        public int DataStatus { get; set; }

        [Required]
		public string INSTPAIR_MODEL_1 { get; set; }

		[Required]
		public string INSTPAIR_SN_1 { get; set; }

		[Required]
		public string INSTPAIR_MODEL_2 { get; set; }

		[Required]
		public string INSTPAIR_SN_2 { get; set; }

		[Required]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal INSTPAIR_HEIGHT_SEP { get; set; }

		[Required]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal INSTPAIR_EASTWARD_SEP { get; set; }

		[Required]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal INSTPAIR_NORTHWARD_SEP { get; set; }

		[Required]
		[IsoDate]
		public string INSTPAIR_DATE { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal INSTPAIR_DATE_UNC { get; set; }

		public string INSTPAIR_COMMENT { get; set; }

		public List<SelectListItem> INSTPAIR_MODEL_1Types { get; set; }
		public List<SelectListItem> INSTPAIR_MODEL_2Types { get; set; }

	}
}