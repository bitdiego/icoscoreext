using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_LITTERPNTViewModel 
	{
		public GRP_LITTERPNTViewModel()
		{
			LITTERPNT_TYPETypes = new List<SelectListItem>();
			LITTERPNT_FRACTIONTypes = new List<SelectListItem>();
		}

        public int Id { get; set; }
        public int DataStatus { get; set; }

        [Required]
		[RegularExpression("^((CP)_([0][1-9]|[1][0-9]|[2][0-9]|[3][0-9]|[4][0-9]|50)|((SP-I)_([0][1-9]|[1][0-9]|20))|((SP-II)_([0][1-9]|[1][0-9]|20)-([0][1-9]|[1][0-9]|20)))$",
							ErrorMessage = "Invalid format for LITTERPNT_PLOT")]
		public string LITTERPNT_PLOT { get; set; }

		public int? LITTERPNT_ID { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? LITTERPNT_AREA { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? LITTERPNT_EASTWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? LITTERPNT_NORTHWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? LITTERPNT_DISTANCE_POLAR { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? LITTERPNT_ANGLE_POLAR { get; set; }
		
		[Required]
		public string LITTERPNT_TYPE { get; set; }

		public string LITTERPNT_FRACTION { get; set; }

		public string LITTERPNT_SPP { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? LITTERPNT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? LITTERPNT_LEAVESAREA { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? LITTERPNT_COARSE_DIAM { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? LITTERPNT_COARSE_LENGTH { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? LITTERPNT_COARSE_ANGLE { get; set; }

		public int? LITTERPNT_COARSE_DECAY { get; set; }

		public string LITTERPNT_COMMENT { get; set; }

		[Required]
		[IsoDate]
		public string LITTERPNT_DATE { get; set; }

		//Nav properties
		public List<SelectListItem> LITTERPNT_TYPETypes { get; set; }
		public List<SelectListItem> LITTERPNT_FRACTIONTypes { get; set; }

	}
}