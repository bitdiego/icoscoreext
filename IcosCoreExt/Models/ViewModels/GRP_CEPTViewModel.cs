using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_CEPTViewModel 
	{
		public GRP_CEPTViewModel(){ }

        public int Id { get; set; }
        public int DataStatus { get; set; }
		[Column(TypeName = "decimal(18, 8)")]
		public decimal? CEPT_ELADP { get; set; }
		[Column(TypeName = "decimal(18, 8)")]
		public decimal? CEPT_ABSORP { get; set; }

		[Range(1, Int16.MaxValue, ErrorMessage = "Error: CEPT_FIRST must be a positive number")]
		public int? CEPT_FIRST { get; set; }
		
		[Range(1, Int16.MaxValue, ErrorMessage = "Error: CEPT_LAST must be a positive number")]
		public int? CEPT_LAST { get; set; }

		public string CEPT_COMMENT { get; set; }

		[Required]
		[IsoDate]
		public string CEPT_DATE { get; set; }

	}
}