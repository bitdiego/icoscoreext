using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_ECViewModel 
{
        public GRP_ECViewModel()
        {
			EcTypes = new List<SelectListItem>();
			EcYn = new List<SelectListItem>();
			EcSaAlign = new List<SelectListItem>();
			EcSaFormat = new List<SelectListItem>();
        }
        public int Id { get; set; }
		public int DataStatus { get; set; }
		[Required]
		public string EC_MODEL { get; set; }

		[Required]
		public string EC_SN { get; set; }

		[Required]
		public string EC_TYPE { get; set; }

		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? EC_HEIGHT { get; set; }

		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? EC_EASTWARD_DIST { get; set; }

		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? EC_NORTHWARD_DIST { get; set; }

		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? EC_SAMPLING_INT { get; set; }

		public string EC_SA_HEAT { get; set; }

		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? EC_SA_OFFSET_N { get; set; }

		public string EC_SA_WIND_FORMAT { get; set; }

		public string EC_SA_GILL_ALIGN { get; set; }

		public string EC_SA_GILL_PCI { get; set; }

		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? EC_GA_FLOW_RATE { get; set; }

		public string EC_GA_LICOR_FM_SN { get; set; }

		public string EC_GA_LICOR_TP_SN { get; set; }

		public string EC_GA_LICOR_AIU_SN { get; set; }

		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? EC_GA_CAL_CO2_ZERO { get; set; }

		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? EC_GA_CAL_CO2_OFFSET { get; set; }

		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? EC_GA_CAL_CO2_REF { get; set; }

		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? EC_GA_CAL_H2O_ZERO { get; set; }

		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? EC_GA_CAL_TA { get; set; }

		[DefaultValue(null)]
		public int? EC_LOGGER { get; set; }

		[DefaultValue(null)]
		public int? EC_FILE { get; set; }

		[RequiredIfDates("EC_DATE,EC_DATE_START,EC_DATE_END")]
		[IsoDate]
		public string EC_DATE { get; set; }

		[RequiredIfDates("EC_DATE,EC_DATE_START,EC_DATE_END")]
		[IsoDate]
		public string EC_DATE_START { get; set; }

		[RequiredIfDates("EC_DATE,EC_DATE_START,EC_DATE_END")]
		[IsoDate]
		public string EC_DATE_END { get; set; }

		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? EC_DATE_UNC { get; set; }

		public string EC_COMMENT { get; set; }

		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? EC_SA_NORTH_MAGDEC { get; set; }

		////////////////////////////////
		public List<SelectListItem> EcTypes { get; set; }
		public List<SelectListItem> EcYn { get; set; }
		public List<SelectListItem> EcSaFormat { get; set; }
		public List<SelectListItem> EcSaAlign { get; set; }

	}
}