using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_VAR_AGG 
{
public GRP_VAR_AGG(){ }

	[Required]
	public string VAR_AGG_VARNAME { get; set; }

	[Required]
	public string VAR_AGG_MEMBERS { get; set; }

	[Required]
	public string VAR_AGG_STATISTIC { get; set; }

	[IsoDate]
	public string VAR_AGG_DATE { get; set; }

	public string VAR_AGG_COMMENT { get; set; }

}
}