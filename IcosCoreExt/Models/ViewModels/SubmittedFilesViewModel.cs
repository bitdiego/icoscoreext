﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IcosModelLib.DTO;

namespace IcosCoreExt.Models.ViewModels
{
    public class SubmittedFilesViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string OriginalName { get; set; }
        public string DataInfo { get; set; }
        public bool Exclusion { get; set; }
        public string Status { get; set; }
        public int UserId { get; set; }
        public int SiteId { get; set; }
        public DateTime Date { get; set; }
        public FileType FType { get; set; }
        public Site Site { get; set; }
    }
}
