using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosCoreExt.Models.ViewModels
{

    public class GRP_LOCATIONViewModel
    {
        public GRP_LOCATIONViewModel() { }

        public int Id { get; set; }
        public int DataStatus { get; set; } 
        [Required]
        [Range(typeof(decimal),"-90","90",ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
        public decimal LOCATION_LAT { get; set; }

        [Required]
        [Range(typeof(decimal), "-180", "180", ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
        public decimal LOCATION_LONG { get; set; }

        [Range(0.0, Double.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
        public decimal? LOCATION_ELEV { get; set; }

        [IsoDate]
        [Required]
        public string LOCATION_DATE { get; set; }

        public string LOCATION_COMMENT { get; set; }

    }
}