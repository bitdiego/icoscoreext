using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;


namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_WTDPNTViewModel 
	{
		public GRP_WTDPNTViewModel(){ }
		public int Id { get; set; }
		public int DataStatus { get; set; }


		[RegularExpression("^((CP)_([0][1-9]|[1][0-9]|[2][0-9]|[3][0-9]|[4][0-9]|50)|((SP-I)_([0][1-9]|[1][0-9]|20))|(Outside_CP))$", ErrorMessage = "Invalid format for D_SNOW_PLOT. Must be CP_XX, SP-I_XX or Outside_CP")]
		public string WTDPNT_PLOT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? WTDPNT_EASTWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? WTDPNT_NORTHWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? WTDPNT_DISTANCE_POLAR { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? WTDPNT_ANGLE_POLAR { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? WTDPNT_WELL_DEPTH { get; set; }

		[RegularExpression("^(WTD)_([1-9]|[1][0-9]|20)_([1-9]|[1][0-9]|20)_([1-9]|[1][0-9]|20)$")]
		public string WTDPNT_VARMAP { get; set; }

		[Required]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal WTDPNT { get; set; }

		public string WTDPNT_COMMENT { get; set; }

		[Required]
		[IsoDate]
		public string WTDPNT_DATE { get; set; }

	}
}