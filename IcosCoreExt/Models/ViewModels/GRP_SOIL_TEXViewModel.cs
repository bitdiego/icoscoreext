using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_SOIL_TEX 
{
public GRP_SOIL_TEX(){ }

	public decimal SOIL_TEX_SAND { get; set; }

	public string SOIL_TEX_SAND_STATISTIC { get; set; }

	public string SOIL_TEX_SAND_STATISTIC_METHOD { get; set; }

	public int SOIL_TEX_SAND_STATISTIC_NUMBER { get; set; }

	public decimal SOIL_TEX_SILT { get; set; }

	public string SOIL_TEX_SILT_STATISTIC { get; set; }

	public string SOIL_TEX_SILT_STATISTIC_METHOD { get; set; }

	public int SOIL_TEX_SILT_STATISTIC_NUMBER { get; set; }

	public decimal SOIL_TEX_CLAY { get; set; }

	public string SOIL_TEX_CLAY_STATISTIC { get; set; }

	public string SOIL_TEX_CLAY_STATISTIC_METHOD { get; set; }

	public int SOIL_TEX_CLAY_STATISTIC_NUMBER { get; set; }

	public decimal SOIL_TEX_ROCK { get; set; }

	public string SOIL_TEX_ROCK_STATISTIC { get; set; }

	public string SOIL_TEX_ROCK_STATISTIC_METHOD { get; set; }

	public int SOIL_TEX_ROCK_STATISTIC_NUMBER { get; set; }

	public decimal SOIL_TEX_WATER_HOLD_CAP { get; set; }

	public string SOIL_TEX_WATER_HOLD_CAP_STATISTIC { get; set; }

	public string SOIL_TEX_WATER_HOLD_CAP_STATISTIC_METHOD { get; set; }

	public int SOIL_TEX_WATER_HOLD_CAP_STATISTIC_NUMBER { get; set; }

	public decimal SOIL_TEX_WILT { get; set; }

	public string SOIL_TEX_WILT_STATISTIC { get; set; }

	public string SOIL_TEX_WILT_STATISTIC_METHOD { get; set; }

	public int SOIL_TEX_WILT_STATISTIC_NUMBER { get; set; }

	public decimal SOIL_TEX_SAT { get; set; }

	public string SOIL_TEX_SAT_STATISTIC { get; set; }

	public string SOIL_TEX_SAT_STATISTIC_METHOD { get; set; }

	public int SOIL_TEX_SAT_STATISTIC_NUMBER { get; set; }

	public decimal SOIL_TEX_FIELD_CAP { get; set; }

	public string SOIL_TEX_FIELD_CAP_STATISTIC { get; set; }

	public string SOIL_TEX_FIELD_CAP_STATISTIC_METHOD { get; set; }

	public int SOIL_TEX_FIELD_CAP_STATISTIC_NUMBER { get; set; }

	public string SOIL_TEX_PROFILE_ZERO_REF { get; set; }

	public decimal SOIL_TEX_PROFILE_MIN { get; set; }

	public decimal SOIL_TEX_PROFILE_MAX { get; set; }

	public string SOIL_TEX_HORIZON { get; set; }

	public string SOIL_TEX_APPROACH { get; set; }

	[Required]
	[IsoDate]
	public string SOIL_TEX_DATE { get; set; }

	public decimal SOIL_TEX_DATE_UNC { get; set; }

	public string SOIL_TEX_COMMENT { get; set; }

}
}