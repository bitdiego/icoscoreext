using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_D_SNOWViewModel 
	{
		public GRP_D_SNOWViewModel()
		{
		}

        public int Id { get; set; }

        public int DataStatus { get; set; }

        [RegularExpression("^((CP)_([0][1-9]|[1][0-9]|[2][0-9]|[3][0-9]|[4][0-9]|50)|((SP-I)_([0][1-9]|[1][0-9]|20))|(Outside_CP))$", ErrorMessage = "Invalid format for D_SNOW_PLOT. Must be CP_XX, SP-I_XX or Outside_CP")]
		public string D_SNOW_PLOT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? D_SNOW_EASTWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? D_SNOW_NORTHWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? D_SNOW_DISTANCE_POLAR { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? D_SNOW_ANGLE_POLAR { get; set; }

		[RegularExpression("^(D_SNOW)_([1-9]|[1][0-9]|20)_([1-9]|[1][0-9]|20)_([1-9]|[1][0-9]|20)$")]
		public string D_SNOW_VARMAP { get; set; }

		[Required]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal D_SNOW { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public string D_SNOW_COMMENT { get; set; }

		[Required]
		[IsoDate]
		public string D_SNOW_DATE { get; set; }

	}
}