using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;



namespace IcosCoreExt.Models.ViewModels
{

    public class GRP_CLIM_AVGViewModel
    {
        public GRP_CLIM_AVGViewModel() { }

        public int Id { get; set; }
        public int DataStatus { get; set; }

        [Range(0.0, Double.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
        public decimal? MAT { get; set; }

        [Range(0.0, Double.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
        public decimal? MAP { get; set; }

        [Range(0.0, Double.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
        public decimal? MAR { get; set; }

        [Range(0.0, Double.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
        public decimal? MAC_YEARS { get; set; }

        [Required]
        [IsoDate]
        public string MAC_DATE { get; set; }

        public string MAC_COMMENTS { get; set; }

        [Range(0.0, Double.MaxValue, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
        public decimal? MAS { get; set; }

        public string CLIMATE_KOEPPEN { get; set; }

    }
}