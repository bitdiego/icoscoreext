using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_TREEViewModel
	{
		public GRP_TREEViewModel()
		{
			TREE_STATUSTypes = new List<SelectListItem>();
		}

        public int Id { get; set; }

        public int DataStatus { get; set; }

        [Required]
		[RegularExpression("^((CP)_([0][1-9]|[1][0-9]|[2][0-9]|[3][0-9]|[4][0-9]|50)|((SP-I)_([0][1-9]|[1][0-9]|20))|(Outside_CP))$", ErrorMessage = "Invalid format for PLOT_ID. Must be CP_XX, SP-I_XX or Outside_CP")] // | ^(SP-I)_([0][1-9]|[1][0-9]|20)$ | ^(SP-II)_([0][1-9]|[1][0-9]|20)-([0][1-9]|[1][0-9]|20)$
		public string TREE_PLOT { get; set; }

		public int? TREE_ID { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? TREE_EASTWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? TREE_NORTHWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? TREE_DISTANCE_POLAR { get; set; }

		[Range(0,360, ErrorMessage = "TREE_ANGLE_POLAR must be between 0 and 360 degrees")]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? TREE_ANGLE_POLAR { get; set; }

		[RegularExpression(@"^(DBH)_([1-9]|[1-9][0-9]|100)_([1-9]|[1-9][0-9]|100)_([1-9]|[1-9][0-9]|100)$", ErrorMessage = "Invalid input format: please enter DBH_X_Y_Z")]
		public string TREE_VARMAP { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? TREE_DBH { get; set; }

		public int? TREE_STUMP { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? TREE_HEIGHT { get; set; }

		public string TREE_SPP { get; set; }

		public string TREE_STATUS { get; set; }

		public string TREE_COMMENT { get; set; }

		[Required]
		[IsoDate]
		[StringLength(maximumLength:8, MinimumLength = 8, ErrorMessage = "TREE_DATE must be in the form YYYYHHMM")]
		//[RegularExpression("^[0-9]$", ErrorMessage ="Wrong Iso Date format: only digits allowed")]
		public string TREE_DATE { get; set; }

		/////////////////
		public List<SelectListItem> TREE_STATUSTypes { get; set; }

	}
}