using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_SPP_ass 
{
public GRP_SPP_ass(){ }

	[Required]
	public string SPP_O { get; set; }

	[Required]
	public string SPP_O_VEGTYPE { get; set; }

	public decimal SPP_O_PERC { get; set; }

	public string SPP_O_PERC_STATISTIC { get; set; }

	public string SPP_O_PERC_STATISTIC_METHOD { get; set; }

	public int SPP_O_PERC_STATISTIC_NUMBER { get; set; }

	[Required]
	public string SPP_U { get; set; }

	[Required]
	public string SPP_U_VEGTYPE { get; set; }

	public decimal SPP_U_PERC { get; set; }

	public string SPP_U_PERC_STATISTIC { get; set; }

	public string SPP_U_PERC_STATISTIC_METHOD { get; set; }

	public int SPP_U_PERC_STATISTIC_NUMBER { get; set; }

	public string SPP_PERC_UNIT { get; set; }

	public string SPP_APPROACH { get; set; }

	[Required]
	[IsoDate]
	public string SPP_DATE { get; set; }

	[Required]
	[IsoDate]
	public string SPP_DATE_START { get; set; }

	[Required]
	[IsoDate]
	public string SPP_DATE_END { get; set; }

	public decimal SPP_DATE_UNC { get; set; }

	public string SPP_COMMENT { get; set; }

	public string SPP_O_PREVALENCE { get; set; }

	public string SPP_U_PREVALENCE { get; set; }

}
}