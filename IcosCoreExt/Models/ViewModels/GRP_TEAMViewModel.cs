﻿using IcosModelLib.Utils;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Models.ViewModels
{
    public class GRP_TEAMViewModel
    {
        public GRP_TEAMViewModel()
        {
			Roles = new List<SelectListItem>();
			Contracts = new List<SelectListItem>();
			Experts = new List<SelectListItem>();
			Percs = new List<SelectListItem>();
		}
		public int Id { get; set; }
        public int DataStatus { get; set; }

        [Required]
		public string TEAM_MEMBER_FIRSTNAME { get; set; }

		[Required]
		public string TEAM_MEMBER_LASTNAME { get; set; }

		public string TEAM_MEMBER_TITLE { get; set; }

		//[Required]
		public string TEAM_MEMBER_ROLE { get; set; }

		public string TEAM_MEMBER_MAIN_EXPERT { get; set; }

		public string TEAM_MEMBER_PERC_ICOS { get; set; }

		public string TEAM_MEMBER_CONTRACT { get; set; }

		[Required]
		[RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
		public string TEAM_MEMBER_EMAIL { get; set; }

		[RegularExpression(@"^[0]{4}-[0]{3}[1-3]-[0-9]{4}-([0-9]{4}|[0-9]{3}[X]{1}$)", 
							ErrorMessage = @"Invalid ORCID ID code: correct format is 0000-000A-DDDD-DDD[D|X], where A is a digit between 1 and 3, D is a digit from 0 to 9 
							and X can be only the last character of the 4th quartet")]
		public string TEAM_MEMBER_ORCID { get; set; }

		public string TEAM_MEMBER_SOCIALMEDIA { get; set; }

		[RegularExpression(@"^\+[0-9]+$", ErrorMessage = "Invalid phone number: correct format is +123456789")]
		public string TEAM_MEMBER_PHONE { get; set; }

		public string TEAM_MEMBER_INSTITUTION { get; set; }

		public string TEAM_MEMBER_INST_STREET { get; set; }

		public string TEAM_MEMBER_INST_POSTCODE { get; set; }

		public string TEAM_MEMBER_INST_CITY { get; set; }

		public string TEAM_MEMBER_INST_COUNTRY { get; set; }
		
		[IsoDate]
		public string TEAM_MEMBER_WORKEND { get; set; }

		public string TEAM_MEMBER_COMMENT { get; set; }

		//[Required]
		//....to be linked to TEAM_MEMBER_WORKEND:
		//can be empty if TEAM_MEMBER_WORKEND is submitted
		[IsoDate]
		public string TEAM_MEMBER_DATE { get; set; } //DateTime

		[Range(1, ushort.MaxValue)]
		public int? TEAM_MEMBER_AUTHORDER { get; set; }

        /****************************************************/
        public List<SelectListItem> Roles { get; set; }
		public List<SelectListItem> Experts { get; set; }
		public List<SelectListItem> Percs { get; set; }
		public List<SelectListItem> Contracts { get; set; }

		public string Original_Email { get; set; } //DateTime
	}
}
