using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_ECSYSViewModel
	{
		public GRP_ECSYSViewModel(){ }
        public int Id { get; set; }
        public int DataStatus { get; set; }

        [Required]
		public string ECSYS_GA_MODEL { get; set; }

		[Required]
		public string ECSYS_GA_SN { get; set; }

		[Required]
		public string ECSYS_SA_MODEL { get; set; }

		[Required]
		public string ECSYS_SA_SN { get; set; }

		[Required]
		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.###}", ApplyFormatInEditMode = true)]
		public decimal ECSYS_SEP_VERT { get; set; }
		
		[Required]
		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.###}", ApplyFormatInEditMode = true)]
		public decimal ECSYS_SEP_EASTWARD { get; set; }

		[Required]
		[DefaultValue(null)]
		[DisplayFormat(DataFormatString = "{0:0.###}", ApplyFormatInEditMode = true)]
		public decimal ECSYS_SEP_NORTHWARD { get; set; }

		/*public decimal ECSYS_WIND_EXCL { get; set; }

		public decimal ECSYS_WIND_EXCL_RANGE { get; set; }*/

		[Required]
		[IsoDate]
		public string ECSYS_DATE { get; set; }

		public string ECSYS_COMMENT { get; set; }

	}
}