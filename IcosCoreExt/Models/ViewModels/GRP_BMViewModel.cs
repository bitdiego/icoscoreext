using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IcosCoreExt.Models.ViewModels
{

    public class GRP_BMViewModel
    {
        public GRP_BMViewModel()
		{
			BM_MODELTypes= new List<SelectListItem>();
			BM_TYPETypes= new List<SelectListItem>();
			BM_VARIABLE_H_V_RTypes= new List<SelectListItem>();
			BM_INST_HEATTypes= new List<SelectListItem>();
			BM_INST_SHIELDINGTypes= new List<SelectListItem>();
			BM_INST_ASPIRATIONTypes= new List<SelectListItem>();
        }

        public int Id { get; set; }
        public int DataStatus { get; set; }

		[Required]
		public string BM_MODEL { get; set; }

		[Required]
		public string BM_SN { get; set; }

		[Required]
		public string BM_TYPE { get; set; }
		
		[RegularExpression(@"^([a-zA-Z0-9_]{1,})_([1-9]|[1-9][0-9]|100)_([1-9]|[1-9][0-9]|100)_([1-9]|[1-9][0-9]|100)$", ErrorMessage ="Invalid input format: please enter VAR_NAME_X_Y_Z")]
		public string BM_VARIABLE_H_V_R { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? BM_HEIGHT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? BM_EASTWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? BM_NORTHWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? BM_SAMPLING_INT { get; set; }
		public string BM_INST_HEAT { get; set; }
		public string BM_INST_SHIELDING { get; set; }
		public string BM_INST_ASPIRATION { get; set; }
		public int? BM_LOGGER { get; set; }
		public int? BM_FILE { get; set; }

		[IsoDate]
		[RequiredIfDates("BM_DATE,BM_DATE_START,BM_DATE_END")]
		public string BM_DATE { get; set; }

		[IsoDate]
		[RequiredIfDates("BM_DATE,BM_DATE_START,BM_DATE_END")]
		public string BM_DATE_START { get; set; }

		[IsoDate]
		[RequiredIfDates("BM_DATE,BM_DATE_START,BM_DATE_END")]
		public string BM_DATE_END { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? BM_DATE_UNC { get; set; }
		public string BM_COMMENT { get; set; }


		/// <summary>
		/// Nav properties
		/// </summary>
		public List<SelectListItem> BM_MODELTypes { get; set; }
		public List<SelectListItem> BM_TYPETypes { get; set; }
		public List<SelectListItem> BM_VARIABLE_H_V_RTypes { get; set; }
		public List<SelectListItem> BM_INST_HEATTypes { get; set; }
		public List<SelectListItem> BM_INST_SHIELDINGTypes { get; set; }
		public List<SelectListItem> BM_INST_ASPIRATIONTypes { get; set; }


    }
}
