using IcosModelLib.Utils;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IcosCoreExt.Models.ViewModels
{

    public class GRP_LAND_OWNERSHIPViewModel
    {
        public GRP_LAND_OWNERSHIPViewModel() {
            LANDOWNERSHIP = new List<SelectListItem>();
        }

        public int Id { get; set; }
        public int DataStatus { get; set; }
        public string LAND_OWNERSHIP { get; set; }

        public string LAND_OWNER { get; set; }

        [IsoDate]
        public string LAND_DATE { get; set; }

        public string LAND_COMMENT { get; set; }

        public List<SelectListItem> LANDOWNERSHIP { get; set; }

    }
}