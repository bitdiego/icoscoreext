using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_FLUX_MEASUREMENTS 
{
public GRP_FLUX_MEASUREMENTS(){ }

	[Required]
	public string FLUX_MEASUREMENTS_VARIABLE { get; set; }

	[Required]
	public string FLUX_MEASUREMENTS_METHOD { get; set; }

	[Required]
	public string FLUX_MEASUREMENTS_OPERATIONS { get; set; }

	[Required]
	[IsoDate]
	public string FLUX_MEASUREMENTS_DATE_START { get; set; }

	[IsoDate]
	public string FLUX_MEASUREMENTS_DATE_END { get; set; }

	public string FLUX_MEASUREMENTS_COMMENT { get; set; }

}
}