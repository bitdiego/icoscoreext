using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_DBH 
{
	public GRP_DBH() { }

	public decimal DBH { get; set; }

	[Required]
	public string DBH_SPP { get; set; }

	public string DBH_VEGTYPE { get; set; }

	[Required]
	public string DBH_STATISTIC { get; set; }

	public string DBH_STATISTIC_METHOD { get; set; }

	public int DBH_STATISTIC_NUMBER { get; set; }

	public decimal DBH_HEIGHT { get; set; }

	public decimal DBH_MIN { get; set; }

	public string DBH_APPROACH { get; set; }

	[Required]
	[IsoDate]
	public string DBH_DATE { get; set; }

	[Required]
	[IsoDate]
	public string DBH_DATE_START { get; set; }

	[Required]
	[IsoDate]
	public string DBH_DATE_END { get; set; }

	public decimal DBH_DATE_UNC { get; set; }

	public string DBH_COMMENT { get; set; }

}
}