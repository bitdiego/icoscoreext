using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_LOGGERViewModel 
{
	public GRP_LOGGERViewModel(){ }
		public int Id { get; set; }
		public int DataStatus { get; set; }

		[Required]
		public string LOGGER_MODEL { get; set; }

		[Required]
		public string LOGGER_SN { get; set; }

		[Required]
		[Range(1, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
		public int LOGGER_ID { get; set; }

		[Required]
		[IsoDate]
		public string LOGGER_DATE { get; set; }

		public string LOGGER_COMMENTS { get; set; }

	}
}