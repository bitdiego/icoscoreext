using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_SITE_DESC 
{
public GRP_SITE_DESC(){ }

	public string SITE_DESC_SHORT { get; set; }

	public string URL_PICTURE { get; set; }

	public string URL_KMZ { get; set; }

	public string SITE_DESC { get; set; }

}
}