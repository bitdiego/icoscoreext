using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_SOIL_DEPTH 
{
public GRP_SOIL_DEPTH(){ }

	public decimal SOIL_DEPTH { get; set; }

	public string SOIL_DEPTH_STATISTIC { get; set; }

	public string SOIL_DEPTH_STATISTIC_METHOD { get; set; }

	public int SOIL_DEPTH_STATISTIC_NUMBER { get; set; }

	public decimal SOIL_DEPTH_ORG { get; set; }

	public string SOIL_DEPTH_ORG_STATISTIC { get; set; }

	public string SOIL_DEPTH_ORG_STATISTIC_METHOD { get; set; }

	public int SOIL_DEPTH_ORG_STATISTIC_NUMBER { get; set; }

	public string SOIL_DEPTH_APPROACH { get; set; }

	[Required]
	[IsoDate]
	public string SOIL_DEPTH_DATE { get; set; }

	public decimal SOIL_DEPTH_DATE_UNC { get; set; }

	public string SOIL_DEPTH_COMMENT { get; set; }

}
}