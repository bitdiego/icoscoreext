using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_BASAL_AREA
	{
public GRP_BASAL_AREA(){ }

	public decimal BASAL_AREA { get; set; }

	[Required]
	public string BASAL_AREA_SPP { get; set; }

	public string BASAL_AREA_VEGTYPE { get; set; }

	[Required]
	public string BASAL_AREA_STATISTIC { get; set; }

	public string BASAL_AREA_STATISTIC_METHOD { get; set; }

	public int BASAL_AREA_STATISTIC_NUMBER { get; set; }

	public decimal BASAL_AREA_DBH_MIN { get; set; }

	public string BASAL_AREA_APPROACH { get; set; }

	[Required]
	[IsoDate]
	public string BASAL_AREA_DATE { get; set; }

	[Required]
	[IsoDate]
	public string BASAL_AREA_DATE_START { get; set; }

	[Required]
	[IsoDate]
	public string BASAL_AREA_DATE_END { get; set; }

	public decimal BASAL_AREA_DATE_UNC { get; set; }

	public string BASAL_AREA_COMMENT { get; set; }

}
}