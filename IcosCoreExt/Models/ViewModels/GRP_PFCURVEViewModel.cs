using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_PFCURVE 
{
public GRP_PFCURVE(){ }

	public decimal PFCURVE_MP { get; set; }

	public string PFCURVE_MP_STATISTIC { get; set; }

	public string PFCURVE_MP_STATISTIC_METHOD { get; set; }

	public int PFCURVE_MP_STATISTIC_NUMBER { get; set; }

	[Required]
	public decimal PFCURVE_SWC { get; set; }

	public string PFCURVE_SWC_STATISTIC { get; set; }

	public string PFCURVE_SWC_STATISTIC_METHOD { get; set; }

	public int PFCURVE_SWC_STATISTIC_NUMBER { get; set; }

	[Required]
	public string PFCURVE_SWC_UNIT { get; set; }

	public string PFCURVE_APPROACH { get; set; }

	[Required]
	[IsoDate]
	public string PFCURVE_DATE { get; set; }

	public decimal PFCURVE_DATE_UNC { get; set; }

	public string PFCURVE_COMMENT { get; set; }

}
}