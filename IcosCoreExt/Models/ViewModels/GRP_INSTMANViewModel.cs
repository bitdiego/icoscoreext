using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_INSTMANViewModel 
	{
	public GRP_INSTMANViewModel()
		{
			INSTMAN_MODELTypes = new List<SelectListItem>();
			INSTMAN_TYPETypes = new List<SelectListItem>();
			INSTMAN_HEATTypes = new List<SelectListItem>();
			INSTMAN_SHIELDINGTypes = new List<SelectListItem>();
			INSTMAN_ASPIRATIONTypes = new List<SelectListItem>();
			INSTMAN_SA_WIND_FORMATTypes = new List<SelectListItem>();
			INSTMAN_SA_GILL_ALIGNTypes = new List<SelectListItem>();
			INSTMAN_GA_CP_TUBE_MATTypes = new List<SelectListItem>();
			INSTMAN_GA_CP_TUBE_THERMTypes = new List<SelectListItem>();
			INSTMAN_GA_CP_MFCTypes = new List<SelectListItem>();
		}

        public int Id { get; set; }
        public int DataStatus { get; set; }

		[Required]
        public string INSTMAN_MODEL { get; set; }
		
		[Required]
		public string INSTMAN_SN { get; set; }
		
		[Required]
		public string INSTMAN_TYPE { get; set; }

		[RegularExpression(@"^([a-zA-Z0-9_]{1,})_([1-9]|[1-9][0-9]|100)_([1-9]|[1-9][0-9]|100)_([1-9]|[1-9][0-9]|100)$", ErrorMessage = "Invalid input format: please enter VAR_NAME_X_Y_Z")]
		public string INSTMAN_VARIABLE_H_V_R { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_HEIGHT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_EASTWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_NORTHWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_SAMPLING_INT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_AVERAGING_INT { get; set; }

		public string INSTMAN_HEAT { get; set; }

		public string INSTMAN_SHIELDING { get; set; }

		public string INSTMAN_ASPIRATION { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_SA_OFFSET_NORTH { get; set; }

		public string INSTMAN_SA_WIND_FORMAT { get; set; }

		public string INSTMAN_SA_GILL_ALIGN { get; set; }

		public string INSTMAN_GA_CP_FILTERS { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_GA_CP_TUBE_LENGTH { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_GA_CP_TUBE_IN_DIAM { get; set; }

		public string INSTMAN_GA_CP_TUBE_MAT { get; set; }

		public string INSTMAN_GA_CP_TUBE_THERM { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_GA_CP_FLOW_RATE { get; set; }

		public string INSTMAN_GA_CP_MFC { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_GA_OP_AZIM_MOUNT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_GA_OP_VERT_MOUNT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_GA_CO2_ZERO { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_GA_CO2_OFFSET { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_GA_CO2_REF { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_GA_H2O_ZERO { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_GA_H2O_OFFSET { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_GA_H2O_REF { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_GA_CAL_TA { get; set; }

		[IsoDate]
		[RequiredIfDates("INSTMAN_DATE,INSTMAN_DATE_START,INSTMAN_DATE_END")]
		public string INSTMAN_DATE { get; set; }

		[IsoDate]
		[RequiredIfDates("INSTMAN_DATE,INSTMAN_DATE_START,INSTMAN_DATE_END")]
		public string INSTMAN_DATE_START { get; set; }

		[IsoDate]
		[RequiredIfDates("INSTMAN_DATE,INSTMAN_DATE_START,INSTMAN_DATE_END")]
		public string INSTMAN_DATE_END { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? INSTMAN_DATE_UNC { get; set; }

		public string INSTMAN_COMMENT { get; set; }

		public string INSTMAN_FIRMWARE { get; set; }

		public List<SelectListItem> INSTMAN_MODELTypes { get; set; }
		public List<SelectListItem> INSTMAN_TYPETypes { get; set; }
		public List<SelectListItem> INSTMAN_HEATTypes { get; set; }
		public List<SelectListItem> INSTMAN_SHIELDINGTypes { get; set; }
		public List<SelectListItem> INSTMAN_ASPIRATIONTypes { get; set; }
		public List<SelectListItem> INSTMAN_SA_WIND_FORMATTypes { get; set; }
		public List<SelectListItem> INSTMAN_SA_GILL_ALIGNTypes { get; set; }
		public List<SelectListItem> INSTMAN_GA_CP_TUBE_MATTypes { get; set; }
		public List<SelectListItem> INSTMAN_GA_CP_TUBE_THERMTypes { get; set; }
		public List<SelectListItem> INSTMAN_GA_CP_MFCTypes { get; set; }

	}
}