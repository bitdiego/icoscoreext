using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;


namespace IcosCoreExt.Models.ViewModels{

public class GRP_ACKNOWLEDGEMENT 
	{
		public GRP_ACKNOWLEDGEMENT(){ }

		public string ACKNOWLEDGEMENT { get; set; }

		public string ACKNOWLEDGEMENT_COMMENT { get; set; }

	}
}