using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;


namespace IcosCoreExt.Models.ViewModels{

	public class GRP_FUNDINGViewModel 
	{
		public GRP_FUNDINGViewModel(){ }

        public int Id { get; set; }
        public int DataStatus { get; set; }

        [Required]
		public string FUNDING_ORGANIZATION { get; set; }

		public string FUNDING_GRANT { get; set; }

		public string FUNDING_GRANT_URL { get; set; }
			
		public string FUNDING_TITLE { get; set; }

		public string FUNDING_COMMENT { get; set; }

		[Required]
		[IsoDate]
		[RequiredIfDates(",FUNDING_DATE_START,FUNDING_DATE_END")]
		public string FUNDING_DATE_START { get; set; }

		[Required]
		[IsoDate]
		[RequiredIfDates(",FUNDING_DATE_START,FUNDING_DATE_END")]
		public string FUNDING_DATE_END { get; set; }

	}
}