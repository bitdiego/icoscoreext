using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_GAIViewModel
	{

		public GRP_GAIViewModel()
		{
			GAI_PLOT_TYPETypes = new List<SelectListItem>();
			GAI_METHODTypes = new List<SelectListItem>();
			GAI_PHENTypes = new List<SelectListItem>();
			GAI_PTYPETypes = new List<SelectListItem>();
		}

		public int Id { get; set; }

        public int DataStatus { get; set; }

        [Required]
		[RegularExpression("^((CP)_([0][1-9]|[1][0-9]|[2][0-9]|[3][0-9]|[4][0-9]|50)|((SP-I)_([0][1-9]|[1][0-9]|20))|((SP-II)_([0][1-9]|[1][0-9]|20)-([0][1-9]|[1][0-9]|20)))$", ErrorMessage = "Invalid format for PLOT_ID")]
		public string GAI_PLOT { get; set; }

		public string GAI_PLOT_TYPE { get; set; }

		[Range(1, Int16.MaxValue,ErrorMessage ="Error: GAI_LOCATION must be a positive number")]
		public int? GAI_LOCATION { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_AREA { get; set; }

		[Required]
		public string GAI_METHOD { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_HEIGHTC { get; set; }

		public string GAI_PHEN { get; set; }

		public string GAI_SPP { get; set; }

		public string GAI_PTYPE { get; set; }

		[Range(1, Int16.MaxValue, ErrorMessage = "Error: GAI_DHP_ID must be a positive number")]
		public int? GAI_DHP_ID { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_DHP_SLOPE { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_DHP_ASPECT { get; set; }

		public string GAI_COMMENT { get; set; }

		[Required]
		[IsoDate]
		public string GAI_DATE { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_DHP_EASTWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_DHP_NORTHWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_DHP_DISTANCE_POLAR { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_DHP_ANGLE_POLAR { get; set; }

		[Range(1, Int16.MaxValue, ErrorMessage = "Error: GAI_CEPT_ID must be a positive number")]
		public int? GAI_CEPT_ID { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_CEPT_EASTWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_CEPT_NORTHWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_CEPT_DISTANCE_POLAR { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_CEPT_ANGLE_POLAR { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_LOCATION_HEIGHT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_PLOT_SLOPE { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? GAI_PLOT_ASPECT { get; set; }

		/////
		///Nav properties
		///
		public List<SelectListItem> GAI_PLOT_TYPETypes { get; set; }
		public List<SelectListItem> GAI_METHODTypes { get; set; }
		public List<SelectListItem> GAI_PHENTypes { get; set; }
		public List<SelectListItem> GAI_PTYPETypes { get; set; }

		/// <summary>
		/// Not for Web UI!!!!
		/// </summary>
		public int? GAI_DHP_SETUP_ID { get; set; }

		public string GAI_DHP_QC { get; set; }

		public string GAI_DHP_QC_EXT { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? GAI_DHP_EFF { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? GAI_DHP_THRESH { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? GAI_DHP_CLUMP { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? GAI_DHP_OVEREXP { get; set; }

		[Column(TypeName = "decimal(18, 8)")]
		public decimal? GAI_CEPT_LOCATION_MEAS { get; set; }

		public string GAI_CAMPAIGN { get; set; }

	}
}