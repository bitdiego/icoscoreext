﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Models.ViewModels
{
    public class WGUserViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string surname { get; set; }
        [Required]
        public string Institution { get; set; }
        [Required]
        [EmailAddress]
        public string Mail { get; set; }

        public List<WorkingGroupViewModel> workingGroups { get; set; }
    }
}
