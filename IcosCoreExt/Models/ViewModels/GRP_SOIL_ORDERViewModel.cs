using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_SOIL_ORDER 
{
public GRP_SOIL_ORDER(){ }

	public string SOIL_ORDER { get; set; }

	public string SOIL_ORDER_APPROACH { get; set; }

	public string SOIL_ORDER_COMMENT { get; set; }

}
}