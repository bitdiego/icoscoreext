using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_STOViewModel
	{
		public GRP_STOViewModel()
		{
			STO_CONFIGTypes = new List<SelectListItem>();
			STO_TYPETypes = new List<SelectListItem>();
			STO_PROF_TUBE_MATTypes = new List<SelectListItem>();
			STO_PROF_TUBE_THERMTypes = new List<SelectListItem>();
			STO_GA_MODELTypes = new List<SelectListItem>();
			STO_GA_VARIABLETypes = new List<SelectListItem>();
			STO_GA_TUBE_MATTypes = new List<SelectListItem>();
			STO_GA_TUBE_THERMTypes = new List<SelectListItem>();
			STO_GA_CAL_VARIABLETypes = new List<SelectListItem>();
		}

        public int Id { get; set; }

        public int DataStatus { get; set; }

        public string STO_CONFIG { get; set; }

		[Required]
		public string STO_TYPE { get; set; }

		public int? STO_PROF_ID { get; set; }

		public int? STO_PROF_LEVEL { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_PROF_HEIGHT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_PROF_EASTWARD_DIST { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_PROF_NORTHWARD_DIST { get; set; }

		public int? STO_PROF_HORIZ_S_POINTS { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_PROF_BUFFER_VOL { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_PROF_BUFFER_FLOWRATE { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_PROF_TUBE_LENGTH { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_PROF_TUBE_DIAM { get; set; }

		public string STO_PROF_TUBE_MAT { get; set; }

		public string STO_PROF_TUBE_THERM { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_PROF_SAMPLING_TIME { get; set; }

		public string STO_GA_MODEL { get; set; }

		public string STO_GA_SN { get; set; }

		public string STO_GA_VARIABLE { get; set; }

		public int? STO_GA_PROF_ID { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_GA_FLOW_RATE { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_GA_SAMPLING_INT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_GA_AZIM_MOUNT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_GA_VERT_MOUNT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_GA_TUBE_LENGTH { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_GA_TUBE_DIAM { get; set; }

		public string STO_GA_TUBE_MAT { get; set; }

		public string STO_GA_TUBE_THERM { get; set; }

		public string STO_GA_CAL_VARIABLE { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_GA_CAL_VALUE { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_GA_CAL_REF { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_GA_CAL_TA { get; set; }

		public int? STO_LOGGER { get; set; }

		public int? STO_FILE { get; set; }

		[IsoDate]
		public string STO_DATE { get; set; }

		[IsoDate]
		public string STO_DATE_START { get; set; }

		[IsoDate]
		public string STO_DATE_END { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? STO_DATE_UNC { get; set; }

		public string STO_COMMENT { get; set; }

		///////////
		///Nav properties
		public List<SelectListItem> STO_CONFIGTypes { get; set; }
		public List<SelectListItem> STO_TYPETypes { get; set; }
		public List<SelectListItem> STO_PROF_TUBE_MATTypes { get; set; }
		public List<SelectListItem> STO_PROF_TUBE_THERMTypes { get; set; }
		public List<SelectListItem> STO_GA_MODELTypes { get; set; }
		public List<SelectListItem> STO_GA_VARIABLETypes { get; set; }
		public List<SelectListItem> STO_GA_TUBE_MATTypes { get; set; }
		public List<SelectListItem> STO_GA_TUBE_THERMTypes { get; set; }
		public List<SelectListItem> STO_GA_CAL_VARIABLETypes { get; set; }
	}
}