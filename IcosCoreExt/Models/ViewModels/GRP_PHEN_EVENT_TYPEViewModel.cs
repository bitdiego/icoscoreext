using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_PHEN_EVENT_TYPE 
{
public GRP_PHEN_EVENT_TYPE(){ }

	public string PHEN_EVENT_TYPE { get; set; }

	[Required]
	public string PHEN_EVENT_STATUS { get; set; }

	[Required]
	public string PHEN_EVENT_SPP { get; set; }

	public string PHEN_EVENT_VEGTYPE { get; set; }

	[Required]
	[IsoDate]
	public string PHEN_EVENT_DATE { get; set; }

	[Required]
	public string PHEN_EVENT_DATE_STATISTIC { get; set; }

	public string PHEN_EVENT_DATE_STATISTIC_METHOD { get; set; }

	public int PHEN_EVENT_DATE_STATISTIC_NUMBER { get; set; }

	public string PHEN_EVENT_APPROACH { get; set; }

	public string PHEN_EVENT_COMMENT { get; set; }

	public decimal PHEN_EVENT_DATE_STD_DEV { get; set; }

	public decimal PHEN_EVENT_DATE_MEAS_UNC { get; set; }

}
}