using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_FILEViewModel 
	{
		public GRP_FILEViewModel()
		{
			FILE_FORMATTypes = new List<SelectListItem>();
			FILE_TYPETypes = new List<SelectListItem>();
			FILE_EXTENSIONTypes = new List<SelectListItem>();
			FILE_MISSING_VALUETypes = new List<SelectListItem>();
			FILE_TIMESTAMPTypes = new List<SelectListItem>();
			FILE_COMPRESSTypes = new List<SelectListItem>();
		}
		public int Id { get; set; }
        public int DataStatus { get; set; }

        [Required]
		[Range(1, int.MaxValue, ErrorMessage = "Please enter a valid, positive integer Number")]
		public int FILE_ID { get; set; }

		[Required]
		[Range(1, int.MaxValue, ErrorMessage = "Please enter a valid, positive integer Number")]
		public int FILE_LOGGER_ID { get; set; }

		public string FILE_FORMAT { get; set; }

		public string FILE_TYPE { get; set; }

		[Range(1, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
		public int? FILE_HEAD_NUM { get; set; }

		[Range(1, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
		public int? FILE_HEAD_VARS { get; set; }

		[Range(1, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
		public int? FILE_HEAD_TYPE { get; set; }

		[IsoDate]
		public string FILE_EPOCH { get; set; }

		[Required]
		[IsoDate]
		public string FILE_DATE { get; set; }

		public string FILE_COMMENTS { get; set; }

		public string FILE_EXTENSION { get; set; }

		public string FILE_MISSING_VALUE { get; set; }

		public string FILE_TIMESTAMP { get; set; }

		public string FILE_COMPRESS { get; set; }

		//////////////////////Nav properties
		public List<SelectListItem> FILE_FORMATTypes { get; set; }
		public List<SelectListItem> FILE_TYPETypes { get; set; }
		public List<SelectListItem> FILE_EXTENSIONTypes { get; set; }
		public List<SelectListItem> FILE_MISSING_VALUETypes { get; set; }
		public List<SelectListItem> FILE_TIMESTAMPTypes { get; set; }
		public List<SelectListItem> FILE_COMPRESSTypes { get; set; }

	}
}