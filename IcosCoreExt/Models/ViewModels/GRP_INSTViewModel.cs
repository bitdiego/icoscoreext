using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using IcosDataLogicLib.Services;
using IcosModelLib.DTO;
using IcosDataLib.Data;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_INSTViewModel
	{
        public GRP_INSTViewModel()
        {
			InstFactory = new List<SelectListItem>();
			//context = new IcosDbContext();
			/*InstFactory = new List<string>();
			foreach(var str in InstFactory)
            {
				InstFactorySelectList.Add(new SelectListItem(str, str));
            }*/
        }

		public int Id { get; set; }
		public int DataStatus { get; set; }

		[Required]
		public string INST_MODEL { get; set; }

		[Required]
		public string INST_SN { get; set; }

		public string INST_FIRMWARE { get; set; }

		[Required]
		public string INST_FACTORY { get; set; }

		public string INST_CALIB_FUNC { get; set; }

		public string INST_COMMENT { get; set; }

		[Required]
		[IsoDate]
		public string INST_DATE { get; set; }

		//////////////////////////
		public List<SelectListItem> InstFactory { get; set; }
	}
}