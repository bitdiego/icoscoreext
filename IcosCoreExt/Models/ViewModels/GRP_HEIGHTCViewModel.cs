using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_HEIGHTC 
{
public GRP_HEIGHTC(){ }

	public decimal HEIGHTC { get; set; }

	public string HEIGHTC_SPP { get; set; }

	public string HEIGHTC_VEGTYPE { get; set; }

	[Required]
	public string HEIGHTC_STATISTIC { get; set; }

	public int HEIGHTC_STATISTIC_NUMBER { get; set; }

	public decimal HEIGHTC_U { get; set; }

	public string HEIGHTC_U_SPP { get; set; }

	public string HEIGHTC_U_VEGTYPE { get; set; }

	[Required]
	public string HEIGHTC_U_STATISTIC { get; set; }

	public string HEIGHTC_U_STATISTIC_METHOD { get; set; }

	public int HEIGHTC_U_STATISTIC_NUMBER { get; set; }

	public string HEIGHTC_APPROACH { get; set; }

	[Required]
	[IsoDate]
	public string HEIGHTC_DATE { get; set; }

	[Required]
	[IsoDate]
	public string HEIGHTC_DATE_START { get; set; }

	[Required]
	[IsoDate]
	public string HEIGHTC_DATE_END { get; set; }

	public decimal HEIGHTC_DATE_UNC { get; set; }

	public string HEIGHTC_COMMENT { get; set; }

	public string HEIGHTC_STATISTIC_METHOD { get; set; }

}
}