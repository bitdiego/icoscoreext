using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_AGBViewModel
	{
		public GRP_AGBViewModel()
		{
			AGB_PLOT_TYPETypes = new List<SelectListItem>();
			AGB_PHENTypes = new List<SelectListItem>();
			AGB_PTYPETypes = new List<SelectListItem>();
			AGB_ORGANTypes = new List<SelectListItem>();
		}

        public int Id { get; set; }

        public int DataStatus { get; set; }

        [Required]
		public string AGB_PLOT { get; set; }

		public string AGB_PLOT_TYPE { get; set; }

		public int? AGB_LOCATION { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? AGB_LOCATION_HEIGHT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? AGB_AREA { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? AGB { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? AGB_HEIGHTC { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? AGB_NPP_MOSS { get; set; }

		public string AGB_PHEN { get; set; }

		public string AGB_SPP { get; set; }

		public string AGB_PTYPE { get; set; }

		public string AGB_ORGAN { get; set; }

		public string AGB_COMMENT { get; set; }

		[Required]
		[IsoDate]
		public string AGB_DATE { get; set; }

		////Nav Properties
		///
		public List<SelectListItem> AGB_PLOT_TYPETypes { get; set; }
		public List<SelectListItem> AGB_PHENTypes { get; set; }
		public List<SelectListItem> AGB_PTYPETypes { get; set; }
		public List<SelectListItem> AGB_ORGANTypes { get; set; }

	}
}