﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Models.ViewModels
{
    public class WorkingGroupViewModel
    {
        public int id { get; set; }
        public string description { get; set; }
        public bool selected { get; set; }
        public List<WGUserViewModel> WGUsers { get; set; }
        public string usersEmails { get; set; }
    }
}
