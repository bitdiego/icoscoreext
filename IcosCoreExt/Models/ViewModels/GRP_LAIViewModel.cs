using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;


namespace IcosCoreExt.Models.ViewModels{

public class GRP_LAI 
{
	public GRP_LAI(){ }

	public decimal LAI { get; set; }

	[Required]
	public string LAI_TYPE { get; set; }

	[Required]
	public string LAI_CANOPY_TYPE { get; set; }

	public string LAI_SPP { get; set; }

	public string LAI_VEGTYPE { get; set; }

	[Required]
	public string LAI_STATISTIC { get; set; }

	public string LAI_STATISTIC_METHOD { get; set; }

	public int LAI_STATISTIC_NUMBER { get; set; }

	public decimal LAI_CLUMP { get; set; }

	public string LAI_METHOD { get; set; }

	public string LAI_APPROACH { get; set; }

	[Required]
	[IsoDate]
	public string LAI_DATE { get; set; }

	[Required]
	[IsoDate]
	public string LAI_DATE_START { get; set; }

	[Required]
	[IsoDate]
	public string LAI_DATE_END { get; set; }

	public decimal LAI_DATE_UNC { get; set; }

	public string LAI_COMMENT { get; set; }

}
}