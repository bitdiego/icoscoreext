using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_SA 
{
public GRP_SA(){ }

	public decimal SA { get; set; }

	[Required]
	public string SA_STATISTIC { get; set; }

	public string SA_STATISTIC_METHOD { get; set; }

	public int SA_STATISTIC_NUMBER { get; set; }

	public string SA_APPROACH { get; set; }

	[Required]
	[IsoDate]
	public string SA_DATE { get; set; }

	[Required]
	[IsoDate]
	public string SA_DATE_START { get; set; }

	[Required]
	[IsoDate]
	public string SA_DATE_END { get; set; }

	public decimal SA_DATE_UNC { get; set; }

	public string SA_COMMENT { get; set; }

}
}