using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_SPPSViewModel 
	{
		public GRP_SPPSViewModel()
		{
			SPPS_TWSP_PCTTypes = new List<SelectListItem>();
			SPPS_PTYPETypes = new List<SelectListItem>();
		}

		public int Id { get; set; }
		public int DataStatus { get; set; }

		[Required]
		[RegularExpression("^((CP)_([0][1-9]|[1][0-9]|[2][0-9]|[3][0-9]|[4][0-9]|50)|((SP-I)_([0][1-9]|[1][0-9]|20))|((SP-II)_([0][1-9]|[1][0-9]|20)-([0][1-9]|[1][0-9]|20)))$", ErrorMessage = "Invalid format for SPPS_PLOT")] // | ^(SP-I)_([0][1-9]|[1][0-9]|20)$ | ^(SP-II)_([0][1-9]|[1][0-9]|20)-([0][1-9]|[1][0-9]|20)$
		public string SPPS_PLOT { get; set; }

		[Range(1,100, ErrorMessage = "Error: SPPS_PLOT must be included between 1 and 100")]
		public int? SPPS_LOCATION { get; set; }

		[Range(-90, 90, ErrorMessage = "SPPS_LOCATION_LAT must be between -90 and 90")]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SPPS_LOCATION_LAT { get; set; }

		[Range(-180, 180, ErrorMessage = "SPPS_LOCATION_LON must be between -180 and 180")]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SPPS_LOCATION_LON { get; set; }

		[Range(0,999.99, ErrorMessage = "SPPS_LOCATION_DIST must be a positive number")]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SPPS_LOCATION_DIST { get; set; }

		[Range(0, 360, ErrorMessage = "Error: SPPS_LOCATION_ANG must be included between 0 and 360 degrees")]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SPPS_LOCATION_ANG { get; set; }

		public string SPPS { get; set; }

		public decimal? SPPS_PERC_COVER { get; set; }

		public string SPPS_TWSP_PCT { get; set; }

		public string SPPS_COMMENT { get; set; }

		[Required]
		[IsoDate]
		public string SPPS_DATE { get; set; }

		public string SPPS_PTYPE { get; set; }

		/// <summary>
		/// nav properties
		/// </summary>
		public List<SelectListItem> SPPS_TWSP_PCTTypes { get; set; }
		public List<SelectListItem> SPPS_PTYPETypes { get; set; }

	}
}