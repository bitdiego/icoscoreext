using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_SOSMViewModel
	{
		public GRP_SOSMViewModel()
		{
			SOSM_SAMPLE_MATTypes = new List<SelectListItem>();
		}

        public int Id { get; set; }

        public int DataStatus { get; set; }

        [Required]
		[RegularExpression("^(((SP-I)_([0][1-9]|[1][0-9]|20))|((SP-II)_([0][1-9]|[1][0-9]|20)-([0][1-9]|[1][0-9]|20)))$", ErrorMessage = "Error: SOSM_PLOT_ID must be in the format SP-I_XX or SP-II_YY-ZZ")]
		public string SOSM_PLOT_ID { get; set; }

		[Required]
		public string SOSM_SAMPLE_ID { get; set; }

		[Required]
		public string SOSM_SAMPLE_MAT { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_AREA { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_VOLUME { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_UD { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_LD { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_THICKNESS { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_W0 { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_W30 { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_W30_E { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_W105_S { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_W70_R { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_WX30_E { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_WX105_E { get; set; }

		public string SOSM_COMMENT { get; set; }

		[Required]
		[IsoDate]
		public string SOSM_DATE { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_W0_OH { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_W0_OS { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? SOSM_W0_OR { get; set; }

		//////
		///Nav properties
		///
		public List<SelectListItem> SOSM_SAMPLE_MATTypes { get; set; }

		/// <summary>
		/// Not submitted via BADM or UI:::: hide !!!!
		/// </summary>
		/// 
		/*
		public decimal? SOSM_CONC_C { get; set; }

		public string SOSM_CONC_C_METHOD { get; set; }

		public decimal? SOSM_CONC_N { get; set; }
		public string SOSM_CONC_N_METHOD { get; set; }
		public string SOSM_SAMPLE_CEES_ID { get; set; }
		public decimal? SOSM_BD { get; set; }
		public decimal? SOSM_TEX_ROCK { get; set; }
		public decimal? SOSM_CN_RATIO { get; set; }
		public decimal? SOSM_STOCK_C { get; set; }
		public decimal? SOSM_STOCK_N { get; set; }
		*/
	}
}

