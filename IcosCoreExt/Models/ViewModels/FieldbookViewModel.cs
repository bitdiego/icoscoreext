﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Models.ViewModels
{
    public class FieldbookViewModel
    {
        [Key]
        public int Id { get; set; }
        public string OriginalName { get; set; }
        
        [Display(Name ="Title")]
        public string DataInfo { get; set; }
        public int SiteId { get; set; }

        [Display(Name = "Submitter")]
        public int UserId { get; set; }

        [Display(Name = "Data Type")]
        public int DataTypeId { get; set; }
        
        [Display(Name = "Submission Date")]
        public DateTime ImportDate { get; set; }
        
        [Display(Name = "File content")]
        public string HtmlContent { get; set; }
    }
}
