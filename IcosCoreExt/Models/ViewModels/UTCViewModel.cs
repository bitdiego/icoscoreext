﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using IcosModelLib.Utils;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Models.ViewModels
{
    public class UTCViewModel
    {
        public int Id { get; set; }
        public int DataStatus { get; set; }
        [Required]
        public decimal UTC_OFFSET { get; set; }

        [IsoDate]
        public string UTC_OFFSET_DATE_START { get; set; }
        public string UTC_OFFSET_COMMENT { get; set; }
    }
}
