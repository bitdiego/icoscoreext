using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_REFERENCE_PAPER 
{
public GRP_REFERENCE_PAPER(){ }

	public string REFERENCE_PAPER { get; set; }

	public string REFERENCE_DOI { get; set; }

	public string REFERENCE_USAGE { get; set; }

	public string REFERENCE_COMMENT { get; set; }

}
}