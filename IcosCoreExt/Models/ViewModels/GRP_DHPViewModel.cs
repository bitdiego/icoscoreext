using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IcosCoreExt.Models.ViewModels
{

    public class GRP_DHPViewModel
    {
        public GRP_DHPViewModel() {
            Cameras = new List<SelectListItem>();
            Lenses = new List<SelectListItem>();
        }

        public int Id { get; set; }
        public int DataStatus { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public int DHP_ID { get; set; }

        [Required]
        public string DHP_CAMERA { get; set; }

        [Required]
        public string DHP_CAMERA_SN { get; set; }

        [Required]
        public string DHP_LENS { get; set; }

        [Required]
        public string DHP_LENS_SN { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public int DHP_OC_ROW { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public int DHP_OC_COL { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        [DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
        public int DHP_RADIUS { get; set; }

        public string DHP_COMMENT { get; set; }

        [IsoDate]
        [Required]
        public string DHP_DATE { get; set; }

        public List<SelectListItem> Cameras { get; set; }
        public List<SelectListItem> Lenses { get; set; }
    }
}