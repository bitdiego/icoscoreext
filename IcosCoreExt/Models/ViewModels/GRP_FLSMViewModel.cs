using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IcosCoreExt.Models.ViewModels
{

	public class GRP_FLSMViewModel
	{
		public GRP_FLSMViewModel()
		{
			FLSM_SAMPLE_TYPETypes = new List<SelectListItem>();
			FLSM_PTYPETypes = new List<SelectListItem>();
		}
        public int Id { get; set; }
        public int DataStatus { get; set; }

        [Required]
		[RegularExpression("^((CP)_([0][1-9]|[1][0-9]|[2][0-9]|[3][0-9]|[4][0-9]|50)|((SP-I)_([0][1-9]|[1][0-9]|20))|((SP-II)_([0][1-9]|[1][0-9]|20)-([0][1-9]|[1][0-9]|20)))$", ErrorMessage = "Invalid format for FLSM_PLOT_ID")]
		public string FLSM_PLOT_ID { get; set; }

		[Range(1, Int32.MaxValue, ErrorMessage = "Error: FLSM_TREE_ID must be a positive integer")]
		public int? FLSM_TREE_ID { get; set; }

		[Required]
		[Range(1, 30, ErrorMessage = "Error: FLSM_SAMPLE_ID must be a positive integer between 1 and 30")]
		public int FLSM_SAMPLE_ID { get; set; }

		//[Required]
		public string FLSM_SPP { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? FLSM_LMA_AREA { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal? FLSM_LMA_DW { get; set; }

		[Required]
		public string FLSM_SAMPLE_TYPE { get; set; }
		
		//[Required]
		public string FLSM_PTYPE { get; set; }

		public string FLSM_COMMENT { get; set; }

		[Required]
		[IsoDate]
		public string FLSM_DATE { get; set; }

		/*
		public decimal? FLSM_CONC_CA { get; set; }

		public decimal? FLSM_CONC_CU { get; set; }

		public decimal? FLSM_CONC_FE { get; set; }

		public decimal? FLSM_CONC_MG { get; set; }

		public decimal? FLSM_CONC_MN { get; set; }

		public decimal? FLSM_CONC_C { get; set; }

		public decimal? FLSM_CONC_N { get; set; }

		public decimal? FLSM_CONC_P { get; set; }

		public decimal? FLSM_CONC_K { get; set; }

		public decimal? FLSM_CONC_ZN { get; set; }

		public decimal? FLSM_CONC_DRYRATIO { get; set; }

		public string FLSM_CONC_USRAVE_SN { get; set; }

		public decimal? FLSM_CONC_CA_UNC { get; set; }

		public decimal? FLSM_CONC_CU_UNC { get; set; }

		public decimal? FLSM_CONC_FE_UNC { get; set; }

		public decimal? FLSM_CONC_MG_UNC { get; set; }

		public decimal? FLSM_CONC_MN_UNC { get; set; }

		public decimal? FLSM_CONC_C_UNC { get; set; }

		public decimal? FLSM_CONC_N_UNC { get; set; }

		public decimal? FLSM_CONC_P_UNC { get; set; }

		public decimal? FLSM_CONC_K_UNC { get; set; }

		public decimal? FLSM_CONC_ZN_UNC { get; set; }

		public decimal? FLSM_DRYRATIO_UNC { get; set; }
		*/
		public List<SelectListItem> FLSM_SAMPLE_TYPETypes { get; set; }
		public List<SelectListItem> FLSM_PTYPETypes { get; set; }

	}
}