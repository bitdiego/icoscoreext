using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;


namespace IcosCoreExt.Models.ViewModels{

	public class GRP_UTC_OFFSETViewModel 
	{
		public GRP_UTC_OFFSETViewModel()
		{
		}
		public int Id { get; set; }
		public int DataStatus { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal UTC_OFFSET { get; set; }

		[IsoDate]
		public string UTC_OFFSET_DATE_START { get; set; }

		public string UTC_OFFSET_COMMENT { get; set; }

	}
}