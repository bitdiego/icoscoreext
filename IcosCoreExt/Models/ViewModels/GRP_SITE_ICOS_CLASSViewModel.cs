using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_SITE_ICOS_CLASS 
{
public GRP_SITE_ICOS_CLASS(){ }

	public string SITE_ICOS_CLASS { get; set; }

	[IsoDate]
	public string SITE_ICOS_LABDATE { get; set; }

}
}