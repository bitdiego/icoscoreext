using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace IcosCoreExt.Models.ViewModels
{

    public class GRP_BULKHViewModel
    {
        public GRP_BULKHViewModel() 
		{
			BULKH_PLOT_TYPETypes= new List<SelectListItem>();
        }

        public int Id { get; set; }
        public int DataStatus { get; set; }

		[Required]
		public string BULKH_PLOT { get; set; }

		[Required]
		public string BULKH_PLOT_TYPE { get; set; }

		[Required]
		[DisplayFormat(DataFormatString = "{0:0.########}", ApplyFormatInEditMode = true)]
		public decimal BULKH { get; set; }
		public string BULKH_COMMENT { get; set; }

		[Required]
		[IsoDate]
		public string BULKH_DATE { get; set; }

		/// <summary>
		/// Navigation properties
		/// </summary>
		public List<SelectListItem> BULKH_PLOT_TYPETypes { get; set; }


    }
}
