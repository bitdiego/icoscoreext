using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_WTD_ass 
{
public GRP_WTD_ass(){ }

	public decimal WTD { get; set; }

	public string WTD_STATISTIC { get; set; }

	public string WTD_STATISTIC_METHOD { get; set; }

	public int WTD_STATISTIC_NUMBER { get; set; }

	public string WTD_APPROACH { get; set; }

	[Required]
	[IsoDate]
	public string WTD_DATE { get; set; }

	public decimal WTD_DATE_UNC { get; set; }

	public string WTD_COMMENT { get; set; }

}
}