using System;
using IcosModelLib.Utils;
using System.ComponentModel.DataAnnotations;

using IcosModelLib.Utils;

namespace IcosCoreExt.Models.ViewModels{

public class GRP_ROOT_DEPTH 
{
public GRP_ROOT_DEPTH(){ }

	public decimal ROOT_DEPTH { get; set; }

	public string ROOT_DEPTH_SPP { get; set; }

	public string ROOT_DEPTH_VEGTYPE { get; set; }

	[Required]
	public string ROOT_DEPTH_STATISTIC { get; set; }

	public string ROOT_DEPTH_STATISTIC_METHOD { get; set; }

	public int ROOT_DEPTH_STATISTIC_NUMBER { get; set; }

	public string ROOT_DEPTH_APPROACH { get; set; }

	[Required]
	[IsoDate]
	public string ROOT_DEPTH_DATE { get; set; }

	[Required]
	[IsoDate]
	public string ROOT_DEPTH_DATE_START { get; set; }

	[Required]
	[IsoDate]
	public string ROOT_DEPTH_DATE_END { get; set; }

	public decimal ROOT_DEPTH_DATE_UNC { get; set; }

	public string ROOT_DEPTH_COMMENT { get; set; }

}
}