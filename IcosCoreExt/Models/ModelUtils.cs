﻿using IcosDataLogicLib.Services;
using IcosModelLib.DTO;
using IcosModelLib.Utils;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IcosCoreExt.Models
{
    public class ModelUtils
    {
        //IGenericService<T> _service;

        /* public ModelUtils(IGenericService<T> service)
         {
             _service = service;
         }*/
        

        /*public static int SerialNumberCheck(string instModel, string instSn)
        {
            if (!regulars.ContainsKey(instModel)) return 0;
            string instReg = regulars[instModel.ToLower()];
            Match instMatch = Regex.Match(instSn, instReg, RegexOptions.IgnoreCase);
            if (!instMatch.Success)
            {
                return (int)Globals.ErrorValidationCodes.WRONG_SERIALNUMBER_FORMAT;
            }
            return 0;
        }*/
        public static void SetSelectItemList(IQueryable<string> iq, List<SelectListItem> list)
        {
            //IQueryable<string> iFactory = _service.GetBadmListData(cvIndex);
            //ModelUtils.SetSelectItemList(iFactory, instViewModel.InstFactory);
            foreach (var item in iq)
            {
                list.Add(new SelectListItem(item, item));
            }
        }

        public static void FillSelectItemLists(List<SelectListItem> list, IQueryable<string> iqList)
        {
            if (list.Count == 0)
            {
                foreach (var item in iqList)
                {
                    list.Add(new SelectListItem(item, item));
                }
            }
        }

        public static void FormatError(ref string errorString, params string[] list)
        {
            //string err = errorString;
            for (int i = 0; i < list.Length; i += 2)
            {
                errorString = errorString.Replace(list[i], list[i + 1]);
            }
            //errorString += Environment.NewLine + errorString;
        }

        public static bool IsAnyPropNotNull<T>(T model)
        {
            Type myType = model.GetType();
            IList<PropertyInfo> props = new List<System.Reflection.PropertyInfo>(myType.GetProperties());

            var subList = props.Where(item => !item.Name.Contains("_DATE") &&
                                               !item.Name.Contains("COMMENT") &&
                                               item.Name != "Id" &&
                                               item.Name != "DataStatus" &&
                                               !item.Name.Contains("UserId") &&
                                               !item.Name.Contains("Date") &&
                                               !item.Name.Contains("SiteId") &&
                                               !item.Name.Contains("GroupId") &&
                                               !item.Name.Contains("DataOrigin")).ToList();

            var isAnyVAlue = subList.Any(item => item.GetValue(model, null) != null);

            return isAnyVAlue;
        }

    }
}
