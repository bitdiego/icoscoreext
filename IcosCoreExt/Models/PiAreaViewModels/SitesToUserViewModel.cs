﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Models.PiAreaViewModels
{
    public class SitesToUserViewModel
    {
        public SitesToUserViewModel()
        {
            SitesPiList = new List<SelectListItem>();
        }
        public int UserId { get; set; }
        public List<SelectListItem> SitesPiList;
    }
}
