﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IcosCoreExt.Models.PiAreaViewModels
{
    public class SelectorVM
    {
        //SiteId
        public int Id_site { get; set; }

        [Display(Name = "Sites", Prompt = "Sites", Description = "Pick a site")]
        public SelectList SiteList { get; set; }
    }
}
