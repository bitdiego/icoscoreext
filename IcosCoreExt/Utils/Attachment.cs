﻿namespace IcosCoreExt.Utils
{
    public class Attachment
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
