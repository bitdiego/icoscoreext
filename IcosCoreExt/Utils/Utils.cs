﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Azure;
//using Microsoft.WindowsAzure.Storage;
//using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.Azure.Cosmos.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace IcosCoreExt.Utils
{
    public static class Utils
    {

        public static int GetUser(HttpContext httpContext)
        {
            //   return 95;
            try
            {
                return Convert.ToInt32(httpContext.Session.GetString("IdUser")); //Uid

            }
            catch (Exception)
            {

                return -1;
            }
        }

        public static void SetUser(HttpContext httpContext, int uid)
        {
            httpContext.Session.SetString("Uid", uid.ToString());
        }

        public static int GetSite(HttpContext httpContext)
        {

            try
            {
                return httpContext.Session.GetInt32("PiSite").Value;
                //PiSite
                //return Convert.ToInt32(httpContext.Session.GetString("Sid"));

            }
            catch (Exception ex)
            {
                return 0;
                //    return 0;
            }
        }

        public static void SetSite(HttpContext httpContext, int sid)
        {
            httpContext.Session.SetString("Sid", sid.ToString());
        }

        public static async void SendMail(string xto, string xsub, string xbo, System.Collections.Generic.List<string> atch, List<string> xcc = null)
        {
            String _cc = "";
            if (xcc != null && xcc.Count > 0)
            {
                for (int i = 0; i < xcc.Count; i++)
                {
                    _cc += xcc[i] + ";";
                }
            }
            if (_cc.Length > 0) _cc = _cc.Substring(0, _cc.Length - 1);

            //https://docs.microsoft.com/en-us/azure/app-service/tutorial-send-email?tabs=dotnetcore
            // requires using System.Net.Http;
            var client = new HttpClient();
            // requires using System.Text.Json;
            var jsonData = JsonSerializer.Serialize(new
            {
                email = xto,
                due = xbo,
                task = xsub,
                mycc = _cc/*,
                myatch=atch*/
            });


            HttpResponseMessage result = await client.PostAsync(
              // Requires DI configuration to access app settings. See https://docs.microsoft.com/azure/app-service/configure-language-dotnetcore#access-environment-variables
              "https://prod-26.centralus.logic.azure.com:443/workflows/d3f5f60247b640d79fbfbbb7011fabe2/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=Tx7QXPjVLXpXRPQ5CQ1-JldZlhknKe0aykAYFaNA02I",
                new StringContent(jsonData, Encoding.UTF8, "application/json"));

            var statusCode = result.StatusCode.ToString();
            //"https://prod-26.centralus.logic.azure.com:443/workflows/d3f5f60247b640d79fbfbbb7011fabe2/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=Tx7QXPjVLXpXRPQ5CQ1-JldZlhknKe0aykAYFaNA02I"
        }

        public static async void SendMailwithAttachments(string xto, string xsub, string xbo, List<Attachment> atch, List<string> xcc = null)
        {

            String _cc = "";
            if (xcc != null && xcc.Count > 0)
            {
                for (int i = 0; i < xcc.Count; i++)
                {
                    _cc += xcc[i] + ";";
                }
            }
            if (_cc.Length > 0) _cc = _cc.Substring(0, _cc.Length - 1);

            //List<string> xat = new List<string>();

            //if (atch != null && atch.Count > 0)
            //{
            //    foreach (var item in atch)
            //    {
            //        xat.Add(item.Path + "\\" + item.Name);
            //    }
            //}

            //https://docs.microsoft.com/en-us/azure/app-service/tutorial-send-email?tabs=dotnetcore
            // requires using System.Net.Http;
            var client = new HttpClient();
            // requires using System.Text.Json;
            var jsonData = JsonSerializer.Serialize(new
            {
                to = xto,
                html = xbo,
                subject = xsub,
                cc = _cc,
                attachments = atch.ToArray()
            });


            HttpResponseMessage result = await client.PostAsync(
              // Requires DI configuration to access app settings. See https://docs.microsoft.com/azure/app-service/configure-language-dotnetcore#access-environment-variables
              //"https://prod-20.northcentralus.logic.azure.com:443/workflows/77cc10570b694649a9b78c15ca7b83c7/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=DjC3L7hIKSD0mFRcgrGOfuBAFb5veVIEEZGphMkv-C0",
              "https://prod-11.northcentralus.logic.azure.com:443/workflows/1472809b68f4433cabdcc4b539923884/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=14PBsXx3Qw61fMRqtuvL35P6daS7bI--9avpajjxyOA",
                new StringContent(jsonData, Encoding.UTF8, "application/json"));

            var statusCode = result.StatusCode.ToString();
            //"https://prod-26.centralus.logic.azure.com:443/workflows/d3f5f60247b640d79fbfbbb7011fabe2/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=Tx7QXPjVLXpXRPQ5CQ1-JldZlhknKe0aykAYFaNA02I"
        }





        public static async Task<string> GetHtmlFromStorageAsync(string partition, string row)
        {
            // Retrieve the storage account from the connection string.

            String xx = "DefaultEndpointsProtocol=https;AccountName=icos;AccountKey=KnyTe3J/vek3L53ygGPLhGbJvffagd5iOinY/lmRvA/VDMBfZ3d1yOIkUwY+cR/UxlZtA9nwGIqp/K5siBN+2A==;EndpointSuffix=core.windows.net";
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(xx); // "StorageConnectionString"));


            // Create the table client.
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            // Create the CloudTable object that represents the "HtmlAreas" table.
            CloudTable table = tableClient.GetTableReference("HtmlAreas");

            // Create a retrieve operation that takes a customer entity.
            TableOperation retrieveOperation = TableOperation.Retrieve<CustomHtml>(partition, row);

            // Execute the retrieve operation.
            TableResult retrievedResult = await table.ExecuteAsync(retrieveOperation);

            // Print the phone number of the result.
            if (retrievedResult.Result != null)
            {
                return (((CustomHtml)retrievedResult.Result).Code);
            }
            else
            {
                return "";
            }

        }


        public static string GenerateRandomPassword(PasswordOptions opts = null)
        {
            if (opts == null) opts = new PasswordOptions()
            {
                RequiredLength = 8,
                RequiredUniqueChars = 4,
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonAlphanumeric = true,
                RequireUppercase = true
            };

            string[] randomChars = new[] {
            "ABCDEFGHJKLMNOPQRSTUVWXYZ",    // uppercase 
            "abcdefghijkmnopqrstuvwxyz",    // lowercase
            "0123456789",                   // digits
            "!@$?_-"                        // non-alphanumeric
        };

            Random rand = new Random(Environment.TickCount);
            List<char> chars = new List<char>();

            if (opts.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (opts.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (opts.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);

            if (opts.RequireNonAlphanumeric)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (int i = chars.Count; i < opts.RequiredLength
                || chars.Distinct().Count() < opts.RequiredUniqueChars; i++)
            {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }
    }
}
