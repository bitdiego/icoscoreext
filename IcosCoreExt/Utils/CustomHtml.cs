﻿//using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.Azure.Cosmos.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IcosCoreExt.Utils
{
    public class CustomHtml: TableEntity
    {
       
            public CustomHtml(string pagina, string indice)
            {
                this.PartitionKey = pagina;
                this.RowKey = indice;
            }

            public CustomHtml() { }

            public string Code { get; set; }

    }
}