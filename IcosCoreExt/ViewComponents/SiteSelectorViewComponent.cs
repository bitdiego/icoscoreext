﻿using IcosDataLib.Data;
using Microsoft.AspNetCore.Mvc;
using IcosCoreExt.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IcosCoreExt.Models.PiAreaViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Distributed;

namespace IcosCoreExt.ViewComponents
{
    public class SiteSelectorViewComponent : ViewComponent
    {
        private readonly IcosDbContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IDistributedCache _distributedCache;

        public SiteSelectorViewComponent(IcosDbContext ctx, IHttpContextAccessor httpContextAccessor, IDistributedCache distributedCache)
        {
            _context = ctx;
            _httpContextAccessor = httpContextAccessor;   
            _distributedCache = distributedCache;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            await HttpContext.Session.LoadAsync();
     //       var sessionstartTime = HttpContext.Session.GetString("PiSite");
            //  var xx = await _httpContextAccessor.HttpContext.Session.LoadAsync("").
            // int xId_site = Utils.Utils.GetSite(HttpContext);

            SelectorVM selectorVM = new SelectorVM();
            int xId_site = -1;
            byte[] bId_site;
            if (_httpContextAccessor.HttpContext.Session.TryGetValue("PiSite", out bId_site))
            {
                xId_site = _httpContextAccessor.HttpContext.Session.GetInt32("PiSite").Value;
//                xId_site = Convert.ToInt32(cc);
            }

            int userID = 0;
            try
            {
                String cc = _httpContextAccessor.HttpContext.Session.GetString("IdUser");
                userID = Convert.ToInt32(cc);
            }
            catch (Exception)
            {
                // not logged???
           //     throw;
            }


            List<SelectListItem> items = new List<SelectListItem>();
            //items.Add(new SelectListItem { Text = "Select", Value = "0" });
            bool xsel = xId_site == -1;

            // try to simplify  items.Add(new SelectListItem { Text = "Please select", Value = "0" });
            using (_context)
            {
                var xitems = _context.IcosSitePIS.Where(v => v.id_user == userID).OrderBy(s => s.site_code).ToList();
                foreach (var item in xitems)
                {
                    items.Add(new SelectListItem { Text = item.site_code, Value = item.id_site.ToString(), Selected = (item.id_site == xId_site) });
                }
            }

            // autoselectioon
            if (xId_site == -1 && items.Count>0)
            {
                Int32.TryParse(items[0].Value, out xId_site);
                _httpContextAccessor.HttpContext.Session.SetInt32("PiSite",Convert.ToInt32(items[0].Value));
            }

            selectorVM.Id_site = xId_site;
            selectorVM.SiteList = new SelectList(items, "Value", "Text");

            //                    items.Add(new SelectListItem { Text = "site1", Value = "0" });
            ViewBag.sitemenu = new SelectList(items, "Value", "Text");
            return View(selectorVM);
        }

    }
}
