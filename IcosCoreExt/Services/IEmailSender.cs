﻿using IcosCoreExt.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentitySample.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);

        //Task SendEmailWithCcAsync(string email, string subject, string message, List<string> atch, List<string> cc);
        Task SendEmailWithCcAsync(string email, string subject, string message, List<Attachment> atch, List<string> cc);
    }
}
