﻿using AutoMapper;
using IcosCoreExt.Models.ViewModels;
using IcosModelLib.DTO;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IcosCoreExt.Mappings
{
    public abstract class MappingProfile<TSource, TDest> : Profile
    {
        //private MapperConfiguration _cfg;
        public MappingProfile()
        {
            CreateMap<TSource, TDest>().IgnoreAllSourcePropertiesWithAnInaccessibleSetter();
        }

        public void List2List(List<string> from, List<SelectListItem> to)
        {
            foreach (var item in from)
            {
                to.Add(new SelectListItem(item, item));
            }
        }

    }

    public class TeamVMMappingProfile : MappingProfile<GRP_TEAM, GRP_TEAMViewModel> { }
    public class TeamMappingProfile : MappingProfile<GRP_TEAMViewModel, GRP_TEAM> { }
    public class UtcVMMappingProfile : MappingProfile<GRP_UTC_OFFSET, GRP_UTC_OFFSETViewModel> { }
    public class UtcMappingProfile : MappingProfile<GRP_UTC_OFFSETViewModel, GRP_UTC_OFFSET> { }
    public class SamplingSchemeVMMappingProfile : MappingProfile<GRP_PLOT, GRP_PLOTViewModel> { }
    public class SamplingSchemeMappingProfile : MappingProfile<GRP_PLOTViewModel, GRP_PLOT> { }
    public class InstModelMappingProfile : MappingProfile<GRP_INST, GRP_INSTViewModel> {}
    public class InstModelVMMappingProfile : MappingProfile<GRP_INSTViewModel, GRP_INST> { }
    public class LocationModelMappingProfile : MappingProfile<GRP_LOCATION, GRP_LOCATIONViewModel> { }
    public class LocationModelVMMappingProfile : MappingProfile<GRP_LOCATIONViewModel, GRP_LOCATION> { }
    public class ECModelMappingProfile : MappingProfile<GRP_EC, GRP_ECViewModel> { }
    public class ECModelVMMappingProfile : MappingProfile<GRP_ECViewModel, GRP_EC> { }
    public class TowerModelMappingProfile : MappingProfile<GRP_TOWER, GRP_TOWERViewModel> { }
    public class TowerModelVMMappingProfile : MappingProfile<GRP_TOWERViewModel, GRP_TOWER> { }
    public class SppsModelMappingProfile : MappingProfile<GRP_SPPS, GRP_SPPSViewModel> { }
    public class SppsModelVMMappingProfile : MappingProfile<GRP_SPPSViewModel, GRP_SPPS> { }
    public class TreeModelMappingProfile : MappingProfile<GRP_TREE, GRP_TREEViewModel> { }
    public class TreeModelVMMappingProfile : MappingProfile<GRP_TREEViewModel, GRP_TREE> { }
    public class FlsmModelMappingProfile : MappingProfile<GRP_FLSM, GRP_FLSMViewModel> { }
    public class FlsmModelVMMappingProfile : MappingProfile<GRP_FLSMViewModel, GRP_FLSM> { }
    public class SosmModelMappingProfile : MappingProfile<GRP_SOSM, GRP_SOSMViewModel> { }
    public class SosmModelVMMappingProfile : MappingProfile<GRP_SOSMViewModel, GRP_SOSM> { }
    public class AgbModelMappingProfile : MappingProfile<GRP_AGB, GRP_AGBViewModel> { }
    public class AgbModelVMMappingProfile : MappingProfile<GRP_AGBViewModel, GRP_AGB> { }
    public class LitterPntModelMappingProfile : MappingProfile<GRP_LITTERPNT, GRP_LITTERPNTViewModel> { }
    public class LitterPntModelVMMappingProfile : MappingProfile<GRP_LITTERPNTViewModel, GRP_LITTERPNT> { }
    public class WtdPntModelMappingProfile : MappingProfile<GRP_WTDPNT, GRP_WTDPNTViewModel> { }
    public class WtdPntModelVMMappingProfile : MappingProfile<GRP_WTDPNTViewModel, GRP_WTDPNT> { }
    public class D_SnowModelMappingProfile : MappingProfile<GRP_D_SNOW, GRP_D_SNOWViewModel> { }
    public class D_SnowModelVMMappingProfile : MappingProfile<GRP_D_SNOWViewModel, GRP_D_SNOW> { }
    public class DmModelMappingProfile : MappingProfile<GRP_DM, GRP_DMViewModel> { }
    public class DmModelVMMappingProfile : MappingProfile<GRP_DMViewModel, GRP_DM> { }
    public class ClimAvgModelMappingProfile : MappingProfile<GRP_CLIM_AVG, GRP_CLIM_AVGViewModel> { }
    public class ClimAvgModelVMMappingProfile : MappingProfile<GRP_CLIM_AVGViewModel, GRP_CLIM_AVG> { }
    public class DHPModelMappingProfile : MappingProfile<GRP_DHP, GRP_DHPViewModel> { }
    public class DHPModelVMMappingProfile : MappingProfile<GRP_DHPViewModel, GRP_DHP> { }
    public class LoggerModelMappingProfile : MappingProfile<GRP_LOGGER, GRP_LOGGERViewModel> { }
    public class LoggerModelVMMappingProfile : MappingProfile<GRP_LOGGERViewModel, GRP_LOGGER> { }
    public class HeaderModelMappingProfile : MappingProfile<GRP_HEADER, GRP_HEADERViewModel> { }
    public class HeaderModelVMMappingProfile : MappingProfile<GRP_HEADERViewModel, GRP_HEADER> { }
    public class FileModelMappingProfile : MappingProfile<GRP_FILE, GRP_FILEViewModel> { }
    public class FileModelVMMappingProfile : MappingProfile<GRP_FILEViewModel, GRP_FILE> { }
    public class BMModelMappingProfile : MappingProfile<GRP_BM, GRP_BMViewModel> { }
    public class BMModelVMMappingProfile : MappingProfile<GRP_BMViewModel, GRP_BM> { }
    public class ECSYSModelMappingProfile : MappingProfile<GRP_ECSYS, GRP_ECSYSViewModel> { }
    public class ECSYSModelVMMappingProfile : MappingProfile<GRP_ECSYSViewModel, GRP_ECSYS> { }
    public class ECWEXCLModelMappingProfile : MappingProfile<GRP_ECWEXCL, GRP_ECWEXCLViewModel> { }
    public class ECWEXCLModelVMMappingProfile : MappingProfile<GRP_ECWEXCLViewModel, GRP_ECWEXCL> { }
    public class GAIModelMappingProfile : MappingProfile<GRP_GAI, GRP_GAIViewModel> { }
    public class GAIModelVMMappingProfile : MappingProfile<GRP_GAIViewModel, GRP_GAI> { }
    public class BulkHModelMappingProfile : MappingProfile<GRP_BULKH, GRP_BULKHViewModel> { }
    public class BulkHModelVMMappingProfile : MappingProfile<GRP_BULKHViewModel, GRP_BULKH> { }
    public class CEPTModelMappingProfile : MappingProfile<GRP_CEPT, GRP_CEPTViewModel> { }
    public class CEPTModelVMMappingProfile : MappingProfile<GRP_CEPTViewModel, GRP_CEPT> { }
    public class StoModelMappingProfile : MappingProfile<GRP_STO, GRP_STOViewModel> { }
    public class StoModelVMMappingProfile : MappingProfile<GRP_STOViewModel, GRP_STO> { }
    public class InstManModelMappingProfile : MappingProfile<GRP_INSTMAN, GRP_INSTMANViewModel> { }
    public class InstManModelVMMappingProfile : MappingProfile<GRP_INSTMANViewModel, GRP_INSTMAN> { }
    public class InstPairModelMappingProfile : MappingProfile<GRP_INSTPAIR, GRP_INSTPAIRViewModel> { }
    public class InstPairModelVMMappingProfile : MappingProfile<GRP_INSTPAIRViewModel, GRP_INSTPAIR> { }
    public class FundingModelMappingProfile : MappingProfile<GRP_FUNDING, GRP_FUNDINGViewModel> { }
    public class FundingModelVMMappingProfile : MappingProfile<GRP_FUNDINGViewModel, GRP_FUNDING> { }
    //GRP_LAND_OWNERSHIPViewModel
    public class LandOwnershipModelMappingProfile : MappingProfile<GRP_LAND_OWNERSHIP, GRP_LAND_OWNERSHIPViewModel> { }
    public class LandOwnershipModelVMMappingProfile : MappingProfile<GRP_LAND_OWNERSHIPViewModel, GRP_LAND_OWNERSHIP> { }
    public class WGUsersModelMappingProfile : MappingProfile<WGUsers, WGUserViewModel> { }
    public class WGUsersModelVMMappingProfile : MappingProfile<WGUserViewModel, WGUsers> { }
    public class WorkGroupsModelMappingProfile : MappingProfile<WorkGroups, WorkingGroupViewModel> { }
    public class WorkGroupsModelVMMappingProfile : MappingProfile<WorkingGroupViewModel, WorkGroups> { }



}
